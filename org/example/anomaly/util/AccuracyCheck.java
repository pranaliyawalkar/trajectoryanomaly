package org.example.anomaly.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;

public class AccuracyCheck {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		BufferedReader fileReader = new BufferedReader(new FileReader(
				"src/outputs/outputValuesTempAccuracy_1.txt"));
		while (true) {
			String line = fileReader.readLine();
			if (line == null)
				break;
			if (line.startsWith("File name :")) {
				System.out.println(line);
				continue;
			}
			if (!line.contains("$") || line.contains(" $$$$$$$$$"))
				continue;
			String[] twoOP = line.split("\\$");
			String[] naiveOuts = twoOP[1].split("\\]")[0].split("\\[")[1]
					.trim().split(", ");
			String[] ourOuts = twoOP[2].split("\\]")[0].split("\\[")[1].trim()
					.split(", ");

			if (naiveOuts.length != ourOuts.length)
				System.out.println("Error:" + line);

			ArrayList<String> naiveList = new ArrayList<String>(
					Arrays.asList(naiveOuts));
			ArrayList<String> ourList = new ArrayList<String>(
					Arrays.asList(ourOuts));

			for (String string : ourList) {
				if (!naiveList.contains(string)) {
					System.out.println("Error:" + line);
				}
			}
			for (String string : naiveList) {
				if (!ourList.contains(string))
					System.out.println("Error:" + line);
			}

		}
		fileReader.close();
	}

}
