package org.example.anomaly.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class EdgeMeanVariance 
{

	public static void main(String[] args) throws Exception 
	{
		String file_add = "src/train";
		//String file_add = "/home/pranali/workspace/DDP_Anomaly/src/train";
		String new_file_add = file_add + "1";
		new EdgeMeanVariance()
				.findMeanVarAdulterated(new_file_add)
				/*findMeanVarAdulterated("/home/pranali/workspace/DDP_Anomaly/src/reduced")*/
				/*findMeanVarAdulterated("/Users/itispris/Documents/trajData/reduced")*/;
		new_file_add = file_add + "2";
		new EdgeMeanVariance()
				.findMeanVarAdulterated(new_file_add)
				/*findMeanVarAdulterated("/home/pranali/workspace/DDP_Anomaly/src/reduced")*/
				/*findMeanVarAdulterated("/Users/itispris/Documents/trajData/reduced")*/;
		new_file_add = file_add + "3";
		new EdgeMeanVariance()
				.findMeanVarAdulterated(new_file_add)
				/*findMeanVarAdulterated("/home/pranali/workspace/DDP_Anomaly/src/reduced")*/
				/*findMeanVarAdulterated("/Users/itispris/Documents/trajData/reduced")*/;
		new_file_add = file_add + "4";
		new EdgeMeanVariance()
				.findMeanVarAdulterated(new_file_add)
				/*findMeanVarAdulterated("/home/pranali/workspace/DDP_Anomaly/src/reduced")*/
				/*findMeanVarAdulterated("/Users/itispris/Documents/trajData/reduced")*/;
		new_file_add = file_add + "5";
		new EdgeMeanVariance()
				.findMeanVarAdulterated(new_file_add)
				/*findMeanVarAdulterated("/home/pranali/workspace/DDP_Anomaly/src/reduced")*/
				/*findMeanVarAdulterated("/Users/itispris/Documents/trajData/reduced")*/;
	}

	public void findMeanVar(String dirLocation) throws Exception 
	{
		PrintWriter out = new PrintWriter(new OutputStreamWriter(
				new FileOutputStream(
				"src/out1234.csv")
				/*"/Users/itispris/Documents/trajData/test/out.csv")*/,
				"UTF-8"));
		File docDir = new File(dirLocation);
		String[] files = docDir.list();
		HashMap<String, ArrayList<Long>> map = new HashMap<String, ArrayList<Long>>();
		for (int file = 0; file < files.length; file++) 
		{

			BufferedReader fileReader = new BufferedReader(new FileReader(
					dirLocation + "/" + files[file]));
			if (!(files[file].contains("txt")))
				continue;
			System.out.println(files[file]);
			while (true) 
			{

				String line = fileReader.readLine();
				if (line == null)
					break;

				if (!line.contains("["))
					continue;

				String[] sarr = line.split("\\]");
				boolean first = true;
				String[] sarrSource, sarrDesti = null;

				for (int i = 0; i < sarr.length; i++) 
				{

					sarrSource = sarrDesti;
					sarrDesti = sarr[i].split("\\[")[1].split(",");

					if (first) 
					{
						first = false;
						sarrSource = sarrDesti;
						i++;
						sarrDesti = sarr[i].split("\\[")[1].split(",");

					}

					String key1 = sarrSource[0] + " " + sarrDesti[0];
					String key2 = sarrDesti[0] + " " + sarrSource[0];

					long startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
							.parse(sarrSource[3]).getTime();

					long endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
							.parse(sarrDesti[3]).getTime();

					long travelTimeMin = (endTime - startTime);
					//					if (travelTimeMin == 0)
					//						continue;
					ArrayList<Long> arr = new ArrayList<Long>();
					if (map.get(key1) == null && map.get(key2) == null) 
					{
						arr.add(travelTimeMin);
						map.put(key1, arr);
					} else if (map.get(key1) == null) {
						arr = map.get(key2);
						arr.add(travelTimeMin);
						map.put(key2, arr);
					} else {
						arr = map.get(key1);
						arr.add(travelTimeMin);
						map.put(key1, arr);
					}
				}

			}
			fileReader.close();
		}
		for (String s : map.keySet()) 
		{
			ArrayList<Long> arr = map.get(s);

			long tot = 0;
			for (Long l : arr)
				tot += l;
			long mean = tot / (long) arr.size();
			tot = 0;
			for (Long l : arr)
				tot += Math.abs(mean - l) * Math.abs(mean - l);
			double var = Math.pow(tot, 0.5);
			out.println(s + "," + mean + "," + tot + "," + var);

		}
		out.close();
	}

	public void findMeanVarAdulterated(String dirLocation) throws Exception 
	{
		String train_number = dirLocation.substring(dirLocation.length()-1);
		String file_name = "src/out_ad" + train_number + ".csv";
		System.out.println(file_name);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(
				new FileOutputStream(file_name),
				"UTF-8"));
		File docDir = new File(dirLocation);
		String[] files = docDir.list();
		HashMap<String, ArrayList<Long>> map = new HashMap<String, ArrayList<Long>>();
		for (int file = 0; file < files.length; file++)
		{

			BufferedReader fileReader = new BufferedReader(new FileReader(
					dirLocation + "/" + files[file]));
			if (!(files[file].contains("txt")))
				continue;
			System.out.println(files[file]);
			String line;
			while (true) 
			{
				line = fileReader.readLine();
				if (line == null)
					break;
				if (!line.contains("["))
					continue;
				String[] sarr = line.split("\\]");
				boolean first = true;
				String[] sarrSource, sarrDesti = null;

				for (int i = 0; i < sarr.length; i++) 
				{

					sarrSource = sarrDesti;
					sarrDesti = sarr[i].split("\\[")[1].split(",");

					if (first) 
					{
						first = false;
						sarrSource = sarrDesti;
						i++;
						sarrDesti = sarr[i].split("\\[")[1].split(",");
					}

					String key1 = sarrSource[0] + " " + sarrDesti[0];
					String key2 = sarrDesti[0] + " " + sarrSource[0];

					long startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
							.parse(sarrSource[3]).getTime();

					long endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
							.parse(sarrDesti[3]).getTime();

					long travelTimeMin = (endTime - startTime);
					//					if (travelTimeMin == 0)
					//						continue;
					ArrayList<Long> arr = new ArrayList<Long>();
					if (map.get(key1) == null && map.get(key2) == null) 
					{
						arr.add(travelTimeMin);
						map.put(key1, arr);
					} 
					else if (map.get(key1) == null) 
					{
						arr = map.get(key2);
						arr.add(travelTimeMin);
						map.put(key2, arr);
					} 
					else 
					{
						arr = map.get(key1);
						arr.add(travelTimeMin);
						map.put(key1, arr);
					}
				}

			}
			fileReader.close();
		}
		int count = 0;
		for (String s : map.keySet()) 
		{
			ArrayList<Long> arr = map.get(s);

			long tot = 0;
			for (Long l : arr)
				tot += l;
			long mean = tot / (long) arr.size();
			tot = 0;
			int c = 0;
			for (Long l : arr)
				if (Math.abs(mean - l) < mean) 
				{
					tot += Math.abs(mean - l) * Math.abs(mean - l);
					c++;
				}
			if (c > 0)
				tot = tot / c;
			if (tot == 0)
				tot = 1;

			double var = Math.pow(tot, 0.5);
			if (var == 0)
				var = 1;
			//if (var > mean)
				//System.out.println("Case" + ++count + "," + var + "," + mean);
			out.println(s + "," + mean + "," + tot + "," + var);

		}
		out.close();
	}

}
