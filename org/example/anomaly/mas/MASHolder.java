package org.example.anomaly.mas;

import geolife_processing.start_end_anom;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.example.anomaly.data.Point;

public class MASHolder {
	double PIx = 3.141592653589793;
	double RADIUS = 6378.16;
	String logFilePath = "";
	String myOutputFilePathValues = "";
	String myOutputFilePathTime = "";
	String myOutputFilePathTimeTxt = "";
	String mapMatchedTrajDirPath = "";
	String learnedEdgeDistroFilePath = "";
	String trajToAnomScoreFilePath;

	PrintWriter outValues/*, outTime, outTimeTxt*/;
	PrintWriter outPercent;
	EdgeProcessing ep;
	int mined_traj_count;
	int tot_traj_count;
	int training_miss_count;
	int my_count_prithu;
	String name;

	public MASHolder() {

	}

	public MASHolder(String name) {
		my_count_prithu = 0;
		mined_traj_count = 0;
		tot_traj_count = 0;
		training_miss_count = 0;
		/*
		mapMatchedTrajDirPath = "src/inputs/car_bus";
		myOutputFilePathTime = "src/comparison_car_13_time.csv";
		myOutputFilePathValues = "src/comparison_car_13_values.txt";
		myOutputFilePathTimeTxt = "src/comparison_car_13_time.txt";
		*/

		// mapMatchedTrajDirPath = "src/inputs/walk_cycle";
		// myOutputFilePathValues = "src/comparison_walk_60_values.txt";

		// mapMatchedTrajDirPath = "src/inputs/walk_cycle";
		// myOutputFilePathValues = "src/comparison_walk.txt";

		mapMatchedTrajDirPath = "/home/pranali/new_workspace/TrajectoryAnomaly/src/inputs";
		// myOutputFilePathValues = "src/comparison_car.txt";

		// learnedEdgeDistroFilePath = "src/car_filename.csv";
		this.name = name;
		mapMatchedTrajDirPath = "inputs";
		learnedEdgeDistroFilePath = "out_car.csv";
		myOutputFilePathValues = "src/comparison_" + name + "_" + 5.0 + "_"
				+ 3.0 + ".txt";
		trajToAnomScoreFilePath = "src/trajToAnom_" + name + ".txt";

		try {
			outValues = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(myOutputFilePathValues), "UTF-8"));
			outPercent = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(trajToAnomScoreFilePath), "UTF-8"));
			// outTime = new PrintWriter(new OutputStreamWriter(new
			// FileOutputStream(myOutputFilePathTime), "UTF-8"));
			// outTimeTxt = new PrintWriter(new OutputStreamWriter(new
			// FileOutputStream(myOutputFilePathTimeTxt), "UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		ep = new EdgeProcessing(learnedEdgeDistroFilePath, 5.0, 3.0);

	}

	public void compare_handler() throws Exception {
		// File docDir = new File(mapMatchedTrajDirPath);
		// String[] files = docDir.list();
		// int fileNum = 0;
		int line_no = 0;
		int total_count = 0;
		// String speed_file_name =
		// "/home/pranali/new_workspace/TrajectoryAnomaly/src/speedtrain_test"
		// + ".csv";
		// System.out.println(speed_file_name);
		// PrintWriter speed_out = new PrintWriter(new OutputStreamWriter(
		// new FileOutputStream(speed_file_name), "UTF-8"));
		// for (fileNum = 0; fileNum < files.length ; fileNum++)
		{
			line_no = 0;
			/*System.out.println("File name : " + files[fileNum]);
			outValues.println("File name : " + files[fileNum]);
			outTime.println("File name : " + files[fileNum]);
			outTimeTxt.println("File name : " + files[fileNum]);
			BufferedReader fileReader = new BufferedReader(new FileReader(
					mapMatchedTrajDirPath + "/" + files[fileNum]));*/
			String file_name = name + ".txt";
			System.out.println("File name : " + mapMatchedTrajDirPath + "/"
					+ file_name);
			outValues.println("File name : " + file_name);
			// outTime.println("File name : " + file_name);
			// outTimeTxt.println("File name : " + file_name);
			BufferedReader fileReader = new BufferedReader(new FileReader(
					mapMatchedTrajDirPath + "/" + file_name));
			int prev_id = 0;
			double prev_lat = 0.0;
			double prev_lon = 0.0;
			long prev_val = 0;
			Date prev_time = new Date();
			int temp_flag = 0;
			String id_line = new String();
			while (true) {
				prev_id = 0;
				prev_lat = 0.0;
				prev_lon = 0.0;
				prev_val = 0;
				prev_time = new Date();
				ArrayList<Point> arr = new ArrayList<Point>();
				String line = fileReader.readLine();
				if (line == null)
					break;
				// if (name == "car" && my_count_prithu > 50)
				// break;
				// if (name == "walk" && my_count_prithu > 100)
				// break;

				if (!line.contains("[")) {
					{
						if (!line.contains("plt"))
							id_line = line;
						else {
							outValues.println(id_line);
							outValues.println(line);
							outPercent.println(line);
							// System.out.println(id_line);
							// System.out.println(line);
						}
					}
					continue;
				}
				line_no++;
				if (name == "car" && line_no == 42)
					continue;
				prev_id = 0;
				prev_lat = 0.0;
				prev_lon = 0.0;
				prev_time = new Date();
				if (line.contains("[")) {
					// speed_out.print(line_no + ",");
					String[] warr = line.split("\\]");
					for (String s : warr) {
						try {
							int id = Integer.parseInt(s.split(",")[0]
									.split("\\[")[1]);
							// double lat =
							// Double.parseDouble(s.split(",")[0].split("\\[")[1]);
							// double lat =
							// Double.parseDouble(s.split(",")[0].split("\\[")[1]);
							double lat = Double.parseDouble(s.split(",")[1]);
							double lon = Double.parseDouble(s.split(",")[2]);
							long val = Long.parseLong(s.split(",")[3]
									.replaceAll("\\s", ""));
							Date date = new Date(val);
							SimpleDateFormat df2 = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							String dateText = df2.format(date);
							Date time = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss").parse(dateText);

							/**
							 * ADDED TO SOLVE STATIONARY AND SAME TIMESTAMP
							 * PROBLEM
							 **/
							// if(! ( (prev_val == val) || (prev_id==id) ))
							// if(! ( (prev_id == id) ))
							arr.add(new Point(id, lat, lon, time, val));
							// else
							{
								/*if(prev_time.equals(time))
								{
									System.out.println("Found the culprits " + line_no + "\n" + line);
									temp_flag = 1;
									System.out.println("*******************************");
									System.out.println("Found the culprits " + line_no + "\n" + prev_id + " "+ prev_lat + " " + prev_lon + " " + prev_val + "  " + prev_time
										+ "\n" + id + " " + lat + " " + lon + " " + val + "  "+ time);
									break;
								}
								*/

							}

							// finding speed
							// if(prev_id == 1)
							// speed_out.print(Math.abs(distance(lat, lon,
							// prev_lat, prev_lon)) * 1000 * 3600 /
							// (Math.abs(prev_val-val)) + ",");

							/**
							 * ADDED TO SOLVE STATIONARY AND SAME TIMESTAMP
							 * PROBLEM
							 **/
							prev_time = time;
							prev_lat = lat;
							prev_lon = lon;
							prev_id = id;
							prev_val = val;
						} catch (Exception e) {
							System.out.println(e);
						}
					}
					// speed_out.println();
					if (temp_flag == 1)
						break;
					// System.out.println(arr.size());
					/*if(line_no == 1)
					{
						for(int x = 0 ; x < arr.size() - 1; x++)
							System.out.println(arr.get(x).id + "  " + arr.get(x+1).id + "  " + (arr.get(x + 1).timeStamp.getTime()- arr.get(x).timeStamp.getTime() ));
					}*/
					total_count++;
					// if(line_no ==1)
					{
						// System.out.println(line);
						// System.out.println(line);
						compare(arr, line_no, warr.length - 1);
						// break;
					}
				}

			}
			// System.out.println(ep.anom_time);
			fileReader.close();
			outValues.println(" $$$$$$$$$$ ");
			// outTime.println(" $$$$$$$$$$ " + "," + " $$$$$$$$$$ ");
			// outTimeTxt.println(" $$$$$$$$$$ ");
			/*System.out.println("Mined trajectory count : " + mined_traj_count
					+ " Missed trajectory Count : " + training_miss_count
					+ " Total trajectory Count : " + tot_traj_count); */
		}
		// outTime.close();
		// outTimeTxt.close();
		outValues.close();
		outPercent.close();
	}

	public void compare(ArrayList<Point> pt, int line_no, int size)
			throws Exception {
		long tot1 = 0;
		long tot2 = 0;
		long tot3 = 0;
		List<Integer> nodeSequence = new ArrayList<Integer>();
		List<Double> travelTime = new ArrayList<Double>();
		for (int i = 0; i < pt.size(); i++) {
			nodeSequence.add(pt.get(i).id);
			if (i < (pt.size() - 1)) {
				double time = pt.get(i + 1).mytime - pt.get(i).mytime;
				// double time = pt.get(i + 1).timeStamp.getTime()-
				// pt.get(i).timeStamp.getTime();
				// if((pt.get(i+1).id == 201398 && pt.get(i).id == 201403) ||
				// (pt.get(i).id == 201398 && pt.get(i+1).id == 201403))
				// System.out.println( " check " + time);
				if (time < 0.01) {
					// System.out.println("less time " + time + "  " + pt.get(i
					// + 1).id + "  " + pt.get(i).id );
					// System.out.println("Found the culprits " + line_no + "\n"
					// + prev_id + " "+ prev_lat + " " + prev_lon + " " +
					// prev_val + "  " + prev_time
					// + "\n" + id + " " + lat + " " + lon + " " + val + "  "+
					// time);
				}
				travelTime.add(time);
			}
		}
		ep.setNodeTime(nodeSequence, travelTime);
		ArrayList<Integer> not_in_training = new ArrayList<Integer>();
		if (ep.notInTraining(not_in_training)) {
			// System.out.println("missed");
			outValues.println("missed once " + line_no);
			System.out.println("missed once " + line_no);
			// outValues.println("*****************************************");

			training_miss_count++;
		}
		// else
		{
			// System.out.println("not missed");
			// ep.howManyAno();
			ArrayList<Integer> anoms = new ArrayList<Integer>();
			int count = ep.howManyAno(anoms);
			outValues.println(anoms);
			outValues.println(" Ano count : " + count);

			long start = System.currentTimeMillis();
			ArrayList<String> our_result = new ArrayList<String>();
			try {
				our_result = ep.findAnomalyOur();
			} catch (Exception e) {
				System.out.println("Not there in training : our");
				e.printStackTrace();
				return;
			}
			tot1 += (System.currentTimeMillis() - start);

			start = System.currentTimeMillis();

			ArrayList<String> naive_result = new ArrayList<String>();
			// System.out.println(line_no + " $ " + our_result + " $ " +
			// naive_result );
			try {
				// naive_result = ep.findAnomalyNaive();
			} catch (Exception e) {
				System.out.println("Not there in training :  naive");
				e.printStackTrace();
				return;
			}
			tot2 += (System.currentTimeMillis() - start);
			/*start = System.currentTimeMillis();
			ArrayList<String> naive_result2 = new ArrayList<String>();
			try 
			{
				naive_result2 = ep.findAnomalyNaive2();
			} 
			catch (Exception e) 
			{
				System.out.println("Not there in training");
				e.printStackTrace();
				return;
			}
			tot3 += System.currentTimeMillis() - start; */
			outValues.println(line_no + " $ " + our_result + " $ " + size
					+ " $ " + not_in_training.size()) /* +  naive_result + " $ " + naive_result2*/;
			System.out.println(line_no + " $ " + our_result + " $ " + (size)
					+ " $ " + not_in_training.size());
			System.out.println("count: " + my_count_prithu++);

			ArrayList<start_end_anom> sub_trajcs = new ArrayList<start_end_anom>();
			int total_anom_count = 0;
			ArrayList<Integer> visited = new ArrayList<Integer>();
			for (int i = 0; i < size; i++)
				visited.add(0);
			for (int x = 0; x < our_result.size(); x++) {
				start_end_anom obj = new start_end_anom();
				String s = our_result.get(x);
				StringTokenizer stt = new StringTokenizer(s, ",");
				obj.start = Integer.parseInt(stt.nextToken().toString());
				obj.end = Integer.parseInt(stt.nextToken().toString());
				for (int z = obj.start; z <= obj.end; z++) {
					if (visited.get(z) == 0) // unvisited
					{
						total_anom_count++;
						visited.set(z, 1);
					}
				}

				sub_trajcs.add(obj);
			}
			for (int x = 0; x < not_in_training.size(); x++) {
				for (int y = 0; y < sub_trajcs.size(); y++) {
					if (sub_trajcs.get(y).start <= not_in_training.get(x)
							&& sub_trajcs.get(y).end >= not_in_training.get(x)) {
						total_anom_count = total_anom_count - 1;
						break;
					}
				}
			}

			int trajSize = size - not_in_training.size();
			outValues
					.println("Size of trajectory after removing missing ones : "
							+ trajSize);
			outValues
					.println("Anom size after removing missing ones from our_result : "
							+ total_anom_count);
			if (trajSize > 0)
				outPercent.println((double) total_anom_count / trajSize);
			else
				outPercent.println("Size zero!!");

			System.out
					.println("Size of trajectory after removing missing ones : "
							+ (size - not_in_training.size()));
			System.out
					.println("Anom size after removing missing ones from our_result : "
							+ total_anom_count);

			// System.out.println(naive_result);
			outValues.println("*****************************************");
			outValues.flush();
			System.out.println("*****************************************");
			// outTime.println(pt.size() + "," + ep.howManyAno() + "," + tot1 +
			// "," + tot2 + "," + tot3);
			// outTimeTxt.println(line_no + " $ " + tot1 + " $ " + tot2 + " $ "
			// + tot3);

			/*if (our_result.size() == naive_result.size()) 
			{
				int flag = 0;
				for (int i = 0; i < our_result.size(); i++) 
				{
					if (!our_result.get(i).equals(naive_result.get(i))) 
					{
						flag = 1;
						break;
					}
				}
				if (flag == 0)
				{
					mined_traj_count++;
				}
			}*/

		}
		tot_traj_count++;

	}

	public static void main(String[] args) throws Exception {
		/*for (int i = 1; i <= 1; i++) 
		{
			ScanThread sc = new ScanThread(i);
			sc.start();
		}*/
		// double[] threshold = new double[1];
		// threshold[0] = 6.0;
		// // threshold[1] = 4.0;
		// // threshold[2] = 5.0;
		// // threshold[3] = 6.0;
		// double[] smooth = new double[1];
		// smooth[0] = 3.0;
		// // smooth[1] = 2.0;
		// // smooth[2] = 3.0;
		// // smooth[3] = 4.0;
		String[] names = new String[2];
		names[0] = "car";
		names[1] = "walk";

		// for (double d1 : threshold) {
		// for (double d2 : smooth) {
		for (String name : names) {
			MASHolder ms = new MASHolder(name);
			ms.compare_handler();
			// }
			// }
		}

	}

	public double Radians(double x) {
		return x * PIx / 180;
	}

	public double distance(double lat1, double lon1, double lat2, double lon2) {
		double dlon = Radians(lon2 - lon1);
		double dlat = Radians(lat2 - lat1);

		double a = (Math.sin(dlat / 2) * Math.sin(dlat / 2))
				+ Math.cos(Radians(lat1)) * Math.cos(Radians(lat2))
				* (Math.sin(dlon / 2) * Math.sin(dlon / 2));
		double angle = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return angle * RADIUS;

	}

}
