The project organisation is as follows : 

TrajectoryAnomaly/org/example/anomaly/util/ : Training code
TrajectoryAnomaly/org/example/anomaly/mas/ : Online processing code

TrajectoryAnomaly/inputs
 * The 720 data files are divided into 5 parts each containing 144 files.
 Directories part1 to part5 contain these 144 files each. They are used in training code to generate the following output files of learnt speed distributions for each edge:
 * out_ad1.csv : the learnt distribution  on part 2,3,4,5 , excluding part 1
 * out_ad2.csv : the learnt distribution  on part 1,3,4,5 , excluding part 2
 * out_ad3.csv : the learnt distribution  on part 1,2,4,5 , excluding part 3
 * out_ad4.csv : the learnt distribution  on part 1,2,3,5 , excluding part 4
 * out_ad5.csv : the learnt distribution  on part 1,2,3,4 , excluding part 5 
 
TrajectoryAnomaly/outputs/ : Outputs of the runs.
 * outputTime1.csv : the "time taken" comparison of Naive vs Our when tested on part 1 using training distribution from  out_ad1.csv
 * outputTime1.txt : the "time taken" comparison of Naive vs Our when tested on part 1 using training distribution from  out_ad1.csv
 * outputValues1.txt : the "anomalous subtrajectories results" comparison of Naive vs Our when tested on part 1 using training distribution from  out_ad1.csv
 * outputTime2.csv : the "time taken" comparison of Naive vs Our when tested on part 2 using training distribution from  out_ad2.csv
 * outputTime2.txt : the "time taken" comparison of Naive vs Our when tested on part 2 using training distribution from  out_ad2.csv
 * outputValues2.txt : the "anomalous subtrajectories results" comparison of Naive vs Our when tested on part 2 using training distribution from  out_ad2.csv
 * outputTime3.csv : the "time taken" comparison of Naive vs Our when tested on part 3 using training distribution from  out_ad3.csv
 * outputTime3.txt : the "time taken" comparison of Naive vs Our when tested on part 3 using training distribution from  out_ad3.csv
 * outputValues3.txt : the "anomalous subtrajectories results" comparison of Naive vs Our when tested on part 3 using training distribution from  out_ad3.csv
 * outputTime4.csv : the "time taken" comparison of Naive vs Our when tested on part 4 using training distribution from  out_ad4.csv
 * outputTime4.txt : the "time taken" comparison of Naive vs Our when tested on part 4 using training distribution from  out_ad4.csv
 * outputValues4.txt : the "anomalous subtrajectories results" comparison of Naive vs Our when tested on part 4 using training distribution from  out_ad4.csv
 * outputTime5.csv : the "time taken" comparison of Naive vs Our when tested on part 5 using training distribution from  out_ad5.csv
 * outputTime5.txt : the "time taken" comparison of Naive vs Our when tested on part 5 using training distribution from  out_ad5.csv
 * outputValues5.txt : the "anomalous subtrajectories results" comparison of Naive vs Our when tested on part 5 using training distribution from  out_ad5.csv
