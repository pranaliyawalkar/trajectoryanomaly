package geolife_processing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;

public class PrithuMapMatchTrajectoryWrapper {

	String filePath;
	String dirPath;
	PrithuProcessTrajectory pt;
	PrintWriter outCSV;
	Hashtable<String, Integer> all_ids = new Hashtable<String, Integer>();
	Hashtable<Integer, Integer> id_counts = new Hashtable<Integer, Integer>();
	int max_id = 1;
	public static final String geolife_directory = "/home/pranali/workspace/DDP_Anomaly/src/inputs/geolife_data/geolife_labeled/geolife_labeled/";
	public PrithuMapMatchTrajectoryWrapper() throws Exception {
		filePath = "/home/pranali/new_workspace/TrajectoryAnomaly/src/speedcar_filename.csv";
		pt = new PrithuProcessTrajectory();
		outCSV = new PrintWriter(new OutputStreamWriter(new FileOutputStream(
				"/home/pranali/new_workspace/TrajectoryAnomaly/src/speedcar_filename_new.csv"), "UTF-8"));

	}

	public void readTrajs() throws Exception 
	{
		BufferedReader fileReader = new BufferedReader(new FileReader(filePath));
		String id = null, name = null;
		while (true) {
			String line = fileReader.readLine();
			if (line == null)
				break;
			if (line.contains(":"))
				id = line.split(":")[1].trim();
			else if (line.contains(".plt"))
				name = line.split("_")[1];
			else {
				processTraj(id, name);
			}

		}
		fileReader.close();
		outCSV.close();
	}
	public void process() throws Exception 
	{
		File docDir = new File(geolife_directory);
		String[] files = docDir.list();
		
		String id_directory = "";
		String[] id_files = new String[9000];
		int i = 0;
		int file = 0;
		//try 
		{
			for(i = 0; i< files.length; i++)
			{
				id_directory = geolife_directory + files[i] + "/";
				
				/** IF THE FILES UNDER CONSIDERATION ARE NOT OUR CODE FILES **/
					/** IF "labels.txt" EXISTS IN THE ID FOLDER **/
				if(check_label_file(id_directory))
				{
					//System.out.println("Processing id : " + files[i]);
					String id = files[i] + "";
					String dirLocation = id_directory + "mappedTrajectory";
					File id_docDir = new File(dirLocation);
					id_files = id_docDir.list();
					//System.out.println(id);
					for (file = 0; file < id_files.length; file++) 
					{
						String filename = id_files[file];
						//System.out.println(filename);
						processTraj(id, filename.substring(7));
					}
					/*id_directory = id_directory + "wrappedTrajectory/";
					File docDir_id = new File(id_directory);
					//System.out.println(id_directory);
					String[] files_id = docDir_id.list();
					for(int j = 0; j < files_id.length; j++)
					{
						File currentFile = new File(docDir_id.getPath(),files_id[j]);
					    currentFile.delete();
					}
					docDir_id.delete();*/
				
				}
				
				else
				{
					//System.out.println("Rejected id : " + files[i]);
				}
			}
			System.out.println(id_counts);
		} 
		/*catch (Exception e) {
			System.out.println(i);
			System.out.println(files[i]);
			System.out.println(file);
			System.out.println(id_files[file]);
		}*/
		//System.out.println(files.length);
	}
	
	
	/** IF THE ID FOLDER CONTAINS "labels.txt, RETURN TRUE" **/
	public boolean check_label_file(String id_directory) throws Exception
	{
		File docDir = new File(id_directory);
		//System.out.println(id_directory);
		String[] files = docDir.list();
		for(int i = 0; i < files.length; i++)
		{
			if(files[i].equals("labels.txt"))
				return true;
		}
		return false;
	}
	private void processTraj(String id, String name) throws Exception {

		//System.out.println(id + " $ " + name + " $ ");
		String path = "/home/pranali/workspace/DDP_Anomaly/src/inputs/geolife_data/geolife_labeled/geolife_labeled";
		String dir = path + File.separator + id;
		String mappedDir = dir + File.separator + "mappedTrajectory";
		String rawDir = dir + File.separator + "Trajectory";

		BufferedReader mappedFileReader = new BufferedReader(new FileReader(
				mappedDir + File.separator + "mapped_"+ name));

		BufferedReader rawFileReader = new BufferedReader(new FileReader(rawDir
				+ File.separator + name));
		ArrayList<PrithuGeolifePoint> rawPoints, mappedPoints;

		rawPoints = readRawTraj(rawFileReader);
		mappedPoints = readMappedTraj(mappedFileReader);
		//if(id.equals("111") && name.equals("20070617002909.plt"))
		{
			//System.out.println(id + "  " + name);
		}
		
		
		if(rawPoints.size()!=0 && mappedPoints.size()!=0)
		{
			printPoints(pt.processPoints(rawPoints, mappedPoints), dir, name);
			printPointsCSV(pt.processPoints(rawPoints, mappedPoints), id, name);
		}

	}

	private void printPointsCSV(ArrayList<PrithuGeolifePoint> processPoints,
			String id, String name) {

		outCSV.println("ID:" + id);
		outCSV.println("mapped_" + name);
		for (int i = 0; i < processPoints.size() - 1; i++) {
			double dist = latLonDist(processPoints.get(i + 1).lat,
					processPoints.get(i + 1).lon, processPoints.get(i).lat,
					processPoints.get(i + 1).lon);
			long time = (processPoints.get(i + 1).timeStamp - processPoints
					.get(i).timeStamp);
			outCSV.print(dist / time * 1000 * 3600 + ",");
		}
		outCSV.println();

	}

	private void printPoints(ArrayList<PrithuGeolifePoint> processPoints, String dir,
			String name) throws Exception {
		String opDir = dir + File.separator + "wrappedTrajectory";
		if (checkAndCreateDir(opDir)) {
			//System.out.println(opDir+"/" + name);
			PrintWriter out = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(opDir + File.separator + name),
					"UTF-8"));
			int id = 0;
			for (PrithuGeolifePoint g : processPoints) 
			{
				String new_point = new String();
				new_point = g.lat + "$" + g.lon;
				if(all_ids.get(new_point)!=null)
				{
					id = all_ids.get(new_point);
					System.out.println(id);
					id_counts.put(id, id_counts.get(id)+1);
				}
				else
				{
					id = max_id;
					
					all_ids.put(new_point, id);
					id_counts.put(id, 1);
					max_id++;
				}
				out.print("[" + id + "," + g.lat + "," + g.lon + "," + g.timeStamp + "]");
			}
			out.close();
		}

	}

	public static boolean checkAndCreateDir(String fqDirName) {
		File dir;

		try {
			dir = new File(fqDirName);
			if (!dir.exists())
				return dir.mkdir();
			/*String[] entries = dir.list();
			for (String s : entries) {
				File currentFile = new File(dir.getPath(), s);
				currentFile.delete();
			}
			dir.delete();
			return dir.mkdir();*/
			return true;

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;

	}

	private ArrayList<PrithuGeolifePoint> readMappedTraj(
			BufferedReader mappedFileReader) throws Exception {

		ArrayList<PrithuGeolifePoint> arr = new ArrayList<PrithuGeolifePoint>();
		String line;
		while (true) {
			line = mappedFileReader.readLine();
			if (line == null)
				break;
			if (line.contains("]")) {
				String[] points = line.split("]");
				for (String s : points) {
					String[] values = s.split(",");
					if (arr.size() > 0
							&& (arr.get(arr.size() - 1).lat == Double
									.parseDouble(values[1].trim()))
							&& (arr.get(arr.size() - 1).lon == Double
									.parseDouble(values[2].trim())))
						continue;
					PrithuGeolifePoint g = new PrithuGeolifePoint(
							Double.parseDouble(values[1].trim()),
							Double.parseDouble(values[2].trim()),
							Long.parseLong(values[3].trim()));
					arr.add(g);
				}
			}
		}

		mappedFileReader.close();
		return arr;
	}

	private ArrayList<PrithuGeolifePoint> readRawTraj(BufferedReader rawFileReader)
			throws Exception {

		ArrayList<PrithuGeolifePoint> arr = new ArrayList<PrithuGeolifePoint>();
		String line;

		for (int i = 0; i < 6; i++) {
			rawFileReader.readLine();
		}
		while ((line = rawFileReader.readLine()) != null) {

			// skip comments and empty lines
			if (isComment(line))
				continue;

			// Get the points
			String[] splitString = line.trim().split(",");

			String timeStr = splitString[splitString.length - 2].trim() + " "
					+ splitString[splitString.length - 1].trim();

			double lat = Double.parseDouble(splitString[0].trim());

			double lon = Double.parseDouble(splitString[1].trim());

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			// Beijing is not present, use shanghai
			sdf.setTimeZone(TimeZone.getTimeZone("China/Shanghai"));
			Date date;
			try {
				date = sdf.parse(timeStr);
			} catch (ParseException e) {

				e.printStackTrace();
				throw new IllegalStateException(
						"Improperly formatted map-match input. Date string invalid"
								+ timeStr);
			}
			if (arr.size() > 0
					&& (arr.get(arr.size() - 1).timeStamp == date.getTime()))
				continue;

			if (arr.size() > 0 && (arr.get(arr.size() - 1).lat == lat)
					&& (arr.get(arr.size() - 1).lon == lon))
				continue;

			PrithuGeolifePoint g = new PrithuGeolifePoint(lat, lon, date.getTime());
			arr.add(g);

		}
		rawFileReader.close();
		return arr;
	}

	private double latLonDist(double dlat, double dlon, double slat, double slon) {
		double radius = 6371;
		double dLat = toRadians((slat - dlat));
		double dLon = toRadians((slon - dlon));
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(toRadians(slat)) * Math.cos(toRadians(dlat))
				* Math.sin(dLon / 2) * Math.sin(dLon / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return (radius * c); // Distance in km }
	}

	private double toRadians(double d) {
		return d * Math.PI / 180;
	}

	private boolean isComment(String str) {
		return (str.startsWith("//") || str.startsWith("#"));
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		PrithuMapMatchTrajectoryWrapper mtw = new PrithuMapMatchTrajectoryWrapper();
		//mtw.readTrajs();
		mtw.process();
	}
}