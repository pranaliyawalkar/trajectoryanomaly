package geolife_processing;

public class PrithuGeolifePoint {

	double lat, lon;
	long timeStamp;

	public PrithuGeolifePoint(double lat, double lon, long timeStamp) {
		this.lat = lat;
		this.lon = lon;
		this.timeStamp = timeStamp;
	}
}