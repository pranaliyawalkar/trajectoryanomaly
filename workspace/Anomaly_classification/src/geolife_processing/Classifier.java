package geolife_processing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.example.anomaly.data.Point;
import org.example.anomaly.mas.EdgeProcessing;
import org.example.anomaly.util.EdgeMeanVariance;

public class Classifier 
{
	public static void main(String[] args) throws Exception 
	{
		
		//my_classifier.analyse_car();
		//my_classifier.analyse("bus", "walk");
		String[] names = new String[4];
		names[0] = "bike";
		names[1] = "bus";
		names[2] = "car";
		names[3] = "walk";
	//	names[4] = "train";
		for (String train : names)
		{
			for (String test : names) 
			{
				if(!train.equals(test))
				{
					Classifier my_classifier = new Classifier();
					my_classifier.smoothing_analyse_singleclass(train, test);
				}
			}
		}
				
	}
	public void analyse_car()
	{
		String line = new String();
		try
		{
			PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/car_analysis.txt"),"UTF-8"));
			PrintWriter out_percent = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/car_analysis.csv"),"UTF-8"));
			BufferedReader fileReader = new BufferedReader(new FileReader("src/comparison_car.txt"));
			String id = new String();
			String file_name = new String();
			double total_anom = 0.0;
			int count = 0;
			
			while(true)
			{
				line = fileReader.readLine();
				if(line == null)
					break;
				if(line.contains("$$$$$$$$$$"))
					break;
				if(line.startsWith("["))
					continue;
				if(!line.contains("$"))
				{
					out.println(line);
					if(!line.contains("*") && !line.contains("plt") && !line.contains("Ano count"))
					{
						id = line;
						line = fileReader.readLine();
						out.println(line);
						file_name = line;
					}
					continue;
				}
				else
				{
					out.println(line);
					StringTokenizer stt = new StringTokenizer(line, "$");
					String line_no = stt.nextToken().toString().replaceAll("\\s","");
					String anom = stt.nextToken().toString().replaceAll("\\s","");
					String size = stt.nextToken().toString().replaceAll("\\s","");
					out.println("Size of trajectory : " + size);
					StringTokenizer stt2 = new StringTokenizer(anom, "[] ,");
					ArrayList<Integer> visited = new ArrayList<Integer>();
					for(int i = 0; i < Integer.parseInt(size); i++)
						visited.add(0);
					int total_anom_count = 0;
					while(stt2.hasMoreTokens())
					{
						int start = Integer.parseInt(stt2.nextToken().toString().replaceAll("\\s",""));
						int end = Integer.parseInt(stt2.nextToken().toString().replaceAll("\\s",""));
						for(int i = start; i <= end; i++)
						{
							if(visited.get(i) == 0)
							{
								total_anom_count++;
								visited.set(i, 1);
							}
						}
					}
					out.println("Anom size : " + (total_anom_count));
					if((Integer.parseInt(size)-1) != 0)
					{
						count++;
						double percent = (double)(total_anom_count*100)/(Integer.parseInt(size)-1);
						total_anom += percent;
						out_percent.println(id+"_"+file_name + "," + percent);
					}
				}
			}
			out_percent.println("Mean : " + (double)total_anom/count);
			out.close();
			out_percent.close();
		}
		catch (Exception e)
		{
			System.out.println(line);
		}
		
	}
	public void analyse(String train, String test)
	{
		String line  = new String();
		try
		{
			//PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/walk_analysis.txt"),"UTF-8"));
			PrintWriter out_percent = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/"+train+ "_"+ test+"_analysis.csv"),"UTF-8"));
			BufferedReader fileReader = new BufferedReader(new FileReader("src/comparison_"+train+"_"+test+".txt"));
			String id = new String();
			String file_name = new String();
			double total_anom = 0.0;
			int count = 0;
			line = fileReader.readLine();
			while(true)
			{
				line = fileReader.readLine();
				if(line == null)
					break;
				if(line.contains("$$$$$$$$$$"))
					break;
				if(!line.contains("missing"))
				{
					if(!line.contains("*") && !line.contains("plt") && !line.contains("Ano count") && !line.contains("missed") & !line.contains("$"))
					{
						id = line;
						line = fileReader.readLine();
						file_name = line;
					}
					continue;
				}
				else
				{
					StringTokenizer stt = new StringTokenizer(line, ":");
					String temp = stt.nextToken();
					int size = Integer.parseInt(stt.nextToken().toString().replaceAll("\\s", ""));
					line = fileReader.readLine();
					stt = new StringTokenizer(line, ":");
					temp = stt.nextToken();
					int anom_size = Integer.parseInt(stt.nextToken().toString().replaceAll("\\s", ""));
					if(size!=0)
					{
						count++;
						double percent = (double)(anom_size*100)/(size);
						total_anom += percent;
						out_percent.println(id+"_"+file_name + "," + percent);
					}
					
				}
			}
			out_percent.println("Mean : " + (double)total_anom/count);
			out_percent.close();
		}
		catch (Exception e)
		{
			System.out.println(line);
		}
	}
	public void smoothing_analyse_singleclass(String train, String test)
	{
		String line;
		try
		{
			//PrintWriter out_percent_train = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/outputs/smoothing/smoothing_analysis_"+train +"_"+ test+ "_"+ train+ ".csv"),"UTF-8"));
			PrintWriter out_percent_test = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/outputs/smoothing/smoothing_analysis_"+train +"_"+ test+ "_"+ test+ ".csv"),"UTF-8"));
			BufferedReader fileReader_train = new BufferedReader(new FileReader("src/outputs/trajToAnom_" + train +"_" + train +".txt"));
			BufferedReader fileReader_test = new BufferedReader(new FileReader("src/outputs/trajToAnom_" + train +"_" + test +".txt"));
			String id = new String();
			String file_name = new String();
			double total_anom = 0.0;
			int count = 0;
			
			ArrayList<Double> train_percent = new ArrayList<Double>();
			ArrayList<Double> test_percent = new ArrayList<Double>();
			int count_train = 0;
			int count_test = 0;
			while(true)
			{
				line = fileReader_train.readLine();
				if(line==null)
					break;
				if(line.contains("plt"))
					continue;
				if(line.contains("zero"))
					continue;
				//train_percent.add(Double.parseDouble(line));
				count_train++;
			}
			while(true)
			{
				line = fileReader_test.readLine();
				if(line==null)
					break;
				if(line.contains("plt"))
					continue;
				if(line.contains("zero"))
					continue;
				//test_percent.add(Double.parseDouble(line));
				count_test++;
			}
			int min = 0;
			if(count_test < count_train)
				min = count_test;
			else 
				min = count_train;
			
			fileReader_train = new BufferedReader(new FileReader("src/outputs/trajToAnom_" + train +"_" + train +".txt"));
			fileReader_test = new BufferedReader(new FileReader("src/outputs/trajToAnom_" + train +"_" + test +".txt"));
			
			count_train = 0;
			count_test = 0;
			while(true)
			{
				line = fileReader_train.readLine();
				if(line==null)
					break;
				if(line.contains("plt"))
					continue;
				if(line.contains("zero"))
					continue;
				train_percent.add(Double.parseDouble(line));
				count_train++;
				if(count_train == min)
					break;
			}
			while(true)
			{
				line = fileReader_test.readLine();
				if(line==null)
					break;
				if(line.contains("plt"))
					continue;
				if(line.contains("zero"))
					continue;
				test_percent.add(Double.parseDouble(line));
				count_test++;
				if(count_test == min)
					break;
			}
			
			
			
			System.out.println("Size : " + train_percent.size() + "  " + test_percent.size());
			double max_f1 = -1.0;
			double max_threshold1 = 0.0;
			double max_f2 = -1.0;
			double max_threshold2 = 0.0;
			//double threshold = 0.0;
			for(double threshold = 0.0 ; threshold <= 1 ; threshold = threshold + 0.01 )
			{
				int tp1 = 0, fp1 = 0, tn1 = 0, fn1 = 0, tp2 = 0, fp2 = 0, tn2 = 0, fn2 = 0;
				for(int i = 0; i < train_percent.size(); i++)
				{
					double val = train_percent.get(i);
					if(val < threshold)
					{
						tp1++;
						tn2++;
					}
					else
					{
						fn1++;
						fp2++;
					}
				}
				for(int i = 0; i < test_percent.size(); i++)
				{
					double val = test_percent.get(i);
					if(val > threshold)
					{
						tp2++;
						tn1++;
					}
					else
					{
						fn2++;
						fp1++;
					}
				}
				double p1 = (double)(tp1) / (tp1 + fp1);
				double p2 = (double)(tp2) / (tp2 + fp2);
				double r1 = (double)(tp1) / (tp1 + fn1);
				double r2 = (double)(tp2) / (tp2 + fn2);
				double f1 = (double)(2*(p1 * r1)) / (p1 + r1);
				double f2 = (double)(2*(p2 * r2)) / (p2 + r2);
				//System.out.println("tp1 : " + tp1 + " tp2 : " + tp2);
				//System.out.println("fp1 : " + fp1 + " fp2 : " + fp2);
				//System.out.println("tn1 : " + tn1 + " tn2 : " + tn2);
				//System.out.println("fn1 : " + fn1 + " fn2 : " + fn2);
				//System.out.println("p1 : " + p1 + " p2 : " + p2);
				//System.out.println("r1 : " + r1 + " r2 : " + r2 );
				//out_percent_train.println(threshold +"," + f1);
				//System.out.println("Threshold : " + threshold + " f1 :" +  f1 );
				if(f1 > max_f1)
				{
					max_f1 = f1;
					max_threshold1 = threshold;
				}
				
				out_percent_test.println(threshold +"," + p2 + "," + r2 + "," + f2);
				//System.out.println("Threshold : " + threshold +"," + p2 + "," + r2 + "," + f2 );
				if(f2 > max_f2)
				{
					max_f2 = f2;
					max_threshold2 = threshold;
				}
			}
			System.out.println("****************************");
			System.out.println("Max f1 : " + max_f1 + " for threshold : " + max_threshold1);
			//out_percent_train.println("Max f1 : " + max_f1 + " for threshold : " + max_threshold1);
			System.out.println("Max f2 : " + max_f2 + " for threshold : " + max_threshold2);
			out_percent_test.println("Max f1 : " + max_f2 + " for threshold : " + max_threshold2);
			//out_percent_train.close();
			out_percent_test.close();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	public void smoothing_analyse_multiclass(String train, String test)
	{
		String line;
		try
		{
			//PrintWriter out_percent_train = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/outputs/smoothing/smoothing_analysis_"+train +"_"+ test+ "_"+ train+ ".csv"),"UTF-8"));
			PrintWriter out_percent_test = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/outputs/smoothing/smoothing_analysis_"+train +"_"+ test+ "_"+ test+ ".csv"),"UTF-8"));
			BufferedReader fileReader_train = new BufferedReader(new FileReader("src/outputs/trajToAnom_" + train +"_" + train +".txt"));
			BufferedReader fileReader_test = new BufferedReader(new FileReader("src/outputs/trajToAnom_" + train +"_" + test +".txt"));
			String id = new String();
			String file_name = new String();
			double total_anom = 0.0;
			int count = 0;
			
			ArrayList<Double> train_percent = new ArrayList<Double>();
			ArrayList<Double> test_percent = new ArrayList<Double>();
			int count_train = 0;
			int count_test = 0;
			while(true)
			{
				line = fileReader_train.readLine();
				if(line==null)
					break;
				if(line.contains("plt"))
					continue;
				if(line.contains("zero"))
					continue;
				//train_percent.add(Double.parseDouble(line));
				count_train++;
			}
			while(true)
			{
				line = fileReader_test.readLine();
				if(line==null)
					break;
				if(line.contains("plt"))
					continue;
				if(line.contains("zero"))
					continue;
				//test_percent.add(Double.parseDouble(line));
				count_test++;
			}
			int min = 0;
			if(count_test < count_train)
				min = count_test;
			else 
				min = count_train;
			
			fileReader_train = new BufferedReader(new FileReader("src/outputs/trajToAnom_" + train +"_" + train +".txt"));
			fileReader_test = new BufferedReader(new FileReader("src/outputs/trajToAnom_" + train +"_" + test +".txt"));
			
			count_train = 0;
			count_test = 0;
			while(true)
			{
				line = fileReader_train.readLine();
				if(line==null)
					break;
				if(line.contains("plt"))
					continue;
				if(line.contains("zero"))
					continue;
				train_percent.add(Double.parseDouble(line));
				count_train++;
				if(count_train == min)
					break;
			}
			while(true)
			{
				line = fileReader_test.readLine();
				if(line==null)
					break;
				if(line.contains("plt"))
					continue;
				if(line.contains("zero"))
					continue;
				test_percent.add(Double.parseDouble(line));
				count_test++;
				if(count_test == min)
					break;
			}
			
			
			
			System.out.println("Size : " + train_percent.size() + "  " + test_percent.size());
			double max_f1 = -1.0;
			double max_threshold1 = 0.0;
			double max_f2 = -1.0;
			double max_threshold2 = 0.0;
			//double threshold = 0.0;
			for(double threshold = 0.0 ; threshold <= 1 ; threshold = threshold + 0.01 )
			{
				int tp1 = 0, fp1 = 0, tn1 = 0, fn1 = 0, tp2 = 0, fp2 = 0, tn2 = 0, fn2 = 0;
				for(int i = 0; i < train_percent.size(); i++)
				{
					double val = train_percent.get(i);
					if(val < threshold)
					{
						tp1++;
						tn2++;
					}
					else
					{
						fn1++;
						fp2++;
					}
				}
				for(int i = 0; i < test_percent.size(); i++)
				{
					double val = test_percent.get(i);
					if(val > threshold)
					{
						tp2++;
						tn1++;
					}
					else
					{
						fn2++;
						fp1++;
					}
				}
				double p1 = (double)(tp1) / (tp1 + fp1);
				double p2 = (double)(tp2) / (tp2 + fp2);
				double r1 = (double)(tp1) / (tp1 + fn1);
				double r2 = (double)(tp2) / (tp2 + fn2);
				double f1 = (double)(2*(p1 * r1)) / (p1 + r1);
				double f2 = (double)(2*(p2 * r2)) / (p2 + r2);
				//System.out.println("tp1 : " + tp1 + " tp2 : " + tp2);
				//System.out.println("fp1 : " + fp1 + " fp2 : " + fp2);
				//System.out.println("tn1 : " + tn1 + " tn2 : " + tn2);
				//System.out.println("fn1 : " + fn1 + " fn2 : " + fn2);
				//System.out.println("p1 : " + p1 + " p2 : " + p2);
				//System.out.println("r1 : " + r1 + " r2 : " + r2 );
				//out_percent_train.println(threshold +"," + f1);
				//System.out.println("Threshold : " + threshold + " f1 :" +  f1 );
				if(f1 > max_f1)
				{
					max_f1 = f1;
					max_threshold1 = threshold;
				}
				
				out_percent_test.println(threshold +"," + p2 + "," + r2 + "," + f2);
				//System.out.println("Threshold : " + threshold +"," + p2 + "," + r2 + "," + f2 );
				if(f2 > max_f2)
				{
					max_f2 = f2;
					max_threshold2 = threshold;
				}
			}
			System.out.println("****************************");
			System.out.println("Max f1 : " + max_f1 + " for threshold : " + max_threshold1);
			//out_percent_train.println("Max f1 : " + max_f1 + " for threshold : " + max_threshold1);
			System.out.println("Max f2 : " + max_f2 + " for threshold : " + max_threshold2);
			out_percent_test.println("Max f1 : " + max_f2 + " for threshold : " + max_threshold2);
			//out_percent_train.close();
			out_percent_test.close();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
}