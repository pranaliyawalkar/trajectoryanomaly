package geolife_processing;
import java.io.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

public class geolife_processing
{
	
	/** CONSTANTS : CONVERTING TIME INTO SECONDS. EX, ONE_YEAR = 31536000 SECS **/
	/*one non year = 31536000 secs
	one leap year = 31622400 secs
	jan, mar, may, jul, aug, oct, dec month =  2678400 secs
	apr, jun, sept, nov = 2592000 secs
	feb in leap year = 2505600 secs
	feb in non leap year = 2419200 secs
	one day = 86400 secs
	one hour = 3600 secs */
	public static final BigInteger one_year =  BigInteger.valueOf(31536000);
	public static final BigInteger leap_one_year =  BigInteger.valueOf(31622400);
	public static final BigInteger one_month_31 =  BigInteger.valueOf(2678400);
	public static final BigInteger one_month_30 = BigInteger.valueOf(2592000);
	public static final BigInteger one_month_29 = BigInteger.valueOf(2505600);
	public static final BigInteger one_month_28 = BigInteger.valueOf(2419200);
	public static final BigInteger one_day = BigInteger.valueOf(86400);
	public static final BigInteger one_hour = BigInteger.valueOf(3600);
	public static final BigInteger one_min = BigInteger.valueOf(60);
	
	
	PrintWriter walk_out
	, bike_out, bus_out, car_out, subway_out, 
	train_out, airplane_out, boat_out, run_out, motorcycle_out, taxi_out;
	
	/***  CHANGE THIS TO THE SUITABLE FOLDER NEEDED ***/
	public static final String geolife_directory = "/home/pranali/workspace/DDP_Anomaly/src/inputs/geolife_data/geolife_labeled/geolife_labeled/";
	
	/** GETTING MODIFIED FOR EVERY ID **/
	public String id_directory = "";
	
	public static void main(String[] args) throws Exception 
	{
		geolife_processing process_object = new geolife_processing();
		//process_object.process();
		process_object.count_handler();
		//process_object.test_data();
	}
	
	public void process() throws Exception 
	{
		File docDir = new File(geolife_directory);
		String[] files = docDir.list();
		
		int count = 0;
		for(int i = 0; i< files.length; i++)
		{
			id_directory = geolife_directory + files[i] + "/";
			
			/** IF THE FILES UNDER CONSIDERATION ARE NOT OUR CODE FILES **/
				/** IF "labels.txt" EXISTS IN THE ID FOLDER **/
			if(check_label_file())
			{
				System.out.println("Processing id : " + files[i]);
				count++;
				init();
			}
			
			else
			{
				System.out.println("Rejected id : " + files[i]);
			}
		}
		//System.out.println("Total processed files : " + count);
	}
	
	/** IF THE ID FOLDER CONTAINS "labels.txt, RETURN TRUE" **/
	public boolean check_label_file() throws Exception
	{
		File docDir = new File(id_directory);
		//System.out.println(id_directory);
		String[] files = docDir.list();
		for(int i = 0; i < files.length; i++)
		{
			if(files[i].equals("labels.txt"))
				return true;
		}
		return false;
	}
	
	/** START PROCESSING FOR THAT ID. EX, id = 089. IF WE ARE HERE, THEN "labels.txt" EXISTS IN THAT FOLDER **/
	public void init() throws Exception
	{
		String dirLocation = id_directory + "wrappedTrajectory";
		File docDir = new File(dirLocation);
		String[] files = docDir.list();
		String output_file_name =  id_directory + "class.txt";
		PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(output_file_name),"UTF-8"));
		ArrayList<geolife_label> labels = new ArrayList<geolife_label>();
		labels.addAll(read_labels());
		
		// READING ALL FILES IN THE "mappedTrajectory" FOLDER
		if(files!=null)
		{
			for (int file = 0; file < files.length; file++) 
			{
				String filename = files[file];
				StringTokenizer stt = new StringTokenizer(filename, ".");
				String token_date_time = stt.nextToken();
				//token_date_time = stt.nextToken();
				
				/**  THE BASE TIME IS : 2007/01/01 00:00:00  **/
				
				/** CONVERTING ITS START TIME TO A FORMAT WITH RESPECT TO THE BASE. 
				EX, 2008/01/01 00:00:00 WOULD BE 31536000 BECAUSE IT IS ONE YEAR PAST THE BASE **/
				
				BigInteger converted = convert(token_date_time);
				
				int flag = 0;
				for(int i = 0; i< labels.size(); i++)
				{
					if( ( converted.compareTo(labels.get(i).start) == 0 || converted.compareTo(labels.get(i).start) == 1) 
							&& ( converted.compareTo(labels.get(i).end) == 0 || converted.compareTo(labels.get(i).end) == -1) )
					{
							/** WRITING THE LABEL IF "converted" VARIABLE FALLS WITHIN "start" 
							 * AND "end" TIME OF A PARTICULAR LABEL 
							 * EXAMPLE : 20080415034030 (2008/04/15 03:40:30 FALLS WITHIN 
							 * [2008/04/15 03:40:30	2008/04/15 04:16:10	car] AND IS HENCE LABELLED AS "car" )
							 * **/
							out.println(files[file] + "  " + labels.get(i).label);
							flag = 1;
							break;
					}
				}
				
				/** IF NO LABEL FOUND, i.e THE START TIME OF THIS TRAJECTORY DOESN'T FALL 
				 * WITHIN THE START AND END TIME OF ANY STORED LABELS **/
				if(flag == 0)
				{
					out.println(files[file] + "  " + "NO LABEL");
				}
				
			}
		}
		out.close();
	}
	
	
	/** FUNCTION TO CONVERT 20080101000000 TO 31536000 WITH RESPECT TO THE BASE  **/
	public BigInteger convert(String token_date_time)
	{
		
		//base is 2007/01/01 00:00:00
		//example 20080411083823
		
		String string_year = token_date_time.substring(0, 4);
		int our_year = Integer.parseInt(string_year);
		
		String string_month = token_date_time.substring(4, 6);
		int our_month = Integer.parseInt(string_month);
		
		String string_date = token_date_time.substring(6, 8);
		int our_date = Integer.parseInt(string_date);
		
		String string_hour = token_date_time.substring(8, 10);
		int our_hour = Integer.parseInt(string_hour);
		
		String string_min = token_date_time.substring(10, 12);
		int our_min = Integer.parseInt(string_min);
		
		String string_sec = token_date_time.substring(12, 14);
		int our_sec = Integer.parseInt(string_sec);

		BigInteger total = BigInteger.ZERO;
		
		/** FOR ALL YEARS PASSED OVER THE BASE, ADDING APPROPRIATE SECONDS CONSIDERING LEAP YEARS TOO **/
		for(int year = 2007; year < our_year; year++)
		{
			if(leap_year(year))
				total = total.add(leap_one_year);
			else
				total = total.add(one_year);
		}
		
		/** FOR ALL MONTHS PASSED OVER THE BASE, ADDING APPROPRIATE SECONDS**/
		for(int month = 1; month < our_month; month++)
		{
			/** CONSIDERING MONTHS OF 31 DAYS **/
			if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
				total = total.add(one_month_31);
			
			/** CONSIDERNG MONTHS OF 30 DAYS **/
			else if (month == 4 || month == 6 || month == 9 || month == 11)
				total = total.add(one_month_30);
			
			/** CONSIDERING FEB IN LEAP YEAR **/
			else if(month == 2 && leap_year(our_year))
				total = total.add(one_month_29);
			
			/** CONSIDERING FEB IN NON LEAP YEAR **/
			else if(month == 2)
				total = total.add(one_month_28);
		}
		
		/** FOR ALL DAYS PASSED OVER THE BASE, ADDING APPROPRIATE SECONDS**/
		for(int date = 1; date < our_date; date++)
		{
			total = total.add(one_day);
		}
		
		/** FOR ALL HOURS PASSED OVER THE BASE, ADDING APPROPRIATE SECONDS**/
		for(int hour = 0; hour < our_hour; hour++)
		{
			total = total.add(one_hour);
		}
		
		/** FOR ALL MINUTES PASSED OVER THE BASE, ADDING APPROPRIATE SECONDS**/
		for(int min = 0; min < our_min; min++)
		{
			total = total.add(one_min);
		}
		
		/** FOR ALL SECONDS PASSED OVER THE BASE, ADDING APPROPRIATE SECONDS**/
		total = total.add(BigInteger.valueOf(our_sec));		
		
		return total;
	}
	
	/** CHECK IF AN YEAR IS A LEAP YEAR **/
	public boolean leap_year(int year)
	{
		if ( year%400 == 0)
		    return true;
		else if ( year%100 == 0)
			return false;
		else if ( year%4 == 0 )
			return true;
		else
			return false;
	}
	
	/** READ AND STORE LABELS **/
	public ArrayList<geolife_label> read_labels() throws Exception

	{
		ArrayList<geolife_label> labels = new ArrayList<geolife_label>();
		BufferedReader fileReader = new BufferedReader(new FileReader( id_directory + "labels.txt"));
		String line = "";
		while(true)
		{
			line = fileReader.readLine();
			if(line==null)
				break;
			if(line.startsWith("Start Time"))
				continue;
			else
			{
				//2008/04/11 11:44:53	2008/04/11 12:10:53	car
				StringTokenizer stt = new StringTokenizer(line, " \t");
				String start_date = stt.nextToken().toString();
				String start_time = stt.nextToken().toString();
				String end_date = stt.nextToken().toString();
				String end_time = stt.nextToken().toString();
				String label = stt.nextToken().toString();
				start_date = start_date.replaceAll("/", "");
				start_time = start_time.replaceAll(":", "");
				end_date = end_date.replaceAll("/", "");
				end_time = end_time.replaceAll(":", "");
				//System.out.println(start_date + " " + start_time);
				geolife_label new_label = new geolife_label();
				/**
				 *  1. CONVERTING START (DATE+TIME) TO RELATIVE SECONDS FROM THE BASE
				 *  2. CONVERTING END (DATE+TIME) TO RELATIVE SECONDS FROM THE BASE
				 *  3. STORING THE CORRESPONDING LABEL
				 *  **/
				
				new_label.start = convert(start_date + start_time);
				new_label.end = convert(end_date + end_time);
				new_label.label = label;
				
				labels.add(new_label);
			}
			
		}
		return labels;
	}

	public void count_handler() throws Exception
	{
		try 
		{
			String file_name =    "src/inputs/walk.txt";
			walk_out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file_name),"UTF-8"));
			
			file_name =    "src/inputs/bike.txt";
			bike_out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file_name),"UTF-8"));
			
			file_name =    "src/inputs/bus.txt";
			bus_out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file_name),"UTF-8"));
			
			file_name =    "src/inputs/car.txt";
			car_out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file_name),"UTF-8"));
			
			file_name =    "src/inputs/subway.txt";
			subway_out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file_name),"UTF-8"));
			
			file_name =    "src/inputs/train.txt";
			train_out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file_name),"UTF-8"));
			
			file_name =    "src/inputs/airplane.txt";
			airplane_out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file_name),"UTF-8"));
			
			file_name =    "src/inputs/boat.txt";
			boat_out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file_name),"UTF-8"));
			
			file_name =    "src/inputs/run.txt";
			run_out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file_name),"UTF-8"));
			
			file_name =    "src/inputs/motorcycle.txt";
			motorcycle_out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file_name),"UTF-8"));
			
			file_name =    "src/inputs/taxi.txt";
			taxi_out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file_name),"UTF-8"));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.out.println(e);
		}
		//String labeled_data_loc = "src/geolife_labeled/Data2/";
		String labeled_data_loc = "/home/pranali/workspace/DDP_Anomaly/src/inputs/geolife_data/geolife_labeled/geolife_labeled/";
		//String labeled_data_loc = "src/inputs//";
		File docDir = new File(labeled_data_loc);
		String[] files = docDir.list();
		/*walk, bike, bus, car, subway, train, airplane, boat, run, motorcycle, taxi*/
		HashMap<String, Integer> counts = new HashMap<String, Integer>();
		counts.put("walk", 0);
		counts.put("bike", 0);
		counts.put("bus", 0);
		counts.put("car", 0);
		counts.put("subway", 0);
		counts.put("train", 0);
		counts.put("airplane", 0);
		counts.put("boat", 0);
		counts.put("run", 0);
		counts.put("motorcycle", 0);
		counts.put("taxi", 0);
		
		/** READING ALL FOLDER NAMES IN  "src/geolife_labeled/Data2/" **/
		for(int i = 0; i< files.length; i++)
		{
			this.id_directory = labeled_data_loc + files[i] + "/";
			
			/** IF "labels.txt" EXISTS IN THE ID FOLDER **/
			//System.out.println(id_directory);
			if(this.check_label_file())
			{
				System.out.println("Processing id : " + files[i]);
				this.count(counts);
			}
			
			else
			{
				System.out.println("Rejected id : " + files[i]);
			}
		}
		System.out.println(counts);
		/*walk, bike, bus, car, subway, train, airplane, boat, run, motorcycle, taxi*/
		walk_out.close();
		bike_out.close();
		bus_out.close();
		car_out.close();
		subway_out.close();
		train_out.close();
		airplane_out.close();
		boat_out.close();
		run_out.close();
		motorcycle_out.close();
		taxi_out.close();
	}
	public void count(HashMap<String, Integer> counts) throws IOException
	{
		String file_name =  id_directory + "class.txt";
		File inFile = new File(file_name);
		FileInputStream fstream;
		try 
		{
			fstream = new FileInputStream(inFile);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine = new String();
			int count_walk = 0;
			int count_bike = 0;
			int count_bus = 0;
			int count_car = 0;
			int count_subway = 0;
			int count_train = 0;
			int count_airplane = 0;
			int count_boat = 0;
			int count_run = 0;
			int count_motorcycle = 0;
			int count_taxi = 0;
			while (( strLine = br.readLine()) != null )
			{
				/*walk, bike, bus, car, subway, train, airplane, boat, run, motorcycle*/
				if(strLine.contains("walk"))
				{
					counts.put("walk", counts.get("walk")+1);
					StringTokenizer stt = new StringTokenizer(strLine, " ");
					String token = stt.nextToken();
					String mapped_trajectory_filename = id_directory + "wrappedTrajectory/" + token;
					FileInputStream fstream1 = new FileInputStream(mapped_trajectory_filename);
					DataInputStream in1 = new DataInputStream(fstream1);
					BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
					String strLine1 = new String();
					while (( strLine1 = br1.readLine()) != null )
					{
						if(( !strLine1.contains("(") ) && ( !strLine1.equals("") ) )
						{
							if(count_walk == 0)
								walk_out.println(id_directory.substring(id_directory.length()-4, id_directory.length()-1));
							count_walk = 1;
							walk_out.println(token);
							walk_out.println(strLine1);
						}
					}
					
				}
				if(strLine.contains("bike"))
				{
					counts.put("bike", counts.get("bike")+1);
					StringTokenizer stt = new StringTokenizer(strLine, " ");
					String token = stt.nextToken();
					String mapped_trajectory_filename = id_directory + "wrappedTrajectory/" + token;
					FileInputStream fstream1 = new FileInputStream(mapped_trajectory_filename);
					DataInputStream in1 = new DataInputStream(fstream1);
					BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
					String strLine1 = new String();
					while (( strLine1 = br1.readLine()) != null )
					{
						if(( !strLine1.contains("(") ) && ( !strLine1.equals("") ) )
						{
							if(count_bike == 0)
								bike_out.println(id_directory.substring(id_directory.length()-4, id_directory.length()-1));
							count_bike = 1;
							bike_out.println(token);
							bike_out.println(strLine1);
						}
					}
				}
				if(strLine.contains("bus"))
				{
					counts.put("bus", counts.get("bus")+1);
					StringTokenizer stt = new StringTokenizer(strLine, " ");
					String token = stt.nextToken();
					String mapped_trajectory_filename = id_directory + "wrappedTrajectory/" + token;
					FileInputStream fstream1 = new FileInputStream(mapped_trajectory_filename);
					DataInputStream in1 = new DataInputStream(fstream1);
					BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
					String strLine1 = new String();
					while (( strLine1 = br1.readLine()) != null )
					{
						if(( !strLine1.contains("(") ) && ( !strLine1.equals("") ) )
						{
							if(count_bus == 0)
								bus_out.println(id_directory.substring(id_directory.length()-4, id_directory.length()-1));
							count_bus = 1;
							bus_out.println(token);
							bus_out.println(strLine1);
						}
					}
				}
				if(strLine.contains("car"))
				{
					counts.put("car", counts.get("car")+1);
					StringTokenizer stt = new StringTokenizer(strLine, " ");
					String token = stt.nextToken();
					String mapped_trajectory_filename = id_directory + "wrappedTrajectory/" + token;
					FileInputStream fstream1 = new FileInputStream(mapped_trajectory_filename);
					DataInputStream in1 = new DataInputStream(fstream1);
					BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
					String strLine1 = new String();
					while (( strLine1 = br1.readLine()) != null )
					{
						if(( !strLine1.contains("(") ) && ( !strLine1.equals("") ) )
						{
							if(count_car == 0)
								car_out.println(id_directory.substring(id_directory.length()-4, id_directory.length()-1));
							count_car = 1;
							car_out.println(token);
							car_out.println(strLine1);
						}
					}
				}
				if(strLine.contains("subway"))
				{
					counts.put("subway", counts.get("subway")+1);
					StringTokenizer stt = new StringTokenizer(strLine, " ");
					String token = stt.nextToken();
					String mapped_trajectory_filename = id_directory + "wrappedTrajectory/" + token;
					FileInputStream fstream1 = new FileInputStream(mapped_trajectory_filename);
					DataInputStream in1 = new DataInputStream(fstream1);
					BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
					String strLine1 = new String();
					while (( strLine1 = br1.readLine()) != null )
					{
						if(( !strLine1.contains("(") ) && ( !strLine1.equals("") ) )
						{
							if(count_subway == 0)
								subway_out.println(id_directory.substring(id_directory.length()-4, id_directory.length()-1));
							count_subway = 1;
							subway_out.println(token);
							subway_out.println(strLine1);
						}
					}
				}
				if(strLine.contains("train"))
				{
					counts.put("train", counts.get("train")+1);
					StringTokenizer stt = new StringTokenizer(strLine, " ");
					String token = stt.nextToken();
					String mapped_trajectory_filename = id_directory + "wrappedTrajectory/" + token;
					FileInputStream fstream1 = new FileInputStream(mapped_trajectory_filename);
					DataInputStream in1 = new DataInputStream(fstream1);
					BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
					String strLine1 = new String();
					while (( strLine1 = br1.readLine()) != null )
					{
						if(( !strLine1.contains("(") ) && ( !strLine1.equals("") ) )
						{
							if(count_train == 0)
								train_out.println(id_directory.substring(id_directory.length()-4, id_directory.length()-1));
							count_train = 1;
							train_out.println(token);
							train_out.println(strLine1);
						}
					}
				}
				if(strLine.contains("airplane"))
				{
					counts.put("airplane", counts.get("airplane")+1);
					StringTokenizer stt = new StringTokenizer(strLine, " ");
					String token = stt.nextToken();
					String mapped_trajectory_filename = id_directory + "wrappedTrajectory/" + token;
					FileInputStream fstream1 = new FileInputStream(mapped_trajectory_filename);
					DataInputStream in1 = new DataInputStream(fstream1);
					BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
					String strLine1 = new String();
					while (( strLine1 = br1.readLine()) != null )
					{
						if(( !strLine1.contains("(") ) && ( !strLine1.equals("") ) )
						{
							if(count_airplane == 0)
								airplane_out.println(id_directory.substring(id_directory.length()-4, id_directory.length()-1));
							count_airplane = 1;
							airplane_out.println(token);
							airplane_out.println(strLine1);
						}
					}
				}
				if(strLine.contains("boat"))
				{
					counts.put("boat", counts.get("boat")+1);
					StringTokenizer stt = new StringTokenizer(strLine, " ");
					String token = stt.nextToken();
					String mapped_trajectory_filename = id_directory + "wrappedTrajectory/" + token;
					FileInputStream fstream1 = new FileInputStream(mapped_trajectory_filename);
					DataInputStream in1 = new DataInputStream(fstream1);
					BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
					String strLine1 = new String();
					while (( strLine1 = br1.readLine()) != null )
					{
						if(( !strLine1.contains("(") ) && ( !strLine1.equals("") ) )
						{
							if(count_boat == 0)
								boat_out.println(id_directory.substring(id_directory.length()-4, id_directory.length()-1));
							count_boat = 1;
							boat_out.println(token);
							boat_out.println(strLine1);
						}
					}
				}
				if(strLine.contains("run"))
				{
					counts.put("run", counts.get("run")+1);
					//System.out.println(id_directory + "  " + strLine);
					StringTokenizer stt = new StringTokenizer(strLine, " ");
					String token = stt.nextToken();
					String mapped_trajectory_filename = id_directory + "wrappedTrajectory/" + token;
					FileInputStream fstream1 = new FileInputStream(mapped_trajectory_filename);
					DataInputStream in1 = new DataInputStream(fstream1);
					BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
					String strLine1 = new String();
					while (( strLine1 = br1.readLine()) != null )
					{
						if(( !strLine1.contains("(") ) && ( !strLine1.equals("") ) )
						{
							if(count_run == 0)
								run_out.println(id_directory.substring(id_directory.length()-4, id_directory.length()-1));
							count_run = 1;
							run_out.println(token);
							run_out.println(strLine1);
						}
					}
				}
				if(strLine.contains("motorcycle"))
				{
					counts.put("motorcycle", counts.get("motorcycle")+1);
					StringTokenizer stt = new StringTokenizer(strLine, " ");
					String token = stt.nextToken();
					String mapped_trajectory_filename = id_directory + "wrappedTrajectory/" + token;
					FileInputStream fstream1 = new FileInputStream(mapped_trajectory_filename);
					DataInputStream in1 = new DataInputStream(fstream1);
					BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
					String strLine1 = new String();
					while (( strLine1 = br1.readLine()) != null )
					{
						if(( !strLine1.contains("(") ) && ( !strLine1.equals("") ) )
						{
							if(count_motorcycle == 0)
								motorcycle_out.println(id_directory.substring(id_directory.length()-4, id_directory.length()-1));
							count_motorcycle = 1;
							motorcycle_out.println(token);
							motorcycle_out.println(strLine1);
						}
					}
				}
				if(strLine.contains("taxi"))
				{
					counts.put("taxi", counts.get("taxi")+1);
					StringTokenizer stt = new StringTokenizer(strLine, " ");
					String token = stt.nextToken();
					String mapped_trajectory_filename = id_directory + "wrappedTrajectory/" + token;
					FileInputStream fstream1 = new FileInputStream(mapped_trajectory_filename);
					DataInputStream in1 = new DataInputStream(fstream1);
					BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
					String strLine1 = new String();
					while (( strLine1 = br1.readLine()) != null )
					{
						if(( !strLine1.contains("(") ) && ( !strLine1.equals("") ) )
						{
							if(count_taxi == 0)
								taxi_out.println(id_directory.substring(id_directory.length()-4, id_directory.length()-1));
							count_taxi = 1;
							taxi_out.println(token);
							taxi_out.println(strLine1);
						}
					}
				}
			}
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			System.out.println(e);
		}
	}
	public void test_data()
	{
		//walk, bike, bus, car, subway, train, airplane, boat, run, motorcycle taxi
		try 
		{
			String file_name =    "src/inputs/walk.txt";
			FileInputStream fstream = new FileInputStream(file_name);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine = new String();
			int count = 0;
			while (( strLine = br.readLine()) != null )
			{
				count++;
			}
			System.out.println("Walk : " + count);
			
			file_name =    "src/inputs/bike.txt";
			fstream = new FileInputStream(file_name);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			strLine = new String();
			count = 0;
			while (( strLine = br.readLine()) != null )
			{
				count++;
			}
			System.out.println("bike : " + count);	
			
			
			file_name =    "src/inputs/bus.txt";
			fstream = new FileInputStream(file_name);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			strLine = new String();
			count = 0;
			while (( strLine = br.readLine()) != null )
			{
				count++;
			}
			System.out.println("bus : " + count);
			
			
			file_name =    "src/inputs/car.txt";
			fstream = new FileInputStream(file_name);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			strLine = new String();
			count = 0;
			while (( strLine = br.readLine()) != null )
			{
				count++;
			}
			System.out.println("car : " + count);
			
			
			file_name =    "src/inputs/subway.txt";
			fstream = new FileInputStream(file_name);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			strLine = new String();
			count = 0;
			while (( strLine = br.readLine()) != null )
			{
				count++;
			}
			System.out.println("subway : " + count);
			
			
			file_name =    "src/inputs/train.txt";
			fstream = new FileInputStream(file_name);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			strLine = new String();
			count = 0;
			while (( strLine = br.readLine()) != null )
			{
				count++;
			}
			System.out.println("train : " + count);
			
			
			file_name =    "src/inputs/airplane.txt";
			fstream = new FileInputStream(file_name);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			strLine = new String();
			count = 0;
			while (( strLine = br.readLine()) != null )
			{
				count++;
			}
			System.out.println("airplane : " + count);
			
			
			file_name =    "src/inputs/boat.txt";
			fstream = new FileInputStream(file_name);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			strLine = new String();
			count = 0;
			while (( strLine = br.readLine()) != null )
			{
				count++;
			}
			System.out.println("boat : " + count);
			
			
			file_name =    "src/inputs/run.txt";
			fstream = new FileInputStream(file_name);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			strLine = new String();
			count = 0;
			while (( strLine = br.readLine()) != null )
			{
				count++;
			}
			System.out.println("run : " + count);
			
			
			file_name =    "src/inputs/motorcycle.txt";
			fstream = new FileInputStream(file_name);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			strLine = new String();
			count = 0;
			while (( strLine = br.readLine()) != null )
			{
				count++;
			}
			System.out.println("motorcycle : " + count);
			
			
			file_name =    "src/inputs/taxi.txt";
			fstream = new FileInputStream(file_name);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			strLine = new String();
			count = 0;
			while (( strLine = br.readLine()) != null )
			{
				count++;
			}
			System.out.println("taxi : " + count);
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.out.println(e);
		}
		
		
	}
}