package org.example.anomaly.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.StringTokenizer;

public class AccuracyCheck {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception 
	{
		BufferedReader fileReader = new BufferedReader(new FileReader("src/car_train_values.txt"));
		HashMap<Integer, Integer> line_end = new HashMap<Integer, Integer>();
		while (true) 
		{
			String line = fileReader.readLine();
			if (line == null)
				break;
			if (line.startsWith("File name :")) 
			{
				System.out.println(line);
				continue;
			}
			if (!line.contains("$") || line.contains(" $$$$$$$$$"))
				continue;
			String[] twoOP = line.split("\\$");
			int line_number = Integer.parseInt(twoOP[0].replaceAll("\\s",""));
			String[] naiveOuts = twoOP[1].split("\\]")[0].split("\\[")[1]
					.trim().split(", ");
			String[] ourOuts = twoOP[2].split("\\]")[0].split("\\[")[1].trim()
					.split(", ");

			if (naiveOuts.length != ourOuts.length)
				System.out.println("Error:" + line);

			ArrayList<String> naiveList = new ArrayList<String>(Arrays.asList(naiveOuts));
			ArrayList<String> ourList = new ArrayList<String>(Arrays.asList(ourOuts));

			int count = 0;
			for (String string : ourList) 
			{
				if (!naiveList.contains(string)) 
					System.out.println("Error:" + line);
				count++;
			}
			if(count != 1)
			{
				System.out.println("Error in count1");
				continue;
			}
			count = 0;
			for (String string : naiveList) 
			{
				if (!ourList.contains(string))
					System.out.println("Error:" + line);
				count ++;
			}
			if(count != 1)
			{
				System.out.println(" in count2");
				continue;
			}
			String naive = naiveList.get(0);
			StringTokenizer stt = new StringTokenizer(naive, "[] ,");
			if(stt.countTokens() != 2)
			{
				System.out.println("Error in tokens");
				continue;
			}
			int start = Integer.parseInt(stt.nextToken().toString().replaceAll("\\s",""));
			int end = Integer.parseInt(stt.nextToken().toString().replaceAll("\\s",""));
			if(start!=0)
			{
				System.out.println("Error in start");
				continue;
			}
			
			line_end.put(line_number,end);
			
		}
		
		String file  = "src/inputs/train.txt";
		BufferedReader fileReader1 = new BufferedReader(new FileReader(file));
		int line_no = 0;
		System.out.println( "Size : " + line_end.size());
		while(true)
		{
			String line1 = fileReader1.readLine();
			if (line1 == null)
				break;
			if(line_no == 11)
				System.out.println("here");
			line_no++;
			if(line_end.get(line_no) !=null)
			{
				StringTokenizer stt1 = new StringTokenizer(line1, "[");
				int end = line_end.get(line_no);
				if(stt1.countTokens() != end)
				{
					System.out.println("Error in tokens " + stt1.countTokens() + " end : " + end);
					continue;
				}
			}
		}
		
		fileReader.close();
		fileReader1.close();
	}

}