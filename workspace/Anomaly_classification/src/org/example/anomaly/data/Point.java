package org.example.anomaly.data;

import java.util.Date;

public class Point {

	public int id;
	public double lat;
	public double lon;
	public Date timeStamp;
	public long mytime;

	public Point(int id, double lat, double lon, Date timeStamp, long time) {
		this.id = id;
		this.lat = lat;
		this.lon = lon;
		this.timeStamp = timeStamp;
		this.mytime = time;

	}
}