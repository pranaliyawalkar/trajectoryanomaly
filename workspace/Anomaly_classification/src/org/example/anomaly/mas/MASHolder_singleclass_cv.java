package org.example.anomaly.mas;

import geolife_processing.start_end_anom;

import java.beans.FeatureDescriptor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import org.example.anomaly.data.Point;
import org.example.anomaly.util.EdgeMeanVariance;

public class MASHolder_singleclass_cv 
{
	double PIx = 3.141592653589793;
	double RADIUS = 6378.16;
	
	
	String mapMatchedTrajDirPath = "";
	String learnedEdgeDistroFilePath = "";
	
	PrintWriter outValues;
	
	EdgeProcessing ep1;
	EdgeProcessing ep2;
	boolean modify_speed;
	
	Hashtable<String, Double> new_speeds;
	
	public static final Hashtable<String, Integer> sizes = new Hashtable<String, Integer>();

	public MASHolder_singleclass_cv() {

	}

	public MASHolder_singleclass_cv(String train, String test, double threshold) 
	{
		
		//double smoothing = Math.sqrt(threshold) + 1 ;
		double smoothing = 3.0;
		String dest = "src/inputs/singleclass/" + train + "_" + test +"/";
		String source = "src/inputs/singleclass/" + train + "_" + test +"/";
		mapMatchedTrajDirPath = source;
		this.sizes.put("bike", 649);
		this.sizes.put("bus", 512);
		this.sizes.put("car", 569);
		this.sizes.put("walk", 1262);
		
		//train data : testing on part 5
		
		
		for(int i =  1 ; i <=5 ; i++)
		//int i = j;
		{
			try
			{
				//double threshold = 5.0;
				//for(double threshold = 6.0; threshold <= 10.0; threshold = threshold + 1)
				new_speeds = new Hashtable<String, Double>();
				{
					learnedEdgeDistroFilePath = source + "out_" + train + "_" + "train_" + i + ".csv"; 
					ep1 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, smoothing);
					learnedEdgeDistroFilePath = source + "out_" + test + "_" + "train_" + i + ".csv"; 
					ep2 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, smoothing);
					outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "percent" +"_" + threshold +"_" + train + "_" + i + ".csv"), "UTF-8"));
					//outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "percent" +"_" + train + "_" + i + ".csv"), "UTF-8"));
					/*outValues.println("size" +"," + "out_" + train + "_" + "train_" + i + ".csv" + "," + "out_" + test + "_" + "train_" + i + ".csv"
							+ "," + "anom edges " + train + "," + "anom edges " + test 
							+ "," + "missing edges " + train + "," + "missing edges " + test
							);*/
					outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "size" +"_" + train + "_" + i + ".csv"), "UTF-8"));
					
					System.out.println("Train 1 : " + "out_" + train + "_" + "train_" + i + ".csv");
					System.out.println("Train 2 : " + "out_" + test + "_" + "train_" + i + ".csv");
					System.out.println("Test : " + train + "_" + "test_" + i + ".txt");
					modify_speed = false;
					compare_handler(train + "_" + "test_" + i + ".txt");
					outValues.close();
				}
				
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
		//test data : testing on part 5
		for(int i =  1 ; i <=5 ; i++)
		{
			try
			{
				//double threshold = 5.0;
				//for(double threshold = 6.0; threshold <= 10.0; threshold = threshold + 1)
				new_speeds = new Hashtable<String, Double>();
				{
					learnedEdgeDistroFilePath = source + "out_" + train + "_" + "train_" + i + ".csv"; 
					ep1 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, smoothing);
					learnedEdgeDistroFilePath = source + "out_" + test + "_" + "train_" + i + ".csv"; 
					ep2 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, smoothing);
					outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest+"percent" + "_" + threshold +"_" + test + "_" + i + ".csv"), "UTF-8"));
					//outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "percent" +"_" + test + "_" + i + ".csv"), "UTF-8"));
					/*outValues.println("size" +"," + "out_" + train + "_" + "train_" + i + ".csv" + "," + "out_" + test + "_" + "train_" + i + ".csv"
							+ "," + "anom edges " + train + "," + "anom edges " + test + "," +  "missing edges " + train + "," + "missing edges " + test);*/
					outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "size" +"_" + test + "_" + i + ".csv"), "UTF-8"));
					
					System.out.println("Train 1 : " + "out_" + train + "_" + "train_" + i + ".csv");
					System.out.println("Train 2 : " + "out_" + test + "_" + "train_" + i + ".csv");
					System.out.println("Test : " + test + "_" + "test_" + i + ".txt");
					modify_speed = false;
					compare_handler(test + "_" + "test_" + i + ".txt");
					outValues.close();
				}
				
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
	}

	public void compare_handler(String testfile) throws Exception 
	{
		int line_no = 0;
		int total_count = 0;
		{
			line_no = 0;
			System.out.println("File name : " + testfile);
			BufferedReader fileReader = new BufferedReader(new FileReader( mapMatchedTrajDirPath + testfile));
			int prev_id = 0;
			double prev_lat = 0.0;
			double prev_lon = 0.0;
			long prev_val = 0;
			Date prev_time = new Date();
			while (true) 
			{
				ArrayList<Point> arr = new ArrayList<Point>();
				String line = fileReader.readLine();
				if (line == null)
					break;
				line_no++;
				prev_id = -1;
				prev_lat = 0.0;
				prev_lon = 0.0;
				prev_time = new Date();
				double speed = 0.0;
				if (line.contains("[")) 
				{
					String[] warr = line.split("\\]");
					for (String s : warr) 
					{
						try 
						{
							int id = Integer.parseInt(s.split(",")[0]
									.split("\\[")[1]);
							double lat = Double.parseDouble(s.split(",")[1]);
							double lon = Double.parseDouble(s.split(",")[2]);
							long val = Long.parseLong(s.split(",")[3]
									.replaceAll("\\s", ""));
							Date date = new Date(val);
							SimpleDateFormat df2 = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							String dateText = df2.format(date);
							Date time = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss").parse(dateText);

							arr.add(new Point(id, lat, lon, time, val));
							if(prev_id!=-1 && modify_speed == true)
							{
								speed =  Math.abs(distance(lat, lon,prev_lat, prev_lon)) * 1000 * 3600 / (Math.abs(prev_val-val));
								if(speed > 10.0)
								{
									new_speeds.put(id + "_" + prev_id, Math.abs(distance(lat, lon,prev_lat, prev_lon)) * 1000 * 3600 );
									new_speeds.put(prev_id + "_" + id, Math.abs(distance(lat, lon,prev_lat, prev_lon)) * 1000 * 3600 );
								}
							}
							prev_time = time;
							prev_lat = lat;
							prev_lon = lon;
							prev_id = id;
							prev_val = val;
						} 
						catch (Exception e) 
						{
							System.out.println(e);
						}
					}
					total_count++;
					
					outValues.println(warr.length - 1);
					//compare(arr, line_no, warr.length - 1);
				}
			}
			fileReader.close();
		}
	}

	public void compare(ArrayList<Point> pt, int line_no, int size) throws Exception 
	{
		long tot1 = 0;
		long tot2 = 0;
		long tot3 = 0;
		List<Integer> nodeSequence = new ArrayList<Integer>();
		List<Double> travelTime = new ArrayList<Double>();
		for (int i = 0; i < pt.size(); i++) 
		{
			nodeSequence.add(pt.get(i).id);
			if (i < (pt.size() - 1)) 
			{
				double time = pt.get(i + 1).mytime - pt.get(i).mytime;
				if(modify_speed == true && (new_speeds.get(pt.get(i+1).id + "_" + pt.get(i).id)!=null))
				{
					time = new_speeds.get(pt.get(i+1).id + "_" + pt.get(i).id) / 10;
				}
				travelTime.add(time);
			}
		}
		ep1.setNodeTime(nodeSequence, travelTime);
		ep2.setNodeTime(nodeSequence, travelTime);
		
		ArrayList<Integer> not_in_training1 = new ArrayList<Integer>();
		ep1.notInTraining(not_in_training1); 
		
		ArrayList<Integer> not_in_training2 = new ArrayList<Integer>();
		ep2.notInTraining(not_in_training2); 
		
		ArrayList<Integer> anoms1 = new ArrayList<Integer>();
		int count1 = ep1.howManyAno(anoms1);
		
		ArrayList<Integer> anoms2 = new ArrayList<Integer>();
		int count2 = ep2.howManyAno(anoms2);
		
		ArrayList<String> our_result1 = new ArrayList<String>();
		ArrayList<String> our_result2 = new ArrayList<String>();
		try 
		{
			our_result1 = ep1.findAnomalyOur();
			our_result2 = ep2.findAnomalyOur();
		} 
		catch (Exception e) 
		{
			System.out.println("Not there in training : our");
			e.printStackTrace();
			return;
		}
		/*System.out.println(line_no + " $ " + our_result1 + " $ " + size
				+ " $ " + not_in_training1.size()) /* +  naive_result + " $ " + naive_result2*/;
		/*System.out.println(line_no + " $ " + our_result2 + " $ " + (size)
				+ " $ " + not_in_training2.size()); */

		ArrayList<start_end_anom> sub_trajcs1 = new ArrayList<start_end_anom>();
		int total_anom_count1 = 0;
		ArrayList<Integer> visited1 = new ArrayList<Integer>();
		for (int i = 0; i < size; i++)
			visited1.add(0);
		for (int x = 0; x < our_result1.size(); x++) {
			start_end_anom obj = new start_end_anom();
			String s = our_result1.get(x);
			StringTokenizer stt = new StringTokenizer(s, ",");
			obj.start = Integer.parseInt(stt.nextToken().toString());
			obj.end = Integer.parseInt(stt.nextToken().toString());
			for (int z = obj.start; z <= obj.end; z++) 
			{
				if (visited1.get(z) == 0) // unvisited
				{
					total_anom_count1++;
					visited1.set(z, 1);
				}
			}

			sub_trajcs1.add(obj);
		}
		
		for (int x = 0; x < not_in_training1.size(); x++) 
		{
			for (int y = 0; y < sub_trajcs1.size(); y++) 
			{
				if (sub_trajcs1.get(y).start <= not_in_training1.get(x)
						&& sub_trajcs1.get(y).end >= not_in_training1.get(x)) {
					total_anom_count1 = total_anom_count1 - 1;
					break;
				}
			}
		}

		int trajSize1 = size - not_in_training1.size();
		if (size > 0)
			outValues.print((double) (total_anom_count1 + not_in_training1.size()));
		else
			outValues.print("Size zero!!");
		
		
		//SECOND CLASS
		ArrayList<start_end_anom> sub_trajcs2 = new ArrayList<start_end_anom>();
		int total_anom_count2 = 0;
		ArrayList<Integer> visited2 = new ArrayList<Integer>();
		for (int i = 0; i < size; i++)
			visited2.add(0);
		for (int x = 0; x < our_result2.size(); x++) {
			start_end_anom obj = new start_end_anom();
			String s = our_result2.get(x);
			StringTokenizer stt = new StringTokenizer(s, ",");
			obj.start = Integer.parseInt(stt.nextToken().toString());
			obj.end = Integer.parseInt(stt.nextToken().toString());
			for (int z = obj.start; z <= obj.end; z++) 
			{
				if (visited2.get(z) == 0) // unvisited
				{
					total_anom_count2++;
					visited2.set(z, 1);
				}
			}

			sub_trajcs2.add(obj);
		}
		
		for (int x = 0; x < not_in_training2.size(); x++) 
		{
			for (int y = 0; y < sub_trajcs2.size(); y++) 
			{
				if (sub_trajcs2.get(y).start <= not_in_training2.get(x)
						&& sub_trajcs2.get(y).end >= not_in_training2.get(x)) {
					total_anom_count2 = total_anom_count2 - 1;
					break;
				}
			}
		}

		int trajSize2 = size - not_in_training2.size();
		if (size > 0)
			outValues.print( "," + (double) (total_anom_count2 + not_in_training2.size()));
		else
			outValues.print("," + "Size zero!!");
		
		//pringint the anom edges
		outValues.println( "," + count1 + "," + count2 + "," + not_in_training1.size() + "," + not_in_training2.size());
		
		
		//System.out.println("*****************************************");


	}

	public void fscore(String train, String train2, double threshold )
	{
		String dest = "src/inputs/singleclass/" + train + "_" + train2 + "/";
		
		//for(double threshold = 6.0; threshold <= 10.0 ; threshold = threshold + 1.0)
		{
			Hashtable<String, ArrayList<Double>> all_fscore = new Hashtable<String, ArrayList<Double>>();
			all_fscore.put(train, new ArrayList<Double>());
			all_fscore.put(train2, new ArrayList<Double>());
			for(int i = 1; i <= 5 ; i++ )
			{
				try
				{
					Hashtable<String, Integer> tp = new Hashtable<String, Integer>();
					Hashtable<String, Integer> fp = new Hashtable<String, Integer>();
					Hashtable<String, Integer> tn = new Hashtable<String, Integer>();
					Hashtable<String, Integer> fn = new Hashtable<String, Integer>();
					Hashtable<String, Double> p = new Hashtable<String, Double>();
					Hashtable<String, Double> r = new Hashtable<String, Double>();
					Hashtable<String, Double> f1 = new Hashtable<String, Double>();
					
					tp.put(train, 0);
					tp.put(train2, 0);
					fp.put(train, 0);
					fp.put(train2, 0);
					tn.put(train, 0);
					tn.put(train2, 0);
					fn.put(train, 0);
					fn.put(train2, 0);
					
					for(int j = 0 ; j <=1 ; j++)
					{
						BufferedReader fileReader1;
						String me = new String();
						String not_me = new String();
						if(j == 0)
						{
							me = train;
							not_me = train2;
							//System.out.println("percent" + train + "_" + i + ".csv");
							fileReader1 = new BufferedReader(new FileReader( dest + "percent_" + threshold + "_" + train + "_" + i + ".csv" ));
						}
						else
						{
							me = train2;
							not_me = train;
							//System.out.println("percent" + test + "_" + i + ".csv");
							fileReader1 = new BufferedReader(new FileReader( dest + "percent_" + threshold + "_" + train2 + "_" + i + ".csv" ));
						}
						String line = new String();
						while(true)
						{
							line = fileReader1.readLine();
							if(!line.equals(""))
								break;
						}
						
						StringTokenizer stt = new StringTokenizer(line , ",");
						String temp = stt.nextToken().toString();
						String class1 = stt.nextToken().toString();
						String class2 = stt.nextToken().toString();
						
						stt = new StringTokenizer(class1, "_");
						class1 = stt.nextToken().toString();
						class1 = stt.nextToken().toString();
						
						stt = new StringTokenizer(class2, "_");
						class2 = stt.nextToken().toString();
						class2 = stt.nextToken().toString();
						
						boolean first_class_is_my_class;
						if(class1.equals(me))
							first_class_is_my_class = true;
						else
							first_class_is_my_class = false;
						
						while(true)
						{
							line = fileReader1.readLine();
							if(line == null)
								break;
							if(line.contains("zero"))
								continue;
							stt = new StringTokenizer(line, ",");
							double size = Double.parseDouble(stt.nextToken().toString());
							double val1 = Double.parseDouble(stt.nextToken().toString());
							double val2 = Double.parseDouble(stt.nextToken().toString());
							if(first_class_is_my_class)
							{
								if(val1 > val2)
								{
									fn.put(me, fn.get(me)+1);
									fp.put(not_me, fp.get(not_me)+1);
								}
								else if(val1 < val2)
								{
									tp.put(me, tp.get(me)+1);
									tn.put(not_me, tn.get(not_me)+1);
								}
								else
								{
									double anom1 = Double.parseDouble(stt.nextToken().toString());
									double anom2 = Double.parseDouble(stt.nextToken().toString());
									if(anom1 > anom2)
									{
										fn.put(me, fn.get(me)+1);
										fp.put(not_me, fp.get(not_me)+1);
									}
									else
									{
										tp.put(me, tp.get(me)+1);
										tn.put(not_me, tn.get(not_me)+1);
									}
								}
							}
							else
							{
								if(val1 > val2)
								{
									tp.put(me, tp.get(me)+1);
									tn.put(not_me, tn.get(not_me)+1);
								}
								else if(val1 < val2)
								{
									fn.put(me, fn.get(me)+1);
									fp.put(not_me, fp.get(not_me)+1);
								}
								else
								{
									double anom1 = Double.parseDouble(stt.nextToken().toString());
									double anom2 = Double.parseDouble(stt.nextToken().toString());
									if(anom1 >= anom2)
									{
										tp.put(me, tp.get(me)+1);
										tn.put(not_me, tn.get(not_me)+1);
									}
									else
									{
										fn.put(me, fn.get(me)+1);
										fp.put(not_me, fp.get(not_me)+1);
									}
								}
							}
						}
					}
					p.put(train, ((double)(tp.get(train))/(tp.get(train) + fp.get(train))));
					p.put(train2, ((double)(tp.get(train2))/(tp.get(train2) + fp.get(train2))));
					
					r.put(train, ((double)(tp.get(train))/(tp.get(train) + fn.get(train))));
					r.put(train2, ((double)(tp.get(train2))/(tp.get(train2) + fn.get(train2))));
					
					f1.put(train, ((double)(2*p.get(train)*r.get(train))/(r.get(train) + p.get(train))));
					f1.put(train2, ((double)(2*p.get(train2)*r.get(train2))/(r.get(train2) + p.get(train2))));
					all_fscore.get(train).add(f1.get(train));
					all_fscore.get(train2).add(f1.get(train2));
					//System.out.println("Threshold : " + threshold);
					System.out.println(train+ i + "  " + p.get(train) + "  " + r.get(train) + "  " + f1.get(train)) ;
					System.out.println(train2+ i + "  " + p.get(train2) + "  " + r.get(train2) + "  " + f1.get(train2)) ;
					
				}
				catch (Exception e)
				{
					System.out.println(e);
				}
			}
			System.out.println("Threshold : " + threshold);
			double avg = 0.0;
			for(int i = 0;i < all_fscore.get(train).size(); i++)
			{
				avg += all_fscore.get(train).get(i);
			}
			System.out.println("AVERAGE : " + train + "  " + avg/all_fscore.get(train).size());
			avg = 0.0;
			for(int i = 0;i < all_fscore.get(train).size(); i++)
			{
				avg += all_fscore.get(train2).get(i);
			}
			System.out.println("AVERAGE : " + train2 + "  " + avg/all_fscore.get(train).size());
			System.out.println("**********************************************");
		}
	}
	
	public void generate_parts(String train, String test)
	{
		try
		{
			BufferedReader train_file = new BufferedReader(new FileReader("src/inputs/singleclass/" + train + "_" + test +"/" + train + ".txt"));
			BufferedReader test_file = new BufferedReader(new FileReader("src/inputs/singleclass/" + train + "_" + test +"/" + test + ".txt"));
			
			PrintWriter train_train_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + train +"_train_1.txt"  ), "UTF-8"));
			PrintWriter train_train_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + train +"_train_2.txt"  ), "UTF-8"));
			PrintWriter train_train_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + train +"_train_3.txt"  ), "UTF-8"));
			PrintWriter train_train_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + train +"_train_4.txt"  ), "UTF-8"));
			PrintWriter train_train_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + train +"_train_5.txt"  ), "UTF-8"));
			PrintWriter train_test_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + train +"_test_1.txt"  ), "UTF-8"));
			PrintWriter train_test_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + train +"_test_2.txt"  ), "UTF-8"));
			PrintWriter train_test_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + train +"_test_3.txt"  ), "UTF-8"));
			PrintWriter train_test_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + train +"_test_4.txt"  ), "UTF-8"));
			PrintWriter train_test_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + train +"_test_5.txt"  ), "UTF-8"));


			PrintWriter test_train_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + test +"_train_1.txt"  ), "UTF-8"));
			PrintWriter test_train_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + test +"_train_2.txt"  ), "UTF-8"));
			PrintWriter test_train_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + test +"_train_3.txt"  ), "UTF-8"));
			PrintWriter test_train_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + test +"_train_4.txt"  ), "UTF-8"));
			PrintWriter test_train_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + test +"_train_5.txt"  ), "UTF-8"));
			PrintWriter test_test_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + test +"_test_1.txt"  ), "UTF-8"));
			PrintWriter test_test_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + test +"_test_2.txt"  ), "UTF-8"));
			PrintWriter test_test_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + test +"_test_3.txt"  ), "UTF-8"));
			PrintWriter test_test_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + test +"_test_4.txt"  ), "UTF-8"));
			PrintWriter test_test_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/singleclass/" + train + "_" + test +"/" + test +"_test_5.txt"  ), "UTF-8"));

			int train_size = sizes.get(train);
			int test_size = sizes.get(test);
			if(train_size > test_size)
			{
				String line = new String();
				int count = 0;
				while(true)
				{
					line = test_file.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
						continue;
					if(0 <= count && count < (test_size/5))
					{
						//first test
						test_test_1.println(line);
						test_train_2.println(line);
						test_train_3.println(line);
						test_train_4.println(line);
						test_train_5.println(line);
					}
					else if((test_size/5) <= count && count < ((2*test_size)/5))
					{
						//first test
						test_test_2.println(line);
						test_train_1.println(line);
						test_train_3.println(line);
						test_train_4.println(line);
						test_train_5.println(line);
					}
					else if(((2*test_size)/5) <= count && count < ((3*test_size)/5))
					{
						//first test
						test_test_3.println(line);
						test_train_1.println(line);
						test_train_2.println(line);
						test_train_4.println(line);
						test_train_5.println(line);
					}
					else if(((3*test_size)/5) <= count && count < ((4*test_size)/5))
					{
						//first test
						test_test_4.println(line);
						test_train_1.println(line);
						test_train_2.println(line);
						test_train_3.println(line);
						test_train_5.println(line);
					}
					else
					{
						//first test
						test_test_5.println(line);
						test_train_1.println(line);
						test_train_2.println(line);
						test_train_3.println(line);
						test_train_4.println(line);
					}
					
					count++;
				}
				count = 0;
				while(true)
				{
					line = train_file.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
					{
						continue;
					}
					if(0 <= count && count < (test_size/5))
					{
						//first test
						train_test_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if((test_size/5) <= count && count < ((2*test_size)/5))
					{
						//first test
						train_test_2.println(line);
						train_train_1.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if(((2*test_size)/5) <= count && count < ((3*test_size)/5))
					{
						//first test
						train_test_3.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if(((3*test_size)/5) <= count && count < ((4*test_size)/5))
					{
						//first test
						train_test_4.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_5.println(line);
					}
					else if(((4*test_size)/5) <= count && count < test_size)
					{
						//first test
						train_test_5.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
					}
					else
					{
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					count++;
				}
			}
			else
			{
				String line = new String();
				int count = 0;
				while(true)
				{
					line = train_file.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
						continue;
					if(0 <= count && count < (train_size/5))
					{
						//first test
						train_test_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if((train_size/5) <= count && count < ((2*train_size)/5))
					{
						//first test
						train_test_2.println(line);
						train_train_1.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if(((2*train_size)/5) <= count && count < ((3*train_size)/5))
					{
						//first test
						train_test_3.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if(((3*train_size)/5) <= count && count < ((4*train_size)/5))
					{
						//first test
						train_test_4.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_5.println(line);
					}
					else
					{
						//first test
						train_test_5.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
					}
					
					count++;
				}
				count = 0;
				while(true)
				{
					line = test_file.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
						continue;
					if(0 <= count && count < (train_size/5))
					{
						//first test
						test_test_1.println(line);
						test_train_2.println(line);
						test_train_3.println(line);
						test_train_4.println(line);
						test_train_5.println(line);
					}
					else if((train_size/5) <= count && count < ((2*train_size)/5))
					{
						//first test
						test_test_2.println(line);
						test_train_1.println(line);
						test_train_3.println(line);
						test_train_4.println(line);
						test_train_5.println(line);
					}
					else if(((2*train_size)/5) <= count && count < ((3*train_size)/5))
					{
						//first test
						test_test_3.println(line);
						test_train_1.println(line);
						test_train_2.println(line);
						test_train_4.println(line);
						test_train_5.println(line);
					}
					else if(((3*train_size)/5) <= count && count < ((4*train_size)/5))
					{
						//first test
						test_test_4.println(line);
						test_train_1.println(line);
						test_train_2.println(line);
						test_train_3.println(line);
						test_train_5.println(line);
					}
					else if(((4*train_size)/5) <= count && count < train_size)
					{
						//first test
						test_test_5.println(line);
						test_train_1.println(line);
						test_train_2.println(line);
						test_train_3.println(line);
						test_train_4.println(line);
					}
					else
					{
						test_train_1.println(line);
						test_train_2.println(line);
						test_train_3.println(line);
						test_train_4.println(line);
						test_train_5.println(line);
					}
					count++;
				}
			}
			train_test_1.close();
			train_test_2.close();
			train_test_3.close();
			train_test_4.close();
			train_test_5.close();
			train_train_1.close();
			train_train_2.close();
			train_train_3.close();
			train_train_4.close();
			train_train_5.close();
			test_test_1.close();
			test_test_2.close();
			test_test_3.close();
			test_test_4.close();
			test_test_5.close();
			test_train_1.close();
			test_train_2.close();
			test_train_3.close();
			test_train_4.close();
			test_train_5.close();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}
	public int get_size(String dest)
	{
		int count = 0;
		try
		{
			BufferedReader fileReader = new BufferedReader(new FileReader(dest));
			String line = new String();
			while(true)
			{
				line = fileReader.readLine();
				if(line == null)
					break;
				if(line.contains("["))
					count++;
			}
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
		return count;
	}
	public static void main(String[] args) throws Exception 
	{
		ArrayList<ArrayList<String>> files = new ArrayList<ArrayList<String>>();
		
		/*ArrayList<String> model = new ArrayList<String>();
		model.add("bike");
		model.add("bus");
		files.add(model);
		*/
		ArrayList<String> model1 = new ArrayList<String>();
		model1.add("bike");
		model1.add("car");
		files.add(model1);
		
		/*
		ArrayList<String> model2 = new ArrayList<String>();
		model2.add("bus");
		model2.add("walk");
		files.add(model2);
		
		ArrayList<String> model3 = new ArrayList<String>();
		model3.add("car");
		model3.add("bus");
		files.add(model3);
		
		ArrayList<String> model4 = new ArrayList<String>();
		model4.add("car");
		model4.add("walk");
		files.add(model4);
		
		ArrayList<String> model5 = new ArrayList<String>();
		model5.add("walk");
		model5.add("bike");
		files.add(model5);
		*/
		
		for(int i = 0 ; i < files.size(); i++)
		{
			String train = files.get(i).get(0);
			String train2 = files.get(i).get(1);
			MASHolder_singleclass_cv ms = new MASHolder_singleclass_cv(train, train2, 5.0);
			//MASHolder_singleclass_cv ms = new MASHolder_singleclass_cv();
			ms.fscore(train, train2, 5.0);
			//ms.learn(train, train2);
		}
	}

	
	public double Radians(double x) {
		return x * PIx / 180;
	}

	public double distance(double lat1, double lon1, double lat2, double lon2) {
		double dlon = Radians(lon2 - lon1);
		double dlat = Radians(lat2 - lat1);

		double a = (Math.sin(dlat / 2) * Math.sin(dlat / 2))
				+ Math.cos(Radians(lat1)) * Math.cos(Radians(lat2))
				* (Math.sin(dlon / 2) * Math.sin(dlon / 2));
		double angle = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return angle * RADIUS;

	}
	public void learn(String train, String train2) throws Exception
	{
		String dirLocation = "src/inputs/singleclass/" + train + "_" + train2 + "/";
		for(int i = 1; i <=5 ; i++)
		{
			EdgeMeanVariance em = new EdgeMeanVariance();
			em.findMeanVarAdulterated(dirLocation, train2 + "_train_" + i + ".txt");
		}
	}

}