package org.example.anomaly.mas;

import geolife_processing.start_end_anom;

import java.beans.FeatureDescriptor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import org.example.anomaly.data.Point;
import org.example.anomaly.util.EdgeMeanVariance;

public class MASHolder_fourclass 
{
	String mapMatchedTrajDirPath = "";
	String learnedEdgeDistroFilePath = "";
	String dest = "";
	
	double PIx = 3.141592653589793;
	double RADIUS = 6378.16;
	
	PrintWriter outValues;
	
	ArrayList<EdgeProcessing> ep = new ArrayList<EdgeProcessing>();
	
	boolean modify_speed;
	Hashtable<String, Double> new_speeds;
	
	public static final Hashtable<String, Integer> sizes = new Hashtable<String, Integer>();

	public MASHolder_fourclass () 
	{
		this.sizes.put("bike", 649);
		this.sizes.put("bus", 512);
		this.sizes.put("car", 569);
		this.sizes.put("walk", 1262);
	}

	public MASHolder_fourclass (ArrayList<String> trains) 
	{
		
		mapMatchedTrajDirPath = dest;
		this.sizes.put("bike", 649);
		this.sizes.put("bus", 512);
		this.sizes.put("car", 569);
		this.sizes.put("walk", 1262);
		
		//train data : testing on part 5
		
		for(int j = 0; j < trains.size(); j++)
		{
			for(int i =  1 ; i <=5 ; i++)
			{
				try
				{
					new_speeds = new Hashtable<String, Double>();
					{
						for(int k = 0; k < trains.size(); k++)
						{
							learnedEdgeDistroFilePath = dest + "out_" + trains.get(k) + "_" + "train_" + i + ".csv"; 
							ep.set(k, new EdgeProcessing(learnedEdgeDistroFilePath, 5.0, 3.0)) ;
						}
						outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "percent" +"_" + trains.get(j) + "_" + i + ".csv"), "UTF-8"));
						String val = "size";
						for(int x = 0; x< trains.size(); x++)
						{
							val = val + "," + "out_" + trains.get(x) + "_" + "train_" + i + ".csv" ;
						}
						for(int x = 0; x< trains.size(); x++)
						{
							val = val + "," + "anom edges " + trains.get(x) ;
						}
						for(int x = 0; x< trains.size(); x++)
						{
							val = val + "," + "missing edges " + trains.get(x) ;
						}
						outValues.println(val);
						
						for(int x = 0; x < trains.size(); x++)
						{
							System.out.println("Train " + x  + " : " + "out_" + trains.get(x) + "_" + "train_" + i + ".csv");
						}
						
						System.out.println("Test : " + trains.get(j) + "_" + "test_" + i + ".txt");
						modify_speed = true;
						if(trains.get(j).equals("walk"))
						{
							this.modify_speed = true;
							System.out.println(trains.get(j));
						}
						else
							this.modify_speed = false;
						compare_handler(trains.get(j) + "_" + "test_" + i + ".txt");
						outValues.close();
					}
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		}
	}

	public void compare_handler(String testfile) throws Exception 
	{
		int line_no = 0;
		int total_count = 0;
		{
			line_no = 0;
			System.out.println("File name : " + testfile);
			BufferedReader fileReader = new BufferedReader(new FileReader( mapMatchedTrajDirPath + testfile));
			int prev_id = 0;
			double prev_lat = 0.0;
			double prev_lon = 0.0;
			long prev_val = 0;
			Date prev_time = new Date();
			while (true) 
			{
				ArrayList<Point> arr = new ArrayList<Point>();
				String line = fileReader.readLine();
				if (line == null)
					break;
				line_no++;
				prev_id = -1;
				prev_lat = 0.0;
				prev_lon = 0.0;
				prev_time = new Date();
				double speed = 0.0;
				if (line.contains("[")) 
				{
					String[] warr = line.split("\\]");
					for (String s : warr) 
					{
						try 
						{
							int id = Integer.parseInt(s.split(",")[0]
									.split("\\[")[1]);
							double lat = Double.parseDouble(s.split(",")[1]);
							double lon = Double.parseDouble(s.split(",")[2]);
							long val = Long.parseLong(s.split(",")[3]
									.replaceAll("\\s", ""));
							Date date = new Date(val);
							SimpleDateFormat df2 = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							String dateText = df2.format(date);
							Date time = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss").parse(dateText);

							arr.add(new Point(id, lat, lon, time, val));
							if(prev_id!=-1 && modify_speed == true)
							{
								speed =  Math.abs(distance(lat, lon,prev_lat, prev_lon)) * 1000 * 3600 / (Math.abs(prev_val-val));
								if(speed > 10.0)
								{
									new_speeds.put(id + "_" + prev_id, Math.abs(distance(lat, lon,prev_lat, prev_lon)) * 1000 * 3600 );
									new_speeds.put(prev_id + "_" + id, Math.abs(distance(lat, lon,prev_lat, prev_lon)) * 1000 * 3600 );
								}
							}
							prev_time = time;
							prev_lat = lat;
							prev_lon = lon;
							prev_id = id;
							prev_val = val;
						} 
						catch (Exception e) 
						{
							System.out.println(e);
						}
					}
					total_count++;
					outValues.print(warr.length - 1);
					compare(arr, line_no, warr.length - 1);
				}
			}
			fileReader.close();
		}
	}

	public void test(ArrayList<String> trains)
	{
		try
		{
			BufferedReader train_file = new BufferedReader(new FileReader(dest + trains.get(0) + "_" + "train_1.txt"));
			BufferedReader train2_file = new BufferedReader(new FileReader(dest + trains.get(0) + "_" + "test_1.txt"));	
			ArrayList<String> walk = new ArrayList<String>();
			ArrayList<String> bike = new ArrayList<String>();
			String line = new String();
			int count = 0;
			while(true)
			{
				line = train_file.readLine();
				if(line == null)
					break;
				if(line.contains("["))
					walk.add(line);
				count++;
			}
			System.out.println(count);
			count = 0;
			while(true)
			{
				line = train2_file.readLine();
				if(line == null)
					break;
				if(walk.contains(line))
					System.out.println("FUCKED");
				if(line.contains("["))
					bike.add(line);
				count++;
			}
			System.out.println(count);
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
		
	}
	public void compare(ArrayList<Point> pt, int line_no, int size) throws Exception 
	{
		List<Integer> nodeSequence = new ArrayList<Integer>();
		List<Double> travelTime = new ArrayList<Double>();
		for (int i = 0; i < pt.size(); i++) 
		{
			nodeSequence.add(pt.get(i).id);
			if (i < (pt.size() - 1)) 
			{
				double time = pt.get(i + 1).mytime - pt.get(i).mytime;
				if(modify_speed == true && (new_speeds.get(pt.get(i+1).id + "_" + pt.get(i).id)!=null))
				{
					time = new_speeds.get(pt.get(i+1).id + "_" + pt.get(i).id) / 10;
				}
				travelTime.add(time);
			}
		}
		
		ArrayList<ArrayList<Integer>> not_in_training = new ArrayList<ArrayList<Integer>>();
		ArrayList<ArrayList<Integer>> anoms = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> count = new ArrayList<Integer>();
		ArrayList<ArrayList<String>> our_result = new ArrayList<ArrayList<String>>();
		
		for(int x = 0; x < ep.size(); x++)
		{
			ep.get(x).setNodeTime(nodeSequence, travelTime);
			
			not_in_training.add(new ArrayList<Integer>());
			ep.get(x).notInTraining(not_in_training.get(x));
			
			anoms.add(new ArrayList<Integer>());
			count.add(ep.get(x).howManyAno(anoms.get(x)));
			our_result.add(new ArrayList<String>());
			
		}
		try 
		{
			for(int x = 0; x< ep.size(); x++)
			{
				our_result.set(x, ep.get(x).findAnomalyOur());
			}
		} 
		catch (Exception e) 
		{
			System.out.println("Not there in training : our");
			e.printStackTrace();
			return;
		}
		
		ArrayList<ArrayList<start_end_anom>> sub_trajcs = new ArrayList<ArrayList<start_end_anom>>();
		ArrayList<Integer> total_anom_count = new ArrayList<Integer>();
		ArrayList<ArrayList<Integer>> visited = new ArrayList<ArrayList<Integer>>();
		
		for(int x = 0; x < our_result.size(); x++)
		{
			sub_trajcs.add(new ArrayList<start_end_anom>());
			visited.add(new ArrayList<Integer>());
			total_anom_count.add(0);
			for (int i = 0; i < size; i++)
				visited.get(x).add(0);
			
			for (int y = 0; y < our_result.get(x).size(); y++) 
			{
				start_end_anom obj = new start_end_anom();
				String s = our_result.get(x).get(y);
				StringTokenizer stt = new StringTokenizer(s, ",");
				obj.start = Integer.parseInt(stt.nextToken().toString());
				obj.end = Integer.parseInt(stt.nextToken().toString());
				for (int z = obj.start; z <= obj.end; z++) 
				{
					if (visited.get(x).get(z) == 0) // unvisited
					{
						total_anom_count.set(x, total_anom_count.get(x) + 1);
						visited.get(x).set(z, 1);
					}
				}

				sub_trajcs.get(x).add(obj);
			}
			
			for (int z = 0; z < not_in_training.get(x).size(); z++) 
			{
				for (int y = 0; y < sub_trajcs.get(x).size(); y++) 
				{
					if (sub_trajcs.get(x).get(y).start <= not_in_training.get(x).get(z)
							&& sub_trajcs.get(x).get(y).end >= not_in_training.get(x).get(z)) 
					{
						total_anom_count.set(x, total_anom_count.get(x) - 1);
						break;
					}
				}
			}

			int trajSize = size - not_in_training.get(x).size();
			if (size > 0)
				outValues.print( "," + (double) (total_anom_count.get(x) + not_in_training.get(x).size()));
			else
				outValues.print( "," + "Size zero!!");
			
		}
		for(int x = 0; x < count.size(); x++)
		{
			outValues.print( "," + count.get(x));
		}
		for(int x = 0; x < count.size(); x++)
		{
			outValues.print( "," + not_in_training.get(x).size());
		}
		outValues.println();
	}

	public void fscore(ArrayList<String> trains)
	{
		Hashtable<String, ArrayList<Double>> all_fscore = new Hashtable<String, ArrayList<Double>>();
		for(int x = 0 ; x< trains.size(); x++)
		{
			all_fscore.put(trains.get(x), new ArrayList<Double>());
		}
		for(int i = 1; i <= 5 ; i++ )
		{
			try
			{
				Hashtable<String, Integer> tp = new Hashtable<String, Integer>();
				Hashtable<String, Integer> fp = new Hashtable<String, Integer>();
				Hashtable<String, Integer> fn = new Hashtable<String, Integer>();
				Hashtable<String, Double> p = new Hashtable<String, Double>();
				Hashtable<String, Double> r = new Hashtable<String, Double>();
				Hashtable<String, Double> f1 = new Hashtable<String, Double>();
				
				for(int x = 0 ; x< trains.size(); x++)
				{
					tp.put(trains.get(x), 0);
					fp.put(trains.get(x), 0);
					fn.put(trains.get(x), 0);
					
				}
				for(int j = 0; j < trains.size(); j++)
				{
					String me = trains.get(j);
					BufferedReader fileReader1 = new BufferedReader(new FileReader( dest + "percent" + "_" + me + "_" + i + ".csv" ));
					String line = new String();
					while(true)
					{
						line = fileReader1.readLine();
						if(!line.equals(""))
							break;
					}
					
					int my_class = j;
					while(true)
					{
						line = fileReader1.readLine();
						if(line == null)
							break;
						if(line.contains("zero"))
							continue;
						StringTokenizer stt = new StringTokenizer(line, ",");
						int size = Integer.parseInt(stt.nextToken().toString());
						ArrayList<Double> val = new ArrayList<Double>();
						double min = 999;
						int min_class = 0;
						for(int x = 0 ;x < trains.size(); x++)
						{
							double temp = Double.parseDouble(stt.nextToken().toString()); 
							val.add(temp);
							if(temp < min)
							{
								min = temp;
								min_class = x;
							}
						
						}
						if(my_class != min_class)
						{
							//false negative
							fn.put(trains.get(my_class), fn.get(trains.get(my_class))+1);
							fp.put(trains.get(min_class), fp.get(trains.get(min_class))+1);
						}
						else
						{
							//correct classification
							tp.put(trains.get(my_class), tp.get(trains.get(my_class)) +1);
						}
					}
				}
				for(int x = 0 ; x < trains.size(); x++)
				{
					p.put(trains.get(x), ((double)(tp.get(trains.get(x)))/(tp.get(trains.get(x)) + fp.get(trains.get(x)))));
					r.put(trains.get(x), ((double)(tp.get(trains.get(x)))/(tp.get(trains.get(x)) + fn.get(trains.get(x)))));
					f1.put(trains.get(x), ((double)(2*p.get(trains.get(x))*r.get(trains.get(x)))/(r.get(trains.get(x)) + p.get(trains.get(x)))));
					all_fscore.get(trains.get(x)).add(f1.get(trains.get(x)));
					System.out.println(trains.get(x) + i + "  " + p.get(trains.get(x)) + "  " 
							+ r.get(trains.get(x)) + "  " + f1.get(trains.get(x))) ;
				}
			}
			catch (Exception e)
			{
				System.out.println(e);
			}
		}
		double avg = 0.0;
		for(int x = 0 ; x < trains.size(); x++)
		{
			for(int i = 0;i < all_fscore.get(trains.get(x)).size(); i++)
			{
				avg += all_fscore.get(trains.get(x)).get(i);
			}
			System.out.println("AVERAGE : " + trains.get(x) + "  " + avg/all_fscore.get(trains.get(x)).size());
			avg = 0.0;
		}
			System.out.println("**********************************************");
	}
	
	public void generate_parts(ArrayList<String> trains)
	{
		System.out.println(dest);
		try
		{
			ArrayList<BufferedReader> trains_file = new ArrayList<BufferedReader>();
			for(int x = 0 ;x < trains.size(); x++)
			{
				trains_file.add(new BufferedReader(new FileReader(dest + trains.get(x) + ".txt")));
			}
			
			ArrayList<ArrayList<PrintWriter>> outs = new ArrayList<ArrayList<PrintWriter>>();
			for(int x = 0; x < trains.size(); x++)
			{
				outs.add(new ArrayList<PrintWriter>());
				for(int y = 0; y < 10; y++)
				{
					String out_name = dest + trains.get(x) + "_";
					if(y < 5)
					{
						out_name += "train_" + (y+1) + ".txt";
					}
					else
						out_name += "test_" + (y+1 - 5) + ".txt";
					System.out.println(out_name);
					outs.get(x).add( new PrintWriter(new OutputStreamWriter(new FileOutputStream( out_name ), "UTF-8")));
				}
			}
			int min = 9999;
			int min_train = 0;
			for(int x = 0; x< trains.size(); x++)
			{
				if(sizes.get(trains.get(x)) < min)
				{
					min = sizes.get(trains.get(x));
					min_train = x;
				}
			}
			
			
			for(int x = 0; x < trains.size(); x++)
			{
				String line = new String();
				int count = 0;
				if(x == min_train)
				{
					while(true)
					{
						line = trains_file.get(x).readLine();
						if(line == null)
							break;
						if(!line.contains("["))
							continue;
						int test;
						if(0 <= count && count < (min/5))
							test = 5;
						else if((min/5) <= count && count < ((2*min)/5))
							test = 6;
						else if(((2*min)/5) <= count && count < ((3*min)/5))
							test = 7;
						else if(((3*min)/5) <= count && count < ((4*min)/5))
							test = 8;
						else
							test = 9;
						outs.get(x).get(test).println(line);
						for(int y = 0 ; y < 5 ; y++ )
						{
							if((test-5) != y)
							{
								outs.get(x).get(y).println(line);
							}
						}
						count++;
					}
				}
				else
				{
					count = 0;
					while(true)
					{
						line = trains_file.get(x).readLine();
						if(line == null)
							break;
						if(!line.contains("["))
						{
							continue;
						}
						int test;
						if(0 <= count && count < (min/5))
							test = 5;
						else if((min/5) <= count && count < ((2*min)/5))
							test = 6;
						else if(((2*min)/5) <= count && count < ((3*min)/5))
							test = 7;
						else if(((3*min)/5) <= count && count < ((4*min)/5))
							test = 8;
						else if (((4*min)/5) <= count && count < min)
							test = 9;
						else 
							test = 100;
						if(test!=100)
							outs.get(x).get(test).println(line);
						for(int y = 0 ; y < 5 ; y++ )
						{
							if((test-5) != y)
							{
								outs.get(x).get(y).println(line);
							}
						}
						count++;
					}	
				}
			}
			for(int x = 0; x < outs.size(); x++)
			{
				for(int y = 0; y < outs.get(x).size(); y++)
					outs.get(x).get(y).close();
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}

	
	public void print_size(ArrayList<String> trains)
	{
		File docDir = new File(dest);
		String[] files = docDir.list();
		
		int count = 0;
		for(int i = 0; i< files.length; i++)
		{
			count = 0;
			try
			{
				BufferedReader fileReader = new BufferedReader(new FileReader(dest + files[i]));
				String line = "";
				while(true)
				{
					line = fileReader.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
						continue;
					count++;
				}
				System.out.println(files[i] + "  " + count);
			}
			catch (Exception e)
			{
				System.out.println(e);
			}
		}
	}
	public static void main(String[] args) throws Exception 
	{
		MASHolder_fourclass ms = new MASHolder_fourclass();
		ArrayList<String> trains = new ArrayList<String>();
		trains.add("bike");
		trains.add("bus");
		trains.add("car");
		trains.add("walk");
		ms.dest = "src/inputs/fourclass/";
		for(int i = 0; i< trains.size(); i++)
		{
			if(i != trains.size()-1)
				ms.dest = ms.dest + trains.get(i) + "_";
			else
				ms.dest = ms.dest + trains.get(i) + "/";
			ms.ep.add(new EdgeProcessing());
		}
		//ms.learn(trains);
		//ms.generate_parts(trains);
		//ms.print_size(trains);
		//ms.test(trains);
		ms.fscore(trains);
		//ms.learn(trains);
		//MASHolder_fourclass ms2 = new MASHolder_fourclass(trains);
		//ms2.generate_parts(trains);
		//ms2.print_size(trains);
	}
	public void learn(ArrayList<String> trains) throws Exception
	{
		String dest = "src/inputs/fourclass/";
		for(int i = 0; i< trains.size(); i++)
		{
			if(i != trains.size()-1)
				dest = dest + trains.get(i) + "_";
			else
				dest = dest + trains.get(i) + "/";
		}
		for(int x = 0 ;x < trains.size(); x++)
		{
			for(int i = 1; i <=5 ; i++)
			{
				EdgeMeanVariance em = new EdgeMeanVariance();
				if(trains.get(x).equals("walk"))
					em.modify = true;
				em.findMeanVarAdulterated(dest, trains.get(x) + "_train_" + i + ".txt");
			}
		}
		
	}
	public double Radians(double x) 
	{
		return x * PIx / 180;
	}

	public double distance(double lat1, double lon1, double lat2, double lon2) 
	{
		double dlon = Radians(lon2 - lon1);
		double dlat = Radians(lat2 - lat1);

		double a = (Math.sin(dlat / 2) * Math.sin(dlat / 2))
				+ Math.cos(Radians(lat1)) * Math.cos(Radians(lat2))
				* (Math.sin(dlon / 2) * Math.sin(dlon / 2));
		double angle = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return angle * RADIUS;

	}
}