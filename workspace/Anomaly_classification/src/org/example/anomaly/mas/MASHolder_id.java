package org.example.anomaly.mas;

import geolife_processing.start_end_anom;

import java.beans.FeatureDescriptor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.example.anomaly.data.Point;
import org.example.anomaly.util.EdgeMeanVariance;

public class MASHolder_id 
{
	
	EdgeProcessing ep1;
	String mapMatchedTrajDirPath  = "";
	String learnedEdgeDistroFilePath = "";
	String curr_id = "";
	String train = "";
	
	Hashtable<String, ArrayList<Double>> id_anom = new Hashtable<String, ArrayList<Double>>();
	
	public static final Hashtable<String, Integer> sizes = new Hashtable<String, Integer>();

	public MASHolder_id()
	{
		
	}
	
	public MASHolder_id(String run) throws Exception 
	{
		train = run;
		mapMatchedTrajDirPath = "src/inputs/id/";
		this.sizes.put("bike", 649);
		this.sizes.put("bus", 512);
		this.sizes.put("car", 569);
		this.sizes.put("walk", 1262);
		learnedEdgeDistroFilePath = "src/inputs/id/out_" + train + ".csv";
		ep1 = new EdgeProcessing(learnedEdgeDistroFilePath, 5.0, 3.0);
		compare_handler(train + ".txt");
	}

	public void compare_handler(String testfile) throws Exception 
	{
		int line_no = 0;
		int total_count = 0;
		{
			line_no = 0;
			System.out.println("File name : " + testfile);
			BufferedReader fileReader = new BufferedReader(new FileReader( mapMatchedTrajDirPath + testfile));
			while (true) 
			{
				ArrayList<Point> arr = new ArrayList<Point>();
				String line = fileReader.readLine();
				if (line == null)
					break;
				line_no++;
				if(!line.contains("["))
				{
					if(!line.contains(".plt"))
					{
						curr_id = line;
					}
					continue;
				}
				else 
				{
					String[] warr = line.split("\\]");
					for (String s : warr) 
					{
						try 
						{
							int id = Integer.parseInt(s.split(",")[0]
									.split("\\[")[1]);
							double lat = Double.parseDouble(s.split(",")[1]);
							double lon = Double.parseDouble(s.split(",")[2]);
							long val = Long.parseLong(s.split(",")[3]
									.replaceAll("\\s", ""));
							Date date = new Date(val);
							SimpleDateFormat df2 = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							String dateText = df2.format(date);
							Date time = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss").parse(dateText);

							arr.add(new Point(id, lat, lon, time, val));
						} 
						catch (Exception e) 
						{
							System.out.println(e);
						}
					}
					total_count++;
					compare(arr, line_no, warr.length - 1);
				}
			}
			fileReader.close();
		}
	}

	public void compare(ArrayList<Point> pt, int line_no, int size) throws Exception 
	{
		long tot1 = 0;
		long tot2 = 0;
		long tot3 = 0;
		List<Integer> nodeSequence = new ArrayList<Integer>();
		List<Double> travelTime = new ArrayList<Double>();
		for (int i = 0; i < pt.size(); i++) 
		{
			nodeSequence.add(pt.get(i).id);
			if (i < (pt.size() - 1)) 
			{
				double time = pt.get(i + 1).mytime - pt.get(i).mytime;
				travelTime.add(time);
			}
		}
		ep1.setNodeTime(nodeSequence, travelTime);
		
		ArrayList<Integer> not_in_training1 = new ArrayList<Integer>();
		ep1.notInTraining(not_in_training1); 
		
		ArrayList<Integer> anoms1 = new ArrayList<Integer>();
		int count1 = ep1.howManyAno(anoms1);
		
		
		ArrayList<String> our_result1 = new ArrayList<String>();
		try 
		{
			our_result1 = ep1.findAnomalyOur();
		} 
		catch (Exception e) 
		{
			System.out.println("Not there in training : our");
			e.printStackTrace();
			return;
		}
		ArrayList<start_end_anom> sub_trajcs1 = new ArrayList<start_end_anom>();
		int total_anom_count1 = 0;
		ArrayList<Integer> visited1 = new ArrayList<Integer>();
		for (int i = 0; i < size; i++)
			visited1.add(0);
		for (int x = 0; x < our_result1.size(); x++) {
			start_end_anom obj = new start_end_anom();
			String s = our_result1.get(x);
			StringTokenizer stt = new StringTokenizer(s, ",");
			obj.start = Integer.parseInt(stt.nextToken().toString());
			obj.end = Integer.parseInt(stt.nextToken().toString());
			for (int z = obj.start; z <= obj.end; z++) 
			{
				if (visited1.get(z) == 0) // unvisited
				{
					total_anom_count1++;
					visited1.set(z, 1);
				}
			}

			sub_trajcs1.add(obj);
		}
		
		for (int x = 0; x < not_in_training1.size(); x++) 
		{
			for (int y = 0; y < sub_trajcs1.size(); y++) 
			{
				if (sub_trajcs1.get(y).start <= not_in_training1.get(x)
						&& sub_trajcs1.get(y).end >= not_in_training1.get(x)) {
					total_anom_count1 = total_anom_count1 - 1;
					break;
				}
			}
		}

		int trajSize1 = size - not_in_training1.size();
		if(id_anom.get(curr_id)==null)
		{
			ArrayList<Double> values = new ArrayList<Double>();
			if(size > 0)
				values.add((double) (total_anom_count1 + not_in_training1.size()) / (size));
			id_anom.put(curr_id, values);
		}
		else 
		{
			ArrayList<Double> values = new ArrayList<Double>();
			values.addAll(id_anom.get(curr_id));
			if(size > 0)
				values.add((double) (total_anom_count1 + not_in_training1.size()) / (size));
			id_anom.put(curr_id, values);
		}
	}

	public static void main(String[] args) throws Exception 
	{
		MASHolder_id ms = new MASHolder_id("car");
		ms.train = "car";
		//ms.learn();
		ms.analyse();
		//ms.learn("car");
		//System.out.println(ms.id_anom);
	}

	public void learn() throws Exception
	{
		String dirLocation = "src/inputs/id/";
		EdgeMeanVariance em = new EdgeMeanVariance();
		em.findMeanVarAdulterated(dirLocation, train+".txt");
	}
	public void analyse() throws UnsupportedEncodingException, FileNotFoundException
	{
		Hashtable<String, Double> avg = new Hashtable<String, Double>();
		Iterator it = id_anom.entrySet().iterator();
		double max = 0.0;
		double min = 9999;
		int max_id = 0;
		int min_id = 0;
		String dirLocation = "src/inputs/id/";
		PrintWriter outValues = new PrintWriter(new OutputStreamWriter(
				new FileOutputStream(dirLocation + "analyse_" + train + ".csv"), "UTF-8"));
	    while (it.hasNext()) 
	    {
	    	ArrayList<Double> values = new ArrayList<Double>();
	    	Map.Entry pairs = (Map.Entry)it.next();
	    	values = id_anom.get(pairs.getKey().toString());
	    	double val = 0.0;
	    	for(int i = 0; i < values.size(); i++)
	    	{
	    		val = val+ values.get(i);
	    	}
	    	val = (double)(val / values.size());
	    	outValues.println(pairs.getKey().toString() + "," + val + "," + values.size());
	    	avg.put(pairs.getKey().toString(), val);
	    	if(val > max)
	    	{
	    		max = val;
	    		max_id = Integer.parseInt(pairs.getKey().toString());
	    	}
	    	if(val < min)
	    	{
	    		min = val;
	    		min_id = Integer.parseInt(pairs.getKey().toString());
	    	}
	    }
	    outValues.close();
	    System.out.println("**********************************");
	    System.out.println("Min anom : " + min_id + " value : " + min);
	    System.out.println("Min anom : " + max_id + " value : " + max);
	}

}