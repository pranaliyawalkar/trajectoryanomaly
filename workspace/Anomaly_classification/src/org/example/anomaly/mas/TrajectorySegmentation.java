package org.example.anomaly.mas;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

import org.example.anomaly.data.Point;


//THIS CLASS IS FOR DATA GENERATION. CHECK car_walk_mixed.txt for our data.
public class TrajectorySegmentation 
{
	public static void main(String[] args) throws Exception
	{
		TrajectorySegmentation ts = new TrajectorySegmentation();
		//ts.generate_nodes();
		//ts.process();
		//ts.regenerate_train_data();
		ts.count_lines();
	}
	
	public void generate_nodes() throws Exception
	{
		HashMap<Integer, ArrayList<Integer>> car_nodes = new HashMap<Integer, ArrayList<Integer>>();
		HashMap<Integer, ArrayList<Integer>> walk_nodes = new HashMap<Integer, ArrayList<Integer>>();
		BufferedReader fileReader = new BufferedReader(new FileReader("src/inputs/car.txt"));
		int traj_no = 0;
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			if(!line.contains("["))
				continue;
			
			String[] warr = line.split("\\]");
			ArrayList<Integer> ids = new ArrayList<Integer>();
			for (String s : warr) 
			{
				int id = Integer.parseInt(s.split(",")[0]
						.split("\\[")[1]);
				double lat = Double.parseDouble(s.split(",")[1]);
				double lon = Double.parseDouble(s.split(",")[2]);
				long val = Long.parseLong(s.split(",")[3].replaceAll("\\s", ""));
				ids.add(id);
			}
			//adding middle ids/4 nodes
			int size = ids.size();
			for(int i = (4*size/8); i <= (4*size/8); i++)
			{
				if(car_nodes.get(ids.get(i))==null)
					car_nodes.put(ids.get(i), new ArrayList<Integer>());
				car_nodes.get(ids.get(i)).add(traj_no);
			}
			traj_no++;
		}
		fileReader.close();
		fileReader = new BufferedReader(new FileReader("src/inputs/walk.txt"));
		
		traj_no = 0;
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			if(!line.contains("["))
				continue;
			String[] warr = line.split("\\]");
			ArrayList<Integer> ids = new ArrayList<Integer>();
			for (String s : warr) 
			{
				int id = Integer.parseInt(s.split(",")[0]
						.split("\\[")[1]);
				double lat = Double.parseDouble(s.split(",")[1]);
				double lon = Double.parseDouble(s.split(",")[2]);
				long val = Long.parseLong(s.split(",")[3].replaceAll("\\s", ""));
				ids.add(id);
			}
			//adding middle ids/4 nodes
			int size = ids.size();
			for(int i = (4*size/8); i <= (4*size/8); i++)
			{
				if(walk_nodes.get(ids.get(i))==null)
					walk_nodes.put(ids.get(i), new ArrayList<Integer>());
				walk_nodes.get(ids.get(i)).add(traj_no);
			}
			traj_no++;
		}
		fileReader.close();
		System.out.println("Done reading");
		System.out.println(car_nodes.size() + "  " + walk_nodes.size());
		System.out.println("*************************************");
		int count = 0;
		Set<Integer> car_keyset = car_nodes.keySet();
		Set<Integer> walk_keyset = walk_nodes.keySet();
		
		ArrayList<Integer> car_ids = new ArrayList<Integer>();
		ArrayList<Integer> walk_ids = new ArrayList<Integer>();
		PrintWriter mapping = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/inputs/mapping.txt"), "UTF-8"));
		
		for(int t : car_keyset)
		{
			if(walk_keyset.contains(t))
			{
				//System.out.println(car_nodes.get(t) + "  " + walk_nodes.get(t));
				mapping.print(t + " $ ");
				car_ids.addAll(car_nodes.get(t));
				walk_ids.addAll(walk_nodes.get(t));
				
				for(int i = 0 ; i <  car_nodes.get(t).size(); i++)
					mapping.print(car_nodes.get(t).get(i) + " ");
				
				mapping.print("$ ");
				
				for(int i = 0 ; i <  walk_nodes.get(t).size(); i++)
					mapping.print(walk_nodes.get(t).get(i) + " ");
				
				mapping.print("\n");
				count++;
			}
		}
		System.out.println(count);
		
		
		ArrayList<String> concerned_cars = new ArrayList<String>();
		ArrayList<String> concerned_walk = new ArrayList<String>();
		PrintWriter car_out = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/inputs/concerned_car.txt"), "UTF-8"));
		PrintWriter walk_out = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/inputs/concerned_walk.txt"), "UTF-8"));
		
		
		traj_no = 0;
		fileReader = new BufferedReader(new FileReader("src/inputs/car.txt"));
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			if(!line.contains("["))
				continue;
			if(car_ids.contains(traj_no))
			{
				concerned_cars.add(line);
				car_out.println(traj_no);
				car_out.println(line);
			}
			traj_no++;
		}
		fileReader.close();
		
		traj_no = 0;
		fileReader = new BufferedReader(new FileReader("src/inputs/walk.txt"));
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			if(!line.contains("["))
				continue;
			if(walk_ids.contains(traj_no))
			{
				concerned_walk.add(line);
				walk_out.println(traj_no);
				walk_out.println(line);
			}
			traj_no++;
		}
		mapping.close();
		car_out.close();
		walk_out.close();
	}
	public void process() throws Exception
	{
		BufferedReader fileReader = new BufferedReader(new FileReader("src/inputs/mapping.txt"));
		HashMap<Integer, ArrayList<complementary_node>> complements = new HashMap<Integer, ArrayList<complementary_node>>();
		
		
		HashMap<Integer, String> concerned_car = new HashMap<Integer, String>();
		HashMap<Integer, String> concerned_walk = new HashMap<Integer, String>();
		
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			StringTokenizer stt = new StringTokenizer(line , "$");
			int node = Integer.parseInt(stt.nextToken().toString().replaceAll("\\s", ""));
			ArrayList <Integer> car_ids = new ArrayList<Integer>();
			
			StringTokenizer stt2 = new StringTokenizer(stt.nextToken().toString(), " ");
			while(stt2.hasMoreTokens())
			{
				car_ids.add(Integer.parseInt(stt2.nextToken().toString().replaceAll("\\s", "")));
			}
			
			ArrayList <Integer> walk_ids = new ArrayList<Integer>();
			
			stt2 = new StringTokenizer(stt.nextToken().toString(), " ");
			while(stt2.hasMoreTokens())
			{
				walk_ids.add(Integer.parseInt(stt2.nextToken().toString().replaceAll("\\s", "")));
			}
			
			for(int i = 0; i < car_ids.size(); i++)
			{
				for(int j = 0; j < walk_ids.size(); j++)
				{
					complementary_node cn1 = new complementary_node();
					cn1.complement_id = walk_ids.get(j);
					cn1.complement_node = node;
					if(complements.get(car_ids.get(i))==null)
						complements.put(car_ids.get(i), new ArrayList<complementary_node>());
					complements.get(car_ids.get(i)).add(cn1);
					
				}
			}
		}
		fileReader = new BufferedReader(new FileReader("src/inputs/concerned_car.txt"));
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			int traj_no = Integer.parseInt(line);
			line = fileReader.readLine();
			concerned_car.put(traj_no, line);
		}
		
		fileReader = new BufferedReader(new FileReader("src/inputs/concerned_walk.txt"));
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			int traj_no = Integer.parseInt(line);
			line = fileReader.readLine();
			concerned_walk.put(traj_no, line);
		}
		PrintWriter mixed = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/inputs/car_walk_mixed.txt"), "UTF-8"));
		
		Iterator it = concerned_car.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry pairs = (Map.Entry)it.next();
			int traj_no = Integer.parseInt(pairs.getKey().toString());
			ArrayList<complementary_node> complement = complements.get(traj_no);
			for(int i = 0; i < complement.size(); i++)
			{
				complementary_node cn = complement.get(i);
				String car = concerned_car.get(traj_no);
				int node = cn.complement_node;
				String walk = concerned_walk.get(cn.complement_id);
				
				ArrayList<Point> c = new ArrayList<Point>();
				ArrayList<Point> c1 = new ArrayList<Point>();
				ArrayList<Point> c2 = new ArrayList<Point>();
				ArrayList<Point> w = new ArrayList<Point>();
				ArrayList<Point> w1 = new ArrayList<Point>();
				ArrayList<Point> w2 = new ArrayList<Point>();
				
				String[] car_split = car.split("\\]");
				String[] walk_split = walk.split("\\]");
				
				int flag = 0;
				
				for (String s : car_split) 
				{
					int id = Integer.parseInt(s.split(",")[0]
							.split("\\[")[1]);
					double lat = Double.parseDouble(s.split(",")[1]);
					double lon = Double.parseDouble(s.split(",")[2]);
					long val = Long.parseLong(s.split(",")[3]
							.replaceAll("\\s", ""));
					Date date = new Date(val);
					SimpleDateFormat df2 = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					String dateText = df2.format(date);
					Date time = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss").parse(dateText);
					
					
					c.add(new Point(id, lat, lon, time, val));
				}
				
				c1.addAll(c.subList(0, c.size()/2 + 1));
				c2.addAll(c.subList(c.size()/2, c.size() ));
				
				flag = 0;
				
				for (String s : walk_split) 
				{
					int id = Integer.parseInt(s.split(",")[0]
							.split("\\[")[1]);
					double lat = Double.parseDouble(s.split(",")[1]);
					double lon = Double.parseDouble(s.split(",")[2]);
					long val = Long.parseLong(s.split(",")[3]
							.replaceAll("\\s", ""));
					Date date = new Date(val);
					SimpleDateFormat df2 = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					String dateText = df2.format(date);
					Date time = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss").parse(dateText);
					
					
					w.add(new Point(id, lat, lon, time, val));
				}
				
				w1.addAll(w.subList(0, w.size()/2 + 1));
				w2.addAll(w.subList(w.size()/2, w.size() ));
				
				/*System.out.println("C : " + c.get(0).id + " " + c.get((c.size()/2)-1).id + "  " + 
						c.get((c.size()/2)).id + "  " + c.get((c.size()/2)+1).id + "  " + c.get(c.size()-1).id);
				
				
				System.out.println("C1 : " + c1.get(0).id + "  " + c1.get(c1.size()-1).id);
				System.out.println("C2 : " + c2.get(0).id + "  " + c2.get(c2.size()-1).id);
				
				System.out.println("W : " + w.get(0).id + " " + w.get((w.size()/2)-1).id + "  " + 
						w.get((w.size()/2)).id + "  " + w.get((w.size()/2)+1).id + "  " + w.get(w.size()-1).id);
				
				System.out.println("W1 : " + w1.get(0).id + "  " + w1.get(w1.size()-1).id);
				System.out.println("W2 : " + w2.get(0).id + "  " + w2.get(w2.size()-1).id);
				
				System.out.println("**********************");*/
				
				mixed.println(traj_no + " " + cn.complement_id + " " + node + " c1:" + c1.size() + " w2:" + w2.size());
				for(int x = 0; x < c1.size(); x++)
				{
					Point p = c1.get(x);
					mixed.print("[" + p.id + "," + p.lat + "," + p.lon + "," + p.mytime + "]");
				}
				for(int x = 0; x < w2.size(); x++)
				{
					Point p = w2.get(x);
					mixed.print("[" + p.id + "," + p.lat + "," + p.lon + "," + p.mytime + "]");
				}
				mixed.println();
				mixed.println(traj_no + " " + cn.complement_id + " " + node + " w1:" + w1.size() + " c2:" + c2.size());
				for(int x = 0; x < w1.size(); x++)
				{
					Point p = w1.get(x);
					mixed.print("[" + p.id + "," + p.lat + "," + p.lon + "," + p.mytime + "]");
				}
				for(int x = 0; x < c2.size(); x++)
				{
					Point p = c2.get(x);
					mixed.print("[" + p.id + "," + p.lat + "," + p.lon + "," + p.mytime + "]");
				}
				mixed.println();
			}
		}
		
		mixed.close();
		
	}
	public void regenerate_train_data() throws Exception
	{
		BufferedReader fileReader = new BufferedReader(new FileReader("src/inputs/concerned_car.txt"));
		ArrayList<Integer> car_ids = new ArrayList<Integer>();
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			if(!line.contains("["))
				car_ids.add(Integer.parseInt(line));
		}
		fileReader.close();
		fileReader = new BufferedReader(new FileReader("src/inputs/concerned_walk.txt"));
		ArrayList<Integer> walk_ids = new ArrayList<Integer>();
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			if(!line.contains("["))
				walk_ids.add(Integer.parseInt(line));
		}
		fileReader.close();
		PrintWriter new_car = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/inputs/new_car.txt"), "UTF-8"));
		fileReader = new BufferedReader(new FileReader("src/inputs/car.txt"));
		int traj_no = 0;
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			if(!line.contains("["))
				continue;
			if(car_ids.contains(traj_no))
			{
				System.out.println(traj_no);
				traj_no++;
				continue;
			}
			else
			{
				new_car.println(line);
				traj_no++;
			}
			
		}
		new_car.close();
		
		PrintWriter new_walk = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/inputs/new_walk.txt"), "UTF-8"));
		fileReader = new BufferedReader(new FileReader("src/inputs/walk.txt"));
		traj_no = 0;
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			if(!line.contains("["))
				continue;
			if(walk_ids.contains(traj_no))
			{
				System.out.println(traj_no);
				traj_no++;
				continue;
			}
			else
			{
				new_walk.println(line);
				traj_no++;
			}
			
		}
		new_walk.close();
	}
	public void count_lines() throws Exception
	{
		String file = "src/inputs/car.txt";
		BufferedReader fileReader = new BufferedReader(new FileReader(file));
		int count = 0;
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			if(line.contains("["))
					count++;
		}
		System.out.println(file + "  " + count);
		
		/************/
		
		file = "src/inputs/new_car.txt";
		fileReader = new BufferedReader(new FileReader(file));
		count = 0;
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			if(line.contains("["))
					count++;
		}
		System.out.println(file + "  " + count);
		
		/************/
		
		file = "src/inputs/new_walk.txt";
		fileReader = new BufferedReader(new FileReader(file));
		count = 0;
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			if(line.contains("["))
					count++;
		}
		System.out.println(file + "  " + count);
		
		/************/
		
		file = "src/inputs/walk.txt";
		fileReader = new BufferedReader(new FileReader(file));
		count = 0;
		while(true)
		{
			String line = fileReader.readLine();
			if(line == null)
				break;
			if(line.contains("["))
					count++;
		}
		System.out.println(file + "  " + count);
	}
}