package org.example.anomaly.mas;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EdgeProcessing_maha {

	final HashMap<String, double[]> edgeTimeMap = new HashMap<String, double[]>();
	ArrayList<String> seedSet;
	List<Integer> nodeSequence;
	List<Double> travelTime;
	HashMap<String, Boolean> alive;
	HashMap<String, Boolean> newE;
	List<double[]> arrList;
	public BigInteger edgeProcessedWindow;
	public BigInteger edgeProcessedOur;
	public BigInteger edgeProcessedNaive;
	double threshold;

	// PrintWriter log;
	// String edgeProcLogFilePath = "data/outLog/epLog1234.txt";

	public EdgeProcessing_maha(String path, double threshold) {
		try {
			loadMeanVar(path);
			// log = new PrintWriter(new OutputStreamWriter(new
			// FileOutputStream(
			// edgeProcLogFilePath),
			// "UTF-8"));
			// edgeProcessedOur = new BigInteger("0");
			// edgeProcessedWindow = new BigInteger("0");
			this.threshold = threshold;

		} catch (Exception e) {
			System.out.println("Cannot read the mean variance properly");
			e.printStackTrace();
		}
	}

	public void setNodeTime(List<Integer> nodeSequence, List<Double> travelTime) {
		arrList = new ArrayList<double[]>();
		this.travelTime = travelTime;
		this.nodeSequence = nodeSequence;
		for (int i = 0; i < (nodeSequence.size() - 1); i++) {
			int source = nodeSequence.get(i);
			int dest = nodeSequence.get(i + 1);
			String key = source + " " + dest;
			if (!edgeTimeMap.containsKey(key))
				key = dest + " " + source;
			arrList.add(edgeTimeMap.get(key));
		}

	}

	public boolean isAno() {

		for (int i = 0; i < travelTime.size(); i++) {

			double[] arr = arrList.get(i);
			double mean = arr[0];
			double var = arr[1];
			double time = travelTime.get(i);
			double diff = Math.pow((mean - time), 2.0) / var;
			if (Math.sqrt(diff) >= threshold)
				return true;

		}
		return false;
	}

	public int howManyAno() {
		int count = 0;
		for (int i = 0; i < travelTime.size(); i++) {

			double[] arr = arrList.get(i);
			double mean = arr[0];
			double var = arr[1];
			double time = travelTime.get(i);
			double diff = Math.pow((mean - time), 2.0) / var;
			if (Math.sqrt(diff) >= threshold)
				count++;

		}
		return count;
	}

	public ArrayList<String> findAnomalyNaive() {

		edgeProcessedNaive = new BigInteger("0");
		ArrayList<String> ano = new ArrayList<String>();
		for (int i = 0; i < travelTime.size(); i++) {
			double mean = 0.0;
			double var = 0.0;
			double time = 0.0;
			double diff = 0.0;
			for (int j = i; j < travelTime.size(); j++) {
				int source = nodeSequence.get(j);
				int dest = nodeSequence.get(j + 1);
				String key = source + " " + dest;
				if (!edgeTimeMap.containsKey(key))
					key = dest + " " + source;

				double[] arr = edgeTimeMap.get(key);
				mean = arr[0];
				var = arr[1];
				time = travelTime.get(j);
				diff += Math.pow((mean - time), 2.0) / var;
				if (Math.sqrt(diff) >= (Math.sqrt(j - i + 1) * threshold)) { // pvalue
					// compu
					String pot = i + "," + j;
					ano.add(pot);
					// if (length(pot) > length(max))
					// max = pot;
				}
				edgeProcessedNaive = edgeProcessedNaive
						.add(new BigInteger("1"));

			}

		}
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < ano.size(); i++) {
			int l1 = Integer.parseInt(ano.get(i).split(",")[0]);
			int u1 = Integer.parseInt(ano.get(i).split(",")[1]);
			boolean add = true;
			for (int j = 0; j < ano.size(); j++) {
				int l2 = Integer.parseInt(ano.get(j).split(",")[0]);
				int u2 = Integer.parseInt(ano.get(j).split(",")[1]);
				if (((l2 < l1) && (u2 > u1)) || ((l1 == l2) && (u2 > u1))
						|| ((u1 == u2) && (l2 < l1))) {
					add = false;
					break;
				}

				edgeProcessedNaive = edgeProcessedNaive
						.add(new BigInteger("1"));

			}
			if (add)
				ret.add(ano.get(i));
		}
		// log.println(ano);
		// log.println(ret);
		// log.close();
		// System.out.println("Naive result : ");
		// System.out.println(ret);
		return ret;

	}

	public ArrayList<String> findAnomalyNaive2() {

		edgeProcessedWindow = new BigInteger("0");
		ArrayList<String> ret = new ArrayList<String>();
		int rightSoFar = -1;
		for (int i = 0; i < travelTime.size(); i++) {

			if (rightSoFar == travelTime.size() - 1)
				return ret;
			for (int j = travelTime.size() - 1; j > rightSoFar; j--) {
				if (j - i > 0) {
					String s = String.valueOf((j - i));
					edgeProcessedWindow = edgeProcessedWindow
							.add(new BigInteger(s));
				} else
					edgeProcessedWindow = edgeProcessedWindow
							.add(new BigInteger("1"));
				if (isAnomalous(i + "," + j)) {
					ret.add(i + "," + j);
					rightSoFar = j;
					break;
				}
				if (rightSoFar == travelTime.size() - 1)
					break;
			}
		}
		// System.out.println("Naive2 result : ");
		// System.out.println(ret);
		return ret;
	}

	public ArrayList<String> findAnomalyOur() {

		edgeProcessedOur = new BigInteger("0");
		seedSet = new ArrayList<String>();
		alive = new HashMap<String, Boolean>();
		newE = new HashMap<String, Boolean>();
		for (int i = 0; i < travelTime.size(); i++) {
			edgeProcessedOur = edgeProcessedOur.add(new BigInteger("1"));
			double[] arr = arrList.get(i);

			double mean = arr[0];

			double var = arr[1];

			double time = travelTime.get(i);

			double diff = Math.pow((mean - time), 2.0) / var;

			if ((Math.sqrt(diff) >= threshold)) { // pvalue computation
				// int l = i;
				// while (true) {
				// i++;
				// if (i == travelTime.size())
				// break;
				// arr = arrList.get(i);
				//
				// mean = arr[0];
				//
				// var = arr[1];
				//
				// time = travelTime.get(i);
				//
				// diff = Math.pow((mean - time), 2.0) / var;
				// if (Math.sqrt(diff) < 3.0)
				// break;
				// }
				// int u = i - 1;
				int j = i;
				while (true) {
					i++;
					if (i == travelTime.size())
						break;
					double[] arrt = arrList.get(i);

					double meant = arrt[0];

					double vart = arrt[1];

					double timet = travelTime.get(i);

					double difft = Math.pow((meant - timet), 2.0) / vart;
					if (Math.sqrt(difft) < threshold)
						break;
				}
				i--;

				seedSet.add(j + "," + i);
			}

		}

		return findLA();
	}

	private ArrayList<String> findLA() {

		HashMap<String, String> LB = new HashMap<String, String>();
		HashMap<String, String> RB = new HashMap<String, String>();
		for (String s : seedSet) {
			alive.put(s, true);
			LB.put(s, findLB(s));
			RB.put(s, findRB(s));
			newE.put(s, false);
		}
		while (true) {
			boolean repeat = false;

			for (String s : seedSet) {
				newE.put(s, false);
			}
			for (int i = 0; i < seedSet.size(); i++) {
				if ((newE.get(seedSet.get(i))) || (!alive.get(seedSet.get(i))))
					continue;
				String st1 = seedSet.get(i);
				int lbl1 = Integer.parseInt(LB.get(st1).split(",")[0]);
				int rbu1 = Integer.parseInt(RB.get(st1).split(",")[1]);
				for (int j = i + 1; j < seedSet.size(); j++) {
					if ((!alive.get(seedSet.get(j)))
							|| newE.get(seedSet.get(j)))
						continue;
					String st2 = seedSet.get(j);
					int lbl2 = Integer.parseInt(LB.get(st2).split(",")[0]);
					int rbu2 = Integer.parseInt(RB.get(st2).split(",")[1]);
					if (lbl1 >= lbl2 && rbu1 <= rbu2) {
						alive.put(st1, false);
						LB.remove(st1);
						RB.remove(st1);
						break;
					} else if (lbl2 >= lbl1 && rbu2 <= rbu1) {
						alive.put(st2, false);
						LB.remove(st2);
						RB.remove(st2);
					}

					else if (rbu1 >= lbl2 || (rbu1 + 1) == lbl2
							|| rbu1 == (lbl2 - 2)) {
						int u = Integer.parseInt(st2.split(",")[1]);
						int l = Integer.parseInt(st1.split(",")[0]);
						String st;
						if (l > u) {
							int t = l;
							l = u;
							u = t;
						}
						st = l + "," + u;
						alive.put(st1, false);
						LB.remove(st1);
						RB.remove(st1);
						alive.put(st2, false);
						LB.remove(st2);
						RB.remove(st2);
						seedSet.add(st);

						alive.put(st, true);
						newE.put(st, true);
						int l1 = Integer.parseInt(findLB(st).split(",")[0]);
						if (lbl1 < lbl2) {
							if (lbl1 < l1)
								l1 = lbl1;
						} else {
							if (lbl2 < l1)
								l1 = lbl2;
						}
						int r1 = Integer.parseInt(findRB(st).split(",")[1]);
						if (rbu2 > rbu1) {
							if (rbu2 > r1)
								r1 = rbu2;
						} else {
							if (rbu1 > r1)
								r1 = rbu1;
						}

						LB.put(st, l1 + "," + u);
						RB.put(st, l + "," + r1);
						repeat = true;
						break;
					}

				}
			}

			seedSet = sort(seedSet);
			if (!repeat)
				break;

		}

		ArrayList<String> ret = new ArrayList<String>();
		for (String s : seedSet) {
			if (alive.get(s)) {
				int l = Integer.parseInt(LB.get(s).split(",")[0]);
				int r = Integer.parseInt(RB.get(s).split(",")[1]);
				if (l == r) {
					edgeProcessedOur = edgeProcessedOur
							.add(new BigInteger("1"));
					ret.add(l + "," + r);
					continue;
				}
				int rb = l - 1;
				for (int i = l; i <= r; i++) {

					for (int j = r; j > rb; j--) {// || (rb == i && j >= i)
						String stt = String.valueOf((j - i));
						edgeProcessedOur = edgeProcessedOur.add(new BigInteger(
								stt));
						if (isAnomalous(i + "," + j)) {
							ret.add(i + "," + j);
							rb = j;
							break;
						}
					}
					if (rb == r)
						break;
				}
			}
		}
		// System.out.println("Our result : ");
		// System.out.println(ret);
		return ret;
	}

	private ArrayList<String> sort(ArrayList<String> seedSet2) {
		ArrayList<String> tempSeed = new ArrayList<String>();
		for (String st : seedSet2)
			if (alive.get(st))
				tempSeed.add(st);
		for (int i = 0; i < tempSeed.size() - 1; i++) {
			for (int j = 0; j < tempSeed.size() - i - 1; j++) {
				int l1 = Integer.parseInt(tempSeed.get(j).split(",")[0]);
				int l2 = Integer.parseInt(tempSeed.get(j + 1).split(",")[0]);
				if (l1 > l2) {
					String swap = tempSeed.get(j);
					tempSeed.set(j, tempSeed.get(j + 1));
					tempSeed.set(j + 1, swap);
				}
			}
		}

		return tempSeed;
	}

	private int length(String s) {
		int l = Integer.parseInt(s.split(",")[0]);
		int u = Integer.parseInt(s.split(",")[1]);
		if (l > u) {
			System.out.println("FATAL ERROR");
			return -1;
		}
		if (l == -1)
			return -1;
		return (u - l + 1);

	}

	public boolean isAnomalous(String st) {
		int l = Integer.parseInt(st.split(",")[0]);
		int u = Integer.parseInt(st.split(",")[1]);
		double mean = 0.0;
		double var = 0.0;
		double time = 0.0;
		double diff = 0.0;
		if (u < l)
			return false;
		for (int i = l; i <= u; i++) {
			if (arrList.get(i) != null) {
				double[] arr = arrList.get(i);
				mean = arr[0];
				var = arr[1];
				time = travelTime.get(i);
				diff += Math.pow((mean - time), 2.0) / var;
			}
		}

		if (Math.sqrt(diff) >= (Math.sqrt(u - l + 1) * threshold)) // pvalue
																	// computation
			return true;
		return false;
	}

	private String findRB(String s) {

		int l = Integer.parseInt(s.split(",")[0]);
		int u = Integer.parseInt(s.split(",")[1]);
		double mean = 0.0;
		double var = 0.0;
		double time = 0.0;
		double diff = 0.0;
		for (int i = l; i <= u; i++) {

			edgeProcessedOur = edgeProcessedOur.add(new BigInteger("1"));
			double[] arr = arrList.get(i);
			mean = arr[0];
			var = arr[1];
			time = travelTime.get(i);
			diff += Math.pow((mean - time), 2.0) / var;

		}

		while (true) {

			edgeProcessedOur = edgeProcessedOur.add(new BigInteger("1"));
			u++;
			if (u >= arrList.size())
				break;
			if (arrList.get(u) != null) {
				double[] arr = arrList.get(u);
				mean = arr[0];
				var = arr[1];
				time = travelTime.get(u);
				diff += Math.pow((mean - time), 2.0) / var;
				if (Math.sqrt(diff) < (Math.sqrt(u - l + 1) * threshold)) // pvalue
					// computation
					break;
			}
		}
		u--;
		return l + "," + u;
	}

	private String findLB(String s) {
		int l = Integer.parseInt(s.split(",")[0]);
		int u = Integer.parseInt(s.split(",")[1]);
		double mean = 0.0;
		double var = 0.0;
		double time = 0.0;
		double diff = 0.0;
		for (int i = l; i <= u; i++) {

			edgeProcessedOur = edgeProcessedOur.add(new BigInteger("1"));
			double[] arr = arrList.get(i);
			mean = arr[0];
			var = arr[1];
			time = travelTime.get(i);
			diff += Math.pow((mean - time), 2.0) / var;

		}

		while (true) {

			edgeProcessedOur = edgeProcessedOur.add(new BigInteger("1"));
			l--;
			if (l < 0)
				break;
			if (arrList.get(l) != null) {
				double[] arr = arrList.get(l);
				mean = arr[0];
				var = arr[1];
				time = travelTime.get(l);
				diff += Math.pow((mean - time), 2.0) / var;
				if (Math.sqrt(diff) < (Math.sqrt(u - l + 1) * threshold)) // pvalue
					// computation
					break;
			}
		}
		l++;
		return l + "," + u;
	}

	private void loadMeanVar(String filePath) throws Exception {

		BufferedReader fileReader = new BufferedReader(new FileReader(filePath));
		while (true) {
			String line = fileReader.readLine();
			if (line == null)
				break;
			String[] warr = line.split(",");
			double[] arr = new double[3];

			String key = warr[0];
			arr[0] = Double.parseDouble(warr[1]);
			arr[1] = Double.parseDouble(warr[2]);
			arr[2] = Math.ceil(Double.parseDouble(warr[3]));

			edgeTimeMap.put(key, arr);

		}
		fileReader.close();

	}

	public boolean notInTraining() {
		for (int i = 0; i < travelTime.size(); i++)
			if (arrList.get(i) == null)
				return true;
		return false;
	}
}