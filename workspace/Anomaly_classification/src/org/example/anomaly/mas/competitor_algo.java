package org.example.anomaly.mas;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.StringTokenizer;

import org.example.anomaly.data.Point;

public class competitor_algo 
{
	String dest = "";
	public static final Hashtable<String, Integer> sizes = new Hashtable<String, Integer>();
	double PIx = 3.141592653589793;
	double RADIUS = 6378.16;
	boolean modify_speed;
	
	String label = "";
	ArrayList<PrintWriter> train;
	ArrayList<PrintWriter> test;
	
	public competitor_algo () 
	{
		this.sizes.put("bike", 649);
		this.sizes.put("bus", 512);
		this.sizes.put("car", 569);
		this.sizes.put("walk", 1262);
	}
	
	public static void main(String[] args)  throws Exception
	{
		competitor_algo comp = new competitor_algo();
		comp.dest = "src/inputs/car_walk/";
		ArrayList<String> trains = new ArrayList<String>();
		trains.add("car");
		trains.add("walk");
		comp.NN(trains);
		//comp.generate_parts(trains);
		/*comp.train = new ArrayList<PrintWriter>();
		comp.test = new ArrayList<PrintWriter>();
		for(int i = 1; i <= 5; i++)
		{
			comp.train.add(new PrintWriter(new BufferedWriter(new FileWriter(comp.dest + "train_"+ i + ".csv" , true)))) ;
			comp.test.add(new PrintWriter(new BufferedWriter(new FileWriter(comp.dest + "test_"+ i + ".csv" , true)))) ;
			for(int j = 0; j < trains.size(); j++)
			{
				comp.label = trains.get(j);
				comp.count_lines(comp.dest + trains.get(j) + "_train_" + i + ".txt");
				comp.count_lines(comp.dest + trains.get(j) + "_test_" + i + ".txt");
				if(trains.get(j).equals("walk"))
					comp.modify_speed = true;
				else
					comp.modify_speed = false;
				
				
				comp.finding_speed(comp.dest + trains.get(j) + "_train_" + i + ".txt");
				
				comp.finding_speed(comp.dest + trains.get(j) + "_test_" + i + ".txt");
			}
			comp.train.get(comp.train.size() - 1).close();
			comp.test.get(comp.test.size() - 1).close();
			
			comp.count_lines(comp.dest + "train_"+ i + ".csv");
			comp.count_lines(comp.dest + "test_"+ i + ".csv");
		}*/
	}

	public void generate_parts(ArrayList<String> trains)
	{
		System.out.println(dest);
		try
		{
			ArrayList<BufferedReader> trains_file = new ArrayList<BufferedReader>();
			for(int x = 0 ;x < trains.size(); x++)
			{
				trains_file.add(new BufferedReader(new FileReader(dest + trains.get(x) + ".txt")));
			}
			
			ArrayList<ArrayList<PrintWriter>> outs = new ArrayList<ArrayList<PrintWriter>>();
			for(int x = 0; x < trains.size(); x++)
			{
				outs.add(new ArrayList<PrintWriter>());
				for(int y = 0; y < 10; y++)
				{
					String out_name = dest + trains.get(x) + "_";
					if(y < 5)
					{
						out_name += "train_" + (y+1) + ".txt";
					}
					else
						out_name += "test_" + (y+1 - 5) + ".txt";
					System.out.println(out_name);
					outs.get(x).add( new PrintWriter(new OutputStreamWriter(new FileOutputStream( out_name ), "UTF-8")));
				}
			}
			int min = 9999;
			int min_train = 0;
			for(int x = 0; x< trains.size(); x++)
			{
				if(sizes.get(trains.get(x)) < min)
				{
					min = sizes.get(trains.get(x));
					min_train = x;
				}
			}
			
			
			for(int x = 0; x < trains.size(); x++)
			{
				String line = new String();
				int count = 0;
				if(x == min_train)
				{
					while(true)
					{
						line = trains_file.get(x).readLine();
						if(line == null)
							break;
						if(!line.contains("["))
							continue;
						int test;
						if(0 <= count && count < (min/5))
							test = 5;
						else if((min/5) <= count && count < ((2*min)/5))
							test = 6;
						else if(((2*min)/5) <= count && count < ((3*min)/5))
							test = 7;
						else if(((3*min)/5) <= count && count < ((4*min)/5))
							test = 8;
						else
							test = 9;
						outs.get(x).get(test).println(line);
						for(int y = 0 ; y < 5 ; y++ )
						{
							if((test-5) != y)
							{
								outs.get(x).get(y).println(line);
							}
						}
						count++;
					}
				}
				else
				{
					count = 0;
					while(true)
					{
						line = trains_file.get(x).readLine();
						if(line == null)
							break;
						if(!line.contains("["))
						{
							continue;
						}
						int test;
						if(0 <= count && count < (min/5))
							test = 5;
						else if((min/5) <= count && count < ((2*min)/5))
							test = 6;
						else if(((2*min)/5) <= count && count < ((3*min)/5))
							test = 7;
						else if(((3*min)/5) <= count && count < ((4*min)/5))
							test = 8;
						else if (((4*min)/5) <= count && count < min)
							test = 9;
						else 
							test = 100;
						if(test!=100)
							outs.get(x).get(test).println(line);
						for(int y = 0 ; y < 5 ; y++ )
						{
							if((test-5) != y)
							{
								outs.get(x).get(y).println(line);
							}
						}
						count++;
					}	
				}
			}
			for(int x = 0; x < outs.size(); x++)
			{
				for(int y = 0; y < outs.get(x).size(); y++)
					outs.get(x).get(y).close();
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}
	public double Radians(double x) 
	{
		return x * PIx / 180;
	}

	public double distance(double lat1, double lon1, double lat2, double lon2) 
	{
		double dlon = Radians(lon2 - lon1);
		double dlat = Radians(lat2 - lat1);

		double a = (Math.sin(dlat / 2) * Math.sin(dlat / 2))
				+ Math.cos(Radians(lat1)) * Math.cos(Radians(lat2))
				* (Math.sin(dlon / 2) * Math.sin(dlon / 2));
		double angle = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return angle * RADIUS;

	}
	
	public void finding_speed(String testfile) throws Exception
	{
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("test" , true))) ;
		if(testfile.contains("train"))
			out = train.get(train.size() - 1);
		else if(testfile.contains("test"))
			out = test.get(test.size() - 1);
		
		int line_no = 0;
		int total_count = 0;
		ArrayList<Double> speeds = new ArrayList<Double>();
		line_no = 0;
		BufferedReader fileReader = new BufferedReader(new FileReader( testfile));
		int prev_id = 0;
		double prev_lat = 0.0;
		double prev_lon = 0.0;
		long prev_val = 0;
		Date prev_time = new Date();
		while (true) 
		{
			ArrayList<Point> arr = new ArrayList<Point>();
			String line = fileReader.readLine();
			if (line == null)
				break;
			line_no++;
			prev_id = -1;
			prev_lat = 0.0;
			prev_lon = 0.0;
			prev_time = new Date();
			double speed = 0.0;
			if (line.contains("[")) 
			{
				speeds = new ArrayList<Double>();
				String[] warr = line.split("\\]");
				for (String s : warr) 
				{
					try 
					{
						int id = Integer.parseInt(s.split(",")[0]
								.split("\\[")[1]);
						double lat = Double.parseDouble(s.split(",")[1]);
						double lon = Double.parseDouble(s.split(",")[2]);
						long val = Long.parseLong(s.split(",")[3]
								.replaceAll("\\s", ""));
						Date date = new Date(val);
						SimpleDateFormat df2 = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						String dateText = df2.format(date);
						Date time = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss").parse(dateText);

						arr.add(new Point(id, lat, lon, time, val));
						if(prev_id != -1)
						{
							speed =  Math.abs(distance(lat, lon,prev_lat, prev_lon)) * 1000 * 3600 / (Math.abs(prev_val-val));
							if(speed > 10.0 && modify_speed == true)
								speeds.add(10.0);
							else 
								speeds.add(speed);
						}
						prev_time = time;
						prev_lat = lat;
						prev_lon = lon;
						prev_id = id;
						prev_val = val;
					} 
					catch (Exception e) 
					{
						System.out.println(e);
					}
				}
				total_count++;
				double min = 999999.0;
				double max = 0.0000;
				double sum = 0;
				for(int i = 0; i < speeds.size(); i++)
				{
					if(speeds.get(i) < min)
						min = speeds.get(i);
					if(speeds.get(i) > max)
						max = speeds.get(i);
					sum += speeds.get(i);
				}
				double avg = sum / (double)speeds.size();
			//	System.out.println("Edge count : " + speeds.size());
				if(speeds.size()!=0)
					out.print(min + "," + max + "," + avg + "," + label + "\n");
			}
		}
		fileReader.close();
		System.out.println("File name : " + testfile);
		System.out.println("Total count : " + total_count);
	}
	public int count_lines(String file)
	{
		try
		{
			BufferedReader train_file = new BufferedReader(new FileReader(file));
			String line = "";
			int count = 0;
			while(true)
			{
				line = train_file.readLine();
				if(line == null)
					break;
				count++;
			}
			System.out.println(file + " : " + count);
			return count;
		}
		catch (Exception e)
		{
			System.out.println(e);
			return 0;
		}
		
	}

	public void NN (ArrayList<String> trains) throws Exception
	{
		for(int i = 1 ; i <=5; i++)
		{
			Hashtable<String, ArrayList<Double>> all_fscore = new Hashtable<String, ArrayList<Double>>();
			Hashtable<String, Integer> tp = new Hashtable<String, Integer>();
			Hashtable<String, Integer> fp = new Hashtable<String, Integer>();
			Hashtable<String, Integer> fn = new Hashtable<String, Integer>();
			Hashtable<String, Double> p = new Hashtable<String, Double>();
			Hashtable<String, Double> r = new Hashtable<String, Double>();
			Hashtable<String, Double> f1 = new Hashtable<String, Double>();
			
			for(int x = 0 ; x< trains.size(); x++)
			{
				all_fscore.put(trains.get(x), new ArrayList<Double>());
				tp.put(trains.get(x), 0);
				fp.put(trains.get(x), 0);
				fn.put(trains.get(x), 0);
			}
			BufferedReader train = new BufferedReader(new FileReader(dest + "train_" + i + ".csv"));
			BufferedReader test = new BufferedReader(new FileReader(dest + "test_" + i + ".csv"));
			ArrayList<competitor_algo_element> train_elements = new ArrayList<competitor_algo_element>();
			ArrayList<competitor_algo_element> test_elements = new ArrayList<competitor_algo_element>();
			
			while(true)
			{
				String line = train.readLine();
				if(line == null)
					break;
				StringTokenizer stt = new StringTokenizer(line, ",");
				competitor_algo_element new_element = new competitor_algo_element();
				new_element.min = Double.parseDouble(stt.nextToken().toString());
				new_element.max = Double.parseDouble(stt.nextToken().toString());
				new_element.avg = Double.parseDouble(stt.nextToken().toString());
				new_element.label = stt.nextToken().toString();
				train_elements.add(new_element);
				
			}
			
			while(true)
			{
				String line = test.readLine();
				if(line == null)
					break;
				StringTokenizer stt = new StringTokenizer(line, ",");
				competitor_algo_element new_element = new competitor_algo_element();
				new_element.min = Double.parseDouble(stt.nextToken().toString());
				new_element.max = Double.parseDouble(stt.nextToken().toString());
				new_element.avg = Double.parseDouble(stt.nextToken().toString());
				new_element.label = stt.nextToken().toString();
				test_elements.add(new_element);
				
			}
			
			for(int j = 0 ;j < test_elements.size(); j++)
			{
				double min = 99999999;
				String label = "";
				for(int k = 0; k < train_elements.size(); k++)
				{
					double dist = 0.0;
					dist += Math.pow(train_elements.get(k).min - test_elements.get(j).min, 2);
					dist += Math.pow(train_elements.get(k).max - test_elements.get(j).max, 2);
					dist += Math.pow(train_elements.get(k).avg - test_elements.get(j).avg, 2);
					if(dist < min)
					{
						min = dist;
						label = train_elements.get(k).label;
					}
				}
				if(label.equals(test_elements.get(j).label))
				{
					tp.put(label, tp.get(label) + 1);
				}
				else
				{
					fp.put(label, fp.get(label) + 1);
					fn.put(test_elements.get(j).label, fn.get(test_elements.get(j).label) + 1);
				}
			}
			for(int x = 0 ; x < trains.size(); x++)
			{
				p.put(trains.get(x), ((double)(tp.get(trains.get(x)))/(tp.get(trains.get(x)) + fp.get(trains.get(x)))));
				r.put(trains.get(x), ((double)(tp.get(trains.get(x)))/(tp.get(trains.get(x)) + fn.get(trains.get(x)))));
				f1.put(trains.get(x), ((double)(2*p.get(trains.get(x))*r.get(trains.get(x)))/(r.get(trains.get(x)) + p.get(trains.get(x)))));
				all_fscore.get(trains.get(x)).add(f1.get(trains.get(x)));
				System.out.println(trains.get(x) + i + "  " + p.get(trains.get(x)) + "  " 
						+ r.get(trains.get(x)) + "  " + f1.get(trains.get(x))) ;
			}
		}
	}
}