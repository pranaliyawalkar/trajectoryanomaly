package org.example.anomaly.mas;

import geolife_processing.start_end_anom;

import java.beans.FeatureDescriptor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import org.example.anomaly.data.Point;
import org.example.anomaly.util.EdgeMeanVariance;

public class MASHolder_multiclass_cv 
{
	String mapMatchedTrajDirPath = "";
	String learnedEdgeDistroFilePath = "";
	
	double PIx = 3.141592653589793;
	double RADIUS = 6378.16;
	
	PrintWriter outValues;
	
	EdgeProcessing ep1;
	EdgeProcessing ep2;
	EdgeProcessing ep3;
	boolean modify_speed;
	Hashtable<String, Double> new_speeds;
	
	public static final Hashtable<String, Integer> sizes = new Hashtable<String, Integer>();

	public MASHolder_multiclass_cv () 
	{
		this.sizes.put("bike", 649);
		this.sizes.put("bus", 512);
		this.sizes.put("car", 569);
		this.sizes.put("walk", 1262);
	}

	public MASHolder_multiclass_cv (String train, String train2, String train3) 
	{
		
		String dest = "src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/";
		mapMatchedTrajDirPath = dest;
		this.sizes.put("bike", 649);
		this.sizes.put("bus", 512);
		this.sizes.put("car", 569);
		this.sizes.put("walk", 1262);
		
		//train data : testing on part 5
		
		for(int i =  1 ; i <=5 ; i++)
		{
			try
			{
				double threshold = 5.0;
				new_speeds = new Hashtable<String, Double>();
				//for(double threshold = 1.0; threshold <= 5.0; threshold = threshold + 1)
				{
					learnedEdgeDistroFilePath = dest + "out_" + train + "_" + "train_" + i + ".csv"; 
					ep1 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, 3.0);
					learnedEdgeDistroFilePath = dest + "out_" + train2 + "_" + "train_" + i + ".csv"; 
					ep2 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, 3.0);
					learnedEdgeDistroFilePath = dest + "out_" + train3 + "_" + "train_" + i + ".csv"; 
					ep3 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, 3.0);
					outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "percent" +"_" + train + "_" + i + ".csv"), "UTF-8"));
					outValues.println("size" + "," + "out_" + train + "_" + "train_" + i + ".csv" + "," + "out_" + train2 + "_" + "train_" + i + ".csv"
							+ "," + "out_" + train3 + "_" + "train_" + i + ".csv"
							+ "," + "anom edges " + train + "," + "anom edges " + train2 + "," + "anom edges " + train3 
							+ "," + "missing edges " + train + "," + "missing edges " + train2 + "," + "missing edges " + train3);
					System.out.println("Train 1 : " + "out_" + train + "_" + "train_" + i + ".csv");
					System.out.println("Train 2 : " + "out_" + train2 + "_" + "train_" + i + ".csv");
					System.out.println("Train 3 : " + "out_" + train3 + "_" + "train_" + i + ".csv");
					
					System.out.println("Test : " + train + "_" + "test_" + i + ".txt");
					modify_speed = false;
					compare_handler(train + "_" + "test_" + i + ".txt");
					outValues.close();
				}
				
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
		//train2 data : testing on part 5
		for(int i =  1 ; i <=5 ; i++)
		{
			try
			{
				double threshold = 5.0;
				new_speeds = new Hashtable<String, Double>();
				//for(double threshold = 1.0; threshold <= 5.0; threshold = threshold + 1)
				{
					learnedEdgeDistroFilePath = dest + "out_" + train + "_" + "train_" + i + ".csv"; 
					ep1 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, 3.0);
					learnedEdgeDistroFilePath = dest + "out_" + train2 + "_" + "train_" + i + ".csv"; 
					ep2 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, 3.0);
					learnedEdgeDistroFilePath = dest + "out_" + train3 + "_" + "train_" + i + ".csv"; 
					ep3 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, 3.0);
					outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "percent" +"_" + train2 + "_" + i + ".csv"), "UTF-8"));
					outValues.println("size" + "," + "out_" + train + "_" + "train_" + i + ".csv" + "," + "out_" + train2 + "_" + "train_" + i + ".csv"
							+ "," + "out_" + train3 + "_" + "train_" + i + ".csv"
							+ "," + "anom edges " + train + "," + "anom edges " + train2 + "," + "anom edges " + train3 
							+ "," + "missing edges " + train + "," + "missing edges " + train2 + "," + "missing edges " + train3);
					System.out.println("Train 1 : " + "out_" + train + "_" + "train_" + i + ".csv");
					System.out.println("Train 2 : " + "out_" + train2 + "_" + "train_" + i + ".csv");
					System.out.println("Train 3 : " + "out_" + train3 + "_" + "train_" + i + ".csv");
					
					System.out.println("Test : " + train2 + "_" + "test_" + i + ".txt");
					modify_speed = false;
					compare_handler(train2 + "_" + "test_" + i + ".txt");
					outValues.close();
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
				
		}
		for(int i =  1 ; i <=5 ; i++)
		{
			try
			{
				double threshold = 5.0;
				new_speeds = new Hashtable<String, Double>();
				//for(double threshold = 1.0; threshold <= 5.0; threshold = threshold + 1)
				{
					learnedEdgeDistroFilePath = dest + "out_" + train + "_" + "train_" + i + ".csv"; 
					ep1 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, 3.0);
					learnedEdgeDistroFilePath = dest + "out_" + train2 + "_" + "train_" + i + ".csv"; 
					ep2 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, 3.0);
					learnedEdgeDistroFilePath = dest + "out_" + train3 + "_" + "train_" + i + ".csv"; 
					ep3 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, 3.0);
					outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "percent" +"_" + train3 + "_" + i + ".csv"), "UTF-8"));
					outValues.println("size" + "," + "out_" + train + "_" + "train_" + i + ".csv" + "," + "out_" + train2 + "_" + "train_" + i + ".csv"
							+ "," + "out_" + train3 + "_" + "train_" + i + ".csv"
							+ "," + "anom edges " + train + "," + "anom edges " + train2 + "," + "anom edges " + train3 
							+ "," + "missing edges " + train + "," + "missing edges " + train2 + "," + "missing edges " + train3);
					System.out.println("Train 1 : " + "out_" + train + "_" + "train_" + i + ".csv");
					System.out.println("Train 2 : " + "out_" + train2 + "_" + "train_" + i + ".csv");
					System.out.println("Train 3 : " + "out_" + train3 + "_" + "train_" + i + ".csv");
					
					System.out.println("Test : " + train3 + "_" + "test_" + i + ".txt");
					modify_speed = false;
					compare_handler(train3 + "_" + "test_" + i + ".txt");
					outValues.close();
				}
				
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
	}

	public void compare_handler(String testfile) throws Exception 
	{
		int line_no = 0;
		int total_count = 0;
		{
			line_no = 0;
			System.out.println("File name : " + testfile);
			BufferedReader fileReader = new BufferedReader(new FileReader( mapMatchedTrajDirPath + testfile));
			int prev_id = 0;
			double prev_lat = 0.0;
			double prev_lon = 0.0;
			long prev_val = 0;
			Date prev_time = new Date();
			while (true) 
			{
				ArrayList<Point> arr = new ArrayList<Point>();
				String line = fileReader.readLine();
				if (line == null)
					break;
				line_no++;
				prev_id = -1;
				prev_lat = 0.0;
				prev_lon = 0.0;
				prev_time = new Date();
				double speed = 0.0;
				if (line.contains("[")) 
				{
					String[] warr = line.split("\\]");
					for (String s : warr) 
					{
						try 
						{
							int id = Integer.parseInt(s.split(",")[0]
									.split("\\[")[1]);
							double lat = Double.parseDouble(s.split(",")[1]);
							double lon = Double.parseDouble(s.split(",")[2]);
							long val = Long.parseLong(s.split(",")[3]
									.replaceAll("\\s", ""));
							Date date = new Date(val);
							SimpleDateFormat df2 = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							String dateText = df2.format(date);
							Date time = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss").parse(dateText);

							arr.add(new Point(id, lat, lon, time, val));
							if(prev_id!=-1 && modify_speed == true)
							{
								speed =  Math.abs(distance(lat, lon,prev_lat, prev_lon)) * 1000 * 3600 / (Math.abs(prev_val-val));
								if(speed > 10.0)
								{
									new_speeds.put(id + "_" + prev_id, Math.abs(distance(lat, lon,prev_lat, prev_lon)) * 1000 * 3600 );
									new_speeds.put(prev_id + "_" + id, Math.abs(distance(lat, lon,prev_lat, prev_lon)) * 1000 * 3600 );
								}
							}
							prev_time = time;
							prev_lat = lat;
							prev_lon = lon;
							prev_id = id;
							prev_val = val;
						} 
						catch (Exception e) 
						{
							System.out.println(e);
						}
					}
					total_count++;
					outValues.print(warr.length - 1 + ",");
					compare(arr, line_no, warr.length - 1);
				}
			}
			fileReader.close();
		}
	}

	public void compare(ArrayList<Point> pt, int line_no, int size) throws Exception 
	{
		long tot1 = 0;
		long tot2 = 0;
		long tot3 = 0;
		List<Integer> nodeSequence = new ArrayList<Integer>();
		List<Double> travelTime = new ArrayList<Double>();
		for (int i = 0; i < pt.size(); i++) 
		{
			nodeSequence.add(pt.get(i).id);
			if (i < (pt.size() - 1)) 
			{
				double time = pt.get(i + 1).mytime - pt.get(i).mytime;
				if(modify_speed == true && (new_speeds.get(pt.get(i+1).id + "_" + pt.get(i).id)!=null))
				{
					time = new_speeds.get(pt.get(i+1).id + "_" + pt.get(i).id) / 10;
				}
				travelTime.add(time);
			}
		}
		ep1.setNodeTime(nodeSequence, travelTime);
		ep2.setNodeTime(nodeSequence, travelTime);
		ep3.setNodeTime(nodeSequence, travelTime);
		
		ArrayList<Integer> not_in_training1 = new ArrayList<Integer>();
		ep1.notInTraining(not_in_training1); 
		
		ArrayList<Integer> not_in_training2 = new ArrayList<Integer>();
		ep2.notInTraining(not_in_training2); 
		
		ArrayList<Integer> not_in_training3 = new ArrayList<Integer>();
		ep3.notInTraining(not_in_training3); 
		
		ArrayList<Integer> anoms1 = new ArrayList<Integer>();
		int count1 = ep1.howManyAno(anoms1);
		
		ArrayList<Integer> anoms2 = new ArrayList<Integer>();
		int count2 = ep2.howManyAno(anoms2);
		
		ArrayList<Integer> anoms3 = new ArrayList<Integer>();
		int count3 = ep1.howManyAno(anoms3);
		
		ArrayList<String> our_result1 = new ArrayList<String>();
		ArrayList<String> our_result2 = new ArrayList<String>();
		ArrayList<String> our_result3 = new ArrayList<String>();
		try 
		{
			our_result1 = ep1.findAnomalyOur();
			our_result2 = ep2.findAnomalyOur();
			our_result3 = ep3.findAnomalyOur();
		} 
		catch (Exception e) 
		{
			System.out.println("Not there in training : our");
			e.printStackTrace();
			return;
		}
		/*System.out.println(line_no + " $ " + our_result1 + " $ " + size
				+ " $ " + not_in_training1.size()) /* +  naive_result + " $ " + naive_result2*/;
		/*System.out.println(line_no + " $ " + our_result2 + " $ " + (size)
				+ " $ " + not_in_training2.size()); */

		ArrayList<start_end_anom> sub_trajcs1 = new ArrayList<start_end_anom>();
		int total_anom_count1 = 0;
		ArrayList<Integer> visited1 = new ArrayList<Integer>();
		for (int i = 0; i < size; i++)
			visited1.add(0);
		for (int x = 0; x < our_result1.size(); x++) {
			start_end_anom obj = new start_end_anom();
			String s = our_result1.get(x);
			StringTokenizer stt = new StringTokenizer(s, ",");
			obj.start = Integer.parseInt(stt.nextToken().toString());
			obj.end = Integer.parseInt(stt.nextToken().toString());
			for (int z = obj.start; z <= obj.end; z++) 
			{
				if (visited1.get(z) == 0) // unvisited
				{
					total_anom_count1++;
					visited1.set(z, 1);
				}
			}

			sub_trajcs1.add(obj);
		}
		
		for (int x = 0; x < not_in_training1.size(); x++) 
		{
			for (int y = 0; y < sub_trajcs1.size(); y++) 
			{
				if (sub_trajcs1.get(y).start <= not_in_training1.get(x)
						&& sub_trajcs1.get(y).end >= not_in_training1.get(x)) {
					total_anom_count1 = total_anom_count1 - 1;
					break;
				}
			}
		}

		int trajSize1 = size - not_in_training1.size();
		if (size > 0)
			outValues.print((double) (total_anom_count1 + not_in_training1.size()));
		else
			outValues.print("Size zero!!");
		
		
		//SECOND CLASS
		ArrayList<start_end_anom> sub_trajcs2 = new ArrayList<start_end_anom>();
		int total_anom_count2 = 0;
		ArrayList<Integer> visited2 = new ArrayList<Integer>();
		for (int i = 0; i < size; i++)
			visited2.add(0);
		for (int x = 0; x < our_result2.size(); x++) {
			start_end_anom obj = new start_end_anom();
			String s = our_result2.get(x);
			StringTokenizer stt = new StringTokenizer(s, ",");
			obj.start = Integer.parseInt(stt.nextToken().toString());
			obj.end = Integer.parseInt(stt.nextToken().toString());
			for (int z = obj.start; z <= obj.end; z++) 
			{
				if (visited2.get(z) == 0) // unvisited
				{
					total_anom_count2++;
					visited2.set(z, 1);
				}
			}

			sub_trajcs2.add(obj);
		}
		
		for (int x = 0; x < not_in_training2.size(); x++) 
		{
			for (int y = 0; y < sub_trajcs2.size(); y++) 
			{
				if (sub_trajcs2.get(y).start <= not_in_training2.get(x)
						&& sub_trajcs2.get(y).end >= not_in_training2.get(x)) {
					total_anom_count2 = total_anom_count2 - 1;
					break;
				}
			}
		}

		int trajSize2 = size - not_in_training2.size();
		if (size > 0)
			outValues.print( "," + (double) (total_anom_count2 + not_in_training2.size()));
		else
			outValues.print("," + "Size zero!!");
		
		
		//THIRD CLASS
		ArrayList<start_end_anom> sub_trajcs3 = new ArrayList<start_end_anom>();
		int total_anom_count3 = 0;
		ArrayList<Integer> visited3 = new ArrayList<Integer>();
		for (int i = 0; i < size; i++)
			visited3.add(0);
		for (int x = 0; x < our_result3.size(); x++) {
			start_end_anom obj = new start_end_anom();
			String s = our_result3.get(x);
			StringTokenizer stt = new StringTokenizer(s, ",");
			obj.start = Integer.parseInt(stt.nextToken().toString());
			obj.end = Integer.parseInt(stt.nextToken().toString());
			for (int z = obj.start; z <= obj.end; z++) 
			{
				if (visited3.get(z) == 0) // unvisited
				{
					total_anom_count3++;
					visited3.set(z, 1);
				}
			}

			sub_trajcs3.add(obj);
		}
		
		for (int x = 0; x < not_in_training3.size(); x++) 
		{
			for (int y = 0; y < sub_trajcs3.size(); y++) 
			{
				if (sub_trajcs3.get(y).start <= not_in_training3.get(x)
						&& sub_trajcs3.get(y).end >= not_in_training3.get(x)) {
					total_anom_count3 = total_anom_count3 - 1;
					break;
				}
			}
		}

		int trajSize3 = size - not_in_training3.size();
		if (size > 0)
			outValues.print( "," +(double) (total_anom_count3 + not_in_training3.size()));
		else
			outValues.print( "," +"Size zero!!");
		
		
		
		
		//pringint the anom edges
		outValues.println( "," + count1 + "," + count2 + "," + count3 +"," + not_in_training1.size() + "," + not_in_training2.size() + "," + not_in_training3.size());
		
		
		//System.out.println("*****************************************");


	}

	public void fscore(String train, String train2, String train3)
	{
		String dest = "src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/";
		
		//for(double threshold = 1.0; threshold <= 5.0 ; threshold = threshold + 1.0)
		{
			Hashtable<String, ArrayList<Double>> all_fscore = new Hashtable<String, ArrayList<Double>>();
			all_fscore.put(train, new ArrayList<Double>());
			all_fscore.put(train2, new ArrayList<Double>());
			all_fscore.put(train3, new ArrayList<Double>());
			for(int i = 1; i <= 5 ; i++ )
			{
				try
				{
					Hashtable<String, Integer> tp = new Hashtable<String, Integer>();
					Hashtable<String, Integer> fp = new Hashtable<String, Integer>();
					Hashtable<String, Integer> fn = new Hashtable<String, Integer>();
					Hashtable<String, Double> p = new Hashtable<String, Double>();
					Hashtable<String, Double> r = new Hashtable<String, Double>();
					Hashtable<String, Double> f1 = new Hashtable<String, Double>();
					
					tp.put(train, 0);
					tp.put(train2, 0);
					tp.put(train3, 0);
					fp.put(train, 0);
					fp.put(train2, 0);
					fp.put(train3, 0);
					fn.put(train, 0);
					fn.put(train2, 0);
					fn.put(train3, 0);
					
					ArrayList<String> classes = new ArrayList<String>();
					classes.add(train);
					classes.add(train2);
					classes.add(train3);
					
					for(int j = 0; j < classes.size(); j++)
					{
						String me = classes.get(j);
						BufferedReader fileReader1 = new BufferedReader(new FileReader( dest + "percent" + "_" + me + "_" + i + ".csv" ));
						String line = new String();
						while(true)
						{
							line = fileReader1.readLine();
							if(!line.equals(""))
								break;
						}
						
						int my_class = j;
						while(true)
						{
							line = fileReader1.readLine();
							if(line == null)
								break;
							if(line.contains("zero"))
								continue;
							StringTokenizer stt = new StringTokenizer(line, ",");
							int size = Integer.parseInt(stt.nextToken().toString());
							double val1 = Double.parseDouble(stt.nextToken().toString());
							double val2 = Double.parseDouble(stt.nextToken().toString());
							double val3 = Double.parseDouble(stt.nextToken().toString());
							double min = val1;
							int min_class = 0;
							if(val2 < min)
							{
								min = val2;
								min_class = 1;
							}
							if(val3 < min)
							{
								min = val3;
								min_class = 2;
							}
							if(my_class != min_class)
							{
								//false negative
								fn.put(classes.get(my_class), fn.get(classes.get(my_class))+1);
								fp.put(classes.get(min_class), fp.get(classes.get(min_class))+1);
							}
							else
							{
								//correct classification
								tp.put(classes.get(my_class), tp.get(classes.get(my_class)) +1);
							}
						}
					}
					p.put(train, ((double)(tp.get(train))/(tp.get(train) + fp.get(train))));
					p.put(train2, ((double)(tp.get(train2))/(tp.get(train2) + fp.get(train2))));
					p.put(train3, ((double)(tp.get(train3))/(tp.get(train3) + fp.get(train3))));
					
					r.put(train, ((double)(tp.get(train))/(tp.get(train) + fn.get(train))));
					r.put(train2, ((double)(tp.get(train2))/(tp.get(train2) + fn.get(train2))));
					r.put(train3, ((double)(tp.get(train3))/(tp.get(train3) + fn.get(train3))));
					
					f1.put(train, ((double)(2*p.get(train)*r.get(train))/(r.get(train) + p.get(train))));
					f1.put(train2, ((double)(2*p.get(train2)*r.get(train2))/(r.get(train2) + p.get(train2))));
					f1.put(train3, ((double)(2*p.get(train3)*r.get(train3))/(r.get(train3) + p.get(train3))));
					
					all_fscore.get(train).add(f1.get(train));
					all_fscore.get(train2).add(f1.get(train2));
					all_fscore.get(train3).add(f1.get(train3));
				System.out.println(train + i + "  " + p.get(train) + "  " + r.get(train) + "  " + f1.get(train)) ;
				System.out.println(train2 + i + "  " + p.get(train2) + "  " + r.get(train2) + "  " + f1.get(train2)) ;
				System.out.println(train3 + i + "  " + p.get(train3) + "  " + r.get(train3) + "  " + f1.get(train3)) ;
				
			}
			catch (Exception e)
			{
				System.out.println(e);
			}
		}
		double avg = 0.0;
		for(int i = 0;i < all_fscore.get(train).size(); i++)
		{
			avg += all_fscore.get(train).get(i);
		}
		System.out.println("AVERAGE : " + train + "  " + avg/all_fscore.get(train).size());
		avg = 0.0;
		
		for(int i = 0;i < all_fscore.get(train2).size(); i++)
		{
			avg += all_fscore.get(train2).get(i);
		}
		System.out.println("AVERAGE : " + train2 + "  " + avg/all_fscore.get(train2).size());
		avg = 0.0;
		
		for(int i = 0;i < all_fscore.get(train3).size(); i++)
		{
			avg += all_fscore.get(train3).get(i);
		}
		System.out.println("AVERAGE : " + train3 + "  " + avg/all_fscore.get(train3).size());
		System.out.println("**********************************************");
		}
	}
	
	public void generate_parts(String train, String train2, String train3)
	{
		try
		{
			BufferedReader train_file = new BufferedReader(new FileReader("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3  +"/" + train + ".txt"));
			BufferedReader train2_file = new BufferedReader(new FileReader("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3  +"/" + train2 + ".txt"));
			BufferedReader train3_file = new BufferedReader(new FileReader("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3  +"/" + train3 + ".txt"));
			
			PrintWriter train_train_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train +"_train_1.txt"  ), "UTF-8"));
			PrintWriter train_train_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train +"_train_2.txt"  ), "UTF-8"));
			PrintWriter train_train_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train +"_train_3.txt"  ), "UTF-8"));
			PrintWriter train_train_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train +"_train_4.txt"  ), "UTF-8"));
			PrintWriter train_train_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3  +"/" + train +"_train_5.txt"  ), "UTF-8"));
			PrintWriter train_test_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train +"_test_1.txt"  ), "UTF-8"));
			PrintWriter train_test_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train +"_test_2.txt"  ), "UTF-8"));
			PrintWriter train_test_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train +"_test_3.txt"  ), "UTF-8"));
			PrintWriter train_test_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train +"_test_4.txt"  ), "UTF-8"));
			PrintWriter train_test_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train +"_test_5.txt"  ), "UTF-8"));


			PrintWriter train2_train_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train2 +"_train_1.txt"  ), "UTF-8"));
			PrintWriter train2_train_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train2 +"_train_2.txt"  ), "UTF-8"));
			PrintWriter train2_train_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train2 +"_train_3.txt"  ), "UTF-8"));
			PrintWriter train2_train_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train2 +"_train_4.txt"  ), "UTF-8"));
			PrintWriter train2_train_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train2 +"_train_5.txt"  ), "UTF-8"));
			PrintWriter train2_test_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train2 +"_test_1.txt"  ), "UTF-8"));
			PrintWriter train2_test_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train2 +"_test_2.txt"  ), "UTF-8"));
			PrintWriter train2_test_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train2 +"_test_3.txt"  ), "UTF-8"));
			PrintWriter train2_test_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train2 +"_test_4.txt"  ), "UTF-8"));
			PrintWriter train2_test_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train2 +"_test_5.txt"  ), "UTF-8"));
			
			
			
			PrintWriter train3_train_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train3 +"_train_1.txt"  ), "UTF-8"));
			PrintWriter train3_train_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train3 +"_train_2.txt"  ), "UTF-8"));
			PrintWriter train3_train_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train3 +"_train_3.txt"  ), "UTF-8"));
			PrintWriter train3_train_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train3 +"_train_4.txt"  ), "UTF-8"));
			PrintWriter train3_train_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train3 +"_train_5.txt"  ), "UTF-8"));
			PrintWriter train3_test_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train3 +"_test_1.txt"  ), "UTF-8"));
			PrintWriter train3_test_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train3 +"_test_2.txt"  ), "UTF-8"));
			PrintWriter train3_test_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train3 +"_test_3.txt"  ), "UTF-8"));
			PrintWriter train3_test_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train3 +"_test_4.txt"  ), "UTF-8"));
			PrintWriter train3_test_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 +"/" + train3 +"_test_5.txt"  ), "UTF-8"));
			

			int train_size = sizes.get(train);
			int train2_size = sizes.get(train2);
			int train3_size = sizes.get(train3);
			int min = train_size;
			if(train2_size < min)
				min = train2_size;
			if(train3_size < min)
				min = train3_size;
			if(min == train2_size)
			{
				String line = new String();
				int count = 0;
				while(true)
				{
					line = train2_file.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
						continue;
					if(0 <= count && count < (train2_size/5))
					{
						//first test
						train2_test_1.println(line);
						train2_train_2.println(line);
						train2_train_3.println(line);
						train2_train_4.println(line);
						train2_train_5.println(line);
					}
					else if((train2_size/5) <= count && count < ((2*train2_size)/5))
					{
						//first test
						train2_test_2.println(line);
						train2_train_1.println(line);
						train2_train_3.println(line);
						train2_train_4.println(line);
						train2_train_5.println(line);
					}
					else if(((2*train2_size)/5) <= count && count < ((3*train2_size)/5))
					{
						//first test
						train2_test_3.println(line);
						train2_train_1.println(line);
						train2_train_2.println(line);
						train2_train_4.println(line);
						train2_train_5.println(line);
					}
					else if(((3*train2_size)/5) <= count && count < ((4*train2_size)/5))
					{
						//first test
						train2_test_4.println(line);
						train2_train_1.println(line);
						train2_train_2.println(line);
						train2_train_3.println(line);
						train2_train_5.println(line);
					}
					else
					{
						//first test
						train2_test_5.println(line);
						train2_train_1.println(line);
						train2_train_2.println(line);
						train2_train_3.println(line);
						train2_train_4.println(line);
					}
					
					count++;
				}
				count = 0;
				while(true)
				{
					line = train_file.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
					{
						continue;
					}
					if(0 <= count && count < (train2_size/5))
					{
						//first test
						train_test_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if((train2_size/5) <= count && count < ((2*train2_size)/5))
					{
						//first test
						train_test_2.println(line);
						train_train_1.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if(((2*train2_size)/5) <= count && count < ((3*train2_size)/5))
					{
						//first test
						train_test_3.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if(((3*train2_size)/5) <= count && count < ((4*train2_size)/5))
					{
						//first test
						train_test_4.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_5.println(line);
					}
					else if(((4*train2_size)/5) <= count && count < train2_size)
					{
						//first test
						train_test_5.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
					}
					else
					{
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					count++;
				}
				count = 0;
				while(true)
				{
					line = train3_file.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
					{
						continue;
					}
					if(0 <= count && count < (train2_size/5))
					{
						//first test
						train3_test_1.println(line);
						train3_train_2.println(line);
						train3_train_3.println(line);
						train3_train_4.println(line);
						train3_train_5.println(line);
					}
					else if((train2_size/5) <= count && count < ((2*train2_size)/5))
					{
						//first test
						train3_test_2.println(line);
						train3_train_1.println(line);
						train3_train_3.println(line);
						train3_train_4.println(line);
						train3_train_5.println(line);
					}
					else if(((2*train2_size)/5) <= count && count < ((3*train2_size)/5))
					{
						//first test
						train3_test_3.println(line);
						train3_train_1.println(line);
						train3_train_2.println(line);
						train3_train_4.println(line);
						train3_train_5.println(line);
					}
					else if(((3*train2_size)/5) <= count && count < ((4*train2_size)/5))
					{
						//first test
						train3_test_4.println(line);
						train3_train_1.println(line);
						train3_train_2.println(line);
						train3_train_3.println(line);
						train3_train_5.println(line);
					}
					else if(((4*train2_size)/5) <= count && count < train2_size)
					{
						//first test
						train3_test_5.println(line);
						train3_train_1.println(line);
						train3_train_2.println(line);
						train3_train_3.println(line);
						train3_train_4.println(line);
					}
					else
					{
						train3_train_1.println(line);
						train3_train_2.println(line);
						train3_train_3.println(line);
						train3_train_4.println(line);
						train3_train_5.println(line);
					}
					count++;
				}
			}
			else if(min == train_size)
			{
				String line = new String();
				int count = 0;
				while(true)
				{
					line = train_file.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
						continue;
					if(0 <= count && count < (train_size/5))
					{
						//first test
						train_test_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if((train_size/5) <= count && count < ((2*train_size)/5))
					{
						//first test
						train_test_2.println(line);
						train_train_1.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if(((2*train_size)/5) <= count && count < ((3*train_size)/5))
					{
						//first test
						train_test_3.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if(((3*train_size)/5) <= count && count < ((4*train_size)/5))
					{
						//first test
						train_test_4.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_5.println(line);
					}
					else
					{
						//first test
						train_test_5.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
					}
					
					count++;
				}
				count = 0;
				while(true)
				{
					line = train2_file.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
						continue;
					if(0 <= count && count < (train_size/5))
					{
						//first test
						train2_test_1.println(line);
						train2_train_2.println(line);
						train2_train_3.println(line);
						train2_train_4.println(line);
						train2_train_5.println(line);
					}
					else if((train_size/5) <= count && count < ((2*train_size)/5))
					{
						//first test
						train2_test_2.println(line);
						train2_train_1.println(line);
						train2_train_3.println(line);
						train2_train_4.println(line);
						train2_train_5.println(line);
					}
					else if(((2*train_size)/5) <= count && count < ((3*train_size)/5))
					{
						//first test
						train2_test_3.println(line);
						train2_train_1.println(line);
						train2_train_2.println(line);
						train2_train_4.println(line);
						train2_train_5.println(line);
					}
					else if(((3*train_size)/5) <= count && count < ((4*train_size)/5))
					{
						//first test
						train2_test_4.println(line);
						train2_train_1.println(line);
						train2_train_2.println(line);
						train2_train_3.println(line);
						train2_train_5.println(line);
					}
					else if(((4*train_size)/5) <= count && count < train_size)
					{
						//first test
						train2_test_5.println(line);
						train2_train_1.println(line);
						train2_train_2.println(line);
						train2_train_3.println(line);
						train2_train_4.println(line);
					}
					else
					{
						train2_train_1.println(line);
						train2_train_2.println(line);
						train2_train_3.println(line);
						train2_train_4.println(line);
						train2_train_5.println(line);
					}
					count++;
				}
				count = 0;
				while(true)
				{
					line = train3_file.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
						continue;
					if(0 <= count && count < (train_size/5))
					{
						//first test
						train3_test_1.println(line);
						train3_train_2.println(line);
						train3_train_3.println(line);
						train3_train_4.println(line);
						train3_train_5.println(line);
					}
					else if((train_size/5) <= count && count < ((2*train_size)/5))
					{
						//first test
						train3_test_2.println(line);
						train3_train_1.println(line);
						train3_train_3.println(line);
						train3_train_4.println(line);
						train3_train_5.println(line);
					}
					else if(((2*train_size)/5) <= count && count < ((3*train_size)/5))
					{
						//first test
						train3_test_3.println(line);
						train3_train_1.println(line);
						train3_train_2.println(line);
						train3_train_4.println(line);
						train3_train_5.println(line);
					}
					else if(((3*train_size)/5) <= count && count < ((4*train_size)/5))
					{
						//first test
						train3_test_4.println(line);
						train3_train_1.println(line);
						train3_train_2.println(line);
						train3_train_3.println(line);
						train3_train_5.println(line);
					}
					else if(((4*train_size)/5) <= count && count < train_size)
					{
						//first test
						train3_test_5.println(line);
						train3_train_1.println(line);
						train3_train_2.println(line);
						train3_train_3.println(line);
						train3_train_4.println(line);
					}
					else
					{
						train3_train_1.println(line);
						train3_train_2.println(line);
						train3_train_3.println(line);
						train3_train_4.println(line);
						train3_train_5.println(line);
					}
					count++;
				}
			}
			else if(min == train3_size)
			{
				String line = new String();
				int count = 0;
				while(true)
				{
					line = train3_file.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
						continue;
					if(0 <= count && count < (train3_size/5))
					{
						//first test
						train3_test_1.println(line);
						train3_train_2.println(line);
						train3_train_3.println(line);
						train3_train_4.println(line);
						train3_train_5.println(line);
					}
					else if((train3_size/5) <= count && count < ((2*train3_size)/5))
					{
						//first test
						train3_test_2.println(line);
						train3_train_1.println(line);
						train3_train_3.println(line);
						train3_train_4.println(line);
						train3_train_5.println(line);
					}
					else if(((2*train3_size)/5) <= count && count < ((3*train3_size)/5))
					{
						//first test
						train3_test_3.println(line);
						train3_train_1.println(line);
						train3_train_2.println(line);
						train3_train_4.println(line);
						train3_train_5.println(line);
					}
					else if(((3*train3_size)/5) <= count && count < ((4*train3_size)/5))
					{
						//first test
						train3_test_4.println(line);
						train3_train_1.println(line);
						train3_train_2.println(line);
						train3_train_3.println(line);
						train3_train_5.println(line);
					}
					else
					{
						//first test
						train3_test_5.println(line);
						train3_train_1.println(line);
						train3_train_2.println(line);
						train3_train_3.println(line);
						train3_train_4.println(line);
					}
					
					count++;
				}
				count = 0;
				while(true)
				{
					line = train2_file.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
						continue;
					if(0 <= count && count < (train3_size/5))
					{
						//first test
						train2_test_1.println(line);
						train2_train_2.println(line);
						train2_train_3.println(line);
						train2_train_4.println(line);
						train2_train_5.println(line);
					}
					else if((train3_size/5) <= count && count < ((2*train3_size)/5))
					{
						//first test
						train2_test_2.println(line);
						train2_train_1.println(line);
						train2_train_3.println(line);
						train2_train_4.println(line);
						train2_train_5.println(line);
					}
					else if(((2*train3_size)/5) <= count && count < ((3*train3_size)/5))
					{
						//first test
						train2_test_3.println(line);
						train2_train_1.println(line);
						train2_train_2.println(line);
						train2_train_4.println(line);
						train2_train_5.println(line);
					}
					else if(((3*train3_size)/5) <= count && count < ((4*train3_size)/5))
					{
						//first test
						train2_test_4.println(line);
						train2_train_1.println(line);
						train2_train_2.println(line);
						train2_train_3.println(line);
						train2_train_5.println(line);
					}
					else if(((4*train3_size)/5) <= count && count < train3_size)
					{
						//first test
						train2_test_5.println(line);
						train2_train_1.println(line);
						train2_train_2.println(line);
						train2_train_3.println(line);
						train2_train_4.println(line);
					}
					else
					{
						train2_train_1.println(line);
						train2_train_2.println(line);
						train2_train_3.println(line);
						train2_train_4.println(line);
						train2_train_5.println(line);
					}
					count++;
				}
				count = 0;
				while(true)
				{
					line = train_file.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
						continue;
					if(0 <= count && count < (train3_size/5))
					{
						//first test
						train_test_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if((train3_size/5) <= count && count < ((2*train3_size)/5))
					{
						//first test
						train_test_2.println(line);
						train_train_1.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if(((2*train3_size)/5) <= count && count < ((3*train3_size)/5))
					{
						//first test
						train_test_3.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					else if(((3*train3_size)/5) <= count && count < ((4*train3_size)/5))
					{
						//first test
						train_test_4.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_5.println(line);
					}
					else if(((4*train3_size)/5) <= count && count < train3_size)
					{
						//first test
						train_test_5.println(line);
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
					}
					else
					{
						train_train_1.println(line);
						train_train_2.println(line);
						train_train_3.println(line);
						train_train_4.println(line);
						train_train_5.println(line);
					}
					count++;
				}
			}

			train_test_1.close();
			train_test_2.close();
			train_test_3.close();
			train_test_4.close();
			train_test_5.close();
			train_train_1.close();
			train_train_2.close();
			train_train_3.close();
			train_train_4.close();
			train_train_5.close();
			train2_test_1.close();
			train2_test_2.close();
			train2_test_3.close();
			train2_test_4.close();
			train2_test_5.close();
			train2_train_1.close();
			train2_train_2.close();
			train2_train_3.close();
			train2_train_4.close();
			train2_train_5.close();
			train3_test_1.close();
			train3_test_2.close();
			train3_test_3.close();
			train3_test_4.close();
			train3_test_5.close();
			train3_train_1.close();
			train3_train_2.close();
			train3_train_3.close();
			train3_train_4.close();
			train3_train_5.close();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}

	public void test(String train, String train2, String train3)
	{
		try
		{
			BufferedReader train_file = new BufferedReader(new FileReader("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3  +"/" + "walk_train_4" + ".txt"));
			BufferedReader train2_file = new BufferedReader(new FileReader("src/inputs/multiclass/" + train + "_" + train2 + "_" + train3  +"/" + "bike_train_4" + ".txt"));	
			ArrayList<String> walk = new ArrayList<String>();
			ArrayList<String> bike = new ArrayList<String>();
			String line = new String();
			int count = 0;
			while(true)
			{
				line = train_file.readLine();
				if(line == null)
					break;
				if(line.contains("["))
					walk.add(line);
				count++;
			}
			System.out.println(count);
			count = 0;
			while(true)
			{
				line = train2_file.readLine();
				if(line == null)
					break;
				if(walk.contains(line))
					System.out.println("FUCKED");
				if(line.contains("["))
					bike.add(line);
				count++;
			}
			System.out.println(count);
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
		
	}
	public void print_size(String train, String train2, String train3)
	{
		String dest = "src/inputs/multiclass/" + train + "_" + train2 + "_" + train3  +"/";
		File docDir = new File(dest);
		String[] files = docDir.list();
		
		int count = 0;
		for(int i = 0; i< files.length; i++)
		{
			count = 0;
			try
			{
				BufferedReader fileReader = new BufferedReader(new FileReader(dest + files[i]));
				String line = "";
				while(true)
				{
					line = fileReader.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
						continue;
					count++;
				}
				System.out.println(files[i] + "  " + count);
			}
			catch (Exception e)
			{
				System.out.println(e);
			}
		}
	}
	public static void main(String[] args) throws Exception 
	{
		MASHolder_multiclass_cv ms = new MASHolder_multiclass_cv();
		String train = "bike";
		String train2 = "bus";
		String train3= "car";
		//ms.learn(train, train2, train3);
		//ms.fscore(train, train2, train3);
		//ms.test(train, train2, train3);
		//ms.generate_parts(train, train2, train3);
		//ms.print_size(train, train2, train3);
		//ms.learn(train, train2, train3);
		MASHolder_multiclass_cv ms2 = new MASHolder_multiclass_cv(train, train2, train3);
		ms2.fscore(train, train2, train3);
	}
	public void learn(String train, String train2, String train3) throws Exception
	{
		String dirLocation = "src/inputs/multiclass/" + train + "_" + train2 + "_" + train3 + "/";
		for(int i = 1; i <=5 ; i++)
		{
			EdgeMeanVariance em = new EdgeMeanVariance();
			em.findMeanVarAdulterated(dirLocation, train + "_train_" + i + ".txt");
		}
	}
	public double Radians(double x) {
		return x * PIx / 180;
	}

	public double distance(double lat1, double lon1, double lat2, double lon2) {
		double dlon = Radians(lon2 - lon1);
		double dlat = Radians(lat2 - lat1);

		double a = (Math.sin(dlat / 2) * Math.sin(dlat / 2))
				+ Math.cos(Radians(lat1)) * Math.cos(Radians(lat2))
				* (Math.sin(dlon / 2) * Math.sin(dlon / 2));
		double angle = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return angle * RADIUS;

	}
}