package org.example.anomaly.mas;

import geolife_processing.start_end_anom;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.example.anomaly.data.Point;

public class MASHolder {
	double PIx = 3.141592653589793;
	double RADIUS = 6378.16;
	String logFilePath = "";
	String myOutputFilePathValues1 = "";
	String myOutputFilePathValues2 = "";
	String myOutputFilePathValues3 = "";
	
	String myOutputFilePathTime = "";
	String myOutputFilePathTimeTxt = "";
	String mapMatchedTrajDirPath = "";
	String learnedEdgeDistroFilePath1 = "";
	String learnedEdgeDistroFilePath2 = "";
	String learnedEdgeDistroFilePath3 = "";
	String trajToAnomScoreFilePath1;
	String trajToAnomScoreFilePath2;
	String trajToAnomScoreFilePath3;
	String trajToAnomScoreFilePath_multiclass;

	PrintWriter outValues1/*, outTime, outTimeTxt*/;
	PrintWriter outPercent1;
	
	
	PrintWriter outValues2/*, outTime, outTimeTxt*/;
	PrintWriter outPercent2;
	
	PrintWriter outValues3/*, outTime, outTimeTxt*/;
	PrintWriter outPercent3;
	
	PrintWriter outValues_multiclass;
	
	EdgeProcessing ep1;
	EdgeProcessing ep2;
	EdgeProcessing ep3;
	
	int mined_traj_count;
	int tot_traj_count;
	int training_miss_count;
	int my_count_prithu;
	String test;

	public MASHolder() {

	}

	public MASHolder(String train1, String train2, String test) {
		my_count_prithu = 0;
		mined_traj_count = 0;
		tot_traj_count = 0;
		training_miss_count = 0;
		/*
		mapMatchedTrajDirPath = "src/inputs/car_bus";
		myOutputFilePathTime = "src/comparison_car_13_time.csv";
		myOutputFilePathValues = "src/comparison_car_13_values.txt";
		myOutputFilePathTimeTxt = "src/comparison_car_13_time.txt";
		*/

		// mapMatchedTrajDirPath = "src/inputs/walk_cycle";
		// myOutputFilePathValues = "src/comparison_walk_60_values.txt";

		// mapMatchedTrajDirPath = "src/inputs/walk_cycle";
		// myOutputFilePathValues = "src/comparison_walk.txt";

		mapMatchedTrajDirPath = "/home/pranali/new_workspace/TrajectoryAnomaly/src/inputs";
		// myOutputFilePathValues = "src/comparison_car.txt";

		// learnedEdgeDistroFilePath = "src/car_filename.csv";
		this.test = test;
		mapMatchedTrajDirPath = "src/inputs";
		learnedEdgeDistroFilePath1 = "src/out_"+train1+".csv";
		myOutputFilePathValues1 = "src/outputs/multiclass/comparison_"+train1 +"_" + train2 +"_" + test + "_" + train1 + "_" + 5.0 + "_"
				+ 3.0 + ".txt";
		trajToAnomScoreFilePath1 = "src/outputs/multiclass/trajToAnom_"+ train1 +"_" + train2 +"_" + test + "_" + train1 + ".txt";
		
		
		learnedEdgeDistroFilePath2 = "src/out_"+train2+".csv";
		myOutputFilePathValues2 = "src/outputs/multiclass/comparison_"+train1 +"_" + train2 +"_" + test + "_" + train2 + "_" + 5.0 + "_"
		+ 3.0 + ".txt";
		trajToAnomScoreFilePath2 = "src/outputs/multiclass/trajToAnom_"+ train1 +"_" + train2 +"_" + test + "_" + train2 + ".txt";
		
		
		learnedEdgeDistroFilePath3 = "src/out_"+test+".csv";
		myOutputFilePathValues3 = "src/outputs/multiclass/comparison_"+train1 +"_" + train2 +"_" + test + "_" + test + "_" + 5.0 + "_"
		+ 3.0 + ".txt";
		trajToAnomScoreFilePath3 = "src/outputs/multiclass/trajToAnom_"+ train1 +"_" + train2 +"_" + test + "_" + test + ".txt";
		
		
		trajToAnomScoreFilePath_multiclass = "src/outputs/multiclass/trajToAnom_"+ train1+"_" + train2 + "_" + test  + ".csv";

		try {
			outValues1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(myOutputFilePathValues1), "UTF-8"));
			outPercent1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(trajToAnomScoreFilePath1), "UTF-8"));
			
			outValues2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(myOutputFilePathValues2), "UTF-8"));
			outPercent2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(trajToAnomScoreFilePath2), "UTF-8"));
			
			outValues3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(myOutputFilePathValues3), "UTF-8"));
			outPercent3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(trajToAnomScoreFilePath3), "UTF-8"));
			
			outValues_multiclass = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(trajToAnomScoreFilePath_multiclass), "UTF-8"));
			
			// outTime = new PrintWriter(new OutputStreamWriter(new
			// FileOutputStream(myOutputFilePathTime), "UTF-8"));
			// outTimeTxt = new PrintWriter(new OutputStreamWriter(new
			// FileOutputStream(myOutputFilePathTimeTxt), "UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		ep1 = new EdgeProcessing(learnedEdgeDistroFilePath1, 5.0, 3.0);
		ep2 = new EdgeProcessing(learnedEdgeDistroFilePath2, 5.0, 3.0);
		ep3 = new EdgeProcessing(learnedEdgeDistroFilePath3, 5.0, 3.0);

	}

	public void compare_handler() throws Exception {
		// File docDir = new File(mapMatchedTrajDirPath);
		// String[] files = docDir.list();
		// int fileNum = 0;
		int line_no = 0;
		int total_count = 0;
		// String speed_file_name =
		// "/home/pranali/new_workspace/TrajectoryAnomaly/src/speedtrain_test"
		// + ".csv";
		// System.out.println(speed_file_name);
		// PrintWriter speed_out = new PrintWriter(new OutputStreamWriter(
		// new FileOutputStream(speed_file_name), "UTF-8"));
		// for (fileNum = 0; fileNum < files.length ; fileNum++)
		{
			line_no = 0;
			/*System.out.println("File name : " + files[fileNum]);
			outValues.println("File name : " + files[fileNum]);
			outTime.println("File name : " + files[fileNum]);
			outTimeTxt.println("File name : " + files[fileNum]);
			BufferedReader fileReader = new BufferedReader(new FileReader(
					mapMatchedTrajDirPath + "/" + files[fileNum]));*/
			String file_name = test + ".txt";
			System.out.println("File name : " + mapMatchedTrajDirPath + "/"
					+ file_name);
			outValues1.println("File name : " + file_name);
			outValues2.println("File name : " + file_name);
			// outTime.println("File name : " + file_name);
			// outTimeTxt.println("File name : " + file_name);
			BufferedReader fileReader = new BufferedReader(new FileReader(
					mapMatchedTrajDirPath + "/" + file_name));
			int prev_id = 0;
			double prev_lat = 0.0;
			double prev_lon = 0.0;
			long prev_val = 0;
			Date prev_time = new Date();
			int temp_flag = 0;
			String id_line = new String();
			while (true) {
				prev_id = 0;
				prev_lat = 0.0;
				prev_lon = 0.0;
				prev_val = 0;
				prev_time = new Date();
				ArrayList<Point> arr = new ArrayList<Point>();
				String line = fileReader.readLine();
				if (line == null)
					break;
				// if (name == "car" && my_count_prithu > 50)
				// break;
				// if (name == "walk" && my_count_prithu > 100)
				// break;

				if (!line.contains("[")) {
					{
						if (!line.contains("plt"))
							id_line = line;
						else {
							outValues1.println(id_line + "_" + line);
							//outValues.println(line);
							outPercent1.println(id_line + "_" + line);
							outValues2.println(id_line + "_" + line);
							//outValues.println(line);
							outPercent2.println(id_line + "_" + line);
							
							outValues3.println(id_line + "_" + line);
							outPercent3.println(id_line + "_" + line);
							
							outValues_multiclass.print(id_line + "_" + line);
							
							//outPercent.println(line);
							// System.out.println(id_line);
							// System.out.println(line);
						}
					}
					continue;
				}
				line_no++;
				//if (name == "car" && line_no == 42)
					//continue;
				prev_id = 0;
				prev_lat = 0.0;
				prev_lon = 0.0;
				prev_time = new Date();
				if (line.contains("[")) {
					// speed_out.print(line_no + ",");
					String[] warr = line.split("\\]");
					for (String s : warr) {
						try {
							int id = Integer.parseInt(s.split(",")[0]
									.split("\\[")[1]);
							// double lat =
							// Double.parseDouble(s.split(",")[0].split("\\[")[1]);
							// double lat =
							// Double.parseDouble(s.split(",")[0].split("\\[")[1]);
							double lat = Double.parseDouble(s.split(",")[1]);
							double lon = Double.parseDouble(s.split(",")[2]);
							long val = Long.parseLong(s.split(",")[3]
									.replaceAll("\\s", ""));
							Date date = new Date(val);
							SimpleDateFormat df2 = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							String dateText = df2.format(date);
							Date time = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss").parse(dateText);

							/**
							 * ADDED TO SOLVE STATIONARY AND SAME TIMESTAMP
							 * PROBLEM
							 **/
							// if(! ( (prev_val == val) || (prev_id==id) ))
							// if(! ( (prev_id == id) ))
							arr.add(new Point(id, lat, lon, time, val));
							// else
							{
								/*if(prev_time.equals(time))
								{
									System.out.println("Found the culprits " + line_no + "\n" + line);
									temp_flag = 1;
									System.out.println("*******************************");
									System.out.println("Found the culprits " + line_no + "\n" + prev_id + " "+ prev_lat + " " + prev_lon + " " + prev_val + "  " + prev_time
										+ "\n" + id + " " + lat + " " + lon + " " + val + "  "+ time);
									break;
								}
								*/

							}

							// finding speed
							// if(prev_id == 1)
							// speed_out.print(Math.abs(distance(lat, lon,
							// prev_lat, prev_lon)) * 1000 * 3600 /
							// (Math.abs(prev_val-val)) + ",");

							/**
							 * ADDED TO SOLVE STATIONARY AND SAME TIMESTAMP
							 * PROBLEM
							 **/
							prev_time = time;
							prev_lat = lat;
							prev_lon = lon;
							prev_id = id;
							prev_val = val;
						} catch (Exception e) {
							System.out.println(e);
						}
					}
					// speed_out.println();
					if (temp_flag == 1)
						break;
					// System.out.println(arr.size());
					/*if(line_no == 1)
					{
						for(int x = 0 ; x < arr.size() - 1; x++)
							System.out.println(arr.get(x).id + "  " + arr.get(x+1).id + "  " + (arr.get(x + 1).timeStamp.getTime()- arr.get(x).timeStamp.getTime() ));
					}*/
					total_count++;
					// if(line_no ==1)
					{
						// System.out.println(line);
						// System.out.println(line);
						compare(arr, line_no, warr.length - 1);
						// break;
					}
				}

			}
			// System.out.println(ep.anom_time);
			fileReader.close();
			outValues1.println(" $$$$$$$$$$ ");
			outValues2.println(" $$$$$$$$$$ ");
			outValues3.println(" $$$$$$$$$$ ");
			// outTime.println(" $$$$$$$$$$ " + "," + " $$$$$$$$$$ ");
			// outTimeTxt.println(" $$$$$$$$$$ ");
			/*System.out.println("Mined trajectory count : " + mined_traj_count
					+ " Missed trajectory Count : " + training_miss_count
					+ " Total trajectory Count : " + tot_traj_count); */
		}
		// outTime.close();
		// outTimeTxt.close();
		outValues1.close();
		outPercent1.close();
		outValues2.close();
		outPercent2.close();
		
		outValues3.close();
		outPercent3.close();
		outValues_multiclass.close();
	}

	public void compare(ArrayList<Point> pt, int line_no, int size)
			throws Exception {
		long tot1 = 0;
		long tot2 = 0;
		long tot3 = 0;
		List<Integer> nodeSequence = new ArrayList<Integer>();
		List<Double> travelTime = new ArrayList<Double>();
		for (int i = 0; i < pt.size(); i++) {
			nodeSequence.add(pt.get(i).id);
			if (i < (pt.size() - 1)) {
				double time = pt.get(i + 1).mytime - pt.get(i).mytime;
				// double time = pt.get(i + 1).timeStamp.getTime()-
				// pt.get(i).timeStamp.getTime();
				// if((pt.get(i+1).id == 201398 && pt.get(i).id == 201403) ||
				// (pt.get(i).id == 201398 && pt.get(i+1).id == 201403))
				// System.out.println( " check " + time);
				if (time < 0.01) {
					// System.out.println("less time " + time + "  " + pt.get(i
					// + 1).id + "  " + pt.get(i).id );
					// System.out.println("Found the culprits " + line_no + "\n"
					// + prev_id + " "+ prev_lat + " " + prev_lon + " " +
					// prev_val + "  " + prev_time
					// + "\n" + id + " " + lat + " " + lon + " " + val + "  "+
					// time);
				}
				travelTime.add(time);
			}
		}
		ep1.setNodeTime(nodeSequence, travelTime);
		ep2.setNodeTime(nodeSequence, travelTime);
		ep3.setNodeTime(nodeSequence, travelTime);
		
		ArrayList<Integer> not_in_training1 = new ArrayList<Integer>();
		ArrayList<Integer> not_in_training2 = new ArrayList<Integer>();
		ArrayList<Integer> not_in_training3 = new ArrayList<Integer>();
		
		if (ep1.notInTraining(not_in_training1)) 
		{
			// System.out.println("missed");
			outValues1.println("missed once " + line_no);
			System.out.println("missed once " + line_no);
			// outValues.println("*****************************************");

			training_miss_count++;
		}
		if (ep2.notInTraining(not_in_training2)) 
		{
			// System.out.println("missed");
			outValues2.println("missed once " + line_no);
			System.out.println("missed once " + line_no);
			// outValues.println("*****************************************");

			training_miss_count++;
		}
		if (ep3.notInTraining(not_in_training3)) 
		{
			// System.out.println("missed");
			outValues3.println("missed once " + line_no);
			System.out.println("missed once " + line_no);
			// outValues.println("*****************************************");

			training_miss_count++;
		}
		
		// else
		{
			// System.out.println("not missed");
			// ep.howManyAno();
			ArrayList<Integer> anoms1 = new ArrayList<Integer>();
			int count1 = ep1.howManyAno(anoms1);
			outValues1.println(anoms1);
			outValues1.println(" Ano count : " + count1);
			
			
			ArrayList<Integer> anoms2 = new ArrayList<Integer>();
			int count2 = ep2.howManyAno(anoms2);
			outValues1.println(anoms2);
			outValues1.println(" Ano count : " + count2);
			
			
			ArrayList<Integer> anoms3 = new ArrayList<Integer>();
			int count3 = ep3.howManyAno(anoms3);
			outValues1.println(anoms3);
			outValues1.println(" Ano count : " + count3);
			

			long start = System.currentTimeMillis();
			ArrayList<String> our_result1 = new ArrayList<String>();
			ArrayList<String> our_result2 = new ArrayList<String>();
			ArrayList<String> our_result3 = new ArrayList<String>();
			
			try {
				our_result1 = ep1.findAnomalyOur();
				our_result2 = ep2.findAnomalyOur();
				our_result3 = ep3.findAnomalyOur();
			} catch (Exception e) {
				System.out.println("Not there in training : our");
				e.printStackTrace();
				return;
			}
			tot1 += (System.currentTimeMillis() - start);

			start = System.currentTimeMillis();

			ArrayList<String> naive_result = new ArrayList<String>();
			// System.out.println(line_no + " $ " + our_result + " $ " +
			// naive_result );
			try {
				// naive_result = ep.findAnomalyNaive();
			} catch (Exception e) {
				System.out.println("Not there in training :  naive");
				e.printStackTrace();
				return;
			}
			tot2 += (System.currentTimeMillis() - start);
			/*start = System.currentTimeMillis();
			ArrayList<String> naive_result2 = new ArrayList<String>();
			try 
			{
				naive_result2 = ep.findAnomalyNaive2();
			} 
			catch (Exception e) 
			{
				System.out.println("Not there in training");
				e.printStackTrace();
				return;
			}
			tot3 += System.currentTimeMillis() - start; */
			outValues1.println(line_no + " $ " + our_result1 + " $ " + size
					+ " $ " /*+ not_in_training.size()*/) /* +  naive_result + " $ " + naive_result2*/;
			outValues2.println(line_no + " $ " + our_result2 + " $ " + size
					+ " $ " /*+ not_in_training.size()*/) /* +  naive_result + " $ " + naive_result2*/;
			outValues3.println(line_no + " $ " + our_result3 + " $ " + size
					+ " $ " /*+ not_in_training.size()*/) /* +  naive_result + " $ " + naive_result2*/;
			
			System.out.println(line_no + " $ " + our_result1 + " $ " + (size)
					+ " $ " /*+ not_in_training.size()*/);
			System.out.println(line_no + " $ " + our_result2 + " $ " + (size)
					+ " $ " /*+ not_in_training.size()*/);
			System.out.println(line_no + " $ " + our_result3 + " $ " + (size)
					+ " $ " /*+ not_in_training.size()*/);
			
			System.out.println("count: " + my_count_prithu++);

			
			
			
			//PERCENT CALCULATION FOR CLASS 1
			
			ArrayList<start_end_anom> sub_trajcs1 = new ArrayList<start_end_anom>();
			int total_anom_count1 = 0;
			ArrayList<Integer> visited1 = new ArrayList<Integer>();
			for (int i = 0; i < size; i++)
				visited1.add(0);
			for (int x = 0; x < our_result1.size(); x++) 
			{
				start_end_anom obj = new start_end_anom();
				String s = our_result1.get(x);
				StringTokenizer stt = new StringTokenizer(s, ",");
				obj.start = Integer.parseInt(stt.nextToken().toString());
				obj.end = Integer.parseInt(stt.nextToken().toString());
				for (int z = obj.start; z <= obj.end; z++) 
				{
					if (visited1.get(z) == 0) // unvisited
					{
						total_anom_count1++;
						visited1.set(z, 1);
					}
				}

				sub_trajcs1.add(obj);
			}
			for (int x = 0; x < not_in_training1.size(); x++) 
			{
				for (int y = 0; y < sub_trajcs1.size(); y++) 
				{
					if (sub_trajcs1.get(y).start <= not_in_training1.get(x)
							&& sub_trajcs1.get(y).end >= not_in_training1.get(x)) 
					{
						total_anom_count1 = total_anom_count1 - 1;
						break;
					}
				}
			}

			int trajSize1 = size - not_in_training1.size();
			outValues1
					.println("Size of trajectory after removing missing ones : "
							+ trajSize1);
			outValues1
					.println("Anom size after removing missing ones from our_result : "
							+ total_anom_count1);
			if (trajSize1 > 0)
			{
				outPercent1.println((double) total_anom_count1 / trajSize1);
				outValues_multiclass.print("," + (double) (total_anom_count1 +  not_in_training1.size())/*/ trajSize1*/);
			}
			else
			{
				outPercent1.println("Size zero!!");
				outValues_multiclass.print("," + "Size zero!!");
			}
			
			
			outValues1
			.println("Size of trajectory after removing missing ones : "
					+ trajSize1);
			outValues1
			.println("Anom size after removing missing ones from our_result : "
					+ total_anom_count1);
			
			System.out
			.println("1 :Size of trajectory after removing missing ones : "
					+ (size - not_in_training1.size()));
			System.out
			.println("1 : Anom size after removing missing ones from our_result : "
					+ total_anom_count1);
			
			
			//PERCENT CALCULATION FOR CLASS 2
			
			
			ArrayList<start_end_anom> sub_trajcs2 = new ArrayList<start_end_anom>();
			int total_anom_count2 = 0;
			ArrayList<Integer> visited2 = new ArrayList<Integer>();
			for (int i = 0; i < size; i++)
				visited2.add(0);
			for (int x = 0; x < our_result2.size(); x++) 
			{
				start_end_anom obj = new start_end_anom();
				String s = our_result2.get(x);
				StringTokenizer stt = new StringTokenizer(s, ",");
				obj.start = Integer.parseInt(stt.nextToken().toString());
				obj.end = Integer.parseInt(stt.nextToken().toString());
				for (int z = obj.start; z <= obj.end; z++) 
				{
					if (visited2.get(z) == 0) // unvisited
					{
						total_anom_count2++;
						visited2.set(z, 1);
					}
				}

				sub_trajcs2.add(obj);
			}
			for (int x = 0; x < not_in_training2.size(); x++) 
			{
				for (int y = 0; y < sub_trajcs2.size(); y++) 
				{
					if (sub_trajcs2.get(y).start <= not_in_training2.get(x)
							&& sub_trajcs2.get(y).end >= not_in_training2.get(x)) 
					{
						total_anom_count2 = total_anom_count2 - 1;
						break;
					}
				}
			}
			
			
			
			
			int trajSize2 = size - not_in_training2.size();
			if (trajSize2 > 0)
			{
				outPercent2.println((double) total_anom_count2 / trajSize2);
				outValues_multiclass.print("," + (double) (total_anom_count2 +  not_in_training2.size())/*/ trajSize1*/);
			}
			else
			{
				outPercent2.println("Size zero!!");
				outValues_multiclass.print("," + "Size zero!!");
			}
				
				
				
				System.out
				.println("2 :Size of trajectory after removing missing ones : "
						+ (size - not_in_training2.size()));
				System.out
				.println("2 : Anom size after removing missing ones from our_result : "
						+ total_anom_count2);
				
				
				
			//PERCENT CALCULATION FOR CLASS 3
				
				ArrayList<start_end_anom> sub_trajcs3 = new ArrayList<start_end_anom>();
				int total_anom_count3 = 0;
				ArrayList<Integer> visited3 = new ArrayList<Integer>();
				for (int i = 0; i < size; i++)
					visited3.add(0);
				for (int x = 0; x < our_result3.size(); x++) 
				{
					start_end_anom obj = new start_end_anom();
					String s = our_result3.get(x);
					StringTokenizer stt = new StringTokenizer(s, ",");
					obj.start = Integer.parseInt(stt.nextToken().toString());
					obj.end = Integer.parseInt(stt.nextToken().toString());
					for (int z = obj.start; z <= obj.end; z++) 
					{
						if (visited3.get(z) == 0) // unvisited
						{
							total_anom_count3++;
							visited3.set(z, 1);
						}
					}

					sub_trajcs3.add(obj);
				}
				for (int x = 0; x < not_in_training3.size(); x++) 
				{
					for (int y = 0; y < sub_trajcs3.size(); y++) 
					{
						if (sub_trajcs3.get(y).start <= not_in_training3.get(x)
								&& sub_trajcs3.get(y).end >= not_in_training3.get(x)) 
						{
							total_anom_count3 = total_anom_count3 - 1;
							break;
						}
					}
				}

				int trajSize3 = size - not_in_training3.size();
				outValues3
						.println("Size of trajectory after removing missing ones : "
								+ trajSize3);
				outValues3
						.println("Anom size after removing missing ones from our_result : "
								+ total_anom_count3);
				if (trajSize3 > 0)
				{
					outPercent3.println((double) total_anom_count3 / trajSize3);
					outValues_multiclass.println("," + (double) (total_anom_count3 +  not_in_training3.size())/*/ trajSize1*/);
				}
				else
				{
					outPercent3.println("Size zero!!");
					outValues_multiclass.println("," + "Size zero!!");
				}
				
				
				outValues3
				.println("Size of trajectory after removing missing ones : "
						+ trajSize3);
				outValues3
				.println("Anom size after removing missing ones from our_result : "
						+ total_anom_count3);
				
				System.out
				.println("1 :Size of trajectory after removing missing ones : "
						+ (size - not_in_training3.size()));
				System.out
				.println("1 : Anom size after removing missing ones from our_result : "
						+ total_anom_count3);
				

			// System.out.println(naive_result);
			outValues1.println("*****************************************");
			outValues2.println("*****************************************");
			outValues3.println("*****************************************");
			//outValues_multiclass.println("*****************************************");
			outValues1.flush();
			outValues2.flush();
			outValues3.flush();
			
			outValues_multiclass.flush();
			System.out.println("*****************************************");
			// outTime.println(pt.size() + "," + ep.howManyAno() + "," + tot1 +
			// "," + tot2 + "," + tot3);
			// outTimeTxt.println(line_no + " $ " + tot1 + " $ " + tot2 + " $ "
			// + tot3);

			/*if (our_result.size() == naive_result.size()) 
			{
				int flag = 0;
				for (int i = 0; i < our_result.size(); i++) 
				{
					if (!our_result.get(i).equals(naive_result.get(i))) 
					{
						flag = 1;
						break;
					}
				}
				if (flag == 0)
				{
					mined_traj_count++;
				}
			}*/

		}
		tot_traj_count++;

	}

	public static void main(String[] args) throws Exception {
		/*for (int i = 1; i <= 1; i++) 
		{
			ScanThread sc = new ScanThread(i);
			sc.start();
		}*/
		// double[] threshold = new double[1];
		// threshold[0] = 6.0;
		// // threshold[1] = 4.0;
		// // threshold[2] = 5.0;
		// // threshold[3] = 6.0;
		// double[] smooth = new double[1];
		// smooth[0] = 3.0;
		// // smooth[1] = 2.0;
		// // smooth[2] = 3.0;
		// // smooth[3] = 4.0;
		String[] names = new String[4];
		names[0] = "bike";
		names[1] = "bus";
		names[2] = "car";
		names[3] = "walk";

		// for (double d1 : threshold) {
		// for (double d2 : smooth) {
		for (String train1 : names)
		{
			for(String train2 : names)
			{
				for (String test : names) 
				{
					if(!train1.equals(train2) && !train1.equals(test) && !train2.equals(test))
					{
						MASHolder ms = new MASHolder(train1, train2, test);
						ms.compare_handler();
						System.out.println("-----------------------------------------");
					}
				}
			}
		}

	}

	public double Radians(double x) {
		return x * PIx / 180;
	}

	public double distance(double lat1, double lon1, double lat2, double lon2) {
		double dlon = Radians(lon2 - lon1);
		double dlat = Radians(lat2 - lat1);

		double a = (Math.sin(dlat / 2) * Math.sin(dlat / 2))
				+ Math.cos(Radians(lat1)) * Math.cos(Radians(lat2))
				* (Math.sin(dlon / 2) * Math.sin(dlon / 2));
		double angle = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return angle * RADIUS;

	}

}