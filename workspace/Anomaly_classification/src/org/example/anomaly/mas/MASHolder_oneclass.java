package org.example.anomaly.mas;

import geolife_processing.start_end_anom;

import java.beans.FeatureDescriptor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import org.example.anomaly.data.Point;
import org.example.anomaly.util.EdgeMeanVariance;

public class MASHolder_oneclass 
{
	double PIx = 3.141592653589793;
	double RADIUS = 6378.16;
	
	
	String mapMatchedTrajDirPath = "";
	String learnedEdgeDistroFilePath = "";
	
	PrintWriter outValues;
	boolean modify;
	Hashtable<String, Double> new_speeds = new Hashtable<String, Double>();
	
	EdgeProcessing ep1;
	
	public static final Hashtable<String, Integer> sizes = new Hashtable<String, Integer>();

	public MASHolder_oneclass() 
	{
		this.sizes.put("bike", 649);
		this.sizes.put("bus", 512);
		this.sizes.put("car", 569);
		this.sizes.put("walk", 1262);
	}

	public MASHolder_oneclass(String train, String train2) throws Exception 
	{
		String dest = "src/inputs/oneclass/" + train + "_" + train2 +"/";
		String source = "src/inputs/oneclass/" + train + "_" + train2 +"/";
		mapMatchedTrajDirPath = source;
		this.sizes.put("bike", 649);
		this.sizes.put("bus", 512);
		this.sizes.put("car", 569);
		this.sizes.put("walk", 1262);
		//validation
		ArrayList<Double> optimal_thresholds = this.validation(train, train2);
		//test
		
		
		
		double threshold = 5.0;
		double smoothing = 3.0;
		for(int i =  1 ; i <=5 ; i++)
		{
			try
			{
				{
					learnedEdgeDistroFilePath = source + "out_" + train + "_" + "train_" + i + ".csv"; 
					ep1 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, smoothing);
					//outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "percent" +"_" + threshold +"_" + train + "_" + i + ".csv"), "UTF-8"));
					outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "percent" +"_" + train + "_" + i + ".csv"), "UTF-8"));
					outValues.println("size" +"," + "out_" + train + "_" + "train_" + i + ".csv"
							+ "," + "union anom edges " + train
							+ "," + "seedset anom edges " + train 
							+ "," + "missing edges " + train
							);
					System.out.println("Train 1 : " + "out_" + train + "_" + "train_" + i + ".csv");
					System.out.println("Train 2 : " + "out_" + train2 + "_" + "train_" + i + ".csv");
					System.out.println("Test : " + train2 + "_" + "test_" + i + ".txt");
					modify = true;
					compare_handler(train + "_" + "test_" + i + ".txt");
					outValues.close();
				}
				{
					learnedEdgeDistroFilePath = source + "out_" + train + "_" + "train_" + i + ".csv"; 
					ep1 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, smoothing);
					//outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest+"percent" + "_" + threshold +"_" + test + "_" + i + ".csv"), "UTF-8"));
					outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "percent" +"_" + train2 + "_" + i + ".csv"), "UTF-8"));
					outValues.println("size" +"," + "out_" + train + "_" + "train_" + i + ".csv"
							+ "," + "union anom edges " + train
							+ "," + "seedset anom edges " + train
							+ "," + "missing edges " + train
							);
					System.out.println("Train 1 : " + "out_" + train + "_" + "train_" + i + ".csv");
					System.out.println("Train 2 : " + "out_" + train2 + "_" + "train_" + i + ".csv");
					System.out.println("Test : " + train2 + "_" + "test_" + i + ".txt");
					modify = false;
					compare_handler(train2 + "_" + "test_" + i + ".txt");
					outValues.close();
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
		System.out.println("Optimal : " + optimal_thresholds);
		//ArrayList<Double> optimal_thresholds = new ArrayList<Double>();
		//0.9900000000000007, 0.8000000000000005, 0.3100000000000001, 0.8300000000000005, 0.38000000000000017
		//optimal_thresholds.add(0.99);
		//optimal_thresholds.add(0.80);
		//optimal_thresholds.add(0.31);
		//optimal_thresholds.add(0.83);
		//optimal_thresholds.add(0.38);
		fscore(train, train2, optimal_thresholds);
		
	}

	
	public ArrayList<Double> validation(String train, String train2) throws Exception
	{
		double threshold = 5.0;
		double smoothing = 3.0;
		String dest = "src/inputs/oneclass/" + train + "_" + train2 +"/";
		String source = "src/inputs/oneclass/" + train + "_" + train2 +"/";
		ArrayList<Double> optimal_thresholds = new ArrayList<Double>();
		for(int i =  1 ; i <=5 ; i++)
		{
			try
			{
				
				{
					learnedEdgeDistroFilePath = source + "out_" + train + "_" + "train_" + i + ".csv"; 
					ep1 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, smoothing);
					//outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "percent" +"_" + threshold +"_" + train + "_" + i + ".csv"), "UTF-8"));
					outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "percent" +"_" +"valid_" + train + "_" + i + ".csv"), "UTF-8"));
					outValues.println("size" +"," + "out_" + train + "_" + "train_" + i + ".csv"
							+ "," + "union anom edges " + train
							+ "," + "seedset anom edges " + train 
							+ "," + "missing edges " + train
							);
					System.out.println("Train 1 : " + "out_" + train + "_" + "train_" + i + ".csv");
					System.out.println("Test : " + train + "_" + "valid_" + i + ".txt");
					modify = true;
					compare_handler(train + "_" + "valid_" + i + ".txt");
					outValues.close();
				}
				{
					learnedEdgeDistroFilePath = source + "out_" + train + "_" + "train_" + i + ".csv"; 
					ep1 = new EdgeProcessing(learnedEdgeDistroFilePath, threshold, smoothing);
					//outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest+"percent" + "_" + threshold +"_" + test + "_" + i + ".csv"), "UTF-8"));
					outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(dest + "percent" +"_" +"valid_" + train2 + "_" + i + ".csv"), "UTF-8"));
					outValues.println("size" +"," + "out_" + train + "_" + "train_" + i + ".csv"
							+ "," + "union anom edges " + train
							+ "," + "seedset anom edges " + train
							+ "," + "missing edges " + train
							);
					System.out.println("Train 1 : " + "out_" + train + "_" + "train_" + i + ".csv");
					System.out.println("Test : " + train2 + "_" + "valid_" + i + ".txt");
					modify = false;
					compare_handler(train2 + "_" + "valid_" + i + ".txt");
					outValues.close();
				}
				double optimal_threshold = get_threshold(train, train2, i);
				optimal_thresholds.add(optimal_threshold);
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
		return optimal_thresholds;
	}
	
	public void compare_handler(String testfile) throws Exception 
	{
		int line_no = 0;
		int total_count = 0;
		{
			line_no = 0;
			System.out.println("File name : " + testfile);
			BufferedReader fileReader = new BufferedReader(new FileReader( mapMatchedTrajDirPath + testfile));
			int prev_id = 0;
			double prev_lat = 0.0;
			double prev_lon = 0.0;
			long prev_val = 0;
			Date prev_time = new Date();
			while (true) 
			{
				ArrayList<Point> arr = new ArrayList<Point>();
				String line = fileReader.readLine();
				if (line == null)
					break;
				line_no++;
				prev_id = -1;
				prev_lat = 0.0;
				prev_lon = 0.0;
				prev_time = new Date();
				double speed = 0.0;
				if (line.contains("[")) 
				{
					String[] warr = line.split("\\]");
					for (String s : warr) 
					{
						try 
						{
							int id = Integer.parseInt(s.split(",")[0]
									.split("\\[")[1]);
							double lat = Double.parseDouble(s.split(",")[1]);
							double lon = Double.parseDouble(s.split(",")[2]);
							long val = Long.parseLong(s.split(",")[3]
									.replaceAll("\\s", ""));
							Date date = new Date(val);
							SimpleDateFormat df2 = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							String dateText = df2.format(date);
							Date time = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss").parse(dateText);

							arr.add(new Point(id, lat, lon, time, val));
							if(prev_id!=-1 && modify == true)
							{
								speed =  Math.abs(distance(lat, lon,prev_lat, prev_lon)) * 1000 * 3600 / (Math.abs(prev_val-val));
								if(speed > 10.0)
								{
									new_speeds.put(id + "_" + prev_id, Math.abs(distance(lat, lon,prev_lat, prev_lon)) * 1000 * 3600 );
									new_speeds.put(prev_id + "_" + id, Math.abs(distance(lat, lon,prev_lat, prev_lon)) * 1000 * 3600 );
								}
							}
							prev_time = time;
							prev_lat = lat;
							prev_lon = lon;
							prev_id = id;
							prev_val = val;
						} 
						catch (Exception e) 
						{
							System.out.println(e);
						}
					}
					total_count++;
					outValues.print(warr.length - 1 + ",");
					compare(arr, line_no, warr.length - 1);
				}
			}
			fileReader.close();
		}
	}

	public void compare(ArrayList<Point> pt, int line_no, int size) throws Exception 
	{
		long tot1 = 0;
		long tot2 = 0;
		long tot3 = 0;
		List<Integer> nodeSequence = new ArrayList<Integer>();
		List<Double> travelTime = new ArrayList<Double>();
		for (int i = 0; i < pt.size(); i++) 
		{
			nodeSequence.add(pt.get(i).id);
			if (i < (pt.size() - 1)) 
			{
				double time = pt.get(i + 1).mytime - pt.get(i).mytime;
				if(modify == true && (new_speeds.get(pt.get(i+1).id + "_" + pt.get(i).id)!=null))
				{
					time = new_speeds.get(pt.get(i+1).id + "_" + pt.get(i).id) / 10;
				}
				travelTime.add(time);
			}
		}
		ep1.setNodeTime(nodeSequence, travelTime);
		
		ArrayList<Integer> not_in_training1 = new ArrayList<Integer>();
		ep1.notInTraining(not_in_training1); 
		
		
		ArrayList<Integer> anoms1 = new ArrayList<Integer>();
		int count1 = ep1.howManyAno(anoms1);
		
		
		ArrayList<String> our_result1 = new ArrayList<String>();
		try 
		{
			our_result1 = ep1.findAnomalyOur();
		} 
		catch (Exception e) 
		{
			System.out.println("Not there in training : our");
			e.printStackTrace();
			return;
		}
		/*System.out.println(line_no + " $ " + our_result1 + " $ " + size
				+ " $ " + not_in_training1.size()) /* +  naive_result + " $ " + naive_result2*/;
		/*System.out.println(line_no + " $ " + our_result2 + " $ " + (size)
				+ " $ " + not_in_training2.size()); */

		ArrayList<start_end_anom> sub_trajcs1 = new ArrayList<start_end_anom>();
		int total_anom_count1 = 0;
		ArrayList<Integer> visited1 = new ArrayList<Integer>();
		for (int i = 0; i < size; i++)
			visited1.add(0);
		for (int x = 0; x < our_result1.size(); x++) {
			start_end_anom obj = new start_end_anom();
			String s = our_result1.get(x);
			StringTokenizer stt = new StringTokenizer(s, ",");
			obj.start = Integer.parseInt(stt.nextToken().toString());
			obj.end = Integer.parseInt(stt.nextToken().toString());
			for (int z = obj.start; z <= obj.end; z++) 
			{
				if (visited1.get(z) == 0) // unvisited
				{
					total_anom_count1++;
					visited1.set(z, 1);
				}
			}

			sub_trajcs1.add(obj);
		}
		
		for (int x = 0; x < not_in_training1.size(); x++) 
		{
			for (int y = 0; y < sub_trajcs1.size(); y++) 
			{
				if (sub_trajcs1.get(y).start <= not_in_training1.get(x)
						&& sub_trajcs1.get(y).end >= not_in_training1.get(x)) {
					total_anom_count1 = total_anom_count1 - 1;
					break;
				}
			}
		}

		int trajSize1 = size - not_in_training1.size();
		if (size > 0)
			outValues.print((double) (total_anom_count1 + not_in_training1.size()) / size);
		else
			outValues.print("Size zero!!");
		outValues.println("," + total_anom_count1  + "," + count1 + "," + not_in_training1.size());

	}

	
	public double get_threshold(String train, String train2, int j)
	{
		String dest = "src/inputs/oneclass/" + train + "_" + train2 +"/";
		double optimal = 0.0;
		try
		{
			BufferedReader fileReader = new BufferedReader(new FileReader(dest + "percent" +"_" +"valid_" + train + "_" + j + ".csv") );
			BufferedReader fileReader2 = new BufferedReader(new FileReader(dest + "percent" +"_" +"valid_" + train2 + "_" + j + ".csv") );
			ArrayList<Double> train_values = new ArrayList<Double>();
			ArrayList<Double> train2_values = new ArrayList<Double>();
			Hashtable<String, Integer> tp = new Hashtable<String, Integer>();
			Hashtable<String, Integer> fp = new Hashtable<String, Integer>();
			Hashtable<String, Integer> fn = new Hashtable<String, Integer>();
			Hashtable<String, Double> p = new Hashtable<String, Double>();
			Hashtable<String, Double> r = new Hashtable<String, Double>();
			Hashtable<String, Double> f1 = new Hashtable<String, Double>();
			String line = new String();
			double avg = 0.0;
			while(true)
			{
				line = fileReader.readLine();
				if(line == null)
					break;
				if(line.contains("size") || line.contains("Size"))
					continue;
				StringTokenizer stt = new StringTokenizer(line, ",");
				String temp = stt.nextToken().toString();
				double val = Double.parseDouble(stt.nextToken().toString());
				train_values.add(val);
				avg += val;
			}
			avg = (double)avg / train_values.size();
			double avg2 = 0.0;
			while(true)
			{
				line = fileReader2.readLine();
				if(line == null)
					break;
				if(line.contains("size") || line.contains("Size"))
					continue;
				StringTokenizer stt = new StringTokenizer(line, ",");
				String temp = stt.nextToken().toString();
				double val = Double.parseDouble(stt.nextToken().toString());
				train2_values.add(val);
				avg2 += val;
			}
			avg2 = (double)avg2 / train2_values.size();
			double max_fscore = -1.0;
			System.out.print("Avg 1 : " + avg + " Avg2 : " + avg2);
			double min, max;
			if(avg < avg2)
			{
				min = avg;
				max = avg2;
			}
			else
			{
				min = avg2;
				max = avg;
			}
			for(double threshold = min; threshold <= max; threshold = threshold + 0.01)
			{
				tp.put(train, 0);
				tp.put(train2, 0);
				fp.put(train, 0);
				fp.put(train2, 0);
				fn.put(train, 0);
				fn.put(train2, 0);
				p.put(train, 0.0);
				p.put(train2, 0.0);
				r.put(train, 0.0);
				r.put(train2, 0.0);
				f1.put(train, 0.0);
				f1.put(train2, 0.0);
				
				for(int i = 0; i < train_values.size(); i++)
				{
					if(train_values.get(i) <= threshold)
					{
						tp.put(train, tp.get(train)+1);
					}
					else
					{
						fn.put(train, fn.get(train)+1);
						fp.put(train2, fp.get(train2)+1);
					}
				}
				for(int i = 0; i < train2_values.size(); i++)
				{
					if(train2_values.get(i) > threshold)
					{
						tp.put(train2, tp.get(train2)+1);
					}
					else
					{
						fn.put(train2, fn.get(train2)+1);
						fp.put(train, fp.get(train)+1);
					}
				}
				p.put(train, ((double)(tp.get(train))/(tp.get(train) + fp.get(train))));
				p.put(train2, ((double)(tp.get(train2))/(tp.get(train2) + fp.get(train2))));
				
				r.put(train, ((double)(tp.get(train))/(tp.get(train) + fn.get(train))));
				r.put(train2, ((double)(tp.get(train2))/(tp.get(train2) + fn.get(train2))));
				
				f1.put(train, ((double)(2*p.get(train)*r.get(train))/(r.get(train) + p.get(train))));
				f1.put(train2, ((double)(2*p.get(train2)*r.get(train2))/(r.get(train2) + p.get(train2))));
				
				//positive class is train class.
				if(f1.get(train) > max_fscore)
				{
					max_fscore = f1.get(train);
					optimal = threshold;
				}
			}
			System.out.println("  Optimal : " + optimal);
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		return optimal;
	}
	
	
	public void fscore(String train, String train2, ArrayList<Double> optimal_thresholds )
	{
		String dest = "src/inputs/oneclass/" + train + "_" + train2 +"/";
		Hashtable<String, ArrayList<Double>> all_fscore = new Hashtable<String, ArrayList<Double>>();
		all_fscore.put(train, new ArrayList<Double>());
		all_fscore.put(train2, new ArrayList<Double>());
		try
		{
			for(int j = 1 ;j <=5; j++)
			{
				BufferedReader fileReader = new BufferedReader(new FileReader(dest + "percent" +"_"  + train + "_" + j + ".csv") );
				BufferedReader fileReader2 = new BufferedReader(new FileReader(dest + "percent" +"_" + train2 + "_" + j + ".csv") );
				ArrayList<Double> train_values = new ArrayList<Double>();
				ArrayList<Double> train2_values = new ArrayList<Double>();
				Hashtable<String, Integer> tp = new Hashtable<String, Integer>();
				Hashtable<String, Integer> fp = new Hashtable<String, Integer>();
				Hashtable<String, Integer> fn = new Hashtable<String, Integer>();
				Hashtable<String, Double> p = new Hashtable<String, Double>();
				Hashtable<String, Double> r = new Hashtable<String, Double>();
				Hashtable<String, Double> f1 = new Hashtable<String, Double>();
				String line = new String();
				while(true)
				{
					line = fileReader.readLine();
					if(line == null)
						break;
					if(line.contains("size") || line.contains("Size"))
						continue;
					StringTokenizer stt = new StringTokenizer(line, ",");
					String temp = stt.nextToken().toString();
					train_values.add(Double.parseDouble(stt.nextToken().toString()));
				}
				while(true)
				{
					line = fileReader2.readLine();
					if(line == null)
						break;
					if(line.contains("size") || line.contains("Size"))
						continue;
					StringTokenizer stt = new StringTokenizer(line, ",");
					String temp = stt.nextToken().toString();
					train2_values.add(Double.parseDouble(stt.nextToken().toString()));
				}
					
				double threshold = optimal_thresholds.get(j - 1);
				tp.put(train, 0);
				tp.put(train2, 0);
				fp.put(train, 0);
				fp.put(train2, 0);
				fn.put(train, 0);
				fn.put(train2, 0);
				p.put(train, 0.0);
				p.put(train2, 0.0);
				r.put(train, 0.0);
				r.put(train2, 0.0);
				f1.put(train, 0.0);
				f1.put(train2, 0.0);
				
				for(int i = 0; i < train_values.size(); i++)
				{
					if(train_values.get(i) <= threshold)
					{
						tp.put(train, tp.get(train)+1);
					}
					else
					{
						fn.put(train, fn.get(train)+1);
						fp.put(train2, fp.get(train2)+1);
					}
				}
				for(int i = 0; i < train2_values.size(); i++)
				{
					if(train2_values.get(i) > threshold)
					{
						tp.put(train2, tp.get(train2)+1);
					}
					else
					{
						fn.put(train2, fn.get(train2)+1);
						fp.put(train, fp.get(train)+1);
					}
				}
				p.put(train, ((double)(tp.get(train))/(tp.get(train) + fp.get(train))));
				p.put(train2, ((double)(tp.get(train2))/(tp.get(train2) + fp.get(train2))));
				
				r.put(train, ((double)(tp.get(train))/(tp.get(train) + fn.get(train))));
				r.put(train2, ((double)(tp.get(train2))/(tp.get(train2) + fn.get(train2))));
				
				f1.put(train, ((double)(2*p.get(train)*r.get(train))/(r.get(train) + p.get(train))));
				f1.put(train2, ((double)(2*p.get(train2)*r.get(train2))/(r.get(train2) + p.get(train2))));
				
				System.out.println(train + j + "  " + p.get(train) + "  " + r.get(train) + "  " + f1.get(train)) ;
				System.out.println(train2 + j + "  " + p.get(train2) + "  " + r.get(train2) + "  " + f1.get(train2)) ;
				
				all_fscore.get(train).add(f1.get(train));
				all_fscore.get(train2).add(f1.get(train2));
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		double avg = 0.0;
		for(int i = 0;i < all_fscore.get(train).size(); i++)
		{
			avg += all_fscore.get(train).get(i);
		}
		System.out.println("AVERAGE : " + train + "  " + avg/all_fscore.get(train).size());
		avg = 0.0;
		for(int i = 0;i < all_fscore.get(train).size(); i++)
		{
			avg += all_fscore.get(train2).get(i);
		}
		System.out.println("AVERAGE : " + train2 + "  " + avg/all_fscore.get(train2).size());
		System.out.println("**********************************************");
	}
	
	public void generate_parts(String train, String train2)
	{
		//car and walk
		try
		{
			BufferedReader train_file = new BufferedReader(new FileReader("src/inputs/oneclass/" + train + "_" + train2 +"/" + train + ".txt"));
			BufferedReader test_file = new BufferedReader(new FileReader("src/inputs/oneclass/" + train + "_" + train2 +"/" + train2 + ".txt"));
			
			PrintWriter train_train_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_train_1.txt"  ), "UTF-8"));
			PrintWriter train_train_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_train_2.txt"  ), "UTF-8"));
			PrintWriter train_train_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_train_3.txt"  ), "UTF-8"));
			PrintWriter train_train_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_train_4.txt"  ), "UTF-8"));
			PrintWriter train_train_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_train_5.txt"  ), "UTF-8"));
			PrintWriter train_test_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_test_1.txt"  ), "UTF-8"));
			PrintWriter train_test_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_test_2.txt"  ), "UTF-8"));
			PrintWriter train_test_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_test_3.txt"  ), "UTF-8"));
			PrintWriter train_test_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_test_4.txt"  ), "UTF-8"));
			PrintWriter train_test_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_test_5.txt"  ), "UTF-8"));
			PrintWriter train_valid_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_valid_1.txt"  ), "UTF-8"));
			PrintWriter train_valid_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_valid_2.txt"  ), "UTF-8"));
			PrintWriter train_valid_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_valid_3.txt"  ), "UTF-8"));
			PrintWriter train_valid_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_valid_4.txt"  ), "UTF-8"));
			PrintWriter train_valid_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train +"_valid_5.txt"  ), "UTF-8"));
			
			


			PrintWriter train2_valid_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train2 +"_valid_1.txt"  ), "UTF-8"));
			PrintWriter train2_valid_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train2 +"_valid_2.txt"  ), "UTF-8"));
			PrintWriter train2_valid_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train2 +"_valid_3.txt"  ), "UTF-8"));
			PrintWriter train2_valid_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train2 +"_valid_4.txt"  ), "UTF-8"));
			PrintWriter train2_valid_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train2 +"_valid_5.txt"  ), "UTF-8"));
			PrintWriter train2_test_1 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train2 +"_test_1.txt"  ), "UTF-8"));
			PrintWriter train2_test_2 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train2 +"_test_2.txt"  ), "UTF-8"));
			PrintWriter train2_test_3 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train2 +"_test_3.txt"  ), "UTF-8"));
			PrintWriter train2_test_4 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train2 +"_test_4.txt"  ), "UTF-8"));
			PrintWriter train2_test_5 = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream("src/inputs/oneclass/" + train + "_" + train2 +"/" + train2 +"_test_5.txt"  ), "UTF-8"));

			int train_size = sizes.get(train);
			int train2_size = sizes.get(train2);
			
			String line = new String();
			int count = 0;
			while(true)
			{
				line = train_file.readLine();
				if(line == null)
					break;
				if(!line.contains("["))
					continue;
				if(0 <= count && count < (train_size/5))
				{
					//first test
					train_test_1.println(line);
					train_valid_2.println(line);
					train_train_3.println(line);
					train_train_4.println(line);
					train_train_5.println(line);
				}
				else if((train_size/5) <= count && count < ((2*train_size)/5))
				{
					//first test
					train_test_2.println(line);
					train_valid_3.println(line);
					train_train_1.println(line);
					train_train_4.println(line);
					train_train_5.println(line);
				}
				else if(((2*train_size)/5) <= count && count < ((3*train_size)/5))
				{
					//first test
					train_test_3.println(line);
					train_valid_4.println(line);
					train_train_1.println(line);
					train_train_2.println(line);
					train_train_5.println(line);
				}
				else if(((3*train_size)/5) <= count && count < ((4*train_size)/5))
				{
					//first test
					train_test_4.println(line);
					train_valid_5.println(line);
					train_train_1.println(line);
					train_train_2.println(line);
					train_train_3.println(line);
				}
				else
				{
					train_test_5.println(line);
					train_valid_1.println(line);
					train_train_2.println(line);
					train_train_3.println(line);
					train_train_4.println(line);
				}
				count++;
			}
			count = 0;
			while(true)
			{
				line = test_file.readLine();
				if(line == null)
					break;
				if(!line.contains("["))
					continue;
				if(0 <= count && count < (train_size/5))
				{
					//first test
					train2_test_1.println(line);
					train2_valid_2.println(line);
				}
				else if((train_size/5) <= count && count < ((2*train_size)/5))
				{
					//first test
					train2_test_2.println(line);
					train2_valid_3.println(line);
				}
				else if(((2*train_size)/5) <= count && count < ((3*train_size)/5))
				{
					//first test
					train2_test_3.println(line);
					train2_valid_4.println(line);
				}
				else if(((3*train_size)/5) <= count && count < ((4*train_size)/5))
				{
					//first test
					train2_test_4.println(line);
					train2_valid_5.println(line);
				}
				else if(((4*train_size)/5) <= count && count < train_size)
				{
					//first test
					train2_test_5.println(line);
					train2_valid_1.println(line);
				}
				else
				{
					break;
				}
				count++;
			}
			train_test_1.close();
			train_test_2.close();
			train_test_3.close();
			train_test_4.close();
			train_test_5.close();
			train_train_1.close();
			train_train_2.close();
			train_train_3.close();
			train_train_4.close();
			train_train_5.close();
			train_valid_1.close();
			train_valid_2.close();
			train_valid_3.close();
			train_valid_4.close();
			train_valid_5.close();
			train2_test_1.close();
			train2_test_2.close();
			train2_test_3.close();
			train2_test_4.close();
			train2_test_5.close();
			train2_valid_1.close();
			train2_valid_2.close();
			train2_valid_3.close();
			train2_valid_4.close();
			train2_valid_5.close();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}
	public int get_size(String dest)
	{
		int count = 0;
		try
		{
			BufferedReader fileReader = new BufferedReader(new FileReader(dest));
			String line = new String();
			while(true)
			{
				line = fileReader.readLine();
				if(line == null)
					break;
				if(line.contains("["))
					count++;
			}
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
		return count;
	}
	public void print_size(String train, String train2)
	{
		String dest = "src/inputs/oneclass/" + train + "_" + train2 +"/";
		File docDir = new File(dest);
		String[] files = docDir.list();
		
		int count = 0;
		for(int i = 0; i< files.length; i++)
		{
			count = 0;
			try
			{
				BufferedReader fileReader = new BufferedReader(new FileReader(dest + files[i]));
				String line = "";
				while(true)
				{
					line = fileReader.readLine();
					if(line == null)
						break;
					if(!line.contains("["))
						continue;
					count++;
				}
				System.out.println(files[i] + "  " + count);
			}
			catch (Exception e)
			{
				System.out.println(e);
			}
		}
	}
	public static void main(String[] args) throws Exception 
	{
		String train = "car";
		String train2 = "walk";
		MASHolder_oneclass ms = new MASHolder_oneclass();
		ms.print_size(train, train2);
		//ms.generate_parts(train, train2);
		//ms.modify = false;
		//ms.learn(train, train2);
		//ms = new MASHolder_oneclass(train, train2);
		
	}

	public double Radians(double x) {
		return x * PIx / 180;
	}

	public double distance(double lat1, double lon1, double lat2, double lon2) {
		double dlon = Radians(lon2 - lon1);
		double dlat = Radians(lat2 - lat1);

		double a = (Math.sin(dlat / 2) * Math.sin(dlat / 2))
				+ Math.cos(Radians(lat1)) * Math.cos(Radians(lat2))
				* (Math.sin(dlon / 2) * Math.sin(dlon / 2));
		double angle = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return angle * RADIUS;

	}
	public void learn(String train, String train2) throws Exception
	{
		String dirLocation = "src/inputs/oneclass/" + train + "_" + train2 + "/";
		for(int i = 1; i <=5 ; i++)
		{
			EdgeMeanVariance em = new EdgeMeanVariance();
			em.modify = true;
			em.findMeanVarAdulterated(dirLocation, train + "_train_" + i + ".txt");
		}
	}

}