Pre-requisite: Directory containing all the map-matched trajectories is correct format.
Steps:
Training: First run EgdeMeanVariance.java to train the model with distribution of travel time for each edge. The model will be saved at savedModelFilePath location. For training we need the pre-requisite directory location to be passed as a parameter. I have already done this and placed it in data/out_ad.csv
Inference: MASHolder.java contains the main method for inference. It first loads the trained model from the learnedModelFilePath. The compare method then compares the running time between the naive approach and our approach by printing the average time taken by each of them. The number of iterations can be tuned by it parameter.
Utils: ChooseTrajectory.java chooses a random trajectory from the map-matched trajectory directory for each iteration. 

