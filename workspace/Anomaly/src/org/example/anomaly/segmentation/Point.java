package org.example.anomaly.segmentation;


public class Point {

	public int id;
	public double lat;
	public double lon;
	public double timeStamp;

	public Point(int id, double lat, double lon, double timeStamp) {
		this.id = id;
		this.lat = lat;
		this.lon = lon;
		this.timeStamp = timeStamp;

	}
}
