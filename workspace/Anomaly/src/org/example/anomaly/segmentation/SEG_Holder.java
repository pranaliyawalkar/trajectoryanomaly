package org.example.anomaly.segmentation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.example.anomaly.mas.EdgeProcessing;

public class SEG_Holder {

	String learnedDistributionPathCar, learnedDistributionPathWalk;
	String mixedTrajFilePath;
	EdgeProcessing epCar, epWalk;

	public SEG_Holder() {
		// learnedDistributionPath =
		// "/Users/prithu/trajData/inputs/out_ad_car_geolife_no_zero_edge.txt";
		learnedDistributionPathCar = "/Users/prithu/Documents/pranali_git/out_new_car.csv";
		learnedDistributionPathWalk = "/Users/prithu/Documents/pranali_git/out_new_walk.csv";
		mixedTrajFilePath = "/Users/prithu/Documents/pranali_git/car_walk_mixed.txt";
		epCar = new EdgeProcessing(learnedDistributionPathCar, 0.5);
		epWalk = new EdgeProcessing(learnedDistributionPathWalk, 0.5);
	}

	public void findSegments() throws Exception {
		BufferedReader fileReader = new BufferedReader(new FileReader(
				mixedTrajFilePath));
		String line = null;
		String[] warr;
		int partitionIndex = 0;
		boolean firstWalk = false;
		while (true) {
			line = fileReader.readLine();
			if (line == null)
				break;
			if (line.contains("c1")) {
				firstWalk = false;
				warr = line.split(":");
				partitionIndex = Integer.parseInt(warr[1].split(" ")[0].trim());
			} else if (line.contains("c2")) {
				firstWalk = true;
				warr = line.split(":");
				partitionIndex = Integer.parseInt(warr[1].split(" ")[0].trim());

			} else {
				warr = line.split("\\]");
				ArrayList<Point> carArr = new ArrayList<Point>();
				ArrayList<Point> walkArr = new ArrayList<Point>();
				for (int i = 0; i < partitionIndex; i++) {
					String s = warr[i];
					int id = Integer.parseInt(s.split(",")[0].split("\\[")[1]);
					double lat = Double.parseDouble(s.split(",")[1]);
					double lon = Double.parseDouble(s.split(",")[2]);
					double time = Double.parseDouble(s.split(",")[3]);
					if (!firstWalk)
						carArr.add(new Point(id, lat, lon, time));
					else
						walkArr.add(new Point(id, lat, lon, time));
				}
				for (int i = partitionIndex; i < warr.length; i++) {
					String s = warr[i];
					int id = Integer.parseInt(s.split(",")[0].split("\\[")[1]);
					double lat = Double.parseDouble(s.split(",")[1]);
					double lon = Double.parseDouble(s.split(",")[2]);
					double time = Double.parseDouble(s.split(",")[3]);
					if (!firstWalk)
						walkArr.add(new Point(id, lat, lon, time));
					else
						carArr.add(new Point(id, lat, lon, time));
				}
				compare(carArr, walkArr);
			}
		}
		fileReader.close();

	}

	private boolean compare(ArrayList<Point> carArr, ArrayList<Point> walkArr) {
		List<Integer> nodeSequence = new ArrayList<Integer>();
		List<Double> travelTime = new ArrayList<Double>();
		for (int i = 0; i < carArr.size(); i++) {
			nodeSequence.add(carArr.get(i).id);
			if (i < (carArr.size() - 1)) {
				double time = carArr.get(i + 1).timeStamp
						- carArr.get(i).timeStamp;
				travelTime.add(time);
			}
		}
		epCar.setNodeTime(nodeSequence, travelTime);
		epWalk.setNodeTime(nodeSequence, travelTime);
		if (epCar.notInTraining() && epWalk.notInTraining()) {
			return false;
		}
		int anoCountCarWithCar = epCar.howManyAno();
		int anoCountCarWithWalk = epWalk.howManyAno();

		nodeSequence = new ArrayList<Integer>();
		travelTime = new ArrayList<Double>();
		for (int i = 0; i < walkArr.size(); i++) {
			nodeSequence.add(walkArr.get(i).id);
			if (i < (walkArr.size() - 1)) {
				double time = walkArr.get(i + 1).timeStamp
						- walkArr.get(i).timeStamp;
				travelTime.add(time);
			}
		}
		epCar.setNodeTime(nodeSequence, travelTime);
		epWalk.setNodeTime(nodeSequence, travelTime);
		if (epCar.notInTraining() && epWalk.notInTraining()) {
			return false;
		}

		int anoCountWalkWithCar = epCar.howManyAno();
		int anoCountWalkWithWalk = epWalk.howManyAno();
		System.out.println(anoCountCarWithCar + " " + anoCountWalkWithCar);
		return true;

	}

	public static void main(String[] args) throws Exception {
		new SEG_Holder().findSegments();
	}

}
