package org.example.anomaly.independence_latest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.example.anomaly.data.Point;

public class Network {

	HashMap<String, double[]> edgeTimeMap = new HashMap<String, double[]>();
	List<double[]> arrList;
	List<Integer> nodeSequence;
	List<Double> travelTime;

	private PrintWriter outCorr;

	public Network(String path) {
		try {
			loadNetwork(path);

		} catch (Exception e) {
			System.out.println("Cannot read the mean variance properly");
			e.printStackTrace();
		}

	}

	private void loadNetwork(String filePath) throws Exception {

		BufferedReader fileReader = new BufferedReader(new FileReader(filePath));
		while (true) {
			String line = fileReader.readLine();
			if (line == null)
				break;
			String[] warr = line.split(",");
			double[] arr = new double[3];

			String key = warr[0];
			arr[0] = Double.parseDouble(warr[1]);
			arr[1] = Double.parseDouble(warr[2]);
			arr[2] = Math.ceil(Double.parseDouble(warr[3]));

			edgeTimeMap.put(key, arr);

		}
		fileReader.close();

	}

	public void setSeed(List<Integer> nodeSequence, List<Double> travelTime) {

		arrList = new ArrayList<double[]>();
		this.travelTime = travelTime;
		this.nodeSequence = nodeSequence;
		for (int i = 0; i < (nodeSequence.size() - 1); i++) {
			int source = nodeSequence.get(i);
			int dest = nodeSequence.get(i + 1);
			String key = source + " " + dest;
			if (!edgeTimeMap.containsKey(key))
				key = dest + " " + source;
			arrList.add(edgeTimeMap.get(key));
		}

	}

	public void writeHopCorel(int totC) throws Exception {

		int seedEdgeId = (int) (Math.random() * nodeSequence.size() - 2);

		for (int hop = 6; hop < 11; hop++) {

			if ((seedEdgeId + hop + 1) < nodeSequence.size()) {
				int compareEdgeId = seedEdgeId + hop;
				outCorr = new PrintWriter(new OutputStreamWriter(
						new FileOutputStream("scripts/test_" + totC + "_" + hop
								+ ".csv"), "UTF-8"));
				if (writeCorrNoSameTraj(seedEdgeId, compareEdgeId,
						hop, outCorr))
					outCorr.close();
				else {
					hop--;
					seedEdgeId = (int) (Math.random() * nodeSequence.size() - 2);
				}
			} else {
				hop--;
				seedEdgeId = (int) (Math.random() * nodeSequence.size() - 2);
			}
		}

	}

	private boolean writeCorr(int seedEdgeId, int compareEdgeId, int hop,
			PrintWriter outCorr) throws Exception {

		int seedEdgeSource = nodeSequence.get(seedEdgeId);
		int seedEdgeDesti = nodeSequence.get(seedEdgeId + 1);
		String seedKey = seedEdgeSource + " " + seedEdgeDesti;
		if (!edgeTimeMap.containsKey(seedKey))
			seedKey = seedEdgeDesti + " " + seedEdgeSource;

		int compareEdgeSource = nodeSequence.get(compareEdgeId);
		int compareEdgeDesti = nodeSequence.get(compareEdgeId + 1);
		String compareKey = compareEdgeSource + " " + compareEdgeDesti;
		if (!edgeTimeMap.containsKey(compareKey))
			compareKey = compareEdgeDesti + " " + compareEdgeSource;

		int count = 0;
		ArrayList<Double> arr1 = new ArrayList<Double>();
		ArrayList<Double> arr2 = new ArrayList<Double>();

		for (int part = 2; part < 6; part++) {
			File docDir = new File(
					"/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/part"
							+ part);
			String[] files = docDir.list();
			int fileNum = 0;

			for (fileNum = 0; fileNum < files.length; fileNum++) {

				System.out.println("File name : " + files[fileNum]);

				BufferedReader fileReader = new BufferedReader(new FileReader(
						"/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/part"
								+ part + "/" + files[fileNum]));
				while (true) {

					ArrayList<Point> arr = new ArrayList<Point>();
					String line = fileReader.readLine();
					if (line == null)
						break;

					if (line.contains("[")) {
						String[] warr = line.split("\\]");
						for (String s : warr) {
							try {
								int id = Integer.parseInt(s.split(",")[0]
										.split("\\[")[1]);
								double lat = Double
										.parseDouble(s.split(",")[1]);
								double lon = Double
										.parseDouble(s.split(",")[2]);
								Date time = new SimpleDateFormat(
										"yyyy-MM-dd HH:mm:ss").parse(s
										.split(",")[3]);
								arr.add(new Point(id, lat, lon, time));
							} catch (Exception e) {
								System.out.println(line);
							}
						}

					}
					for (int i = 0; i < arr.size() - 1; i++) {
						if ((arr.get(i).id == seedEdgeSource && arr.get(i + 1).id == seedEdgeDesti)
								|| (arr.get(i).id == seedEdgeDesti && arr
										.get(i + 1).id == seedEdgeSource)) {
							for (int j = 0; j < arr.size() - 1; j++) {

								if (((arr.get(j).id == compareEdgeSource && arr
										.get(j + 1).id == compareEdgeDesti) || (arr
										.get(j).id == compareEdgeDesti && arr
										.get(j + 1).id == compareEdgeSource))
										&& (Math.abs(arr.get(i).timeStamp
												.getTime()
												- arr.get(j).timeStamp
														.getTime()) < 3600000)
										&& ((Math.abs(j - i) == hop))) {
									count++;

									double travelTime1 = arr.get(i + 1).timeStamp
											.getTime()
											- arr.get(i).timeStamp.getTime();
									arr1.add(travelTime1);
									double travelTime2 = arr.get(j + 1).timeStamp
											.getTime()
											- arr.get(j).timeStamp.getTime();
									arr2.add(travelTime2);
									break;
								}
							}
							break;

						}

					}
					if (count > 100)
						break;

				}
				if (count > 100)
					break;
				fileReader.close();

			}
			if (count > 100)
				break;
		}
		if (arr1.size() == arr2.size()) {
			outCorr.println(seedEdgeSource + " " + seedEdgeSource + ":" + arr1
					+ "$" + compareEdgeSource + " " + compareEdgeDesti + ":"
					+ arr2);
			outCorr.flush();
			return true;
		} else
			return false;
	}

	private boolean writeCorrNoSameTraj(int seedEdgeId, int compareEdgeId,
			int hop, PrintWriter outCorr) throws Exception {

		int seedEdgeSource = nodeSequence.get(seedEdgeId);
		int seedEdgeDesti = nodeSequence.get(seedEdgeId + 1);
		String seedKey = seedEdgeSource + " " + seedEdgeDesti;
		if (!edgeTimeMap.containsKey(seedKey))
			seedKey = seedEdgeDesti + " " + seedEdgeSource;

		int compareEdgeSource = nodeSequence.get(compareEdgeId);
		int compareEdgeDesti = nodeSequence.get(compareEdgeId + 1);
		String compareKey = compareEdgeSource + " " + compareEdgeDesti;
		if (!edgeTimeMap.containsKey(compareKey))
			compareKey = compareEdgeDesti + " " + compareEdgeSource;

		boolean isFirstFull = false, isSecondFull = false;
		ArrayList<Double> arr1 = new ArrayList<Double>();
		ArrayList<Double> arr2 = new ArrayList<Double>();

		for (int part = 2; part < 6; part++) {
			File docDir = new File(
					"/Users/prithu/trajData/inputs/part"
							+ part);
			String[] files = docDir.list();
			int fileNum = 0;

			for (fileNum = 0; fileNum < files.length; fileNum++) {

				System.out.println("File name : " + files[fileNum]);

				BufferedReader fileReader = new BufferedReader(new FileReader(
						"/Users/prithu/trajData/inputs/part"
								+ part + "/" + files[fileNum]));
				while (true) {

					if (isFirstFull && isSecondFull)
						break;
					ArrayList<Point> arr = new ArrayList<Point>();
					String line = fileReader.readLine();
					if (line == null)
						break;

					if (line.contains("[")) {
						String[] warr = line.split("\\]");
						for (String s : warr) {
							try {
								int id = Integer.parseInt(s.split(",")[0]
										.split("\\[")[1]);
								double lat = Double
										.parseDouble(s.split(",")[1]);
								double lon = Double
										.parseDouble(s.split(",")[2]);
								Date time = new SimpleDateFormat(
										"yyyy-MM-dd HH:mm:ss").parse(s
										.split(",")[3]);
								arr.add(new Point(id, lat, lon, time));
							} catch (Exception e) {
								System.out.println(line);
							}
						}

					}
					for (int i = 0; i < arr.size() - 1; i++) {
						if (!isFirstFull
								&& ((arr.get(i).id == seedEdgeSource && arr
										.get(i + 1).id == seedEdgeDesti) || (arr
										.get(i).id == seedEdgeDesti && arr
										.get(i + 1).id == seedEdgeSource))) {
							double travelTime1 = arr.get(i + 1).timeStamp
									.getTime() - arr.get(i).timeStamp.getTime();
							arr1.add(travelTime1);
							if (arr1.size() == 100)
								isFirstFull = true;
						}
						if (!isSecondFull
								&& ((arr.get(i).id == compareEdgeSource && arr
										.get(i + 1).id == compareEdgeDesti) || (arr
										.get(i).id == compareEdgeDesti && arr
										.get(i + 1).id == compareEdgeSource))) {
							double travelTime2 = arr.get(i + 1).timeStamp
									.getTime() - arr.get(i).timeStamp.getTime();
							arr2.add(travelTime2);
							if (arr2.size() == 100)
								isSecondFull = true;

						}
						if (isSecondFull && isFirstFull)
							break;

					}
				}
				fileReader.close();
				if (isFirstFull && isSecondFull)
					break;
			}
			if (isFirstFull && isSecondFull)
				break;
		}

		if (arr1.size() == arr2.size()) {
			outCorr.println(seedEdgeSource + " " + seedEdgeDesti + ":" + arr1
					+ "$" + compareEdgeSource + " " + compareEdgeDesti + ":"
					+ arr2);
			outCorr.flush();
			return true;
		} else
			return false;
	}

	private boolean writeCorrNoSameTrajButSameTime(int seedEdgeId,
			int compareEdgeId, int hop, PrintWriter outCorr) throws Exception {

		int seedEdgeSource = nodeSequence.get(seedEdgeId);
		int seedEdgeDesti = nodeSequence.get(seedEdgeId + 1);
		String seedKey = seedEdgeSource + " " + seedEdgeDesti;
		if (!edgeTimeMap.containsKey(seedKey))
			seedKey = seedEdgeDesti + " " + seedEdgeSource;

		int compareEdgeSource = nodeSequence.get(compareEdgeId);
		int compareEdgeDesti = nodeSequence.get(compareEdgeId + 1);
		String compareKey = compareEdgeSource + " " + compareEdgeDesti;
		if (!edgeTimeMap.containsKey(compareKey))
			compareKey = compareEdgeDesti + " " + compareEdgeSource;

		boolean isFirstFull = false, isSecondFull = false;
		ArrayList<Double> arr1 = new ArrayList<Double>();
		ArrayList<Date> arr1_time = new ArrayList<Date>();

		double[] arr2 = new double[100];

		for (int part = 2; part < 6; part++) {
			File docDir = new File(
					"/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/part"
							+ part);
			String[] files = docDir.list();
			int fileNum = 0;

			for (fileNum = 0; fileNum < files.length; fileNum++) {

				System.out.println("File name : " + files[fileNum]);

				BufferedReader fileReader = new BufferedReader(new FileReader(
						"/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/part"
								+ part + "/" + files[fileNum]));
				while (true) {

					if (isFirstFull)
						break;
					ArrayList<Point> arr = new ArrayList<Point>();
					String line = fileReader.readLine();
					if (line == null)
						break;

					if (line.contains("[")) {
						String[] warr = line.split("\\]");
						for (String s : warr) {
							try {
								int id = Integer.parseInt(s.split(",")[0]
										.split("\\[")[1]);
								double lat = Double
										.parseDouble(s.split(",")[1]);
								double lon = Double
										.parseDouble(s.split(",")[2]);
								Date time = new SimpleDateFormat(
										"yyyy-MM-dd HH:mm:ss").parse(s
										.split(",")[3]);
								arr.add(new Point(id, lat, lon, time));
							} catch (Exception e) {
								System.out.println(line);
							}
						}

					}
					for (int i = 0; i < arr.size() - 1; i++) {
						if (!isFirstFull
								&& ((arr.get(i).id == seedEdgeSource && arr
										.get(i + 1).id == seedEdgeDesti) || (arr
										.get(i).id == seedEdgeDesti && arr
										.get(i + 1).id == seedEdgeSource))) {
							double travelTime1 = arr.get(i + 1).timeStamp
									.getTime() - arr.get(i).timeStamp.getTime();
							arr1.add(travelTime1);
							arr1_time.add(arr.get(i).timeStamp);
							if (arr1.size() == 100)
								isFirstFull = true;
						}
						if (isFirstFull)
							break;

					}
				}
				fileReader.close();
				if (isFirstFull)
					break;
			}
			if (isFirstFull)
				break;
		}

		for (int part = 2; part < 6; part++) {
			File docDir = new File(
					"/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/part"
							+ part);
			String[] files = docDir.list();
			int fileNum = 0;

			for (fileNum = 0; fileNum < files.length; fileNum++) {

				System.out.println("File name : " + files[fileNum]);

				BufferedReader fileReader = new BufferedReader(new FileReader(
						"/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/part"
								+ part + "/" + files[fileNum]));
				while (true) {

					if (isFirstFull && isSecondFull)
						break;
					ArrayList<Point> arr = new ArrayList<Point>();
					String line = fileReader.readLine();
					if (line == null)
						break;

					if (line.contains("[")) {
						String[] warr = line.split("\\]");
						for (String s : warr) {
							try {
								int id = Integer.parseInt(s.split(",")[0]
										.split("\\[")[1]);
								double lat = Double
										.parseDouble(s.split(",")[1]);
								double lon = Double
										.parseDouble(s.split(",")[2]);
								Date time = new SimpleDateFormat(
										"yyyy-MM-dd HH:mm:ss").parse(s
										.split(",")[3]);
								arr.add(new Point(id, lat, lon, time));
							} catch (Exception e) {
								System.out.println(line);
							}
						}

					}
					for (int i = 0; i < arr.size() - 1; i++) {
						if (!isSecondFull
								&& ((arr.get(i).id == compareEdgeSource && arr
										.get(i + 1).id == compareEdgeDesti) || (arr
										.get(i).id == compareEdgeDesti && arr
										.get(i + 1).id == compareEdgeSource))) {
							double travelTime2 = arr.get(i + 1).timeStamp
									.getTime() - arr.get(i).timeStamp.getTime();
							for (int j = 0; j < arr1_time.size(); j++) {
								if ((Math.abs(arr1_time.get(j).getTime()
										- arr.get(i).timeStamp.getTime()) < 3600000)
										&& arr2[j] == 0) {
									arr2[j] = travelTime2;
									break;
								}

							}
							if (isFull(arr2))
								isSecondFull = true;

						}
						if (isSecondFull && isFirstFull)
							break;

					}
				}
				fileReader.close();
				if (isFirstFull && isSecondFull)
					break;
			}
			if (isFirstFull && isSecondFull)
				break;
		}

		if (isFirstFull && isSecondFull) {
			outCorr.println(seedEdgeSource + " " + seedEdgeDesti + ":" + arr1
					+ "$" + compareEdgeSource + " " + compareEdgeDesti + ":"
					+ convertList(arr2));
			outCorr.flush();
			return true;
		} else
			return false;
	}

	private ArrayList<Double> convertList(double[] arr2) {
		ArrayList<Double> list = new ArrayList<Double>();
		for (double d : arr2)
			list.add(d);
		return list;
	}

	private boolean isFull(double[] arr2) {
		for (double d : arr2)
			if (d == 0)
				return false;
		return true;
	}

	public void setNodeTime(List<Integer> nodeSequence, List<Double> travelTime) {
		arrList = new ArrayList<double[]>();
		this.travelTime = travelTime;
		this.nodeSequence = nodeSequence;
		for (int i = 0; i < (nodeSequence.size() - 1); i++) {
			int source = nodeSequence.get(i);
			int dest = nodeSequence.get(i + 1);
			String key = source + " " + dest;
			if (!edgeTimeMap.containsKey(key))
				key = dest + " " + source;
			arrList.add(edgeTimeMap.get(key));
		}

	}

	public boolean notInTraining() {
		for (int i = 0; i < travelTime.size(); i++)
			if (arrList.get(i) == null)
				return true;
		return false;
	}
}
