package org.example.anomaly.synthetic;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.example.anomaly.data.Point;
import org.example.anomaly.mas.EdgeProcessing;

public class SyntheticHolder {

	/**
	 * @param args
	 */

	int mined_traj_count, tot_traj_count, training_miss_count;

	int percentAno, clusterSize;

	String mapMatchedTrajDirPath, learnedEdgeDistroFilePath,
			myOutputFilePathTime;

	PrintWriter outTime;

	EdgeProcessing ep;

	public SyntheticHolder(int percentAno, int clusterSize) {
		int partID = 1;
		mined_traj_count = 1;
		tot_traj_count = 1;
		training_miss_count = 1;
		this.percentAno = percentAno;
		this.clusterSize = clusterSize;

		mapMatchedTrajDirPath = "/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/part"
				+ partID;
		learnedEdgeDistroFilePath = "/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/out_ad"
				+ partID + ".csv";

		myOutputFilePathTime = "/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/outputs/sythetic_Test_last_"
				+ percentAno
				+ "_clusterSize_"
				+ clusterSize
				+ "_trajsizeVSnumofanoVSprocessingTimeandProcessedEdges_pri"
				+ partID + ".csv";

		try {
			outTime = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(myOutputFilePathTime), "UTF-8"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ep = new EdgeProcessing(learnedEdgeDistroFilePath, 2.0);

	}

	public void compare_handler() throws Exception {
		ChooseSyntheticTrajectory cst = new ChooseSyntheticTrajectory(
				percentAno, clusterSize, learnedEdgeDistroFilePath,
				mapMatchedTrajDirPath);
		while (mined_traj_count < 100) {
			ArrayList<Point> arr = null;
			try {
				arr = cst.createTrajectory();
			} catch (Exception e) {
				mined_traj_count--;
			}
			mined_traj_count++;
			if (arr != null)
				compare(arr);
			System.out.println(mined_traj_count + "," + training_miss_count
					+ "," + tot_traj_count);
		}
		cst.outTime.close();
		outTime.close();
	}

	private void compare(ArrayList<Point> pt) {

		long tot1 = 0;
		long tot2 = 0;
		long tot3 = 0;
		List<Integer> nodeSequence = new ArrayList<Integer>();
		List<Double> travelTime = new ArrayList<Double>();
		for (int i = 0; i < pt.size(); i++) {
			nodeSequence.add(pt.get(i).id);
			if (i < (pt.size() - 1)) {
				double time = pt.get(i + 1).timeStamp.getTime()
						- pt.get(i).timeStamp.getTime();
				travelTime.add(time);
			}
		}
		ep.setNodeTime(nodeSequence, travelTime);
		if (ep.notInTraining()) {
			training_miss_count++;
		} else {

			long start = System.currentTimeMillis();

			ArrayList<String> our_result = new ArrayList<String>();
			try {
				our_result = ep.findAnomalyOur();
			} catch (Exception e) {
				System.out.println("Not there in training");
				e.printStackTrace();
				return;
			}
			tot1 += (System.currentTimeMillis() - start);
			start = System.currentTimeMillis();

			ArrayList<String> naive_result = new ArrayList<String>();
			try {
				naive_result = ep.findAnomalyNaive();
			} catch (Exception e) {
				System.out.println("Not there in training");
				e.printStackTrace();
				return;
			}
			tot2 += (System.currentTimeMillis() - start);
			start = System.currentTimeMillis();
			ArrayList<String> naive_result2 = new ArrayList<String>();
			try {
				naive_result2 = ep.findAnomalyNaive2();
			} catch (Exception e) {
				System.out.println("Not there in training");
				e.printStackTrace();
				return;
			}
			tot3 += System.currentTimeMillis() - start;

			outTime.println(pt.size() + "," + ep.howManyAno() + "," + tot1
					+ "," + tot2 + "," + tot3 + "," + ep.edgeProcessedOur + ","
					+ ep.edgeProcessedWindow);

			if (our_result.size() == naive_result.size()) {
				int flag = 0;
				for (int i = 0; i < our_result.size(); i++) {
					if (!our_result.get(i).equals(naive_result.get(i))) {
						flag = 1;
						break;
					}
				}

			}

		}
		tot_traj_count++;

	}

	public static void main(String[] args) throws Exception {

		SyntheticHolder sth;
		// for (int i = 1; i < 6; i++) {
		int anPercent = 15;
		// if (percentAno == 30)
		// continue;
		int[] clusPercent = new int[2];
		clusPercent[0] = 15;
		clusPercent[1] = 75;
		// anoPercent[1] = 100;
		// clusterPercent[1] = 100;
		// clusterPercent[2] = 45;
		// clusterPercent[3] = 60;
		// clusterPercent[4] = 75;
		// clusterPercent[5] = 100;

		// for (int j = 2; j < 5; j++) {
		// int clusterSize = 15 * j;
		// if (clusterSize == 0)
		// clusterSize = 1;
		for (int i : clusPercent) {
			sth = new SyntheticHolder(anPercent, i);
			sth.compare_handler();
		}
		// }
		// }

	}

}
