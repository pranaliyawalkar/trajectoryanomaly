package org.example.anomaly.synthetic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.example.anomaly.data.Point;

public class ChooseSyntheticTrajectory {

	/**
	 * @param args
	 */
	int size;
	int percentAno;
	int clusterPercent;
	String mapMatchedTrajDirPath;
	HashMap<String, double[]> edgeTimeMap = new HashMap<String, double[]>();

	String myOutputFilePathTime;
	public PrintWriter outTime;

	public ChooseSyntheticTrajectory(int percentAno, int clusterSize,
			String learnedEdgeDistroFilePath, String mapMatchedTrajDirPath) {
		this.percentAno = percentAno;
		this.clusterPercent = clusterSize;
		this.mapMatchedTrajDirPath = mapMatchedTrajDirPath;
		try {
			loadMeanVar(learnedEdgeDistroFilePath);
		} catch (Exception e) {

			e.printStackTrace();
		}

		myOutputFilePathTime = "/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/outputs/sythetic_"
				+ percentAno
				+ "_clusterSize_"
				+ clusterSize
				+ "_chooseTrajLog"
				+ ".csv";

		try {
			outTime = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(myOutputFilePathTime), "UTF-8"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<Point> createTrajectory() throws Exception {
		ArrayList<Point> arr = new ArrayList<Point>();
		File docDir = new File(mapMatchedTrajDirPath);
		String[] files = docDir.list();
		int fileNum = (int) (Math.random() * files.length);
		BufferedReader fileReader = new BufferedReader(new FileReader(
				mapMatchedTrajDirPath + "/" + files[fileNum]));
		while (true) {
			String line = fileReader.readLine();
			if (line == null)
				break;
			if (line.contains("[")) {
				String[] warr = line.split("\\]");
				for (String s : warr) {
					try {
						int id = Integer
								.parseInt(s.split(",")[0].split("\\[")[1]);
						double lat = Double.parseDouble(s.split(",")[1]);
						double lon = Double.parseDouble(s.split(",")[2]);
						Date time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
								.parse(s.split(",")[3]);
						arr.add(new Point(id, lat, lon, time));
					} catch (Exception e) {
						System.out.println(line);
					}
				}
				if (arr.size() > 200) {
					if (percentAno == 100)
						arr = makeCmpltAnomalous(arr);
					else
						arr = manipulate(arr);
					break;
				}
			}
		}
		fileReader.close();
		outTime.println(howManyAno(arr));
		outTime.println("$$$$$$$$$$$$");
		return arr;
	}

	private int howManyAno(ArrayList<Point> pt) {
		int count = 0;
		for (int i = 0; i < pt.size() - 1; i++) {

			int source = pt.get(i).id;
			int dest = pt.get(i + 1).id;
			long time = pt.get(i + 1).timeStamp.getTime()
					- pt.get(i).timeStamp.getTime();
			String key = source + " " + dest;
			if (!edgeTimeMap.containsKey(key))
				key = dest + " " + source;
			double[] arr = edgeTimeMap.get(key);
			double mean = arr[0];
			double var = arr[1];
			double diff = Math.pow((mean - time), 2.0) / var;
			if (Math.sqrt(diff) >= 2.0)
				count++;

		}
		return count;
	}

	private ArrayList<Point> makeCmpltAnomalous(ArrayList<Point> pt) {

		for (int i = 0; i < pt.size() - 1; i++) {

			int source = pt.get(i).id;
			int dest = pt.get(i + 1).id;
			String key = source + " " + dest;
			if (!edgeTimeMap.containsKey(key))
				key = dest + " " + source;
			double[] arr = edgeTimeMap.get(key);
			double mean = arr[0];
			double var = arr[1];
			long setTime = (long) (mean - 2 * Math.sqrt(var));
			setTime = pt.get(i).timeStamp.getTime() + setTime;
			pt.get(i + 1).timeStamp.setTime(setTime);

		}

		return pt;

	}

	private ArrayList<Point> manipulate(ArrayList<Point> pt) {
		int numOfAno = pt.size() * percentAno / 100;
		// if (numOfAno < clusterSize)
		// numOfAno = clusterSize;
		int clusterSize = numOfAno * clusterPercent / 100;
		int numOfCluster = numOfAno / clusterSize;
		int anoIndex = pt.size() / numOfCluster;
		outTime.print(pt.size() + "," + numOfAno + "," + numOfCluster + ",");

		for (int i = 0; i < pt.size() - 1; i++) {

			int source = pt.get(i).id;
			int dest = pt.get(i + 1).id;
			String key = source + " " + dest;
			if (!edgeTimeMap.containsKey(key))
				key = dest + " " + source;
			double[] arr = edgeTimeMap.get(key);
			double mean = arr[0];
			double var = arr[1];

			if (i % anoIndex == 0) {
				int c = 0;
				while (c < clusterSize) {
					long setTime = (long) (mean - (2 + Math.random() * 6)
							* Math.sqrt(var));
					setTime = pt.get(i).timeStamp.getTime() + setTime;
					pt.get(i + 1).timeStamp.setTime(setTime);
					i++;
					c++;
					if (i == pt.size() - 1)
						break;
					source = pt.get(i).id;
					dest = pt.get(i + 1).id;
					key = source + " " + dest;
					if (!edgeTimeMap.containsKey(key))
						key = dest + " " + source;
					arr = edgeTimeMap.get(key);
					mean = arr[0];
					var = arr[1];
				}
				i--;
			} else {

				pt.get(i + 1).timeStamp.setTime(pt.get(i).timeStamp.getTime()
						+ (long) mean);

			}
		}
		return pt;
	}

	private void loadMeanVar(String filePath) throws Exception {

		BufferedReader fileReader = new BufferedReader(new FileReader(filePath));
		while (true) {
			String line = fileReader.readLine();
			if (line == null)
				break;
			String[] warr = line.split(",");
			double[] arr = new double[3];

			String key = warr[0];
			arr[0] = Double.parseDouble(warr[1]);
			arr[1] = Double.parseDouble(warr[2]);
			arr[2] = Math.ceil(Double.parseDouble(warr[3]));

			edgeTimeMap.put(key, arr);

		}
		fileReader.close();

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
