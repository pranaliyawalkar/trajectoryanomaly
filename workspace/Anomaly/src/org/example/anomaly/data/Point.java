package org.example.anomaly.data;

import java.util.Date;

public class Point {

	public int id;
	public double lat;
	public double lon;
	public Date timeStamp;

	public Point(int id, double lat, double lon, Date timeStamp) {
		this.id = id;
		this.lat = lat;
		this.lon = lon;
		this.timeStamp = timeStamp;

	}
}
