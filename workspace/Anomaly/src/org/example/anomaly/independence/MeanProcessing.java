package org.example.anomaly.independence;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.example.anomaly.data.Point;

public class MeanProcessing {

	HashMap<String, double[]> edgeTimeMap = new HashMap<String, double[]>();
	List<Integer> nodeSequence;
	List<Double> travelTime;
	List<double[]> arrList;

	public MeanProcessing(String path) {

		try {
			loadMeanVar(path);

		} catch (Exception e) {
			System.out.println("Cannot read the mean variance properly");
			e.printStackTrace();
		}

	}

	private void loadMeanVar(String filePath) throws Exception {

		BufferedReader fileReader = new BufferedReader(new FileReader(filePath));
		while (true) {
			String line = fileReader.readLine();
			if (line == null)
				break;
			String[] warr = line.split(",");
			double[] arr = new double[3];

			String key = warr[0];
			arr[0] = Double.parseDouble(warr[1]);
			arr[1] = Double.parseDouble(warr[2]);
			arr[2] = Math.ceil(Double.parseDouble(warr[3]));

			edgeTimeMap.put(key, arr);

		}
		fileReader.close();

	}

	public void setNodeTime(List<Integer> nodeSequence, List<Double> travelTime) {

		arrList = new ArrayList<double[]>();
		this.travelTime = travelTime;
		this.nodeSequence = nodeSequence;
		for (int i = 0; i < (nodeSequence.size() - 1); i++) {
			int source = nodeSequence.get(i);
			int dest = nodeSequence.get(i + 1);
			String key = source + " " + dest;
			if (!edgeTimeMap.containsKey(key))
				key = dest + " " + source;
			arrList.add(edgeTimeMap.get(key));
		}

	}

	public boolean notInTraining() {
		for (int i = 0; i < travelTime.size(); i++)
			if (arrList.get(i) == null)
				return true;
		return false;
	}

	public double findCovar(PrintWriter out) throws Exception {
		int edge1 = (int) (Math.random() * (travelTime.size() - 2));
		int edge2 = (int) (Math.random() * (travelTime.size() - 2));

		double mean1 = arrList.get(edge1)[0];
		double var1 = Math.sqrt(arrList.get(edge1)[1]);
		int source1 = nodeSequence.get(edge1);
		int dest1 = nodeSequence.get(edge1 + 1);

		double mean2 = arrList.get(edge2)[0];
		double var2 = Math.sqrt(arrList.get(edge2)[1]);
		int source2 = nodeSequence.get(edge2);
		int dest2 = nodeSequence.get(edge2 + 1);

		int count = 0;
		double sum = 0.0;
		ArrayList<Double> arr1 = new ArrayList<Double>();
		ArrayList<Double> arr2 = new ArrayList<Double>();

		for (int part = 1; part < 5; part++) {
			File docDir = new File(
					"/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/part"
							+ part);
			String[] files = docDir.list();
			int fileNum = 0;

			for (fileNum = 0; fileNum < files.length; fileNum++) {

				System.out.println("File name : " + files[fileNum]);

				BufferedReader fileReader = new BufferedReader(new FileReader(
						"/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/part"
								+ part + "/" + files[fileNum]));
				while (true) {

					ArrayList<Point> arr = new ArrayList<Point>();
					String line = fileReader.readLine();
					if (line == null)
						break;

					if (line.contains("[")) {
						String[] warr = line.split("\\]");
						for (String s : warr) {
							try {
								int id = Integer.parseInt(s.split(",")[0]
										.split("\\[")[1]);
								double lat = Double
										.parseDouble(s.split(",")[1]);
								double lon = Double
										.parseDouble(s.split(",")[2]);
								Date time = new SimpleDateFormat(
										"yyyy-MM-dd HH:mm:ss").parse(s
										.split(",")[3]);
								arr.add(new Point(id, lat, lon, time));
							} catch (Exception e) {
								System.out.println(line);
							}
						}

					}
					for (int i = 0; i < arr.size() - 1; i++) {
						if ((arr.get(i).id == source1 && arr.get(i + 1).id == dest1)
								|| (arr.get(i).id == dest1 && arr.get(i + 1).id == source1)) {
							for (int j = 0; j < arr.size() - 1; j++) {

								if ((arr.get(j).id == source2 && arr.get(j + 1).id == dest2)
										|| (arr.get(j).id == dest2 && arr
												.get(j + 1).id == source2)) {
									count++;

									double travelTime1 = arr.get(i + 1).timeStamp
											.getTime()
											- arr.get(i).timeStamp.getTime();
									arr1.add(travelTime1);
									double travelTime2 = arr.get(j + 1).timeStamp
											.getTime()
											- arr.get(j).timeStamp.getTime();
									sum += (Math.abs(mean1 - travelTime1))
											* (Math.abs(mean2 - travelTime2));
									arr2.add(travelTime2);
									break;
								}
							}
							break;

						}

					}
					if (count > 100)
						break;

				}
				if (count > 100)
					break;
				fileReader.close();

			}
			if (count > 100)
				break;
		}
		out.println(source1 + " " + dest1 + ":" + arr1 + "$" + source2 + " "
				+ dest2 + ":" + arr2);
		out.flush();
		sum = (double) sum / count;
		sum = sum / (var1 * var2);
		return sum;
	}

}
