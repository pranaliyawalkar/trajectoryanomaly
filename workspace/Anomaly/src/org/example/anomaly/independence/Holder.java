package org.example.anomaly.independence;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.example.anomaly.data.Point;

public class Holder {

	private String mapMatchedTrajDirPath;
	private String learnedEdgeDistroFilePath;
	private PrintWriter outTime;
	private MeanProcessing ep;
	HashMap<String, Integer> edgeTimeMap = new HashMap<String, Integer>();

	public Holder() {
		int partID = 1;

		mapMatchedTrajDirPath = "/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/part"
				+ partID;
		learnedEdgeDistroFilePath = "/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/out_ad"
				+ partID + ".csv";

		String myOutputFilePathTime = "/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/outputs/independence_"
				+ partID + ".csv";

		try {
			outTime = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(myOutputFilePathTime), "UTF-8"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ep = new MeanProcessing(learnedEdgeDistroFilePath);

	}

	public void compare_handler() throws Exception {
		File docDir = new File(mapMatchedTrajDirPath);
		String[] files = docDir.list();
		int fileNum = 0;
		int totC = 0;
		double diff = 0.0;
		PrintWriter out = new PrintWriter(
				new OutputStreamWriter(
						new FileOutputStream(
								"/Users/itispris/Documents/trajData/test/out_temp11.csv"),
						"UTF-8"));

		for (fileNum = 0; fileNum < files.length; fileNum++) {

			if (totC > 200)
				break;
			System.out.println("File name : " + files[fileNum]);
			outTime.println("File name : " + files[fileNum]);
			BufferedReader fileReader = new BufferedReader(new FileReader(
					mapMatchedTrajDirPath + "/" + files[fileNum]));
			while (true) {

				ArrayList<Point> arr = new ArrayList<Point>();
				String line = fileReader.readLine();
				if (line == null)
					break;
				totC++;
				if (totC > 200) {
					System.out.println(diff / 200);
					break;
				}
				if (line.contains("[")) {
					String[] warr = line.split("\\]");
					for (String s : warr) {
						try {
							int id = Integer.parseInt(s.split(",")[0]
									.split("\\[")[1]);
							double lat = Double.parseDouble(s.split(",")[1]);
							double lon = Double.parseDouble(s.split(",")[2]);
							Date time = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss")
									.parse(s.split(",")[3]);
							arr.add(new Point(id, lat, lon, time));
						} catch (Exception e) {
							System.out.println(line);
						}
					}
					if (arr.size() > 75)
						diff += compare(arr, out);
				}
				// if (tot_traj_count == 1000)
				// break;

			}
			fileReader.close();

		}
		System.out.println(diff / (totC * 4));
		out.close();
	}

	private double compare(ArrayList<Point> pt, PrintWriter out)
			throws Exception {

		List<Integer> nodeSequence = new ArrayList<Integer>();
		List<Double> travelTime = new ArrayList<Double>();
		for (int i = 0; i < pt.size(); i++) {
			nodeSequence.add(pt.get(i).id);
			if (i < (pt.size() - 1)) {
				double time = pt.get(i + 1).timeStamp.getTime()
						- pt.get(i).timeStamp.getTime();
				travelTime.add(time);
			}
		}
		ep.setNodeTime(nodeSequence, travelTime);
		if (!ep.notInTraining()) {
			double sum = 0.0;
			for (int m = 0; m < 4; m++)
				sum += ep.findCovar(out);
			return sum;
		}
		return 0;
	}

	public static void main(String[] args) throws Exception {
		Holder hld = new Holder();
		hld.compare_handler();

	}

}
