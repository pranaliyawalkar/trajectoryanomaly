package org.example.anomaly.geoL;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class geolife_processing {

	/** CONSTANTS : CONVERTING TIME INTO SECONDS. EX, ONE_YEAR = 31536000 SECS **/
	/*
	 * one non year = 31536000 secs one leap year = 31622400 secs jan, mar, may,
	 * jul, aug, oct, dec month = 2678400 secs apr, jun, sept, nov = 2592000
	 * secs feb in leap year = 2505600 secs feb in non leap year = 2419200 secs
	 * one day = 86400 secs one hour = 3600 secs
	 */
	public static final BigInteger one_year = BigInteger.valueOf(31536000);
	public static final BigInteger leap_one_year = BigInteger.valueOf(31622400);
	public static final BigInteger one_month_31 = BigInteger.valueOf(2678400);
	public static final BigInteger one_month_30 = BigInteger.valueOf(2592000);
	public static final BigInteger one_month_29 = BigInteger.valueOf(2505600);
	public static final BigInteger one_month_28 = BigInteger.valueOf(2419200);
	public static final BigInteger one_day = BigInteger.valueOf(86400);
	public static final BigInteger one_hour = BigInteger.valueOf(3600);
	public static final BigInteger one_min = BigInteger.valueOf(60);

	/*** CHANGE THIS TO THE SUITABLE FOLDER NEEDED ***/
	public static final String geolife_directory = "/Users/itispris/Dropbox/projects/Anomaly/Data2/";

	/** GETTING MODIFIED FOR EVERY ID **/
	public String id_directory = "";

	public static void main(String[] args) throws Exception {
		File docDir = new File(geolife_directory);
		String[] files = docDir.list();

		/** READING ALL FOLDER NAMES IN "src/geolife/" **/
		for (int i = 0; i < files.length; i++) {
			if (files[i].contains("DS"))
				continue;
			geolife_processing process_object = new geolife_processing();
			process_object.id_directory = geolife_directory + files[i] + "/";
			System.out.println("processing: " + files[i]);

			/** IF THE FILES UNDER CONSIDERATION ARE NOT OUR CODE FILES **/
			if (!files[i].equals("geolife_label.java")
					&& !files[i].equals("geolife_processing.java")) {
				/** IF "labels.txt" EXISTS IN THE ID FOLDER **/
				if (process_object.check_label_file()) {
					System.out.println("Processing id : " + files[i]);
					process_object.init();
				}

				else {
					System.out.println("Rejected id : " + files[i]);
				}
			}
		}
	}

	/** IF THE ID FOLDER CONTAINS "labels.txt, RETURN TRUE" **/
	public boolean check_label_file() throws Exception {
		File docDir = new File(id_directory);
		String[] files = docDir.list();
		for (int i = 0; i < files.length; i++) {
			if (files[i].equals("labels.txt"))
				return true;
		}
		return false;
	}

	/**
	 * START PROCESSING FOR THAT ID. EX, id = 089. IF WE ARE HERE, THEN
	 * "labels.txt" EXISTS IN THAT FOLDER
	 **/
	public void init() throws Exception {
		String dirLocation = id_directory + "mappedTrajectory";
		File docDir = new File(dirLocation);
		String[] files = docDir.list();
		String output_file_name = id_directory + "class.txt";
		PrintWriter out = new PrintWriter(new OutputStreamWriter(
				new FileOutputStream(output_file_name), "UTF-8"));
		ArrayList<geolife_label> labels = new ArrayList<geolife_label>();
		labels.addAll(read_labels());

		// READING ALL FILES IN THE "mappedTrajectory" FOLDER
		for (int file = 0; file < files.length; file++) {
			String filename = files[file];
			StringTokenizer stt = new StringTokenizer(filename, "_.");
			String token_date_time = stt.nextToken();
			token_date_time = stt.nextToken();

			/** THE BASE TIME IS : 2007/01/01 00:00:00 **/

			/**
			 * CONVERTING ITS START TIME TO A FORMAT WITH RESPECT TO THE BASE.
			 * EX, 2008/01/01 00:00:00 WOULD BE 31536000 BECAUSE IT IS ONE YEAR
			 * PAST THE BASE
			 **/

			BigInteger converted = convert(token_date_time);

			int flag = 0;
			for (int i = 0; i < labels.size(); i++) {
				if ((converted.compareTo(labels.get(i).start) == 0 || converted
						.compareTo(labels.get(i).start) == 1)
						&& (converted.compareTo(labels.get(i).end) == 0 || converted
								.compareTo(labels.get(i).end) == -1)) {
					/**
					 * WRITING THE LABEL IF "converted" VARIABLE FALLS WITHIN
					 * "start" AND "end" TIME OF A PARTICULAR LABEL EXAMPLE :
					 * 20080415034030 (2008/04/15 03:40:30 FALLS WITHIN
					 * [2008/04/15 03:40:30 2008/04/15 04:16:10 car] AND IS
					 * HENCE LABELLED AS "car" )
					 * **/
					out.println(files[file] + "  " + labels.get(i).label);
					flag = 1;
					break;
				}
			}

			/**
			 * IF NO LABEL FOUND, i.e THE START TIME OF THIS TRAJECTORY DOESN'T
			 * FALL WITHIN THE START AND END TIME OF ANY STORED LABELS
			 **/
			if (flag == 0) {
				out.println(files[file] + "  " + "NO LABEL");
			}

		}
		out.close();
	}

	/** FUNCTION TO CONVERT 20080101000000 TO 31536000 WITH RESPECT TO THE BASE **/
	public BigInteger convert(String token_date_time) {

		// base is 2007/01/01 00:00:00
		// example 20080411083823

		String string_year = token_date_time.substring(0, 4);
		int our_year = Integer.parseInt(string_year);

		String string_month = token_date_time.substring(4, 6);
		int our_month = Integer.parseInt(string_month);

		String string_date = token_date_time.substring(6, 8);
		int our_date = Integer.parseInt(string_date);

		String string_hour = token_date_time.substring(8, 10);
		int our_hour = Integer.parseInt(string_hour);

		String string_min = token_date_time.substring(10, 12);
		int our_min = Integer.parseInt(string_min);

		String string_sec = token_date_time.substring(12, 14);
		int our_sec = Integer.parseInt(string_sec);

		BigInteger total = BigInteger.ZERO;

		/**
		 * FOR ALL YEARS PASSED OVER THE BASE, ADDING APPROPRIATE SECONDS
		 * CONSIDERING LEAP YEARS TOO
		 **/
		for (int year = 2007; year < our_year; year++) {
			if (leap_year(year))
				total = total.add(leap_one_year);
			else
				total = total.add(one_year);
		}

		/** FOR ALL MONTHS PASSED OVER THE BASE, ADDING APPROPRIATE SECONDS **/
		for (int month = 1; month < our_month; month++) {
			/** CONSIDERING MONTHS OF 31 DAYS **/
			if (month == 1 || month == 3 || month == 5 || month == 7
					|| month == 8 || month == 10 || month == 12)
				total = total.add(one_month_31);

			/** CONSIDERNG MONTHS OF 30 DAYS **/
			else if (month == 4 || month == 6 || month == 9 || month == 11)
				total = total.add(one_month_30);

			/** CONSIDERING FEB IN LEAP YEAR **/
			else if (month == 2 && leap_year(our_year))
				total = total.add(one_month_29);

			/** CONSIDERING FEB IN NON LEAP YEAR **/
			else if (month == 2)
				total = total.add(one_month_28);
		}

		/** FOR ALL DAYS PASSED OVER THE BASE, ADDING APPROPRIATE SECONDS **/
		for (int date = 1; date < our_date; date++) {
			total = total.add(one_day);
		}

		/** FOR ALL HOURS PASSED OVER THE BASE, ADDING APPROPRIATE SECONDS **/
		for (int hour = 0; hour < our_hour; hour++) {
			total = total.add(one_hour);
		}

		/** FOR ALL MINUTES PASSED OVER THE BASE, ADDING APPROPRIATE SECONDS **/
		for (int min = 0; min < our_min; min++) {
			total = total.add(one_min);
		}

		/** FOR ALL SECONDS PASSED OVER THE BASE, ADDING APPROPRIATE SECONDS **/
		total = total.add(BigInteger.valueOf(our_sec));

		return total;
	}

	/** CHECK IF AN YEAR IS A LEAP YEAR **/
	public boolean leap_year(int year) {
		if (year % 400 == 0)
			return true;
		else if (year % 100 == 0)
			return false;
		else if (year % 4 == 0)
			return true;
		else
			return false;
	}

	/** READ AND STORE LABELS **/
	public ArrayList<geolife_label> read_labels() throws Exception {
		ArrayList<geolife_label> labels = new ArrayList<geolife_label>();
		BufferedReader fileReader = new BufferedReader(new FileReader(
				id_directory + "labels.txt"));
		String line = "";
		while (true) {
			line = fileReader.readLine();
			if (line == null)
				break;
			if (line.startsWith("Start Time"))
				continue;
			else {
				// 2008/04/11 11:44:53 2008/04/11 12:10:53 car
				StringTokenizer stt = new StringTokenizer(line, " \t");
				String start_date = stt.nextToken().toString();
				String start_time = stt.nextToken().toString();
				String end_date = stt.nextToken().toString();
				String end_time = stt.nextToken().toString();
				String label = stt.nextToken().toString();
				start_date = start_date.replaceAll("/", "");
				start_time = start_time.replaceAll(":", "");
				end_date = end_date.replaceAll("/", "");
				end_time = end_time.replaceAll(":", "");
				// System.out.println(start_date + " " + start_time);
				geolife_label new_label = new geolife_label();
				/**
				 * 1. CONVERTING START (DATE+TIME) TO RELATIVE SECONDS FROM THE
				 * BASE 2. CONVERTING END (DATE+TIME) TO RELATIVE SECONDS FROM
				 * THE BASE 3. STORING THE CORRESPONDING LABEL
				 * **/

				new_label.start = convert(start_date + start_time);
				new_label.end = convert(end_date + end_time);
				new_label.label = label;

				labels.add(new_label);
			}

		}
		return labels;
	}
}
