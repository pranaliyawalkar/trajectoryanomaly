package org.example.anomaly.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;

public class AccuracyCheck {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		BufferedReader fileReader = new BufferedReader(
				new FileReader(
						"/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/outputs/outputValuesTempAccuracy_trajsizeVSnumofanoVSprocessingTime_pri1.txt"));
		while (true) {
			String line = fileReader.readLine();
			if (line == null)
				break;
			if (line.startsWith("File name :")) {
				System.out.println(line);
				continue;
			}
			if (!line.contains("$") || line.contains(" $$$$$$$$$"))
				continue;
			String[] twoOP = line.split("\\$");
			String[] ourOuts = twoOP[1].split("\\]")[0].split("\\[")[1].trim()
					.split(", ");
			String[] naiveOuts = twoOP[2].split("\\]")[0].split("\\[")[1]
					.trim().split(", ");
			String[] naiveOuts2 = twoOP[3].split("\\]")[0].split("\\[")[1]
					.trim().split(", ");

			if (naiveOuts.length != ourOuts.length)
				System.out.println("Error:" + line);
			ArrayList<String> naiveList = new ArrayList<String>(
					Arrays.asList(naiveOuts));
			ArrayList<String> naiveList2 = new ArrayList<String>(
					Arrays.asList(naiveOuts2));
			ArrayList<String> ourList = new ArrayList<String>(
					Arrays.asList(ourOuts));

			for (String string : ourList) {
				if (!naiveList.contains(string)) {
					System.out.println("Error1:" + line);
				}
				if (!naiveList2.contains(string)) {
					System.out.println("Error2:" + line);
				}
			}
			for (String string : naiveList) {
				if (!ourList.contains(string))
					System.out.println("Error1:" + line);
			}
			for (String string : naiveList2) {
				if (!ourList.contains(string))
					System.out.println("Error2:" + line);
			}
		}
		fileReader.close();
	}

}
