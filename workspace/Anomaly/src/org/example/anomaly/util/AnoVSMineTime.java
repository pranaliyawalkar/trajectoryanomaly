package org.example.anomaly.util;

import java.io.BufferedReader;
import java.io.FileReader;

public class AnoVSMineTime {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		double tot1_1 = 0, tot2_1 = 0, tot3_1 = 0;
		double tot1_2 = 0, tot2_2 = 0, tot3_2 = 0;
		double tot1_3 = 0, tot2_3 = 0, tot3_3 = 0;
		double tot1_4 = 0, tot2_4 = 0, tot3_4 = 0;
		double tot1_5 = 0, tot2_5 = 0, tot3_5 = 0;
		double tot1_6 = 0, tot2_6 = 0, tot3_6 = 0;

		double totO = 0.0, totN = 0.0, totSW = 0.0;

		double tot1 = 0, tot2 = 0, tot3 = 0, tot4 = 0, tot5 = 0, tot6 = 0;
		// 0.0,14536.0,33586.0,4793.0,523.0,46.0
		int count = 0, length = 0, maxLength = 0;
		for (int i = 1; i <= 1; i++) {
			BufferedReader fileReader = new BufferedReader(
					new FileReader(
							"/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/outputs/sythetic_20_clusterSize_45_trajsizeVSnumofanoVSprocessingTimeandProcessedEdges_pri1.csv"));

			// 2.1663326653306614,51.168336673346694,3813.567134268537
			// 0.3927855711422846,88.97394789579158,4540.018036072144
			while (true) {
				String line = fileReader.readLine();
				if (line == null)
					break;
				String[] staArr = line.split(",");
				if (staArr.length < 4)
					continue;
				int lengtht = Integer.parseInt(staArr[0]);
				if (lengtht > maxLength) {
					maxLength = lengtht;
				}
				length += lengtht;
				count++;
				totO += Integer.parseInt(staArr[2]);
				totN += Integer.parseInt(staArr[3]);
				totSW += Integer.parseInt(staArr[4]);
				if (Integer.parseInt(staArr[1]) <= 25) {
					tot1_1 += Integer.parseInt(staArr[2]);
					tot2_1 += Integer.parseInt(staArr[3]);
					tot3_1 += Integer.parseInt(staArr[4]);
					tot1++;
				} else if (Integer.parseInt(staArr[1]) <= 50) {
					tot1_2 += Integer.parseInt(staArr[2]);
					tot2_2 += Integer.parseInt(staArr[3]);
					tot3_2 += Integer.parseInt(staArr[4]);
					tot2++;
				} else if (Integer.parseInt(staArr[1]) <= 100) {
					tot1_3 += Integer.parseInt(staArr[2]);
					tot2_3 += Integer.parseInt(staArr[3]);
					tot3_3 += Integer.parseInt(staArr[4]);
					tot3++;
				} else if (Integer.parseInt(staArr[1]) <= 200) {
					tot1_4 += Integer.parseInt(staArr[2]);
					tot2_4 += Integer.parseInt(staArr[3]);
					tot3_4 += Integer.parseInt(staArr[4]);
					tot4++;
				} else if (Integer.parseInt(staArr[1]) <= 400) {
					tot1_5 += Integer.parseInt(staArr[2]);
					tot2_5 += Integer.parseInt(staArr[3]);
					tot3_5 += Integer.parseInt(staArr[4]);
					tot5++;
				} else {
					tot1_6 += Integer.parseInt(staArr[2]);
					tot2_6 += Integer.parseInt(staArr[3]);
					tot3_6 += Integer.parseInt(staArr[4]);
					tot6++;
				}
			}

			fileReader.close();
		}
		System.out.println((double) totO / count + "," + (double) totN / count
				+ "," + (double) totSW / count);
		System.out.println(count + "," + length / count + "," + maxLength);
		System.out.println(tot1 + "," + tot2 + "," + tot3 + "," + tot4 + ","
				+ tot5 + "," + tot6);
		// System.out.println(tot1_1 + "," + tot1_2 + "," + tot1_3 + "," +
		// tot1_4
		// + "," + tot1_5 + "," + tot1_6);
		System.out.println((double) tot1_1 / tot1 + "," + tot2_1 / tot1 + ","
				+ tot3_1 / tot1);
		System.out.println((double) tot1_2 / tot2 + "," + tot2_2 / tot2 + ","
				+ tot3_2 / tot2);
		System.out.println((double) tot1_3 / tot3 + "," + tot2_3 / tot3 + ","
				+ tot3_3 / tot3);
		System.out.println((double) tot1_4 / tot4 + "," + tot2_4 / tot4 + ","
				+ tot3_4 / tot4);
		System.out.println((double) tot1_5 / tot5 + "," + tot2_5 / tot5 + ","
				+ tot3_5 / tot5);
		System.out.println((double) tot1_6 / tot6 + "," + tot2_6 / tot6 + ","
				+ tot3_6 / tot6);

	}
}
