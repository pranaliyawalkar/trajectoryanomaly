package org.example.anomaly.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigInteger;

public class TimeCheck {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		// int tot1_1 = 0, tot2_1 = 0, tot3_1 = 0;
		// int tot1_2 = 0, tot2_2 = 0, tot3_2 = 0;
		// int tot1_3 = 0, tot2_3 = 0, tot3_3 = 0;
		// int tot1_4 = 0, tot2_4 = 0, tot3_4 = 0;
		// int tot1_5 = 0, tot2_5 = 0, tot3_5 = 0;
		// int tot1_6 = 0, tot2_6 = 0, tot3_6 = 0;

		BigInteger tot1_1 = BigInteger.valueOf(0), tot2_1 = BigInteger
				.valueOf(0), tot3_1 = BigInteger.valueOf(0);
		BigInteger tot1_2 = BigInteger.valueOf(0), tot2_2 = BigInteger
				.valueOf(0), tot3_2 = BigInteger.valueOf(0);
		BigInteger tot1_3 = BigInteger.valueOf(0), tot2_3 = BigInteger
				.valueOf(0), tot3_3 = BigInteger.valueOf(0);
		BigInteger tot1_4 = BigInteger.valueOf(0), tot2_4 = BigInteger
				.valueOf(0), tot3_4 = BigInteger.valueOf(0);
		BigInteger tot1_5 = BigInteger.valueOf(0), tot2_5 = BigInteger
				.valueOf(0), tot3_5 = BigInteger.valueOf(0);
		BigInteger tot1_6 = BigInteger.valueOf(0), tot2_6 = BigInteger
				.valueOf(0), tot3_6 = BigInteger.valueOf(0);

		BigInteger tot1 = BigInteger.valueOf(0), tot2 = BigInteger.valueOf(0), tot3 = BigInteger
				.valueOf(0), tot4 = BigInteger.valueOf(0), tot5 = BigInteger
				.valueOf(0), tot6 = BigInteger.valueOf(0);

		// int tot1 = 0, tot2 = 0, tot3 = 0, tot4 = 0, tot5 = 0, tot6 = 0;

		for (int i = 1; i <= 5; i++) {
			BufferedReader fileReader = new BufferedReader(
					new FileReader(
							"/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/outputs/outputTimeTempAccuracy_sd1.0_trajsizeVSnumofanoVSprocessingTimeandProcessedEdges_test_pri"
									+ i + ".csv"));

			while (true) {
				String line = fileReader.readLine();
				if (line == null)
					break;
				String[] staArr = line.split(",");
				if (staArr.length < 4)
					continue;
				if (Integer.parseInt(staArr[1]) <= 200) {
					// tot1_1 += Integer.parseInt(staArr[2]);
					// tot2_1 += Integer.parseInt(staArr[3]);
					// tot3_1 += Integer.parseInt(staArr[4]);
					// tot1++;

					tot1_1 = tot1_1.add(new BigInteger(staArr[2]));
					tot2_1 = tot2_1.add(new BigInteger(staArr[3]));
					tot3_1 = tot3_1.add(new BigInteger(staArr[4]));
					tot1 = tot1.add(new BigInteger("1"));

				} else if (Integer.parseInt(staArr[1]) <= 300) {
					// tot1_2 += Integer.parseInt(staArr[2]);
					// tot2_2 += Integer.parseInt(staArr[3]);
					// tot3_2 += Integer.parseInt(staArr[4]);
					// tot2++;
					tot1_2 = tot1_2.add(new BigInteger(staArr[2]));
					tot2_2 = tot2_2.add(new BigInteger(staArr[3]));
					tot3_2 = tot3_2.add(new BigInteger(staArr[4]));
					tot2 = tot2.add(new BigInteger("1"));
				} else if (Integer.parseInt(staArr[1]) <= 400) {
					// tot1_3 += Integer.parseInt(staArr[2]);
					// tot2_3 += Integer.parseInt(staArr[3]);
					// tot3_3 += Integer.parseInt(staArr[4]);
					// tot3++;
					tot1_3 = tot1_3.add(new BigInteger(staArr[2]));
					tot2_3 = tot2_3.add(new BigInteger(staArr[3]));
					tot3_3 = tot3_3.add(new BigInteger(staArr[4]));
					tot3 = tot3.add(new BigInteger("1"));
				} else if (Integer.parseInt(staArr[1]) <= 600) {
					// tot1_4 += Integer.parseInt(staArr[2]);
					// tot2_4 += Integer.parseInt(staArr[3]);
					// tot3_4 += Integer.parseInt(staArr[4]);
					// tot4++;
					tot1_4 = tot1_4.add(new BigInteger(staArr[2]));
					tot2_4 = tot2_4.add(new BigInteger(staArr[3]));
					tot3_4 = tot3_4.add(new BigInteger(staArr[4]));
					tot4 = tot4.add(new BigInteger("1"));
				} else if (Integer.parseInt(staArr[1]) <= 800) {
					// tot1_5 += Integer.parseInt(staArr[2]);
					// tot2_5 += Integer.parseInt(staArr[3]);
					// tot3_5 += Integer.parseInt(staArr[4]);
					// tot5++;
					tot1_5 = tot1_5.add(new BigInteger(staArr[2]));
					tot2_5 = tot2_5.add(new BigInteger(staArr[3]));
					tot3_5 = tot3_5.add(new BigInteger(staArr[4]));
					tot5 = tot5.add(new BigInteger("1"));
				} else {
					// tot1_6 += Integer.parseInt(staArr[2]);
					// tot2_6 += Integer.parseInt(staArr[3]);
					// tot3_6 += Integer.parseInt(staArr[4]);
					// tot6++;
					tot1_6 = tot1_6.add(new BigInteger(staArr[2]));
					tot2_6 = tot2_6.add(new BigInteger(staArr[3]));
					tot3_6 = tot3_6.add(new BigInteger(staArr[4]));
					tot6 = tot6.add(new BigInteger("1"));
				}
			}

			fileReader.close();
		}
		// System.out.println(tot1 + "," + tot2 + "," + tot3 + "," + tot4 + ","
		// + tot5);
		// System.out.println(tot1_1 + "," + tot1_2 + "," + tot1_3 + "," +
		// tot1_5);
		// System.out.println((double) tot1_1 / tot1 + "," + tot2_1 / tot1 + ","
		// + tot3_1 / tot1);
		// System.out.println((double) tot1_2 / tot2 + "," + tot2_2 / tot2 + ","
		// + tot3_2 / tot2);
		// System.out.println((double) tot1_3 / tot3 + "," + tot2_3 / tot3 + ","
		// + tot3_3 / tot3);
		// System.out.println((double) tot1_4 / tot4 + "," + tot2_4 / tot4 + ","
		// + tot3_4 / tot4);
		// System.out.println((double) tot1_5 / tot5 + "," + tot2_5 / tot5 + ","
		// + tot3_5 / tot5);
		// System.out.println((double) tot1_6 / tot6 + "," + tot2_6 / tot6 + ","
		// + tot3_6 / tot6);
		System.out.println(tot1 + "," + tot2 + "," + tot3 + "," + tot4 + ","
				+ tot5 + "," + tot6);
		System.out.println(tot1_1.divide(tot1) + "," + tot2_1.divide(tot1)
				+ "," + tot3_1.divide(tot1));
		System.out.println(tot1_2.divide(tot2) + "," + tot2_2.divide(tot2)
				+ "," + tot3_2.divide(tot2));
		System.out.println(tot1_3.divide(tot3) + "," + tot2_3.divide(tot3)
				+ "," + tot3_3.divide(tot3));
		System.out.println(tot1_4.divide(tot4) + "," + tot2_4.divide(tot4)
				+ "," + tot3_4.divide(tot4));
		System.out.println(tot1_5.divide(tot5) + "," + tot2_5.divide(tot5)
				+ "," + tot3_5.divide(tot5));
		System.out.println(tot1_6.divide(tot6) + "," + tot2_6.divide(tot6)
				+ "," + tot3_6.divide(tot6));

		/*
		 * 37452.0,53366.0,17841.0,7644.0,718.0,52.0
		1.1928601943821424,471.33047634305245,74.99813094093773
		2.320653599670202,1191.5682269609863,163.52572799160515
		5.199148029818956,3541.5466061319435,395.5957065186929
		10.564102564102564,10016.422815279959,864.4296180010466
		25.83983286908078,30269.456824512534,1988.2841225626742
		21.153846153846153,63449.36538461538,2268.346153846154
		 */

	}
}
