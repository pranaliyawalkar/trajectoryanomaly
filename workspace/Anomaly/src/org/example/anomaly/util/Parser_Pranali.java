package org.example.anomaly.util;

import java.io.BufferedReader;
import java.io.FileReader;

public class Parser_Pranali {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		BufferedReader fileReader = new BufferedReader(
				new FileReader(
						"/Users/itispris/Dropbox/newWorkspace/Pranali_Anomaly/src/comparison_walk_5.0_3.0.txt"));
		// /Users/itispris/Dropbox/newWorkspace/Pranali_Anomaly/src/comparison_car.txt

		int totC = 0, ourC = 0, countC = 0;
		double percentC = 0.0;
		while (true) {
			String line = fileReader.readLine();
			if (line == null)
				break;
			if (line.contains("Size of trajectory")) {
				int tot = Integer.parseInt(line.split(":")[1].trim());
				totC += tot;
				line = fileReader.readLine();
				int our = Integer.parseInt(line.split(":")[1].trim());
				if (tot > 0) {
					ourC += our;
					countC++;
					System.out.println((double) our / tot);
					percentC += (double) our / tot;

				}
			}

		}
		fileReader.close();
		System.out.println((double) percentC / countC + "," + (double) ourC
				/ totC);
	}

}
