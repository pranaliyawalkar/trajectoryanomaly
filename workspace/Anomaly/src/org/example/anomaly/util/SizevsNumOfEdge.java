package org.example.anomaly.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigInteger;

public class SizevsNumOfEdge {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		BigInteger tot1_1 = BigInteger.valueOf(0), tot2_1 = BigInteger
				.valueOf(0), tot3_1 = BigInteger.valueOf(0);
		BigInteger tot1_2 = BigInteger.valueOf(0), tot2_2 = BigInteger
				.valueOf(0), tot3_2 = BigInteger.valueOf(0);
		BigInteger tot1_3 = BigInteger.valueOf(0), tot2_3 = BigInteger
				.valueOf(0), tot3_3 = BigInteger.valueOf(0);
		BigInteger tot1_4 = BigInteger.valueOf(0), tot2_4 = BigInteger
				.valueOf(0), tot3_4 = BigInteger.valueOf(0);
		BigInteger tot1_5 = BigInteger.valueOf(0), tot2_5 = BigInteger
				.valueOf(0), tot3_5 = BigInteger.valueOf(0);

		BigInteger tot1_6 = BigInteger.valueOf(0), tot2_6 = BigInteger
				.valueOf(0), tot3_6 = BigInteger.valueOf(0);

		BigInteger tot1 = BigInteger.valueOf(0), tot2 = BigInteger.valueOf(0), tot3 = BigInteger
				.valueOf(0), tot4 = BigInteger.valueOf(0), tot5 = BigInteger
				.valueOf(0), tot6 = BigInteger.valueOf(0);

		BigInteger totO = BigInteger.valueOf(0), totSW = BigInteger.valueOf(0);
		for (int i = 1; i <= 1; i++) {
			BufferedReader fileReader = new BufferedReader(
					new FileReader(
							"/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/outputs/sythetic_Test_last_15_clusterSize_5_trajsizeVSnumofanoVSprocessingTimeandProcessedEdges_pri1.csv"));

			while (true) {
				String line = fileReader.readLine();
				if (line == null)
					break;
				String[] staArr = line.split(",");
				if (staArr.length < 4)
					continue;
				totO = totO.add(new BigInteger(staArr[5]));
				totSW = totSW.add(new BigInteger(staArr[6]));
				if (Integer.parseInt(staArr[1]) <= 25) {
					// tot1_1 += Integer.parseInt(staArr[1]);
					tot2_1 = tot2_1.add(new BigInteger(staArr[5]));
					tot3_1 = tot3_1.add(new BigInteger(staArr[6]));
					tot1 = tot1.add(new BigInteger("1"));
				} else if (Integer.parseInt(staArr[1]) <= 50) {
					// tot1_2 += Integer.parseInt(staArr[1]);
					tot2_2 = tot2_2.add(new BigInteger(staArr[5]));
					tot3_2 = tot3_2.add(new BigInteger(staArr[6]));
					tot2 = tot2.add(new BigInteger("1"));
				} else if (Integer.parseInt(staArr[1]) <= 100) {
					// tot1_3 += Integer.parseInt(staArr[1]);
					tot2_3 = tot2_3.add(new BigInteger(staArr[5]));
					tot3_3 = tot3_3.add(new BigInteger(staArr[6]));
					tot3 = tot3.add(new BigInteger("1"));
				} else if (Integer.parseInt(staArr[1]) <= 200) {
					// tot1_4 += Integer.parseInt(staArr[1]);
					tot2_4 = tot2_4.add(new BigInteger(staArr[5]));
					tot3_4 = tot3_4.add(new BigInteger(staArr[6]));
					tot4 = tot4.add(new BigInteger("1"));
				} else if (Integer.parseInt(staArr[1]) <= 400) {
					// tot1_4 += Integer.parseInt(staArr[1]);
					tot2_5 = tot2_5.add(new BigInteger(staArr[5]));
					tot3_5 = tot3_5.add(new BigInteger(staArr[6]));
					tot5 = tot5.add(new BigInteger("1"));
				} else {
					// tot1_5 += Integer.parseInt(staArr[1]);
					tot2_6 = tot2_6.add(new BigInteger(staArr[5]));
					tot3_6 = tot3_6.add(new BigInteger(staArr[6]));
					tot6 = tot6.add(new BigInteger("1"));
				}
			}

			fileReader.close();
		}
		System.out.println(totO.divide(BigInteger.valueOf(99)) + ","
				+ totSW.divide(BigInteger.valueOf(99)));
		System.out.println(tot1 + "," + tot2 + "," + tot3 + "," + tot4 + ","
				+ tot5 + "," + tot6);
		// System.out.println(tot1_1.divide(tot1) + "," + tot2_1.divide(tot1)
		// + "," + tot3_1.divide(tot1));
		System.out.println(tot1_2.divide(tot2) + "," + tot2_2.divide(tot2)
				+ "," + tot3_2.divide(tot2));
		System.out.println(tot1_3.divide(tot3) + "," + tot2_3.divide(tot3)
				+ "," + tot3_3.divide(tot3));
		System.out.println(tot1_4.divide(tot4) + "," + tot2_4.divide(tot4)
				+ "," + tot3_4.divide(tot4));
		System.out.println(tot1_5.divide(tot5) + "," + tot2_5.divide(tot5)
				+ "," + tot3_5.divide(tot5));
		System.out.println(tot1_6.divide(tot6) + "," + tot2_6.divide(tot6)
				+ "," + tot3_6.divide(tot6));

	}
}
