package org.example.anomaly.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.example.anomaly.data.Point;

public class TrajSizeCount {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		String mapMatchedTrajDirPath = "/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/part";
		// PrintWriter outTime = new PrintWriter(new OutputStreamWriter(
		// new FileOutputStream("/Users/itispris/Dropbox/outs.txt"),
		// "UTF-8"));
		int c10 = 0, c250 = 0, c500 = 0, c1000 = 0, c2000 = 0, c5000 = 0;

		for (int i = 1; i <= 5; i++) {
			File docDir = new File(mapMatchedTrajDirPath + i);
			String[] files = docDir.list();
			for (int fileNum = 0; fileNum < files.length; fileNum++) {

				System.out.println("File name : " + files[fileNum]);

				BufferedReader fileReader = new BufferedReader(new FileReader(
						mapMatchedTrajDirPath + i + "/" + files[fileNum]));
				while (true) {
					ArrayList<Point> arr = new ArrayList<Point>();
					String line = fileReader.readLine();
					if (line == null)
						break;
					if (line.contains("[")) {
						String[] warr = line.split("\\]");
						for (String s : warr) {
							try {
								int id = Integer.parseInt(s.split(",")[0]
										.split("\\[")[1]);
								double lat = Double
										.parseDouble(s.split(",")[1]);
								double lon = Double
										.parseDouble(s.split(",")[2]);
								Date time = new SimpleDateFormat(
										"yyyy-MM-dd HH:mm:ss").parse(s
										.split(",")[3]);
								arr.add(new Point(id, lat, lon, time));
							} catch (Exception e) {
								System.out.println(line);
							}
						}

					}
					if (arr.size() <= 10)
						c10++;
					if (arr.size() > 10 && arr.size() <= 250)
						c250++;
					if (arr.size() > 250 && arr.size() <= 500)
						c500++;
					if (arr.size() > 500 && arr.size() <= 1000)
						c1000++;
					if (arr.size() > 1000 && arr.size() <= 2000)
						c2000++;
					if (arr.size() > 2000 && arr.size() <= 5000)
						c5000++;
				}
				fileReader.close();

			}
		}
		System.out.println(c10 + "," + c250 + "," + c500 + "," + c1000 + ","
				+ c2000 + "," + c5000);
	}

}
