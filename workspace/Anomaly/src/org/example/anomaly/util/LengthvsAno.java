package org.example.anomaly.util;

import java.io.BufferedReader;
import java.io.FileReader;

public class LengthvsAno {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// BigInteger tot1 = BigInteger.valueOf(0), tot2 =
		// BigInteger.valueOf(0), tot3 = BigInteger
		// .valueOf(0), tot4 = BigInteger.valueOf(0), tot5 = BigInteger
		// .valueOf(0);
		int tot1 = 0, tot2 = 0, tot3 = 0, tot4 = 0, tot5 = 0;
		double ratio1 = 0.0, ratio2 = 0.0, ratio3 = 0.0, ratio4 = 0.0, ratio5 = 0.0;
		for (int i = 1; i <= 5; i++) {
			BufferedReader fileReader = new BufferedReader(
					new FileReader(
							"/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/outputs/outputTimeTempAccuracy_trajsizeVSnumofanoVSnumofprocessededge_pri"
									+ i + ".csv"));

			while (true) {
				String line = fileReader.readLine();
				if (line == null)
					break;
				String[] staArr = line.split(",");
				if (staArr.length < 4)
					continue;
				if (Integer.parseInt(staArr[1]) <= 200) {
					ratio1 += (double) Integer.parseInt(staArr[1])
							/ Integer.parseInt(staArr[0]);
					tot1++;
				} else if (Integer.parseInt(staArr[1]) <= 400) {
					// tot1_2 += Integer.parseInt(staArr[1]);
					ratio2 += (double) Integer.parseInt(staArr[1])
							/ Integer.parseInt(staArr[0]);
					tot2++;
				} else if (Integer.parseInt(staArr[1]) <= 600) {
					// tot1_3 += Integer.parseInt(staArr[1]);
					ratio3 += (double) Integer.parseInt(staArr[1])
							/ Integer.parseInt(staArr[0]);
					tot3++;
				} else if (Integer.parseInt(staArr[1]) <= 800) {
					// tot1_4 += Integer.parseInt(staArr[1]);
					ratio4 += (double) Integer.parseInt(staArr[1])
							/ Integer.parseInt(staArr[0]);
					tot4++;
				} else if (Integer.parseInt(staArr[1]) <= 1000) {
					// tot1_5 += Integer.parseInt(staArr[1]);
					ratio5 += (double) Integer.parseInt(staArr[1])
							/ Integer.parseInt(staArr[0]);
					tot5++;
				}
			}

			fileReader.close();
		}
		System.out.println(tot1 + "," + tot2 + "," + tot3 + "," + tot4 + ","
				+ tot5);
		System.out.println(ratio1 / tot1 + "," + ratio2 / tot2 + "," + ratio3
				/ tot3 + "," + ratio4 / tot4 + "," + ratio5 / tot5);

	}

}
