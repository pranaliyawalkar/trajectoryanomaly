package org.example.anomaly.util;

import java.io.BufferedReader;
import java.io.FileReader;

public class Parser_car {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		BufferedReader fileReader = new BufferedReader(new FileReader(
				"/Users/itispris/dropbox_backup/comparison_car.txt"));

		int totC = 0, ourC = 0, countC = 0;
		double percentC = 0.0;
		while (true) {
			String line = fileReader.readLine();
			if (line == null)
				break;
			if (line.contains("$") && !line.contains("$$$$")) {
				String[] arr = line.split("\\$");
				System.out.println(line);
				int tot = Integer.parseInt(arr[2].trim());
				int our = 0;
				String[] temp = arr[1].split("\\]");
				String[] temp2 = temp[0].split("\\[");
				if (temp2.length > 1) {
					String[] arr2 = temp2[1].split(",");
					int lastIndex = 0;
					for (int i = 0; i < arr2.length; i = i + 2) {
						int start = Integer.parseInt(arr2[i].trim());
						int end = Integer.parseInt(arr2[i + 1].trim());
						if (start > lastIndex) {
							our += end - start;
							lastIndex = end;
						} else {
							int repeat = lastIndex - start + 1;
							our += end - start - repeat;
						}
					}
				}

				totC += tot;
				ourC += our;
				percentC += (double) our / tot;
				countC++;
			}

		}
		fileReader.close();
		System.out.println((double) percentC / countC + "," + (double) ourC
				/ totC);
	}

}
