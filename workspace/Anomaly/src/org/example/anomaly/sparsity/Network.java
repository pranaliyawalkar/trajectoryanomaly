package org.example.anomaly.sparsity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Network {

	private PrintWriter outCorr;
	ArrayList<Node> nodeList = new ArrayList<Node>();
	HashMap<Integer, Integer> nodeMap = new HashMap<Integer, Integer>();

	public void loadNetwork() throws Exception {
		String line;
		int prevId;
		for (int part = 1; part < 6; part++) {
			File docDir = new File("/Users/prithu/trajData/inputs/part" + part);
			String[] files = docDir.list();
			int fileNum = 0;
			int nodeC = 0;

			for (fileNum = 0; fileNum < files.length; fileNum++) {

				BufferedReader fileReader = new BufferedReader(new FileReader(
						"/Users/prithu/trajData/inputs/part" + part + "/"
								+ files[fileNum]));
				while (true) {

					line = fileReader.readLine();
					if (line == null)
						break;

					prevId = -1;
					if (line.contains("[")) {
						String[] warr = line.split("\\]");
						for (String s : warr) {
							try {
								int id = Integer.parseInt(s.split(",")[0]
										.split("\\[")[1]);
								if (!nodeMap.keySet().contains(id)) {

									Node n = new Node(id);
									nodeList.add(n);
									nodeMap.put(id, nodeC++);
								}
								if (prevId != -1) {
									Node prevNode = nodeList.get(nodeMap
											.get(prevId));
									if (!prevNode.neighbors.contains(id))
										prevNode.neighbors.add(id);
								}
								prevId = id;
							} catch (Exception e) {
								System.out.println(line);
							}
						}

					}

				}
				fileReader.close();
			}
		}
	}

	public boolean writeHopCorel2(List<Integer> nodeSequence, int totC)
			throws Exception {

		int trial = 0;
		int hop = 1;
		if (totC < 18)
			hop = 6;
		// int seedEdgeId = (int) (Math.random() * nodeSequence.size() - 2);
		// if (seedEdgeId < 0)
		// seedEdgeId = 0;
		while (hop < 11) {
			if (trial > 100)
				break;

			int seedEdgeId = (int) (Math.random() * nodeSequence.size() - 2);
			if (seedEdgeId < 0)
				seedEdgeId = 0;
			int compareEdgeId = findEnd(seedEdgeId, hop, nodeSequence);
			if (compareEdgeId == 0) {
				trial++;
				// hop--;
				continue;
			}

			if (compareEdgeId < nodeSequence.size()) {
				// int compareEdgeId = seedEdgeId + hop;
				outCorr = new PrintWriter(new OutputStreamWriter(
						new FileOutputStream("spar/test2_" + totC + "_" + hop
								+ ".csv"), "UTF-8"));
				if (writeNumTraj2(
						nodeSequence.subList(seedEdgeId, compareEdgeId),
						outCorr)) {
					outCorr.close();
				} else {
					trial++;
					hop--;
					// seedEdgeId = (int) (Math.random() * nodeSequence.size() -
					// 2);
					// if (seedEdgeId < 0)
					// seedEdgeId = 0;
				}
			} else {
				trial++;
				hop--;
				// seedEdgeId = (int) (Math.random() * nodeSequence.size() - 2);
				// if (seedEdgeId < 0)
				// seedEdgeId = 0;
			}
			hop++;
		}
		if (trial > 100)
			return false;

		return true;

	}

	private boolean writeNumTraj2(List<Integer> subArr, PrintWriter outCorr)
			throws Exception {
		int count = 0;
		String line;
		for (int part = 1; part < 6; part++) {
			File docDir = new File("/Users/prithu/trajData/inputs/part" + part);
			String[] files = docDir.list();
			int fileNum = 0;

			for (fileNum = 0; fileNum < files.length; fileNum++) {

				// System.out.println("File name : " + files[fileNum]);

				BufferedReader fileReader = new BufferedReader(new FileReader(
						"/Users/prithu/trajData/inputs/part" + part + "/"
								+ files[fileNum]));
				while (true) {

					ArrayList<Integer> arr = new ArrayList<Integer>();
					line = fileReader.readLine();
					if (line == null)
						break;

					if (line.contains("[")) {
						String[] warr = line.split("\\]");
						for (String s : warr) {
							try {
								int id = Integer.parseInt(s.split(",")[0]
										.split("\\[")[1]);
								arr.add(id);
							} catch (Exception e) {
								System.out.println(line);
							}
						}

					}
					if (arr.containsAll(subArr))
						count++;

				}
				fileReader.close();
			}
		}

		outCorr.println(count);
		outCorr.flush();
		return true;
	}

	public boolean writeHopCorel(List<Integer> nodeSequence, int totC)
			throws Exception {

		int trial = 0;
		int seedEdgeId = (int) (Math.random() * nodeSequence.size() - 2);
		if (seedEdgeId < 0)
			seedEdgeId = 0;
		for (int hop = 1; hop < 6; hop++) {
			if (trial > 100)
				break;
			int compareEdgeId = findEnd(seedEdgeId, hop, nodeSequence);
			if (compareEdgeId == 0) {
				trial++;
				hop = 0;
				continue;
			}

			if (compareEdgeId < nodeSequence.size()) {
				// int compareEdgeId = seedEdgeId + hop;
				outCorr = new PrintWriter(new OutputStreamWriter(
						new FileOutputStream("spar/test_" + totC + "_" + hop
								+ ".csv"), "UTF-8"));
				if (writeNumTraj(
						nodeSequence.subList(seedEdgeId, compareEdgeId),
						outCorr)) {
					outCorr.close();
				} else {
					trial++;
					hop = 0;
					seedEdgeId = (int) (Math.random() * nodeSequence.size() - 2);
					if (seedEdgeId < 0)
						seedEdgeId = 0;
				}
			} else {
				trial++;
				hop = 0;
				seedEdgeId = (int) (Math.random() * nodeSequence.size() - 2);
				if (seedEdgeId < 0)
					seedEdgeId = 0;
			}
		}
		if (trial > 100)
			return false;

		return true;

	}

	private int findEnd(int seedEdgeId, int hop, List<Integer> nodeSequence) {

		int returnID = seedEdgeId;
		while (hop > 0) {
			returnID++;
			if (returnID >= nodeSequence.size())
				return 0;
			if (numOfNeighbors(nodeSequence.get(returnID)) > 2) {
				hop--;
			}
		}
		return returnID;
	}

	private int numOfNeighbors(Integer integer) {

		Node n = nodeList.get(nodeMap.get(integer));
		return n.neighbors.size();
	}

	private boolean writeNumTraj(List<Integer> subArr, PrintWriter outCorr)
			throws Exception {

		int count = 0;
		String line;
		for (int part = 1; part < 6; part++) {
			File docDir = new File("/Users/prithu/trajData/inputs/part" + part);
			String[] files = docDir.list();
			int fileNum = 0;

			for (fileNum = 0; fileNum < files.length; fileNum++) {

				// System.out.println("File name : " + files[fileNum]);

				BufferedReader fileReader = new BufferedReader(new FileReader(
						"/Users/prithu/trajData/inputs/part" + part + "/"
								+ files[fileNum]));
				while (true) {

					ArrayList<Integer> arr = new ArrayList<Integer>();
					line = fileReader.readLine();
					if (line == null)
						break;

					if (line.contains("[")) {
						String[] warr = line.split("\\]");
						for (String s : warr) {
							try {
								int id = Integer.parseInt(s.split(",")[0]
										.split("\\[")[1]);
								arr.add(id);
							} catch (Exception e) {
								System.out.println(line);
							}
						}

					}
					if (arr.containsAll(subArr))
						count++;

				}
				fileReader.close();
			}
		}

		outCorr.println(count);
		outCorr.flush();
		return true;

	}

}
