package org.example.anomaly.sparsity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.example.anomaly.data.Point;

public class Holder {
	private String mapMatchedTrajDirPath;
	private Network nw;

	int totC2;

	public Holder() throws Exception {

		int partID = 1;

		mapMatchedTrajDirPath = "/Users/prithu/trajData/inputs/part" + partID;
		nw = new Network();
		nw.loadNetwork();
		// totC1 = 0;
		totC2 = 0;
	}

	private void findCorr() throws Exception {
		System.out.println(nw.nodeMap.get(1856));
		System.out.println(nw.nodeList.get(nw.nodeMap.get(1856)).id);

		File docDir = new File(mapMatchedTrajDirPath);
		String[] files = docDir.list();
		int fileNum = 0;
		double diff = 0.0;

		for (fileNum = 0; fileNum < files.length; fileNum++) {

			if (totC2 > 50)
				break;
			System.out.println("File name : " + files[fileNum]);
			BufferedReader fileReader = new BufferedReader(new FileReader(
					mapMatchedTrajDirPath + "/" + files[fileNum]));
			while (true) {

				ArrayList<Point> arr = new ArrayList<Point>();
				String line = fileReader.readLine();
				if (line == null)
					break;
				if (totC2 > 50) {
					System.out.println(diff / 200);
					break;
				}
				if (line.contains("[")) {
					String[] warr = line.split("\\]");
					for (String s : warr) {
						try {
							int id = Integer.parseInt(s.split(",")[0]
									.split("\\[")[1]);
							double lat = Double.parseDouble(s.split(",")[1]);
							double lon = Double.parseDouble(s.split(",")[2]);
							Date time = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss")
									.parse(s.split(",")[3]);
							arr.add(new Point(id, lat, lon, time));
						} catch (Exception e) {
							System.out.println(line);
						}
					}
					if (arr.size() > 75) {
						compare(arr);

						// totC++;
					}
				}

			}
			fileReader.close();

		}

	}

	private void compare(ArrayList<Point> pt) throws Exception {

		List<Integer> nodeSequence = new ArrayList<Integer>();
		List<Double> travelTime = new ArrayList<Double>();
		for (int i = 0; i < pt.size(); i++) {
			nodeSequence.add(pt.get(i).id);
			if (i < (pt.size() - 1)) {
				double time = pt.get(i + 1).timeStamp.getTime()
						- pt.get(i).timeStamp.getTime();
				travelTime.add(time);
			}
		}
		// if (nw.writeHopCorel(nodeSequence, totC1))
		// totC1++;
		if (nw.writeHopCorel2(nodeSequence, totC2))
			totC2++;
	}

	public static void main(String[] args) throws Exception {
		Holder hld = new Holder();
		hld.findCorr();

	}

}
