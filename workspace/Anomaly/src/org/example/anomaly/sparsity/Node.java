package org.example.anomaly.sparsity;

import java.util.ArrayList;

public class Node {

	public ArrayList<Integer> neighbors;
	int id;

	public Node(int id) {
		this.id = id;
		neighbors = new ArrayList<Integer>();
	}
}
