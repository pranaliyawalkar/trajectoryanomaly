package org.example.anomaly.wrapper;

import java.io.PrintWriter;
import java.util.ArrayList;

public class ProcessTrajectory {

	PrintWriter out;

	public ProcessTrajectory() {

	}

	public ArrayList<GeolifePoint> processPoints(
			ArrayList<GeolifePoint> rawPoints,
			ArrayList<GeolifePoint> mappedPoints) {

		ArrayList<GeolifePoint> retArr = new ArrayList<GeolifePoint>();
		GeolifePoint g = new GeolifePoint(mappedPoints.get(0).lat,
				mappedPoints.get(0).lon, rawPoints.get(0).timeStamp);
		retArr.add(g);

		int mappedSoFar = 1;
		for (int i = 1; i < rawPoints.size(); i++) {
			GeolifePoint source = rawPoints.get(i - 1);
			GeolifePoint desti = rawPoints.get(i);
			double dist = latLonDist(desti.lat, desti.lon, source.lat,
					source.lon);
			long timeDiff = desti.timeStamp - source.timeStamp;
			double mappedDist = 0.0;
			for (int j = mappedSoFar; j < mappedPoints.size(); j++) {
				GeolifePoint mappedSource = mappedPoints.get(j - 1);
				GeolifePoint mappedDesti = mappedPoints.get(j);
				mappedDist += latLonDist(mappedDesti.lat, mappedDesti.lon,
						mappedSource.lat, mappedSource.lon);
				if (mappedDist > dist || j == mappedPoints.size() - 1) {
					// int edges = j - mappedSoFar + 1;
					timeDiff = (long) (timeDiff / dist * mappedDist);
					// long timePerEdge = timeDiff / edges;
					while (mappedSoFar <= j) {
						long timeForThisEdge = (long) (timeDiff / mappedDist * latLonDist(
								mappedPoints.get(mappedSoFar).lat,
								mappedPoints.get(mappedSoFar).lon,
								mappedPoints.get(mappedSoFar - 1).lat,
								mappedPoints.get(mappedSoFar - 1).lon));
						if (timeForThisEdge == 0)
							System.out.println("kelo");
						long time = retArr.get(mappedSoFar - 1).timeStamp
								+ timeForThisEdge;
						g = new GeolifePoint(mappedPoints.get(mappedSoFar).lat,
								mappedPoints.get(mappedSoFar).lon, time);
						retArr.add(g);
						mappedSoFar++;
					}
					break;
				}

			}

		}

		return retArr;

	}

	private double latLonDist(double dlat, double dlon, double slat, double slon) {
		double radius = 6371;
		double dLat = toRadians((slat - dlat));
		double dLon = toRadians((slon - dlon));
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(toRadians(slat)) * Math.cos(toRadians(dlat))
				* Math.sin(dLon / 2) * Math.sin(dLon / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return (radius * c); // Distance in km }
	}

	private double toRadians(double d) {
		return d * Math.PI / 180;
	}

}
