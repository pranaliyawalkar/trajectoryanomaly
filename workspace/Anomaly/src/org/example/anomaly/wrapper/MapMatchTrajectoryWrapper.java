package org.example.anomaly.wrapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class MapMatchTrajectoryWrapper {

	String filePath;
	String dirPath;
	ProcessTrajectory pt;
	PrintWriter outCSV;

	public MapMatchTrajectoryWrapper() throws Exception {
		filePath = "/Users/itispris/Dropbox/speedcar_filename.csv";
		pt = new ProcessTrajectory();
		outCSV = new PrintWriter(new OutputStreamWriter(new FileOutputStream(
				"/Users/itispris/Dropbox/speedcar_filename_new.csv"), "UTF-8"));

	}

	public void readTrajs() throws Exception {
		BufferedReader fileReader = new BufferedReader(new FileReader(filePath));
		String id = null, name = null;
		while (true) {
			String line = fileReader.readLine();
			if (line == null)
				break;
			if (line.contains(":"))
				id = line.split(":")[1].trim();
			else if (line.contains(".plt"))
				name = line.split("_")[1];
			else {
				processTraj(id, name);
			}

		}
		fileReader.close();
		outCSV.close();
	}

	private void processTraj(String id, String name) throws Exception {

		String path = "/Users/itispris/Dropbox/projects/Anomaly/geolife_labeled";
		String dir = path + File.separator + id;
		String mappedDir = dir + File.separator + "mappedTrajectory";
		String rawDir = dir + File.separator + "Trajectory";

		BufferedReader mappedFileReader = new BufferedReader(new FileReader(
				mappedDir + File.separator + "mapped_" + name));

		BufferedReader rawFileReader = new BufferedReader(new FileReader(rawDir
				+ File.separator + name));
		ArrayList<GeolifePoint> rawPoints, mappedPoints;

		rawPoints = readRawTraj(rawFileReader);
		mappedPoints = readMappedTraj(mappedFileReader);

		// printPoints(pt.processPoints(rawPoints, mappedPoints), dir, name);
		printPointsCSV(pt.processPoints(rawPoints, mappedPoints), id, name);

	}

	private void printPointsCSV(ArrayList<GeolifePoint> processPoints,
			String id, String name) {

		outCSV.println("ID:" + id);
		outCSV.println("mapped_" + name);
		for (int i = 0; i < processPoints.size() - 1; i++) {
			double dist = latLonDist(processPoints.get(i + 1).lat,
					processPoints.get(i + 1).lon, processPoints.get(i).lat,
					processPoints.get(i + 1).lon);
			long time = (processPoints.get(i + 1).timeStamp - processPoints
					.get(i).timeStamp);
			outCSV.print(dist / time * 1000 * 3600 + ",");
		}
		outCSV.println();

	}

	private void printPoints(ArrayList<GeolifePoint> processPoints, String dir,
			String name) throws Exception {
		String opDir = dir + File.separator + "wrappedTrajectory";
		if (checkAndCreateDir(opDir)) {
			PrintWriter out = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(opDir + File.separator + name),
					"UTF-8"));
			for (GeolifePoint g : processPoints) {
				out.print("[" + g.lat + "," + g.lon + "," + g.timeStamp + "]");
			}
			out.close();
		}

	}

	public static boolean checkAndCreateDir(String fqDirName) {
		File dir;

		try {
			dir = new File(fqDirName);
			if (!dir.exists())
				return dir.mkdir();
			String[] entries = dir.list();
			for (String s : entries) {
				File currentFile = new File(dir.getPath(), s);
				currentFile.delete();
			}
			dir.delete();
			return dir.mkdir();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;

	}

	private ArrayList<GeolifePoint> readMappedTraj(
			BufferedReader mappedFileReader) throws Exception {

		ArrayList<GeolifePoint> arr = new ArrayList<GeolifePoint>();
		String line;
		while (true) {
			line = mappedFileReader.readLine();
			if (line == null)
				break;
			if (line.contains("]")) {
				String[] points = line.split("]");
				for (String s : points) {
					String[] values = s.split(",");
					if (arr.size() > 0
							&& (arr.get(arr.size() - 1).lat == Double
									.parseDouble(values[1].trim()))
							&& (arr.get(arr.size() - 1).lon == Double
									.parseDouble(values[2].trim())))
						continue;
					GeolifePoint g = new GeolifePoint(
							Double.parseDouble(values[1].trim()),
							Double.parseDouble(values[2].trim()),
							Long.parseLong(values[3].trim()));
					arr.add(g);
				}
			}
		}

		mappedFileReader.close();
		return arr;
	}

	private ArrayList<GeolifePoint> readRawTraj(BufferedReader rawFileReader)
			throws Exception {

		ArrayList<GeolifePoint> arr = new ArrayList<GeolifePoint>();
		String line;

		for (int i = 0; i < 6; i++) {
			rawFileReader.readLine();
		}
		while ((line = rawFileReader.readLine()) != null) {

			// skip comments and empty lines
			if (isComment(line))
				continue;

			// Get the points
			String[] splitString = line.trim().split(",");

			String timeStr = splitString[splitString.length - 2].trim() + " "
					+ splitString[splitString.length - 1].trim();

			double lat = Double.parseDouble(splitString[0].trim());

			double lon = Double.parseDouble(splitString[1].trim());

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			// Beijing is not present, use shanghai
			sdf.setTimeZone(TimeZone.getTimeZone("China/Shanghai"));
			Date date;
			try {
				date = sdf.parse(timeStr);
			} catch (ParseException e) {

				e.printStackTrace();
				throw new IllegalStateException(
						"Improperly formatted map-match input. Date string invalid"
								+ timeStr);
			}
			if (arr.size() > 0
					&& (arr.get(arr.size() - 1).timeStamp == date.getTime()))
				continue;

			if (arr.size() > 0 && (arr.get(arr.size() - 1).lat == lat)
					&& (arr.get(arr.size() - 1).lon == lon))
				continue;

			GeolifePoint g = new GeolifePoint(lat, lon, date.getTime());
			arr.add(g);

		}
		rawFileReader.close();
		return arr;
	}

	private double latLonDist(double dlat, double dlon, double slat, double slon) {
		double radius = 6371;
		double dLat = toRadians((slat - dlat));
		double dLon = toRadians((slon - dlon));
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(toRadians(slat)) * Math.cos(toRadians(dlat))
				* Math.sin(dLon / 2) * Math.sin(dLon / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return (radius * c); // Distance in km }
	}

	private double toRadians(double d) {
		return d * Math.PI / 180;
	}

	private boolean isComment(String str) {
		return (str.startsWith("//") || str.startsWith("#"));
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		MapMatchTrajectoryWrapper mtw = new MapMatchTrajectoryWrapper();
		mtw.readTrajs();
	}
}
