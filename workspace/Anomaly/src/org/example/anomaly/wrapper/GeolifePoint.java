package org.example.anomaly.wrapper;

public class GeolifePoint {

	double lat, lon;
	long timeStamp;

	public GeolifePoint(double lat, double lon, long timeStamp) {
		this.lat = lat;
		this.lon = lon;
		this.timeStamp = timeStamp;
	}
}
