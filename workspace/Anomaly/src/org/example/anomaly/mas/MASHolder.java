package org.example.anomaly.mas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.example.anomaly.data.Point;

public class MASHolder {
	String logFilePath = "";
	String myOutputFilePathValues = "";
	String myOutputFilePathTime = "";
	String myOutputFilePathTimeTxt = "";
	String mapMatchedTrajDirPath = "";
	String learnedEdgeDistroFilePath = "";

	PrintWriter outValues, outTime, outTimeTxt;
	EdgeProcessing ep;
	int mined_traj_count;
	int tot_traj_count;
	int training_miss_count;

	public MASHolder() {
		// ep = new EdgeProcessing(learnedEdgeDistroFilePath, 2.0);

	}

	public MASHolder(int i) {
		mined_traj_count = 1;
		tot_traj_count = 1;
		training_miss_count = 1;
		// int revision = 2;

		// myOutputFilePathTime = "/src/outputs/outputTimeTempAccuracy" + i
		// + ".csv";
		// myOutputFilePathValues = "/src/outputs/outputValuesTempAccuracy" + i
		// + ".txt";
		// myOutputFilePathTimeTxt = "/src/outputs/outputTimeTxtTempAccuracy" +
		// i
		// + ".txt";
		// mapMatchedTrajDirPath = "/src/inputs/part" + i;
		// learnedEdgeDistroFilePath = "/src/inputs/out_ad" + i + ".csv";

		myOutputFilePathTime = "/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/outputs/outputTimeTempAccuracyTest_edgeProcessed_0.5_pri"
				+ i + ".csv";
		myOutputFilePathValues = "/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/outputs/outputValuesTempAccuracy_numofanoedge_pri"
				+ i + ".txt";
		myOutputFilePathTimeTxt = "/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/outputs/outputTimeTxtTempAccuracy_numofanoedge_pri"
				+ i + ".txt";
		mapMatchedTrajDirPath = "/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/part"
				+ i;
		learnedEdgeDistroFilePath = "/Users/itispris/Dropbox/newWorkspace/git/TrajectoryAnomaly/inputs/out_ad"
				+ i + ".csv";

		/*
		 * 
		 * mylogFilePathTime =
		 * "/home/pranali/workspace/DDP_Anomaly/src/outputs/outputTimeTempAccuracy"
		 * + i +".csv"; mylogFilePathValues =
		 * "/home/pranali/workspace/DDP_Anomaly/src/outputs/outputValuesTempAccuracy"
		 * + i +".txt"; mylogFilePathTimeTxt =
		 * "/home/pranali/workspace/DDP_Anomaly/src/outputs/outputTimeTxtTempAccuracy"
		 * + i +".txt"; mapMatchedTrajDirPath =
		 * "/home/pranali/workspace/DDP_Anomaly/src/part" + i;
		 * learnedEdgeDistroFilePath =
		 * "/home/pranali/workspace/DDP_Anomaly/src/out_ad" + i +".csv";
		 */
		try {
			outValues = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(myOutputFilePathValues), "UTF-8"));
			outTime = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(myOutputFilePathTime), "UTF-8"));
			outTimeTxt = new PrintWriter(new OutputStreamWriter(
					new FileOutputStream(myOutputFilePathTimeTxt), "UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		ep = new EdgeProcessing(learnedEdgeDistroFilePath, 0.5);

	}

	public void compare_handler() throws Exception {
		File docDir = new File(mapMatchedTrajDirPath);
		String[] files = docDir.list();
		int fileNum = 0;
		int line_no = 0;
		// int total_count = 0;
		for (fileNum = 0; fileNum < files.length; fileNum++) {

			// if (fileNum < 30)
			// continue;
			// if
			// (!files[fileNum].contains("beijing_sent_matched_timed319.txt"))
			// continue;
			line_no = 0;
			System.out.println("File name : " + files[fileNum]);
			outValues.println("File name : " + files[fileNum]);
			outTime.println("File name : " + files[fileNum]);
			outTimeTxt.println("File name : " + files[fileNum]);
			BufferedReader fileReader = new BufferedReader(new FileReader(
					mapMatchedTrajDirPath + "/" + files[fileNum]));
			while (true) {
				if (mined_traj_count % 2000 == 0) {
					System.gc();
					break;
				}
				ArrayList<Point> arr = new ArrayList<Point>();
				String line = fileReader.readLine();
				if (line == null)
					break;
				line_no++;
				// if (line_no < 140)
				// continue;
				if (line.contains("[")) {
					String[] warr = line.split("\\]");
					for (String s : warr) {
						try {
							int id = Integer.parseInt(s.split(",")[0]
									.split("\\[")[1]);
							double lat = Double.parseDouble(s.split(",")[1]);
							double lon = Double.parseDouble(s.split(",")[2]);
							Date time = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss")
									.parse(s.split(",")[3]);
							arr.add(new Point(id, lat, lon, time));
						} catch (Exception e) {
							System.out.println(line);
						}
					}
					if (arr.size() > 75)
						compare(arr, line_no);
				}
				// if (tot_traj_count == 1000)
				// break;

			}
			fileReader.close();
			outValues.println(" $$$$$$$$$$ ");
			outTime.println(" $$$$$$$$$$ " + "," + " $$$$$$$$$$ ");
			outTimeTxt.println(" $$$$$$$$$$ ");
			System.out.println("Mined trajectory count : " + mined_traj_count
					+ " Missed trajectory Count : " + training_miss_count
					+ " Total trajectory Count : " + tot_traj_count);
			// if (tot_traj_count >= 1000)
			// break;
		}
		outTime.close();
		outTimeTxt.close();
		outValues.close();
	}

	public void compare(ArrayList<Point> pt, int line_no) throws Exception {
		long tot1 = 0;
		long tot2 = 0;
		long tot3 = 0;
		List<Integer> nodeSequence = new ArrayList<Integer>();
		List<Double> travelTime = new ArrayList<Double>();
		for (int i = 0; i < pt.size(); i++) {
			nodeSequence.add(pt.get(i).id);
			if (i < (pt.size() - 1)) {
				double time = pt.get(i + 1).timeStamp.getTime()
						- pt.get(i).timeStamp.getTime();
				travelTime.add(time);
			}
		}
		ep.setNodeTime(nodeSequence, travelTime);
		if (ep.notInTraining()) {
			training_miss_count++;
		} else if (!ep.isAno()) {
			training_miss_count++;
		} else {

			long start = System.currentTimeMillis();

			ArrayList<String> our_result = new ArrayList<String>();
			try {
				our_result = ep.findAnomalyOur();
			} catch (Exception e) {
				System.out.println("Not there in training");
				e.printStackTrace();
				return;
			}
			tot1 += (System.currentTimeMillis() - start);
			start = System.currentTimeMillis();

			ArrayList<String> naive_result = new ArrayList<String>();
			try {
				naive_result = ep.findAnomalyNaive();
			} catch (Exception e) {
				System.out.println("Not there in training");
				e.printStackTrace();
				return;
			}
			tot2 += (System.currentTimeMillis() - start);
			start = System.currentTimeMillis();
			ArrayList<String> naive_result2 = new ArrayList<String>();
			try {
				naive_result2 = ep.findAnomalyNaive2();
			} catch (Exception e) {
				System.out.println("Not there in training");
				e.printStackTrace();
				return;
			}
			tot3 += System.currentTimeMillis() - start;

			// System.out.println(tot1 + "," + our_result + "," + tot2 + ","
			// + naive_result);
			outValues.println(line_no + " $ " + our_result + " $ "
					+ naive_result + " $ " + naive_result2);
			mined_traj_count++;
			// outValues.flush();
			outTime.println(pt.size() + "," + ep.howManyAno() + "," + tot1
					+ "," + tot2 + "," + tot3 + "," + ep.edgeProcessedOur + ","
					+ ep.edgeProcessedWindow);
			outTime.flush();
			outTimeTxt.println(line_no + " $ " + tot1 + " $ " + tot2 + " $ "
					+ tot3);
			// outTimeTxt.flush();

			// if (our_result.size() == naive_result.size()) {
			// int flag = 0;
			// for (int i = 0; i < our_result.size(); i++) {
			// if (!our_result.get(i).equals(naive_result.get(i))) {
			// flag = 1;
			// break;
			// }
			// }
			// if (flag == 0)
			// mined_traj_count++;
			// }

		}
		tot_traj_count++;

	}

	public static void main(String[] args) throws Exception {
		for (int i = 1; i <= 1; i++) {
			ScanThread sc = new ScanThread(i);
			sc.start();
		}

	}

	private static class ScanThread extends Thread {

		private int partID;

		public ScanThread(int partID) {
			this.partID = partID;

		}

		public void run() {
			MASHolder mh = new MASHolder(partID);
			try {
				mh.compare_handler();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
