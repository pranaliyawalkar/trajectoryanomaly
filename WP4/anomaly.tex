\documentclass{svjour3}
\usepackage{fix-cm}
\usepackage[noend]{algorithmic}
\usepackage{epsfig,graphicx,color,amssymb,amsmath,algorithm,verbatim,amsfonts}
\usepackage{mathtools,qtree,multirow}
%\def \baselinestretch{0.9}
%\oddsidemargin -0.45in \evensidemargin -0.3in \textwidth 6.8in
%\headheight 1.0in \topmargin -1.51in \textheight 9 in
\newcommand{\BigO}[1]{\ensuremath{\operatorname{O}\left(#1\right)}}

\begin{document}
\title {Write up for anomaly problem}
\maketitle
\section{Introduction}
Finding anomaly in spatio temporal data is a long studied problem. Here we are focusing on a variation of anomaly that, however, has not been addressed in literature yet. Suppose that before renewing insurance, an insurance provider wants to know whether a driver of interest has the habit of over-speeding. An over-speeding can be defined as deviation from typical speeds at which other, typical set of drivers travel. Hence this stands as an anomaly in the mobility patterns shown by those users. Our goal is to detect those anomalies in an unsupervised way from the dataset itself, without the help of any other external information. Towards that we propose a two stage solution. Our assumption is that the movements of the vehicles are bounded on a road network, where the nodes represent road junctions and edges are the actual road segments. First in the offline stage we learn the typical mobility behavior of users depicted on the road. This involves learning the model of travel time distributions across all edges of the road network. We use the bunch of training trajectories to learn the speed distribution for each of the edges of the road network.\\
Next in the online phase we are provided with a bunch of trajectories of one particular user and the query we would like to answer is whether the user is an anomalous user or not, by observing his set of trajectories. We will explain the stages in more details in following sections, but before that let us look at the definitions that we shall be using to define the problem precisely.
\section{Definitions}
A \textbf{road network} is represented as a graph $G = (V,E)$, where $V$ are the set of vertices depicting road junctions and $E$ is the set of edges depicting the actual road segments connecting those junctions.\\
A \textbf{trajectory} is defined by an ordered sequence of edges and their corresponding travel times. For example a trajectory $\tau=[(e_1, t_{e_1}),\cdots, (e_l,t_{e_l})]$ is an ordered sequence of visited edges $e_1, \cdots, e_l$ and $t_{e_i}$ being the time taken to travel edge $e_i$. We call $l$ to be the length of the trajectory.\\
A \textbf{sub-trajectory} $s$ of a trajectory $\tau$ is a valid sub-sequence $s=[(e_m, t_{e_m}), \cdots, (e_n, t_{e_n})]$ of $t$ where $m \geq 1$ and $n \leq l$. Length of sub-trajectory $s$ is $n-m+1$.\\
A sub-trajectory $s_1=[(e_{m_1}, t_{e_{m_1}}), \cdots, (e_{n_1}, t_{e_{n_1}})]$ is a \textbf{super-trajectory} of another sub-trajectory $s_2=[(e_{m_2}, t_{e_{m_2}}), \cdots, (e_{n_2}, t_{e_{n_2}})]$ iff $m_2 \geq m_1$ and $n_2 \leq n_1$. Thus according to this definition, a trajectory is always a super-trajectory of any of its sub-trajectory. We represent a (sub) trajectory only by its start and end index for brevity. For e.g. sub-trajectory $s$ is represented as $s(m, n)$. If a (sub) trajectory has only one edge then we use only that index to represent that. For e.g. sub-trajectory denoted as $s_1(m_1)$ contains only edge $m_1$ and is of length $(m_1 - m_1 + 1)$, i.e. $1$.\\
A sub-trajectory $s_1(m_1, n_1)$ is said to be \textbf{contained} in another sub-trajectory $s_2(m_2, n_2)$, if $m_1 \geq m_2$ and $n_1 \leq n_2$. It is denoted as $s_1 \subseteq s_2$. In other words $s_2$ is super-trajectory of $s_1$.\\
Two sub-trajectories $s_1(m_1, n_1)$ and $s_2(m_2, n_2)$ are said to be \textbf{meeting} if either $m_2=m_1 + 1$ or $m_1=n_2 + 1$. \textbf{Synergy} of two meeting sub-trajectories $s_1(m_1, n_1)$ and $s_2(m_2, n_2)$ is another sub-trajectory $s_{12}(m_1, n_2)$.\\
Two sub-trajectories $s_1(m_1, n_1)$ and $s_2(m_2, n_2)$ are said to be \textbf{overlapping} if they are not meeting nor one contains other, and at the same time either $m_2 < n_1 < n_2$ or $m_1 <n_2 <n_1$.\\
\section{Offline Learning}
The purpose of the offline learning is to model the typical mobility patterns depicted in the historical trajectories. In other words, we want to learn the typical time taken by users to travel a road segment. Once we have learnt this, then for a given travel time on a road segment, we can derive if the  travel time deviates significantly from the learnt model. If so then we coin it to be anomalous.\\
Thus to learn the mobility patterns of the users, we use the set of historical trajectories. We assume a normal distribution model for the distributions of travel times for each edges.
More formally for each edge $e$ we learn a normal distribution $N(\mu_e,\sigma_e)$, where $\mu_e$ is the mean travel time for edge $e$ and $\sigma_e$ is the variance using the set of historical trajectories. Thus for road $G = (V, E)$, we learn a set $\mathbb{N} = \bigcup N(\mu_e,\sigma_e),$ $\forall e\in E$. This completes the offline learning.\\
\section{Online Inference Phase}
Next in the query phase, we are given with set of trajectories for a particular user and our goal is to find out if the user is an anomalous user or not. Towards that we formulate a three-step inference process. First we find out anomaly at an edge level, then we compute anomaly at trajectory level and lastly we infer aggregated anomaly at user level. Each of these three stages are elaborated below.
\subsection{P-value based anomaly}
First let's define what is an anomalous edge. We first take a trajectory $\tau$ from the set of queried trajectories and we are interested to find whether the travel time of an edge $e$ in that trajectory is anomalous. Let the travel time for that edge be $t_e$. The learnt distribution for edge $e$ be $N(\mu_e,\sigma_e)$. We propose a significance level measurement for this travel time based on $p-value$. Our null hypothesis is that the traveller did not over-speed on that edge. The \textit{Mahalanobis distance} of the observation $t_e$ from distribution $N$ can be computed as per the following equation,
\begin{equation}
  d(e)=\sqrt{\frac{(\mu_{e}-t_{e})^2}{\sigma_{e}^2}}
\end{equation}
\begin{figure}[t]
	\centering
		\includegraphics[width=2.6in]{pdf/anomaly1.pdf}
		\caption{Naive edge count based trajectory anomaly deduction.}
	\label{fig:fig1}
\end{figure}
Now if this distance touches acceptable threshold, then it's an anomalous edge. Formally speaking,
\begin{definition}
	\label{def:roadnetwork}
	\textsc{Anomalous Edge.} Given a normal distribution of travel times $N(\mu_e,\sigma_e)$, traversal time $t_e$ and acceptance threshold $\theta$ for an edge $e$, if the Mahalanobis distance of traversal time $d(e)$ is more than or equals threshold, i.e. $d(e) \geq \theta$, then the corresponding edge $e$ is anomalous.
\end{definition}
These anomalous edges can be derived by doing a simple sequential scan of the trajectory. However this edge level anomaly does directly translate to trajectory level anomaly. To understand the reason let's assume two hypothetical trajectories as shown in figure~\ref{fig:fig1}. For illustration purpose, we are neglecting the model based approach, rather we use deviation from average travel time as our anomaly. The first diagram learnt model, denotes the average travel times learnt for our six edges of interest. There are two query trajectories and edge labels of those correspond to travel time of each edges for those trajectories. Edges marked as red are anomalous ones, where the travel times are less than acceptable norms which are the average time taken. For e.g. first edge in trajectory1 is marked red, because the travel time is 5 which is less than average time 10. The same is computed for other edges as well and in trajectory1 two edges are found to be anomalous, whereas in trajectory2 three edges are found to be so. Hence if we simply collate this information to conclude trajectory level anomaly, we will end up concluding trajectory2 to be more anomalous than trajectory1. However a closer observation reveals, that if we had a way to measure how anomalous an edge is, then the anomaly of edges of trajectory1 will be much more than that of trajectory2 as the differences from averages are much higher. This might also change our trajectory level conclusions. This is illustrated in figure~\ref{fig:fig2}.
\begin{figure}[t]
	\centering
		\includegraphics[width=2.6in]{pdf/anomaly2.pdf}
		\caption{Correct interpretation of anomaly.}
	\label{fig:fig2}
\end{figure}
Here instead of looking at only anomalous edges, we look for anomalous portions in the trajectories. It can be seen 5/6th of trajectory1 is anomalous whereas only 1/2 of trajectory2 is anomalous.\\
Alternatively one may argue to operate directly at trajectory level by learning from the training data, if sub-trajectories are anomalous or not. But this approach also has couple of bottlenecks. We illustrate them using figure~\ref{fig:fig3}.\\
There are three trajectories $T_1, T_2$ and $T_3$ in training set. The query trajectory is one that starts from $Q_1$ and ends $Q_2$. None of the training trajectories contains both $Q_1$ and $Q_2$. It makes this approach impractical, as one need an infinite database of historical trajectories to learn all possible combination of edges. However by stitching trajectories together one can have the necessary information required. Secondly even if it's assumed that training set contains all possible edge combinations, we need to store and search over all possible length of markov. To understand why, let's assume that a trajectory was present in our training set that indeed spans from $Q_1$ to $Q_2$. Now assume only the portions marked is red are the anomalous parts of that trajectory. In order to be able to infer those parts one needs to store all possible sub-trajectories of that trajectories, which will result in scalability bottleneck. The cost increases exponentially to the length of markov. The solution would be to operate at a sub-trajectory level and only learn speed thresholds for each edge and store only those.\\
This motivates us to find out Maximal Anomalous Sub-trajectories (MAS) of a trajectory for a correct interpretation of trajectory level anomaly that also enables scalable mining.
\begin{figure}[t]
	\centering
		\includegraphics[width=2.6in]{pdf/fig3.pdf}
		\caption{Scalability issues.}
	\label{fig:fig3}
\end{figure}
\subsection{Maximal anomalous sub-trajectory}
Before defining MAS, lets first understand formally what is an anomalous sub-trajectory according to our definition of anomaly, in the context of a trajectory.\\
A sub-trajectory $s=[(e_m, t_{e_m}), \cdots, (e_n, t_{e_n})]$ of $\tau$, is said to be \textit{anomalous} for a given threshold $\theta$ if the travel times, on average, are more than $\theta$ standard deviations away from the population in the traversed edges. Mathematically, let $d(s)$ be the Mahalanobis distance from the population.
\begin{equation}
  d(s)=\sqrt{\sum_{i=m}^{n}\frac{(\mu_{e_i}-t_{e_i})^2}{\sigma_{e_i}^2}}
\end{equation}
$s$ is anomalous if
\begin{equation}
  d(s)\geq\sqrt{\sum_{i=m}^{n}\theta^2}=\sqrt{n-m}\theta
\end{equation}
Note that a anomalous sub-trajectory can contain both anomalous and non-anomalous edges. As long as the combination of such, both anomalous and non-anomalous edges are as a whole anomalous, they are good to be coined as anomalous sub-trajectory.
Next we define what is maximal anomalous sub-trajectory
\begin{definition}
	\label{def:roadnetwork}
	\textsc{Maximal Anomalous Sub-trajectory (MAS).} A MAS of a trajectory $\tau$ is a anomalous sub-trajectory $s$ of $\tau$ such that, no super-trajectory of $s$ is also anomalous. 
\end{definition}
Given a trajectory $\tau$ and learnt speed distribution model $\mathbb{N}$, our inference goal is to find out a set $\mathbb{M}$, so that $\mathbb{M}$ contains all MAS of trajectory $\tau$.\\
\subsubsection{Algorithm for longest anomalous sub-sequence generation}
\ \\
\textbf{Naive Approach}\\
\ \\
Let's first see the naive way of finding the set $\mathbb{M}$. It's shown in algorithm~\ref{alg:naive}. It first make a scan through the trajectory to find all anomalous sub-trajectories and put them as potential candidate set (line 3-6). Then it picks each sub-trajectory of candidate set and checks is there is any super-trajectory of that trajectory in the candidate set again. If not then it includes that in final answer set, otherwise it is discarded.\\
As expected this algorithm does not scale for large trajectories. Even when the trajectory does not have any anomalous edge, it does the entire scan without any early termination. This calls for an intelligent alternative, which we propose as our mas finding algorithm (MFA).\\
\ \\
\textbf{Mas Finding Algorithm (MFA)}\\
\ \\
To overcome the bottleneck of exhaustive scans, we first identify a set of potential island in the trajectory. A island is essentially a sub-trajectory of the trajectory inside which the MAS would reside. Thus all the definitions (contained, meeting, overlapping) that we had developed for sub-trajectories in section 2, holds true for islands also. Next we use a faster variation of exhaustive search in those islands only, to report $\mathbb{M}$. The algorithm is shown in algorithm~\ref{alg:mfa}.\\
We first start with smallest length islands, i.e. single anomalous edges present in trajectory. We do a scan of the trajectory to extract out these edges and add them to a $seedset$. Also we find their left and right boundaries and store them as well in maps $LB$ and $RB$ correspondingly. Left Boundary and Right Boundary are defined in following manner:\\
\textbf{Left boundary} of an anomalous sub-trajectory $S_{AN}$, $LB(S_{AN})$, is a sub-trajectory generated by adding, furtherest possible left edge to  anomalous sub-trajectory $S_{AN}$ so that it still remains anomalous.\\
\textbf{Right boundary}, $RB(S_{AN})$ is defined in similar way, but in right direction.\\
So as per our earlier definition, $S_{AN} \subseteq LB(S_{AN})$ and $S_{AN} \subseteq RB(S_{AN})$.\\
Identification of these initial $seedset$, $LB$ and $RB$ is done at lines 1-9 in algorithm~\ref{alg:mfa}.\\
Next we proceed expanding our search space by collating few of these identified islands. We will explain what quantifies these collation decisions in details, but before that let's understand two measurement which play a role in the decision making.\\
\textbf{Deficit at an edge-} For each of the edges we have learnt a normal distribution of travel times $N(\mu_e,\sigma_e)$. Also let the given travel time of the edge in the trajectory be, $t$. Now if the edge is found to be anomalous then as per definition of p-value based anomaly, $\frac{\mu - t}{\sigma} > 1.65 $. Let's assume for a travel time $T$ we see from $z-score$ table that $\frac{\mu - T}{\sigma} = 1.65$. Then deficit for this  edge is the value $T-t$.\\
Using additive property we can define deficit of a sub-trajectory analogously.\\
\textbf{Surplus} is the dual of deficit i.e. in this case $\frac{\mu - t}{\sigma} < 1.65 $. Deficit occurs at an anomalous sub-trajectory, but surplus occurs at a non-anomalous sub-trajectory.\\
Based on this concept of surplus and deficit, we formulate the following axiom:\\
\textbf{Axiom: }Synergy of two anomalous sub-trajectory is anomalous. The anomaly in synergy will be more than the constituent sub-trajectories.\\
\textbf{Proof: }Each anomalous edge can be thought of as an deficit, whereas each non-anomalous edge is surplus. Now a sub-trajectory $S1$ is anomalous means summing all edges of of $S1$ there is some deficit. So for $S2$. Also $S2$ is adjacent to it. That means, there is neither any overlapping edges nor any surplus edges in between. If the deficits are $d1$ and $d2$ respectively for $S1$ and $S2$, then synergy will produce a deficit of at least $d1+d2$, which is also anomalous. The anomaly (i.e. deficit) is also higher than either of $S1$ and $S2$.\\
\textbf{Corollary: }If $LB(S_{AN})$ = $S\{s-e\}$ then either $s$ = 0 or edge $s-1$ is non-anomalous. Analogous for $RB(S_{AN})$.\\
Equipped with these axiom and corollary, we can now merge any two meeting or overlapping islands into a single one. We do that repeatedly in line 10-30 of our algorithm. Moreover in that process if we find an island to be contained by another island, then we delete the smaller island, as it cannot add any extra MAS apart from those what we can get from the larger one (line 18-22). We continue this process until no further collation is possible. Thus after this process we don't have any meeting or overlapping islands, we only have islands that are separated by at least one non-anomalous edge.\\
However, two islands separated by only one non-anomalous edge can be collated to form a larger island. This happens only if the non-anomalousness of the edge can be negated by two adjacent anomalous islands. More formally if the surplus of the edge (as it's non-anomalous) is less than the sum of deficit of two islands (since they are anomalous). I.e. if the deficits of two islands are $de_1$ and $de_2$ respectively, whereas the surplus at edge is $su$ and $su \leq de_1 + de_2$, then only they can be collated together. On the other hand if there exists more than one non-anomalous edges between two islands then in no way they can be collated. This conclusion can be reached from the following axiom.\\
\ \\
\textbf{Axiom: }Synergy of two non-overlapping non-anomalous trajectories cannot be anomalous\\
\textbf{Proof:}\\
\textit{Basis:}\\
Assume two normal distributions:
\begin{itemize}
\item $N_1 (\mu_1, \sigma_1)$
\item $N_2 (\mu_2, \sigma_2)$
\end{itemize}
Let's assume these two edges have travel times $t_1$ and $t_2$ so that they are non-anomalous travel times.\\ 
So,\\
$d(s1)=\sqrt{\frac{(\mu_1-t_1)^2}{\sigma_1^2}}$ and $d(s1) < \theta \ast \sqrt{1}$\\
$d(s2)=\sqrt{\frac{(\mu_2-t_2)^2}{\sigma_2^2}}$ and $d(s2) < \theta \ast \sqrt{1}$\\
After joining these two we get,\\
$d(s)=\sqrt{\frac{(\mu_1-t_1)^2}{\sigma_1^2} + \frac{(\mu_2-t_2)^2}{\sigma_2^2}}$, where we need to show $d(s) < \theta \ast \sqrt{2}$.\\
$\sqrt{\frac{(\mu_1-t_1)^2}{\sigma_1^2} + \frac{(\mu_2-t_2)^2}{\sigma_2^2}}$\\
$=\sqrt{(d(s1))^2 + (d(s2))^2}$\\
$< \theta \ast \sqrt{1 + 1}$, as $d(s1) < \theta \ast \sqrt{1}$ and $d(s2) < \theta \ast \sqrt{1}$\\ 
$< \theta \ast \sqrt{2} $.\\
\textit{Step:}\\
Let's this holds for sub-trajectories, $s_1$ and $s_2$ having length $n_1$ and $n_2$ respectively such that $n_1+n_2 = n$. This means the following holds\\
$d(s_1)=\sqrt{\sum_{i=1}^{n_1}\frac{(\mu_{e_i}-t_{e_i})^2}{\sigma_{e_i}^2}}$, where $d(s_1) < \sqrt{\sum_{i=1}^{n_1}\theta^2}=\sqrt{n_1} \ast \theta $ and\\
$d(s_2)=\sqrt{\sum_{i=1}^{n_2}\frac{(\mu_{e_i}-t_{e_i})^2}{\sigma_{e_i}^2}}$, where $d(s_2) < \sqrt{\sum_{i=1}^{n_2}\theta^2}=\sqrt{n_2} \ast \theta $\\
We need to show for $s = s_1+ s_2$, $d(s)<\sqrt{n} \ast \theta$\\
As $s_1$ and $s_2$ are non-overlapping,\\
$d(s)=\sqrt{\sum_{i=1}^{n}\frac{(\mu_{e_i}-t_{e_i})^2}{\sigma_{e_i}^2}}$\\
$= \sqrt{\sum_{i=1}^{n_1}\frac{(\mu_{e_i}-t_{e_i})^2}{\sigma_{e_i}^2} + \sum_{i=1}^{n_2}\frac{(\mu_{e_i}-t_{e_i})^2}{\sigma_{e_i}^2}}$\\
$=\sqrt{{d(s_1)}^2 + {d(s_2)}^2} $\\
$< \sqrt{n_1 \ast \theta^2 + n_2 \ast \theta^2 }$\\
$< \theta \ast \sqrt{n_1 + n_2}$\\
$< \theta \ast \sqrt{n}$\\
\textbf{N.B.} We cannot conclude anything if they are overlapping. As in that case threshold will $(T_1+T_2 - \delta)$, where $\delta$ is the overlapped portion.\\
Now two two islands separated by at least two edges can be thought of two non-anomalous sub-trajectories by adding one non-anomalous edge to each of the islands. Thus the resultant islands can never be anomalous as per the axiom.\\
Lines 32-47 repeatedly checks for islands separated by one edge and if possible merge them to a single one. It terminates when no further merging can be performed.\\
Lastly MFA reports all MAS from the islands in line 48-61. From the definition of MAS, we can say that any sub-trajectory contained by MAS can never also be a MAS. Following this observation, the right most edge of last MAS found is stored in variable $r$. While looking for the next MAS first left index $i$ is incremented by one, and right index is checked only until $r$, because any right index lower than $r$ will be a sub-trajectory of previous MAS, and hence can be disregarded.\\
\ \\
Proof that islands will always contain all MASs [to be done].
\subsubsection{Aggregating MAS to infer trajectory level anomaly}
[to be done]
\subsection{Aggregating trajectory level inference to conclude person level anomaly}
[to be done]
%Lets first define the purpose of the algorithm clearly. We are given with a trajectory which is a sequence of edges. Each of these edges is labelled as either $N$ or $A$. An edge is labelled $N$ if it is a non-anomalous edge, $A$ if it's anomalous. We need to output all the maximal anomalous sub-trajectories. Note that one maximal anomalous sub-sequence can contain non-anomalous edges as well.\\ To understand it better refer to Figure~\ref{fig:fig1}. For the sake of simplicity lets forget the p-value based computation first. Assume that for each edges we are give with a threshold, any value less than that threshold is anomalous, and higher is non-anomalous. Now lets assume two trajectories over these edges having travel times as follows: $T1:\{2,11,11,11\}$, $T2:\{9,11,12,9\}$. So for $T1$ there is only one anomalous edge i.e. $e1$ ,whereas for $T2$ there are two $e1$ and $e4$. But a closer observation reveals that $T1$ is in fact more anomalous than $T2$ as the anomalous edge the speed was very high. We want to capture this in our output. Towards that, we observe that in $T1$, even if we merge a non-anomalous edge of $e2$ with $e1$ total time becomes $13$ which is still less than threshold $20$ and it remains anomalous. Thus we target to find longest such sequence can be generated. Specifically our algorithm should output for $T1$ edge sequence $e1,e2,e3,e4$, whereas for $T2$ output could be either $e1$ or $e4$.\\
\ \\
\begin{algorithm}[t]
  \caption{Naive ($\mathbb{N}, \tau$)}
\label{alg:naive}
{\scriptsize
\begin{algorithmic}[1]
\STATE {$\mathbb{M} \leftarrow \emptyset$}
\STATE {$pot \leftarrow \emptyset$}
\FOR {$e_i \in \tau$}
	\FOR{$e_j \in \tau $, where $j \geq i$}
		\IF {$isAnomalous(e_i, e_j)$}
			\STATE {add $(e_i, e_j)$ in $pot$}
		\ENDIF
	\ENDFOR
\ENDFOR
\FOR {sub-trajectory $s_i \in pot$}
	\STATE {$add \leftarrow true$}
	\FOR {sub-trajectory $s_j \in pot$}
		\IF{$s_i \subseteq s_j$}
			\STATE {$add \leftarrow false$}
			\STATE {break}
		\ENDIF
	\ENDFOR
	\IF {$add = true$}
		\STATE {$\mathbb{M} \leftarrow \mathbb{M} \cup s_i$}
	\ENDIF
\ENDFOR
\STATE{return $\mathbb{M}$}
\end{algorithmic}}
\end{algorithm}

\begin{comment} 
\subsection{Description of the algorithm}
Consider Figure \ref{fig:fig} as a running example.\\
\ \\
\textbf{Steps enumeration of the algorithm:}\\
1. First scan the trajectory to identify all one length anomalous sub-trajectory. They form the initial seed set, $ST$.\\
2. For each $Si$ in $ST$ find $LB(Si)$ and $RB(Si)$, save them in a list indexed by $Si$. In our example we get the table 1.\\
3. Sort $ST$ based on the $startIndex$ of each trajectory $S$ contained in it.\\
4. If for any $Si$ if it is found that both $RB(Si)$ and $LB(Si)$ is contained in the range $lowerIndex$ of $LB(Sj)$ and $higherIndex$ of $RB(Sj)$, for any $j>i$, then remove $Si$ from index.\\
5. For each $Si$ in $S$ check if there is $Sj$ in $S$ such that, $j > i$ and $RB(Si)$ is adjacent to or is overlapping with $LB(Sj)$. If so then remove $Si$ and $Sj$ from index list and insert sub-trajectory $S_{synergy}\{startIndex of RB(Si) - endIndex of LB(Sj)\}$. Find $LB(S_{synergy})$ and $RB(S_{synergy})$. Set $lowIndex$ as\\ 
$MIN(startIndexOf(LB(S_{synergy})), startIndexOf(LB(Si)))$. Insert $lowIndex - endIndex(S_{synergy})$ as $LB(S_{synergy})$ in the index. Similarly set $highIndex$ as\\
$MAX(endIndexOf(RB(S_{synergy})), endIndexOf(RB(Sj)))$. Insert $startIndex(S_{synergy}) - highIndex$ as $RB(S_{synergy})$ \\
6. If any new entry is added to the index then go to step 3. Terminate if no new entry added in step 5 and go to step 7.
7. For each $Si$ in $ST$ report all the maximal sub-trajectories that are contained in $LB$ and $RB$ of $Si$s following the below steps:
\begin{itemize}
\item Mark two pointers $left$ at $startIndex$ of $LB(Si)$ and $right$ at $highIndex$ of $RB(Si)$. A third pointer $rightTillNow$ is initiated at $left$ first. Following these initialization the below loop is executed.
\item Right pointer is decremented by one if it hits $rightTillNow$ then increment $left$ by one, right is reset at $highIndex$ of $RB(Si)$ and continue with the loop. Else, it's verified whether the sub-trajectory $S\{left-right\}$ is anomalous. If so then $S$ is included in list of maximal sub-trajectory, $rightTillNow$ is set to current $right$, $left$ is incremented by one, right is set at $highIndex$ of $RB(Si)$ and continue with the loop. At any point in the loop if $rightTillNow$ or $left$ reaches $highIndex$ of $RB(Si)$, then exit from the loop.
\end{itemize}
\begin{table}[b]
\centering
{\scriptsize
  \begin{tabular}{|p{0.5in}|p{2.7in}|} \hline
  \textbf{Index} & \textbf{LB and RB} \\
  \hline
$S1\{2\}$ & $(1,2)$ and $(2,3)$. \\
\hline
$S2\{6\}$ & $(5,6)$ and $(6,7)$. \\
\hline
$S3\{9\}$ & $(8,9)$ and $(9,10)$. \\
\hline
\end{tabular}}
\label{tab:notations}
\caption{Table 1}
\end{table}
\begin{table}[b]
\centering
{\scriptsize
  \begin{tabular}{|p{0.5in}|p{2.7in}|} \hline
  \textbf{Index} & \textbf{LB and RB} \\
  \hline
$S1\{2\}$ & $(1,2)$ and $(2,3)$. \\
\hline
$S4\{6-9\}$ & $(5,9)$ and $(6,10)$. \\
\hline
\end{tabular}}
\label{tab:tab2}
\caption{Table 2}
\end{table}
\begin{figure}[t]
	\centering
		\includegraphics[width=2.6in]{pdf/fig.pdf}
		\caption{Figure 2.}
	\label{fig:fig}
\end{figure}
\end{comment}
\begin{algorithm}[t]
  \caption{MFA($\mathbb{N}, \tau$)}
\label{alg:mfa}
{\scriptsize
\begin{algorithmic}[1]
\STATE {$SEEDSET \leftarrow \emptyset$}
\STATE {$LB \leftarrow \emptyset$}
\STATE {$RB \leftarrow \emptyset$}
\FOR {$e \in \tau$}
	\IF {$isAnomalous(e)$}
		\STATE {add $e$ in seedset $SEEDSET$}
		\STATE {add $(e, findLB(e))$ in $LB$}
		\STATE {add $(e, findRB(e))$ in $RB$}
	\ENDIF
\ENDFOR
\STATE {$repeat \leftarrow TRUE$}
\WHILE {$repeat$}
	\STATE {$repeat \leftarrow FALSE$}
	\FOR {$s_i \in ST$}
		\STATE {$lbl1 = lowerIndexOf(LB.get(s_i))$}
		\STATE {$rbu1 = upperIndexOfLB(RB.get(s_i))$}
		\FOR {$s_j \in ST$, where $j>i$}
			\STATE {$lbl2 = lowerIndexOf(LB.get(s_j))$}
			\STATE {$rbu2 = upperIndexOfLB(RB.get(s_j))$}
			\IF {$lbl1 >= lbl2$ and $rbu1 <= rbu2$}
				\STATE {remove $s_i$ from $ST$}
				\STATE {break}
			\ELSIF {$lbl1 < lbl2$ and $rbu1 > rbu2$}
				\STATE {remove $s_j$ from $ST$}
			\ELSIF {$rbu1 >= lbl2$ or $rbu1 == (lbl2 - 1)$}
				\STATE {remove $s_i$ and $s_j$ from $ST$}
				\STATE {insert $lowerIndexOf(s_i),upperIndexOf(s_j)$ to $ST$}
				\STATE {insert $(lowerIndexOf(s_i),upperIndexOf(s_j)), MIN(lbl1, findLB(lowerIndexOf(s_i),upperIndexOf(s_j)))$ to $LB$}
				\STATE {insert $(lowerIndexOf(s_i),upperIndexOf(s_j)), MAX(rbu2, findRB(lowerIndexOf(s_i),upperIndexOf(s_j)))$ to $RB$}
				\STATE $repeat \leftarrow TRUE$
			\ENDIF
		\ENDFOR
	\ENDFOR
	\IF {$repeat$}
		\STATE {sort $ST$ based on start index of $s_i$}
	\ENDIF
\ENDWHILE
\STATE {$repeat \leftarrow TRUE$}
\WHILE {$repeat$}
	\STATE {$repeat \leftarrow FALSE$}
	\FOR {$s_i \in ST$}
		\STATE {$lbl1 = lowerIndexOf(LB.get(s_i))$}
		\STATE {$rbu1 = upperIndexOfLB(RB.get(s_i))$}
		\FOR {$s_j \in ST$, where $j>i$}
			\STATE {$lbl2 = lowerIndexOf(LB.get(s_j))$}
			\STATE {$rbu2 = upperIndexOfLB(RB.get(s_j))$}
			\IF {$rbu1 == (lbl2 - 2)$}
				\IF{$isAnomalous(lowerIndexOf(s_i),upperIndexOf(s_j))$}
					\STATE {insert $lowerIndexOf(s_i),upperIndexOf(s_j)$ to $ST$}
					\STATE {insert $(lowerIndexOf(s_i),upperIndexOf(s_j)), MIN(lbl1, findLB(lowerIndexOf(s_i),upperIndexOf(s_j)))$ to $LB$}
					\STATE {insert $(lowerIndexOf(s_i),upperIndexOf(s_j)), MAX(rbu2, findRB(lowerIndexOf(s_i),upperIndexOf(s_j)))$ to $RB$}
					\STATE {$repeat \leftarrow TRUE$}
				\ENDIF
			\ENDIF
		\ENDFOR
	\ENDFOR	
	\IF {$repeat$}
		\STATE {sort $ST$ based on start index of $s_i$}
	\ENDIF
\ENDWHILE
\STATE {$\mathbb{M} \leftarrow \emptyset$}
	\FOR {$s_i \in ST$}
		\STATE {$l1 \leftarrow lowerIndexOf(LB.get(s_i))$}
		\STATE {$r1 \leftarrow upperIndexOfLB(RB.get(s_i))$}
		\STATE {$r = l1 -1 $}
		\FOR {$i=l1; i<=r1; i++ $}
			\FOR {$j=r1; j>r; j--$}
				\IF {$isAnomalous(i, j)$}
					\STATE{insert $i, j$ in $OUTPUTSET$}
					\STATE{$r \leftarrow j$}
					\STATE{break}
				\ENDIF
			\ENDFOR
			\IF {$r =r1$}
				\STATE{break}
			\ENDIF
		\ENDFOR
	\ENDFOR
	\STATE{return $\mathbb{M}$}
\end{algorithmic}}
\end{algorithm}
%In this example the set obtained after step 1 is: $S$ = $\{S1\{2\}, S2\{6\}, S3\{9\}\}$. Then in step 2 Table 1 gets generated. Next it can be seen that $S2$ and $S3$ can be merged. Resultant list $S4\{6-9\}$ is added to index, after removing $S2$ and $S3$. Then step is called. The updated index after this, is shown in Table 2. Now no further extension and and merging is possible as there are no overlapping/adjacent lists. So this algorithm terminates.\\
%\vspace{-0.05in}
%\vspace{-0.05in}
%\begin{equation}
%	\label{eq:dtw}
%	\nonumber
%	{\small
%	\begin{split}
%   & MaxiAn(T_1(lbl, lbu),T_2(rbl, rbu))=\\
%    & \begin{cases}
%T_1  \mbox{\hspace{1.95in} if $|T_2|=0$}\\
%T_2  \mbox{\hspace{1.9in} if $|T_1|=0$}\\
%T(lbl,rbu) \mbox{\hspace{1.9in} if $|T_1 +T_2 |$ is anomalous}\\
%MaxiAn((lbl+1, lbu),(rbl, rbu)) and \mbox{\hspace{0.6in} otherwise}\\ MaxiAn((lbl, lbu),(rbl, rbu-1))
%        \end{cases}
%\end{split}}
% \end{equation}
%\begin{equation}
%	\label{eq:dtw}
%	\nonumber
%	{\small
%	\begin{split}
%   & MaxiAn(T_1(lbl, lbu),T_2(rbl, rbu))=\\
%    & \begin{cases}
%T_1  \mbox{\hspace{1.95in} if $|T_2|=0$}\\
%T_2  \mbox{\hspace{1.9in} if $|T_1|=0$}\\
%T(lbl,rbu) \mbox{\hspace{1.9in} if $|T_1 + T_2| is anomalous$
%    MaxiAn((lbl+1, lbu),(rbl, rbu)) and \mbox{\hspace{0.6in} otherwise}\\ MaxiAn((lbl, lbu),(rbl, rbu-1))
%        \end{cases}
%\end{split}}
%\end{equation}
%\begin{itemize}
%\item Check of $(1,3)$ and $(5,10)$. Assume it fails.
%\item Check of $(1,3)$ and $(5,9)$. Assume this succeeds. $S5\{1-9\}$ gets added to the index.
%\item Check of $(2,3)$ and $(5,10)$. Assume this succeeds. $S6\{2-10\}$ gets added.
%\item Final check $(2,3)$ and $(5,9)$. This would also succeed $S7\{2-9\}$.
%\end{itemize}
%Final scan will generate $S5$ or $S6$ as output.

\begin{comment}
\section{Proof by induction}
\textbf{Basis:} This holds for all one length sub-trajectories because they are single anomalous edges. Hence they never become non-anomalous.\\

For two length sub-trajectory to be anomalous it must have one edge that is anomalous. As per the previous theorem, synergy of two non-anomalous edges can never be anomalous. Thus this property holds for two length sub-trajectories as well.\\
\\
\textbf{Inductive Step:} Let us assume this property holds for all n-1 length sub-trajectories. We shall then show that it will hold for n length sub-trajectory also. Now let us analyse all the possible ways of generating n length anomalous sub-trajectories.
\begin{enumerate}
\item \textit{It can be generated from an n-1 length anomalous sub-trajectory:} An anomalous or and non-anomalous edge can be added to an n-1 length sub-trajectory and the resultant n-length sub-trajectory could be anomalous. As the property was true for the n-1 length sub-trajectory, it holds for the n length as well. The path can be found by simply appending the newly added edge to the path of n-1 length sub-trajectory.
\item \textit{It can be generated from an n-1 length non-anomalous sub-trajectory:} In this case the edge added to the non-anomalous sub-trajectory, has to be an anomalous edge. Otherwise according to Theorem 1, the synergy would not be anomalous. Now as the edge is anomalous, path for the n length sub-trajectory will be connecting the added edge and the resultant n length sub-trajectory. Thus for this case also we have a path which is always anomalous.
\end{enumerate}
All n length sub-trajectory generation can be mapped to one of the two ways described above. For both of them this property is found to be true. Thus we can claim that the property holds for all n length anomalous sub-trajectory. Hence proved.
\end{comment}
\end{document}
