times=dlmread('txt/trajdist.txt');
x1=times(:,1);
ay=times(:,2);
by=times(:,3);
cy=times(:,5);

plot(x1,ay/sum(ay),'-o',x1,cy/sum(cy),'-^','LineWidth',2.5,'MarkerSize',24);
ylabel('Distribution','FontSize',30);
%set(gca,'yScale','log');
xlabel('Trajectory Size','FontSize',30);

h_legend=legend('T-drive','GeoLife');

set(h_legend,'FontSize',30);

%set(gca,'yLim',[0,10000]);
%set(h_legend,'location','northoutside');
%set(gca,'yLim',[0.45,1]);
%set(gca,'xLim',[1,12]);
set(gca,'FontSize',25)
%set (gca,'YTickLabel',['1000';'10000';'100000';'1000000';'10000000';'100000000'])
