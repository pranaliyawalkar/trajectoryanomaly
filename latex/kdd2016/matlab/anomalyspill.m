times=[0 0.78100951621
1 0.2046355232218
2 0.0105094270749
3 0.00252124381361];

x1=times(:,1);
by=times(:,2)*100;


bar(x1,by);

ylabel('Distribution (%)','FontSize',40);
xlabel('Size of spill-over','FontSize',40);
colormap('summer');
h_legend=legend('T-Drive');
%set(h_legend,'FontSize',30);
%set(gca,'yLim',[0,3600]);
%set(h_legend,'FontSize',30);
%set(h_legend,'location','northoutside');
set(gca,'yLim',[0,100]);
%set(gca,'yScale','log');
set(gca,'FontSize',30)
%set (gca,'YTickLabel',['1000';'10000';'100000';'1000000';'10000000';'100000000'])
legend('boxoff');