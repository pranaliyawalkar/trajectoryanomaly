times=dlmread('txt/covariance.txt');
x1=times(:,1);
ay=times(:,2);
plot(x1,ay,'-o','LineWidth',2.5,'MarkerSize',24);
xlabel('No. of hops','FontSize',40);
ylabel('Correlation','FontSize',40);

%h_legend=legend('Mantra','Naive','Sliding Window');
%set(h_legend,'FontSize',30);
%set(gca,'yLim',[0,10000]);
%set(h_legend,'FontSize',30);
%set(h_legend,'location','northoutside');
%set(gca,'yLim',[0.45,1]);
%set(gca,'xLim',[1,12]);
set(gca,'FontSize',35)
%set (gca,'YTickLabel',['1000';'10000';'100000';'1000000';'10000000';'100000000'])
