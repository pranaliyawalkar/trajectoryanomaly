times=dlmread('txt/ap.txt');
x1=times(:,1);
ay=times(:,2);
by=times(:,3);

plot(x1,ay,'-o',x1,by,'-*','LineWidth',2.5,'MarkerSize',24);
ylabel('No. of edges processed','FontSize',30);
%set(gca,'yScale','log');
xlabel('AP (%)','FontSize',30);

h_legend=legend('Mantra','Sliding Window');

set(h_legend,'FontSize',30);

%set(gca,'yLim',[0,10000]);


set(h_legend,'FontSize',30);
%set(h_legend,'location','northoutside');
%set(gca,'yLim',[0.45,1]);
set(gca,'xLim',[0,50]);
set(gca,'FontSize',25)
%set (gca,'YTickLabel',['1000';'10000';'100000';'1000000';'10000000';'100000000'])
