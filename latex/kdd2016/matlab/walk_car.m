times=dlmread('txt/walk_car.txt');
x1=times(:,1);
ay=times(:,2);
by=times(:,3);
cy=times(:,4);

%plot(by,cy,'o',x1,ay,'+','LineWidth',1.5,'MarkerSize',10);
plot(x1,ay,'o',by,cy,'+','LineWidth',1.5,'MarkerSize',10);

ylabel('f(T,Walk)','FontSize',30);
%set(gca,'yScale','log');
xlabel('f(T,Car)','FontSize',30);

h_legend=legend('Walk','Car');

set(h_legend,'FontSize',30);

%set(gca,'yLim',[0,10000]);


set(h_legend,'FontSize',30);
%set(h_legend,'location','northoutside');
set(gca,'yLim',[0,1]);
set(gca,'xLim',[0,1]);
set(gca,'FontSize',25)
%set (gca,'YTickLabel',['1000';'10000';'100000';'1000000';'10000000';'100000000'])
