times=[1.0 0.682271544997
2.0 0.24013178429
3.0 0.052453615398
4.0 0.016039535287
5.0 0.00442170972776
6.0 0.00190740419629];
str = {'1-2'; '2-3'; '3-4';'4-5';'5-6';'6-7'};

x1=times(:,1);
by=times(:,2)*100;


bar(x1,by);
%,'-s','LineWidth',3.5,'MarkerSize',24);

ylabel('Distribution (%)','FontSize',40);
xlabel('Ratio','FontSize',40);

h_legend=legend('T-Drive');
%set(h_legend,'FontSize',30);
%set(gca,'yLim',[0,3600]);
%set(h_legend,'FontSize',30);
%set(h_legend,'location','northoutside');
set(gca,'yLim',[0,100]);
colormap('summer')
%set(gca,'yScale','log');
set(gca,'FontSize',30)
set (gca,'XTickLabel',str, 'XTick',1:numel(str))
legend('boxoff');