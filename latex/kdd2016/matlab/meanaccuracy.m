times=dlmread('txt/meanaccuracy.txt');

x1=times(:,1);
by=times(:,2);
cy=times(:,3);
dy=times(:,4);
ey=times(:,5);

plot(x1,1-by,'-s',x1,1-cy,'->',x1,1-dy,'-d',x1,1-ey,'-o','LineWidth',3.5,'MarkerSize',25);
ylabel('Accuracy','FontSize',40);
xlabel('Size of Sub-trajectory','FontSize',40);

h_legend=legend('Bus','Car', 'Walk','T-drive');
set(h_legend,'FontSize',40);
set(gca,'yLim',[0.7,1]);
%set(h_legend,'FontSize',30);
%set(h_legend,'location','northoutside');
%set(gca,'xLim',[0,8]);
%set(gca,'yScale','log');
set(gca,'FontSize',40)
%set (gca,'YTickLabel',['1000';'10000';'100000';'1000000';'10000000';'100000000'])
