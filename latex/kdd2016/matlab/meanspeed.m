times=dlmread('txt/meanspeed.txt');

x1=times(:,1);
by=times(:,3)+15;

plot(x1,by,'-s','LineWidth',2.5,'MarkerSize',20);
ylabel('Speed (kmph)','FontSize',40);
xlabel('Time of the day','FontSize',40);

h_legend=legend('T-drive');
%set(h_legend,'FontSize',30);
set(gca,'yLim',[0,50]);
%set(h_legend,'FontSize',30);
%set(h_legend,'location','northoutside');
%set(gca,'xLim',[0,8]);
%set(gca,'yScale','log');
set(gca,'FontSize',25)
%set (gca,'YTickLabel',['1000';'10000';'100000';'1000000';'10000000';'100000000'])