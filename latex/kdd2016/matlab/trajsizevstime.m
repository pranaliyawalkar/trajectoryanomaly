times=dlmread('txt/sizevstime.txt');
x1=times(:,1);
ay=times(:,2);
by=times(:,3);
cy=times(:,4);

plot(x1,ay,'-o',x1,by,'-^',x1,cy,'-*','LineWidth',2.5,'MarkerSize',24);
ylabel('Running Time (in msecs)','FontSize',30);
set(gca,'yScale','log');
xlabel('Trajectory Size','FontSize',30);

h_legend=legend('Mantra','Naive','Sliding Window');

set(h_legend,'FontSize',30);

%set(gca,'yLim',[0,10000]);


set(h_legend,'FontSize',30);
%set(h_legend,'location','northoutside');
%set(gca,'yLim',[0.45,1]);
%set(gca,'xLim',[1,12]);
set(gca,'FontSize',25)
%set (gca,'YTickLabel',['1000';'10000';'100000';'1000000';'10000000';'100000000'])
