package org_phase.example.anomaly.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.example.anomaly.data.Point;

public class ChooseTrajectory 
{
	//GraphM g;
	public ChooseTrajectory() 
	{
		//this.g = g2;
	}

	public ArrayList<Point> chooseTrajectory(String path, PrintWriter log)
			throws Exception 
	{
		File docDir = new File(path);
		String[] files = docDir.list();
		ArrayList<Point> arr = new ArrayList<Point>();
		int fileNum, trajNum;
		fileNum = (int) (files.length * Math.random());
		BufferedReader fileReader = new BufferedReader(new FileReader(path
				+ "/" + files[fileNum]));
		//		BufferedReader fileReader = new BufferedReader(
		//				new FileReader(
		//						"/Users/itispris/Documents/trajData/reduced/beijing_sent_matched_timed303.txt"));
		trajNum = (int) (155 * Math.random());
		//		trajNum = 65;
		//		System.out.println("trajNumber: " + trajNum);
		log.print("chosen file: " + path + "/" + files[fileNum]);
		log.println("trajnum is: " + trajNum);
		log.println();
		log.flush();

		int lineNum = 0;

		while (true) 
		{
			String line = fileReader.readLine();
			if (line == null)
				break;
			if (!line.contains("["))
				lineNum++;
			if (lineNum == trajNum) 
			{	
				line = fileReader.readLine();
				String[] warr = line.split("\\]");
				for (String s : warr) 
				{
					try 
					{
						int id = Integer
								.parseInt(s.split(",")[0].split("\\[")[1]);
						//if (id == 137634)
							//return chooseTrajectory(path, log);
						double lat = Double.parseDouble(s.split(",")[1]);
						double lon = Double.parseDouble(s.split(",")[2]);
						Date time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
								.parse(s.split(",")[3]);
						arr.add(new Point(id, lat, lon, time));
					} 
					catch (Exception e) 
					{
						System.out.println(line);
						System.out.println("trajNum: " + trajNum);
						System.out.println("lineNum: " + lineNum);
						System.out.println("In file " + path + "/"
								+ files[fileNum]);
					}
				}
				break;
			}

		}
		fileReader.close();
		return arr;
	}

	public boolean containCycle(ArrayList<Point> originalTrajectory) 
	{
		HashMap<Integer, Integer> hash = new HashMap<Integer, Integer>();
		for (int i = 0; i < originalTrajectory.size(); i++) 
		{
			if (hash.get(originalTrajectory.get(i).id) != null)
				return true;
			hash.put(originalTrajectory.get(i).id, i);
		}
		return false;
	}

}
