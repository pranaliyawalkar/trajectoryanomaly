package org_phase.example.anomaly.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

public class EdgeMeanVariance 
{

	public static void main(String[] args) throws Exception 
	{
		String file_add = "src/train";
		String new_file_add = file_add + "1";
		new EdgeMeanVariance().findMeanVarAdulterated(new_file_add);
	}

	public void findMeanVarAdulterated(String dirLocation) throws Exception 
	{
		String train_number = dirLocation.substring(dirLocation.length()-1);
		String file_name1 = "src/inputs/out_ad" + train_number + "_phase1" + ".csv";
		String file_name2 = "src/inputs/out_ad" + train_number + "_phase2" + ".csv";
		String file_name3 = "src/inputs/out_ad" + train_number + "_phase3" + ".csv";
		String file_name4 = "src/inputs/out_ad" + train_number + "_phase4" + ".csv";
		
		System.out.println(file_name1);
		System.out.println(file_name2);
		System.out.println(file_name3);
		System.out.println(file_name4);
		
		PrintWriter out1 = new PrintWriter(new OutputStreamWriter(
				new FileOutputStream(file_name1),
				"UTF-8"));
		
		PrintWriter out2 = new PrintWriter(new OutputStreamWriter(
				new FileOutputStream(file_name2),
				"UTF-8"));
		PrintWriter out3 = new PrintWriter(new OutputStreamWriter(
				new FileOutputStream(file_name3),
				"UTF-8"));
		PrintWriter out4 = new PrintWriter(new OutputStreamWriter(
				new FileOutputStream(file_name4),
				"UTF-8"));
		
		File docDir = new File(dirLocation);
		String[] files = docDir.list();
		HashMap<String, ArrayList<Long>> map1 = new HashMap<String, ArrayList<Long>>();
		HashMap<String, ArrayList<Long>> map2 = new HashMap<String, ArrayList<Long>>();
		HashMap<String, ArrayList<Long>> map3 = new HashMap<String, ArrayList<Long>>();
		HashMap<String, ArrayList<Long>> map4 = new HashMap<String, ArrayList<Long>>();
		
		for (int file = 0; file < files.length; file++)
		{
			BufferedReader fileReader = new BufferedReader(new FileReader(
					dirLocation + "/" + files[file]));
			if (!(files[file].contains("txt")))
				continue;
			System.out.println(files[file]);
			String line;
			while (true) 
			{
				line = fileReader.readLine();
				if (line == null)
					break;
				if (!line.contains("["))
					continue;
				String[] sarr = line.split("\\]");
				boolean first = true;
				String[] sarrSource, sarrDesti = null;
				for (int i = 0; i < sarr.length; i++) 
				{
					sarrSource = sarrDesti;
					sarrDesti = sarr[i].split("\\[")[1].split(",");
					if (first) 
					{
						first = false;
						sarrSource = sarrDesti;
						i++;
						sarrDesti = sarr[i].split("\\[")[1].split(",");
					}
					String key1 = sarrSource[0] + " " + sarrDesti[0];
					String key2 = sarrDesti[0] + " " + sarrSource[0];
	
					//sarrSource[3] = sarrSource[3].replaceAll("\\s","");
					//sarrDesti[3] = sarrDesti[3].replaceAll("\\s","");
					
					long startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
					.parse(sarrSource[3]).getTime();

					long endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
					.parse(sarrDesti[3]).getTime();
			
					long travelTimeMin = (endTime - startTime);
					ArrayList<Long> arr = new ArrayList<Long>();
					
					
					StringTokenizer stt = new StringTokenizer(sarrSource[3] , " :");
					String temp = stt.nextToken();
					int hour = Integer.parseInt(stt.nextToken().toString());
					HashMap<String, ArrayList<Long>> map = new HashMap<String, ArrayList<Long>>();
					
					//System.out.println(hour);
					if(hour >=0 && hour <=5)
					{
						map = map1;
					}
					else if(hour >=6 && hour <=11)
					{
						map = map2;
					}
					else if(hour >=12 && hour <=17)
					{
						map = map3;
					}
					else if(hour >=18 && hour <=23)
					{
						map = map4;
					}

					
					
					if (map.get(key1) == null && map.get(key2) == null) 
					{
						arr.add(travelTimeMin);
						map.put(key1, arr);
					} 
					else if (map.get(key1) == null) 
					{
						arr = map.get(key2);
						arr.add(travelTimeMin);
						map.put(key2, arr);
					} 
					else 
					{
						arr = map.get(key1);
						arr.add(travelTimeMin);
						map.put(key1, arr);
					}
				}
	
			}
			fileReader.close();
		}
		for(int phase = 1 ; phase <=4 ; phase ++)
		{
			HashMap<String, ArrayList<Long>> map = new HashMap<String, ArrayList<Long>>();
			if(phase == 1)
			{
				map = map1;
			}
			else if(phase == 2)
			{
				map = map2;
			}
			else if(phase == 3)
			{
				map = map3;
			}
			else if(phase == 4)
			{
				map = map4;
			}
			for (String s : map.keySet()) 
			{
				ArrayList<Long> arr = map.get(s);

				long tot = 0;
				for (Long l : arr)
					tot += l;
				long mean = tot / (long) arr.size();
				tot = 0;
				int c = 0;
				for (Long l : arr)
					if (Math.abs(mean - l) < mean) 
					{
						tot += Math.abs(mean - l) * Math.abs(mean - l);
						c++;
					}
				if (c > 0)
					tot = tot / c;
				if (tot == 0)
					tot = 1;

				double var = Math.pow(tot, 0.5);
				if (var == 0)
					var = 1;
				if(phase == 1)
				{
					out1.println(s + "," + mean + "," + tot + "," + var);
				}
				else if(phase == 2)
				{
					out2.println(s + "," + mean + "," + tot + "," + var);
				}
				else if(phase == 3)
				{
					out3.println(s + "," + mean + "," + tot + "," + var);
				}
				else if(phase == 4)
				{
					out4.println(s + "," + mean + "," + tot + "," + var);
				}
				

			}
		}
			
		out1.close();
		out2.close();
		out3.close();
		out4.close();
	}

}
