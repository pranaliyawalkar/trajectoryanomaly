package org_phase.example.anomaly.mas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.example.anomaly.data.Point;

public class MASHolder 
{
	String logFilePath = "";
	String myOutputFilePathValues = "";
	String myOutputFilePathTime = "";
	String myOutputFilePathTimeTxt = "";
	String mapMatchedTrajDirPath = "";
	String learnedEdgeDistroFilePath1 = "";
	String learnedEdgeDistroFilePath2 = "";
	String learnedEdgeDistroFilePath3 = "";
	String learnedEdgeDistroFilePath4 = "";
	
	PrintWriter outValues, outTime, outTimeTxt;
	EdgeProcessing ep_new;
	int mined_traj_count;
	int tot_traj_count;
	int training_miss_count;

	public MASHolder() 
	{

	}

	public MASHolder(int i) 
	{
		mined_traj_count = 0;
		tot_traj_count = 0;
		training_miss_count = 0;
		myOutputFilePathTime = "src/outputs/output_phase/outputTimeTempAccuracy" + i+ ".csv";
		myOutputFilePathValues = "src/outputs/output_phase/outputValuesTempAccuracy" + i + ".txt";
		myOutputFilePathTimeTxt = "src/outputs/output_phase/outputTimeTxtTempAccuracy" + i + ".txt";
		mapMatchedTrajDirPath = "src/inputs/part" + i;
		learnedEdgeDistroFilePath1 = "src/inputs/out_ad" + i + "_phase1"  + ".csv";
		learnedEdgeDistroFilePath2 = "src/inputs/out_ad" + i + "_phase2"  + ".csv";
		learnedEdgeDistroFilePath3 = "src/inputs/out_ad" + i + "_phase3"  + ".csv";
		learnedEdgeDistroFilePath4 = "src/inputs/out_ad" + i + "_phase4"  + ".csv";


		try 
		{
			outValues = new PrintWriter(new OutputStreamWriter(new FileOutputStream(myOutputFilePathValues), "UTF-8"));
			outTime = new PrintWriter(new OutputStreamWriter(new FileOutputStream(myOutputFilePathTime), "UTF-8"));
			outTimeTxt = new PrintWriter(new OutputStreamWriter(new FileOutputStream(myOutputFilePathTimeTxt), "UTF-8"));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		String[]paths = new String[4];
		paths[0] = learnedEdgeDistroFilePath1;
		paths[1] = learnedEdgeDistroFilePath2;
		paths[2] = learnedEdgeDistroFilePath3;
		paths[3] = learnedEdgeDistroFilePath4;
		
		
		ep_new = new EdgeProcessing(paths);

	}

	public void compare_handler() throws Exception 
	{
		File docDir = new File(mapMatchedTrajDirPath);
		String[] files = docDir.list();
		int fileNum = 0;
		int line_no = 0;
		int total_count = 0;
		
		for (fileNum = 0; fileNum < files.length; fileNum++) 
		{
			line_no = 0;
			System.out.println("File name : " + files[fileNum]);
			outValues.println("File name : " + files[fileNum]);
			outTime.println("File name : " + files[fileNum]);
			outTimeTxt.println("File name : " + files[fileNum]);
			BufferedReader fileReader = new BufferedReader(new FileReader(
					mapMatchedTrajDirPath + "/" + files[fileNum]));
			while (true) 
			{
				ArrayList<Point> arr = new ArrayList<Point>();
				String line = fileReader.readLine();
				if (line == null)
					break;
				line_no++;
				if (line.contains("[")) 
				{
					String[] warr = line.split("\\]");
					for (String s : warr) 
					{
						try 
						{
							int id = Integer.parseInt(s.split(",")[0].split("\\[")[1]);
							double lat = Double.parseDouble(s.split(",")[1]);
							double lon = Double.parseDouble(s.split(",")[2]);
							Date time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(s.split(",")[3]);
							arr.add(new Point(id, lat, lon, time));
						} 
						catch (Exception e) 
						{
							System.out.println(e);
						}
					}
					total_count++;
					compare(arr, line_no);
				}
	
			}
				fileReader.close();
				outValues.println(" $$$$$$$$$$ ");
				outTime.println(" $$$$$$$$$$ " + "," + " $$$$$$$$$$ ");
				outTimeTxt.println(" $$$$$$$$$$ ");
				System.out.println("Mined trajectory count : " + mined_traj_count
						+ " Missed trajectory Count : " + training_miss_count
						+ " Total trajectory Count : " + tot_traj_count);
		}
		outTime.close();
		outTimeTxt.close();
		outValues.close();
	}

	public void compare(ArrayList<Point> pt, int line_no) throws Exception {
		long tot1 = 0;
		long tot2 = 0;
		long tot3 = 0;
		List<Integer> nodeSequence = new ArrayList<Integer>();
		List<Double> travelTime = new ArrayList<Double>();
		List <Integer> start_hours = new ArrayList<Integer>();
		
		for (int i = 0; i < pt.size(); i++) 
		{
			nodeSequence.add(pt.get(i).id);
			if (i < (pt.size() - 1)) 
			{
				double time = pt.get(i + 1).timeStamp.getTime()- pt.get(i).timeStamp.getTime();
				travelTime.add(time);
				start_hours.add(pt.get(i).timeStamp.getHours());
			}
		}
		ep_new.setNodeTime(nodeSequence, travelTime, start_hours);
		if (ep_new.notInTraining()) 
		{
			training_miss_count++;
		} 
		else 
		{
			long start = System.currentTimeMillis();
			ArrayList<String> our_result = new ArrayList<String>();
			try 
			{
				our_result = ep_new.findAnomalyOur();
			} 
			catch (Exception e) 
			{
				System.out.println("Not there in training");
				e.printStackTrace();
				return;
			}
			tot1 += (System.currentTimeMillis() - start);
			start = System.currentTimeMillis();

			ArrayList<String> naive_result = new ArrayList<String>();
			try 
			{
				naive_result = ep_new.findAnomalyNaive();
			} 
			catch (Exception e) 
			{
				System.out.println("Not there in training");
				e.printStackTrace();
				return;
			}
			tot2 += (System.currentTimeMillis() - start);
			/*start = System.currentTimeMillis();
			ArrayList<String> naive_result2 = new ArrayList<String>();
			try 
			{
				naive_result2 = ep_new.findAnomalyNaive2();
			} 
			catch (Exception e) 
			{
				System.out.println("Not there in training");
				e.printStackTrace();
				return;
			}
			tot3 += System.currentTimeMillis() - start;
			*/
			outValues.println(line_no + " $ " + our_result + " $ " + naive_result);
			//System.out.println(line_no + " $ " + our_result + " $ " + naive_result);
			outTime.println(pt.size() + "," + ep_new.howManyAno() + "," + tot1 + "," + tot2);
			outTimeTxt.println(line_no + " $ " + tot1 + " $ " + tot2);

			if (our_result.size() == naive_result.size()) 
			{
				int flag = 0;
				for (int i = 0; i < our_result.size(); i++) 
				{
					if (!our_result.get(i).equals(naive_result.get(i))) 
					{
						flag = 1;
						break;
					}
				}
				if (flag == 0)
				{
					mined_traj_count++;
				}
			}

		}
		tot_traj_count++;

	}

	public static void main(String[] args) throws Exception 
	{
		/*for (int i = 1; i <= 1; i++) 
		{
			ScanThread sc = new ScanThread(i);
			sc.start();
		}*/
		MASHolder ms = new MASHolder(1);
		ms.compare_handler();

	}

	private static class ScanThread extends Thread 
	{

		private int partID;

		public ScanThread(int partID) 
		{
			this.partID = partID;
		}

		public void run() 
		{
			MASHolder mh = new MASHolder(partID);
			try 
			{
				mh.compare_handler();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
	}

}
